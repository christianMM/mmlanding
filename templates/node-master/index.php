<section>
    <?php /* * INIT Variables * */ $n_node="node-112" ; ?>
    <div class="container" id="<?php echo $n_node; ?>">
        <!-- Header -->
        <div class="block-header">

            <figure>
                <?php if (defined( 'DRUPAL_ROOT')): ?>
                <?php img( 'netatmo-b0-background.jpg', 'cdn', null, 'background img-responsive', 'Netatmo', 'Netatmo'); ?>
                <?php else: ?>
                <img class="background" src="img/desktop/netatmo-b0-background.jpg" alt="Termostato para Smarthphone Netatmo">
                <?php endif ?>
            </figure>
        </div>

        <!-- Block 1 -->
        <div class="block content-block-1">

            <div class="bt-price price-1">
                <?php if (!$detect->isMobile()): ?>
                <div class="price xl ref-1233700"></div>
                <?php endif; ?>
                <?php if ($detect->isMobile()): ?>
                <div class="price md ref-1233700"></div>
                <?php endif; ?>
                <div class="btn-1">
                    <a href="http://tiendas.mediamarkt.es/p/termostato-para-smartphone-netatmo-nth-1233700" onclick="ga('send','event','microsites','novedades-netatmo','comprar-termostato');" target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                </div>
            </div>
            <?php if (!$detect->isMobile()): ?>

            <?php endif ?>

        </div>
    </div>
    </div>
</section>
        