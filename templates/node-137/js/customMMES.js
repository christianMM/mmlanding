
/********************************************************************************************************************/
/* @Start 	- Custom */

window.customMMES = window.customMMES || {};

/* @End  	- Custom */
/********************************************************************************************************************/
/* @Start 	- Libraries */

/*! menuAim  -  https://github.com/kamens/jQuery-menu-aim*/
(function(e){function t(t){var n=e(this),r=null,i=[],s=null,o=null,u=e.extend({rowSelector:"> li",submenuSelector:"*",submenuDirection:"right",tolerance:75,enter:e.noop,exit:e.noop,activate:e.noop,deactivate:e.noop,exitMenu:e.noop},t);var a=3,f=300;var l=function(e){i.push({x:e.pageX,y:e.pageY});if(i.length>a){i.shift()}};var c=function(){if(o){clearTimeout(o)}if(u.exitMenu(this)){if(r){u.deactivate(r)}r=null}};var h=function(){if(o){clearTimeout(o)}u.enter(this);m(this)},p=function(){u.exit(this)};var d=function(){v(this)};var v=function(e){if(e==r){return}if(r){u.deactivate(r)}u.activate(e);r=e};var m=function(e){var t=g();if(t){o=setTimeout(function(){m(e)},t)}else{v(e)}};var g=function(){function d(e,t){return(t.y-e.y)/(t.x-e.x)}if(!r||!e(r).is(u.submenuSelector)){return 0}var t=n.offset(),o={x:t.left,y:t.top-u.tolerance},a={x:t.left+n.outerWidth(),y:o.y},l={x:t.left,y:t.top+n.outerHeight()+u.tolerance},c={x:t.left+n.outerWidth(),y:l.y},h=i[i.length-1],p=i[0];if(!h){return 0}if(!p){p=h}if(p.x<t.left||p.x>c.x||p.y<t.top||p.y>c.y){return 0}if(s&&h.x==s.x&&h.y==s.y){return 0}var v=a,m=c;if(u.submenuDirection=="left"){v=l;m=o}else if(u.submenuDirection=="below"){v=c;m=l}else if(u.submenuDirection=="above"){v=o;m=a}var g=d(h,v),y=d(h,m),b=d(p,v),w=d(p,m);if(g<b&&y>w){s=h;return f}s=null;return 0};n.mouseleave(c).find(u.rowSelector).mouseenter(h).mouseleave(p).click(d);e(document).mousemove(l)}e.fn.menuAim=function(e){this.each(function(){t.call(this,e)});return this};})(jQuery)

/*! Bootstrap.js  -  http://www.apache.org/licenses/LICENSE-2.0.txt*/
!function(e){"use strict";e(function(){e.support.transition=function(){var e=function(){var e=document.createElement("bootstrap"),t={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"},n;for(n in t)if(e.style[n]!==undefined)return t[n]}();return e&&{end:e}}()})}(window.jQuery),!function(e){"use strict";var t='[data-dismiss="alert"]',n=function(n){e(n).on("click",t,this.close)};n.prototype.close=function(t){function s(){i.trigger("closed").remove()}var n=e(this),r=n.attr("data-target"),i;r||(r=n.attr("href"),r=r&&r.replace(/.*(?=#[^\s]*$)/,"")),i=e(r),t&&t.preventDefault(),i.length||(i=n.hasClass("alert")?n:n.parent()),i.trigger(t=e.Event("close"));if(t.isDefaultPrevented())return;i.removeClass("in"),e.support.transition&&i.hasClass("fade")?i.on(e.support.transition.end,s):s()};var r=e.fn.alert;e.fn.alert=function(t){return this.each(function(){var r=e(this),i=r.data("alert");i||r.data("alert",i=new n(this)),typeof t=="string"&&i[t].call(r)})},e.fn.alert.Constructor=n,e.fn.alert.noConflict=function(){return e.fn.alert=r,this},e(document).on("click.alert.data-api",t,n.prototype.close)}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.$element=e(t),this.options=e.extend({},e.fn.button.defaults,n)};t.prototype.setState=function(e){var t="disabled",n=this.$element,r=n.data(),i=n.is("input")?"val":"html";e+="Text",r.resetText||n.data("resetText",n[i]()),n[i](r[e]||this.options[e]),setTimeout(function(){e=="loadingText"?n.addClass(t).attr(t,t):n.removeClass(t).removeAttr(t)},0)},t.prototype.toggle=function(){var e=this.$element.closest('[data-toggle="buttons-radio"]');e&&e.find(".active").removeClass("active"),this.$element.toggleClass("active")};var n=e.fn.button;e.fn.button=function(n){return this.each(function(){var r=e(this),i=r.data("button"),s=typeof n=="object"&&n;i||r.data("button",i=new t(this,s)),n=="toggle"?i.toggle():n&&i.setState(n)})},e.fn.button.defaults={loadingText:"loading..."},e.fn.button.Constructor=t,e.fn.button.noConflict=function(){return e.fn.button=n,this},e(document).on("click.button.data-api","[data-toggle^=button]",function(t){var n=e(t.target);n.hasClass("btn")||(n=n.closest(".btn")),n.button("toggle")})}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.$element=e(t),this.$indicators=this.$element.find(".carousel-indicators"),this.options=n,this.options.pause=="hover"&&this.$element.on("mouseenter",e.proxy(this.pause,this)).on("mouseleave",e.proxy(this.cycle,this))};t.prototype={cycle:function(t){return t||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(e.proxy(this.next,this),this.options.interval)),this},getActiveIndex:function(){return this.$active=this.$element.find(".item.active"),this.$items=this.$active.parent().children(),this.$items.index(this.$active)},to:function(t){var n=this.getActiveIndex(),r=this;if(t>this.$items.length-1||t<0)return;return this.sliding?this.$element.one("slid",function(){r.to(t)}):n==t?this.pause().cycle():this.slide(t>n?"next":"prev",e(this.$items[t]))},pause:function(t){return t||(this.paused=!0),this.$element.find(".next, .prev").length&&e.support.transition.end&&(this.$element.trigger(e.support.transition.end),this.cycle(!0)),clearInterval(this.interval),this.interval=null,this},next:function(){if(this.sliding)return;return this.slide("next")},prev:function(){if(this.sliding)return;return this.slide("prev")},slide:function(t,n){var r=this.$element.find(".item.active"),i=n||r[t](),s=this.interval,o=t=="next"?"left":"right",u=t=="next"?"first":"last",a=this,f;this.sliding=!0,s&&this.pause(),i=i.length?i:this.$element.find(".item")[u](),f=e.Event("slide",{relatedTarget:i[0],direction:o});if(i.hasClass("active"))return;this.$indicators.length&&(this.$indicators.find(".active").removeClass("active"),this.$element.one("slid",function(){var t=e(a.$indicators.children()[a.getActiveIndex()]);t&&t.addClass("active")}));if(e.support.transition&&this.$element.hasClass("slide")){this.$element.trigger(f);if(f.isDefaultPrevented())return;i.addClass(t),i[0].offsetWidth,r.addClass(o),i.addClass(o),this.$element.one(e.support.transition.end,function(){i.removeClass([t,o].join(" ")).addClass("active"),r.removeClass(["active",o].join(" ")),a.sliding=!1,setTimeout(function(){a.$element.trigger("slid")},0)})}else{this.$element.trigger(f);if(f.isDefaultPrevented())return;r.removeClass("active"),i.addClass("active"),this.sliding=!1,this.$element.trigger("slid")}return s&&this.cycle(),this}};var n=e.fn.carousel;e.fn.carousel=function(n){return this.each(function(){var r=e(this),i=r.data("carousel"),s=e.extend({},e.fn.carousel.defaults,typeof n=="object"&&n),o=typeof n=="string"?n:s.slide;i||r.data("carousel",i=new t(this,s)),typeof n=="number"?i.to(n):o?i[o]():s.interval&&i.pause().cycle()})},e.fn.carousel.defaults={interval:5e3,pause:"hover"},e.fn.carousel.Constructor=t,e.fn.carousel.noConflict=function(){return e.fn.carousel=n,this},e(document).on("click.carousel.data-api","[data-slide], [data-slide-to]",function(t){var n=e(this),r,i=e(n.attr("data-target")||(r=n.attr("href"))&&r.replace(/.*(?=#[^\s]+$)/,"")),s=e.extend({},i.data(),n.data()),o;i.carousel(s),(o=n.attr("data-slide-to"))&&i.data("carousel").pause().to(o).cycle(),t.preventDefault()})}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.$element=e(t),this.options=e.extend({},e.fn.collapse.defaults,n),this.options.parent&&(this.$parent=e(this.options.parent)),this.options.toggle&&this.toggle()};t.prototype={constructor:t,dimension:function(){var e=this.$element.hasClass("width");return e?"width":"height"},show:function(){var t,n,r,i;if(this.transitioning||this.$element.hasClass("in"))return;t=this.dimension(),n=e.camelCase(["scroll",t].join("-")),r=this.$parent&&this.$parent.find("> .accordion-group > .in");if(r&&r.length){i=r.data("collapse");if(i&&i.transitioning)return;r.collapse("hide"),i||r.data("collapse",null)}this.$element[t](0),this.transition("addClass",e.Event("show"),"shown"),e.support.transition&&this.$element[t](this.$element[0][n])},hide:function(){var t;if(this.transitioning||!this.$element.hasClass("in"))return;t=this.dimension(),this.reset(this.$element[t]()),this.transition("removeClass",e.Event("hide"),"hidden"),this.$element[t](0)},reset:function(e){var t=this.dimension();return this.$element.removeClass("collapse")[t](e||"auto")[0].offsetWidth,this.$element[e!==null?"addClass":"removeClass"]("collapse"),this},transition:function(t,n,r){var i=this,s=function(){n.type=="show"&&i.reset(),i.transitioning=0,i.$element.trigger(r)};this.$element.trigger(n);if(n.isDefaultPrevented())return;this.transitioning=1,this.$element[t]("in"),e.support.transition&&this.$element.hasClass("collapse")?this.$element.one(e.support.transition.end,s):s()},toggle:function(){this[this.$element.hasClass("in")?"hide":"show"]()}};var n=e.fn.collapse;e.fn.collapse=function(n){return this.each(function(){var r=e(this),i=r.data("collapse"),s=e.extend({},e.fn.collapse.defaults,r.data(),typeof n=="object"&&n);i||r.data("collapse",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.collapse.defaults={toggle:!0},e.fn.collapse.Constructor=t,e.fn.collapse.noConflict=function(){return e.fn.collapse=n,this},e(document).on("click.collapse.data-api","[data-toggle=collapse]",function(t){var n=e(this),r,i=n.attr("data-target")||t.preventDefault()||(r=n.attr("href"))&&r.replace(/.*(?=#[^\s]+$)/,""),s=e(i).data("collapse")?"toggle":n.data();n[e(i).hasClass("in")?"addClass":"removeClass"]("collapsed"),e(i).collapse(s)})}(window.jQuery),!function(e){"use strict";function r(){e(t).each(function(){i(e(this)).removeClass("open")})}function i(t){var n=t.attr("data-target"),r;n||(n=t.attr("href"),n=n&&/#/.test(n)&&n.replace(/.*(?=#[^\s]*$)/,"")),r=n&&e(n);if(!r||!r.length)r=t.parent();return r}var t="[data-toggle=dropdown]",n=function(t){var n=e(t).on("click.dropdown.data-api",this.toggle);e("html").on("click.dropdown.data-api",function(){n.parent().removeClass("open")})};n.prototype={constructor:n,toggle:function(t){var n=e(this),s,o;if(n.is(".disabled, :disabled"))return;return s=i(n),o=s.hasClass("open"),r(),o||s.toggleClass("open"),n.focus(),!1},keydown:function(n){var r,s,o,u,a,f;if(!/(38|40|27)/.test(n.keyCode))return;r=e(this),n.preventDefault(),n.stopPropagation();if(r.is(".disabled, :disabled"))return;u=i(r),a=u.hasClass("open");if(!a||a&&n.keyCode==27)return n.which==27&&u.find(t).focus(),r.click();s=e("[role=menu] li:not(.divider):visible a",u);if(!s.length)return;f=s.index(s.filter(":focus")),n.keyCode==38&&f>0&&f--,n.keyCode==40&&f<s.length-1&&f++,~f||(f=0),s.eq(f).focus()}};var s=e.fn.dropdown;e.fn.dropdown=function(t){return this.each(function(){var r=e(this),i=r.data("dropdown");i||r.data("dropdown",i=new n(this)),typeof t=="string"&&i[t].call(r)})},e.fn.dropdown.Constructor=n,e.fn.dropdown.noConflict=function(){return e.fn.dropdown=s,this},e(document).on("click.dropdown.data-api",r).on("click.dropdown.data-api",".dropdown form",function(e){e.stopPropagation()}).on("click.dropdown-menu",function(e){e.stopPropagation()}).on("click.dropdown.data-api",t,n.prototype.toggle).on("keydown.dropdown.data-api",t+", [role=menu]",n.prototype.keydown)}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.options=n,this.$element=e(t).delegate('[data-dismiss="modal"]',"click.dismiss.modal",e.proxy(this.hide,this)),this.options.remote&&this.$element.find(".modal-body").load(this.options.remote)};t.prototype={constructor:t,toggle:function(){return this[this.isShown?"hide":"show"]()},show:function(){var t=this,n=e.Event("show");this.$element.trigger(n);if(this.isShown||n.isDefaultPrevented())return;this.isShown=!0,this.escape(),this.backdrop(function(){var n=e.support.transition&&t.$element.hasClass("fade");t.$element.parent().length||t.$element.appendTo(document.body),t.$element.show(),n&&t.$element[0].offsetWidth,t.$element.addClass("in").attr("aria-hidden",!1),t.enforceFocus(),n?t.$element.one(e.support.transition.end,function(){t.$element.focus().trigger("shown")}):t.$element.focus().trigger("shown")})},hide:function(t){t&&t.preventDefault();var n=this;t=e.Event("hide"),this.$element.trigger(t);if(!this.isShown||t.isDefaultPrevented())return;this.isShown=!1,this.escape(),e(document).off("focusin.modal"),this.$element.removeClass("in").attr("aria-hidden",!0),e.support.transition&&this.$element.hasClass("fade")?this.hideWithTransition():this.hideModal()},enforceFocus:function(){var t=this;e(document).on("focusin.modal",function(e){t.$element[0]!==e.target&&!t.$element.has(e.target).length&&t.$element.focus()})},escape:function(){var e=this;this.isShown&&this.options.keyboard?this.$element.on("keyup.dismiss.modal",function(t){t.which==27&&e.hide()}):this.isShown||this.$element.off("keyup.dismiss.modal")},hideWithTransition:function(){var t=this,n=setTimeout(function(){t.$element.off(e.support.transition.end),t.hideModal()},500);this.$element.one(e.support.transition.end,function(){clearTimeout(n),t.hideModal()})},hideModal:function(){var e=this;this.$element.hide(),this.backdrop(function(){e.removeBackdrop(),e.$element.trigger("hidden")})},removeBackdrop:function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},backdrop:function(t){var n=this,r=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var i=e.support.transition&&r;this.$backdrop=e('<div class="modal-backdrop '+r+'" />').appendTo(document.body),this.$backdrop.click(this.options.backdrop=="static"?e.proxy(this.$element[0].focus,this.$element[0]):e.proxy(this.hide,this)),i&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in");if(!t)return;i?this.$backdrop.one(e.support.transition.end,t):t()}else!this.isShown&&this.$backdrop?(this.$backdrop.removeClass("in"),e.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one(e.support.transition.end,t):t()):t&&t()}};var n=e.fn.modal;e.fn.modal=function(n){return this.each(function(){var r=e(this),i=r.data("modal"),s=e.extend({},e.fn.modal.defaults,r.data(),typeof n=="object"&&n);i||r.data("modal",i=new t(this,s)),typeof n=="string"?i[n]():s.show&&i.show()})},e.fn.modal.defaults={backdrop:!0,keyboard:!0,show:!0},e.fn.modal.Constructor=t,e.fn.modal.noConflict=function(){return e.fn.modal=n,this},e(document).on("click.modal.data-api",'[data-toggle="modal"]',function(t){var n=e(this),r=n.attr("href"),i=e(n.attr("data-target")||r&&r.replace(/.*(?=#[^\s]+$)/,"")),s=i.data("modal")?"toggle":e.extend({remote:!/#/.test(r)&&r},i.data(),n.data());t.preventDefault(),i.modal(s).one("hide",function(){n.focus()})})}(window.jQuery),!function(e){"use strict";var t=function(e,t){this.init("tooltip",e,t)};t.prototype={constructor:t,init:function(t,n,r){var i,s,o,u,a;this.type=t,this.$element=e(n),this.options=this.getOptions(r),this.enabled=!0,o=this.options.trigger.split(" ");for(a=o.length;a--;)u=o[a],u=="click"?this.$element.on("click."+this.type,this.options.selector,e.proxy(this.toggle,this)):u!="manual"&&(i=u=="hover"?"mouseenter":"focus",s=u=="hover"?"mouseleave":"blur",this.$element.on(i+"."+this.type,this.options.selector,e.proxy(this.enter,this)),this.$element.on(s+"."+this.type,this.options.selector,e.proxy(this.leave,this)));this.options.selector?this._options=e.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},getOptions:function(t){return t=e.extend({},e.fn[this.type].defaults,this.$element.data(),t),t.delay&&typeof t.delay=="number"&&(t.delay={show:t.delay,hide:t.delay}),t},enter:function(t){var n=e.fn[this.type].defaults,r={},i;this._options&&e.each(this._options,function(e,t){n[e]!=t&&(r[e]=t)},this),i=e(t.currentTarget)[this.type](r).data(this.type);if(!i.options.delay||!i.options.delay.show)return i.show();clearTimeout(this.timeout),i.hoverState="in",this.timeout=setTimeout(function(){i.hoverState=="in"&&i.show()},i.options.delay.show)},leave:function(t){var n=e(t.currentTarget)[this.type](this._options).data(this.type);this.timeout&&clearTimeout(this.timeout);if(!n.options.delay||!n.options.delay.hide)return n.hide();n.hoverState="out",this.timeout=setTimeout(function(){n.hoverState=="out"&&n.hide()},n.options.delay.hide)},show:function(){var t,n,r,i,s,o,u=e.Event("show");if(this.hasContent()&&this.enabled){this.$element.trigger(u);if(u.isDefaultPrevented())return;t=this.tip(),this.setContent(),this.options.animation&&t.addClass("fade"),s=typeof this.options.placement=="function"?this.options.placement.call(this,t[0],this.$element[0]):this.options.placement,t.detach().css({top:0,left:0,display:"block"}),this.options.container?t.appendTo(this.options.container):t.insertAfter(this.$element),n=this.getPosition(),r=t[0].offsetWidth,i=t[0].offsetHeight;switch(s){case"bottom":o={top:n.top+n.height,left:n.left+n.width/2-r/2};break;case"top":o={top:n.top-i,left:n.left+n.width/2-r/2};break;case"left":o={top:n.top+n.height/2-i/2,left:n.left-r};break;case"right":o={top:n.top+n.height/2-i/2,left:n.left+n.width}}this.applyPlacement(o,s),this.$element.trigger("shown")}},applyPlacement:function(e,t){var n=this.tip(),r=n[0].offsetWidth,i=n[0].offsetHeight,s,o,u,a;n.offset(e).addClass(t).addClass("in"),s=n[0].offsetWidth,o=n[0].offsetHeight,t=="top"&&o!=i&&(e.top=e.top+i-o,a=!0),t=="bottom"||t=="top"?(u=0,e.left<0&&(u=e.left*-2,e.left=0,n.offset(e),s=n[0].offsetWidth,o=n[0].offsetHeight),this.replaceArrow(u-r+s,s,"left")):this.replaceArrow(o-i,o,"top"),a&&n.offset(e)},replaceArrow:function(e,t,n){this.arrow().css(n,e?50*(1-e/t)+"%":"")},setContent:function(){var e=this.tip(),t=this.getTitle();e.find(".tooltip-inner")[this.options.html?"html":"text"](t),e.removeClass("fade in top bottom left right")},hide:function(){function i(){var t=setTimeout(function(){n.off(e.support.transition.end).detach()},500);n.one(e.support.transition.end,function(){clearTimeout(t),n.detach()})}var t=this,n=this.tip(),r=e.Event("hide");this.$element.trigger(r);if(r.isDefaultPrevented())return;return n.removeClass("in"),e.support.transition&&this.$tip.hasClass("fade")?i():n.detach(),this.$element.trigger("hidden"),this},fixTitle:function(){var e=this.$element;(e.attr("title")||typeof e.attr("data-original-title")!="string")&&e.attr("data-original-title",e.attr("title")||"").attr("title","")},hasContent:function(){return this.getTitle()},getPosition:function(){var t=this.$element[0];return e.extend({},typeof t.getBoundingClientRect=="function"?t.getBoundingClientRect():{width:t.offsetWidth,height:t.offsetHeight},this.$element.offset())},getTitle:function(){var e,t=this.$element,n=this.options;return e=t.attr("data-original-title")||(typeof n.title=="function"?n.title.call(t[0]):n.title),e},tip:function(){return this.$tip=this.$tip||e(this.options.template)},arrow:function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},validate:function(){this.$element[0].parentNode||(this.hide(),this.$element=null,this.options=null)},enable:function(){this.enabled=!0},disable:function(){this.enabled=!1},toggleEnabled:function(){this.enabled=!this.enabled},toggle:function(t){var n=t?e(t.currentTarget)[this.type](this._options).data(this.type):this;n.tip().hasClass("in")?n.hide():n.show()},destroy:function(){this.hide().$element.off("."+this.type).removeData(this.type)}};var n=e.fn.tooltip;e.fn.tooltip=function(n){return this.each(function(){var r=e(this),i=r.data("tooltip"),s=typeof n=="object"&&n;i||r.data("tooltip",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.tooltip.Constructor=t,e.fn.tooltip.defaults={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1},e.fn.tooltip.noConflict=function(){return e.fn.tooltip=n,this}}(window.jQuery),!function(e){"use strict";var t=function(e,t){this.init("popover",e,t)};t.prototype=e.extend({},e.fn.tooltip.Constructor.prototype,{constructor:t,setContent:function(){var e=this.tip(),t=this.getTitle(),n=this.getContent();e.find(".popover-title")[this.options.html?"html":"text"](t),e.find(".popover-content")[this.options.html?"html":"text"](n),e.removeClass("fade top bottom left right in")},hasContent:function(){return this.getTitle()||this.getContent()},getContent:function(){var e,t=this.$element,n=this.options;return e=(typeof n.content=="function"?n.content.call(t[0]):n.content)||t.attr("data-content"),e},tip:function(){return this.$tip||(this.$tip=e(this.options.template)),this.$tip},destroy:function(){this.hide().$element.off("."+this.type).removeData(this.type)}});var n=e.fn.popover;e.fn.popover=function(n){return this.each(function(){var r=e(this),i=r.data("popover"),s=typeof n=="object"&&n;i||r.data("popover",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.popover.Constructor=t,e.fn.popover.defaults=e.extend({},e.fn.tooltip.defaults,{placement:"right",trigger:"click",content:"",template:'<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),e.fn.popover.noConflict=function(){return e.fn.popover=n,this}}(window.jQuery),!function(e){"use strict";function t(t,n){var r=e.proxy(this.process,this),i=e(t).is("body")?e(window):e(t),s;this.options=e.extend({},e.fn.scrollspy.defaults,n),this.$scrollElement=i.on("scroll.scroll-spy.data-api",r),this.selector=(this.options.target||(s=e(t).attr("href"))&&s.replace(/.*(?=#[^\s]+$)/,"")||"")+" .nav li > a",this.$body=e("body"),this.refresh(),this.process()}t.prototype={constructor:t,refresh:function(){var t=this,n;this.offsets=e([]),this.targets=e([]),n=this.$body.find(this.selector).map(function(){var n=e(this),r=n.data("target")||n.attr("href"),i=/^#\w/.test(r)&&e(r);return i&&i.length&&[[i.position().top+(!e.isWindow(t.$scrollElement.get(0))&&t.$scrollElement.scrollTop()),r]]||null}).sort(function(e,t){return e[0]-t[0]}).each(function(){t.offsets.push(this[0]),t.targets.push(this[1])})},process:function(){var e=this.$scrollElement.scrollTop()+this.options.offset,t=this.$scrollElement[0].scrollHeight||this.$body[0].scrollHeight,n=t-this.$scrollElement.height(),r=this.offsets,i=this.targets,s=this.activeTarget,o;if(e>=n)return s!=(o=i.last()[0])&&this.activate(o);for(o=r.length;o--;)s!=i[o]&&e>=r[o]&&(!r[o+1]||e<=r[o+1])&&this.activate(i[o])},activate:function(t){var n,r;this.activeTarget=t,e(this.selector).parent(".active").removeClass("active"),r=this.selector+'[data-target="'+t+'"],'+this.selector+'[href="'+t+'"]',n=e(r).parent("li").addClass("active"),n.parent(".dropdown-menu").length&&(n=n.closest("li.dropdown").addClass("active")),n.trigger("activate")}};var n=e.fn.scrollspy;e.fn.scrollspy=function(n){return this.each(function(){var r=e(this),i=r.data("scrollspy"),s=typeof n=="object"&&n;i||r.data("scrollspy",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.scrollspy.Constructor=t,e.fn.scrollspy.defaults={offset:10},e.fn.scrollspy.noConflict=function(){return e.fn.scrollspy=n,this},e(window).on("load",function(){e('[data-spy="scroll"]').each(function(){var t=e(this);t.scrollspy(t.data())})})}(window.jQuery),!function(e){"use strict";var t=function(t){this.element=e(t)};t.prototype={constructor:t,show:function(){var t=this.element,n=t.closest("ul:not(.dropdown-menu)"),r=t.attr("data-target"),i,s,o;r||(r=t.attr("href"),r=r&&r.replace(/.*(?=#[^\s]*$)/,""));if(t.parent("li").hasClass("active"))return;i=n.find(".active:last a")[0],o=e.Event("show",{relatedTarget:i}),t.trigger(o);if(o.isDefaultPrevented())return;s=e(r),this.activate(t.parent("li"),n),this.activate(s,s.parent(),function(){t.trigger({type:"shown",relatedTarget:i})})},activate:function(t,n,r){function o(){i.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"),t.addClass("active"),s?(t[0].offsetWidth,t.addClass("in")):t.removeClass("fade"),t.parent(".dropdown-menu")&&t.closest("li.dropdown").addClass("active"),r&&r()}var i=n.find("> .active"),s=r&&e.support.transition&&i.hasClass("fade");s?i.one(e.support.transition.end,o):o(),i.removeClass("in")}};var n=e.fn.tab;e.fn.tab=function(n){return this.each(function(){var r=e(this),i=r.data("tab");i||r.data("tab",i=new t(this)),typeof n=="string"&&i[n]()})},e.fn.tab.Constructor=t,e.fn.tab.noConflict=function(){return e.fn.tab=n,this},e(document).on("click.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(t){t.preventDefault(),e(this).tab("show")})}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.$element=e(t),this.options=e.extend({},e.fn.typeahead.defaults,n),this.matcher=this.options.matcher||this.matcher,this.sorter=this.options.sorter||this.sorter,this.highlighter=this.options.highlighter||this.highlighter,this.updater=this.options.updater||this.updater,this.source=this.options.source,this.$menu=e(this.options.menu),this.shown=!1,this.listen()};t.prototype={constructor:t,select:function(){var e=this.$menu.find(".active").attr("data-value");return this.$element.val(this.updater(e)).change(),this.hide()},updater:function(e){return e},show:function(){var t=e.extend({},this.$element.position(),{height:this.$element[0].offsetHeight});return this.$menu.insertAfter(this.$element).css({top:t.top+t.height,left:t.left}).show(),this.shown=!0,this},hide:function(){return this.$menu.hide(),this.shown=!1,this},lookup:function(t){var n;return this.query=this.$element.val(),!this.query||this.query.length<this.options.minLength?this.shown?this.hide():this:(n=e.isFunction(this.source)?this.source(this.query,e.proxy(this.process,this)):this.source,n?this.process(n):this)},process:function(t){var n=this;return t=e.grep(t,function(e){return n.matcher(e)}),t=this.sorter(t),t.length?this.render(t.slice(0,this.options.items)).show():this.shown?this.hide():this},matcher:function(e){return~e.toLowerCase().indexOf(this.query.toLowerCase())},sorter:function(e){var t=[],n=[],r=[],i;while(i=e.shift())i.toLowerCase().indexOf(this.query.toLowerCase())?~i.indexOf(this.query)?n.push(i):r.push(i):t.push(i);return t.concat(n,r)},highlighter:function(e){var t=this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&");return e.replace(new RegExp("("+t+")","ig"),function(e,t){return"<strong>"+t+"</strong>"})},render:function(t){var n=this;return t=e(t).map(function(t,r){return t=e(n.options.item).attr("data-value",r),t.find("a").html(n.highlighter(r)),t[0]}),t.first().addClass("active"),this.$menu.html(t),this},next:function(t){var n=this.$menu.find(".active").removeClass("active"),r=n.next();r.length||(r=e(this.$menu.find("li")[0])),r.addClass("active")},prev:function(e){var t=this.$menu.find(".active").removeClass("active"),n=t.prev();n.length||(n=this.$menu.find("li").last()),n.addClass("active")},listen:function(){this.$element.on("focus",e.proxy(this.focus,this)).on("blur",e.proxy(this.blur,this)).on("keypress",e.proxy(this.keypress,this)).on("keyup",e.proxy(this.keyup,this)),this.eventSupported("keydown")&&this.$element.on("keydown",e.proxy(this.keydown,this)),this.$menu.on("click",e.proxy(this.click,this)).on("mouseenter","li",e.proxy(this.mouseenter,this)).on("mouseleave","li",e.proxy(this.mouseleave,this))},eventSupported:function(e){var t=e in this.$element;return t||(this.$element.setAttribute(e,"return;"),t=typeof this.$element[e]=="function"),t},move:function(e){if(!this.shown)return;switch(e.keyCode){case 9:case 13:case 27:e.preventDefault();break;case 38:e.preventDefault(),this.prev();break;case 40:e.preventDefault(),this.next()}e.stopPropagation()},keydown:function(t){this.suppressKeyPressRepeat=~e.inArray(t.keyCode,[40,38,9,13,27]),this.move(t)},keypress:function(e){if(this.suppressKeyPressRepeat)return;this.move(e)},keyup:function(e){switch(e.keyCode){case 40:case 38:case 16:case 17:case 18:break;case 9:case 13:if(!this.shown)return;this.select();break;case 27:if(!this.shown)return;this.hide();break;default:this.lookup()}e.stopPropagation(),e.preventDefault()},focus:function(e){this.focused=!0},blur:function(e){this.focused=!1,!this.mousedover&&this.shown&&this.hide()},click:function(e){e.stopPropagation(),e.preventDefault(),this.select(),this.$element.focus()},mouseenter:function(t){this.mousedover=!0,this.$menu.find(".active").removeClass("active"),e(t.currentTarget).addClass("active")},mouseleave:function(e){this.mousedover=!1,!this.focused&&this.shown&&this.hide()}};var n=e.fn.typeahead;e.fn.typeahead=function(n){return this.each(function(){var r=e(this),i=r.data("typeahead"),s=typeof n=="object"&&n;i||r.data("typeahead",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.typeahead.defaults={source:[],items:8,menu:'<ul class="typeahead dropdown-menu"></ul>',item:'<li><a href="#"></a></li>',minLength:1},e.fn.typeahead.Constructor=t,e.fn.typeahead.noConflict=function(){return e.fn.typeahead=n,this},e(document).on("focus.typeahead.data-api",'[data-provide="typeahead"]',function(t){var n=e(this);if(n.data("typeahead"))return;n.typeahead(n.data())})}(window.jQuery),!function(e){"use strict";var t=function(t,n){this.options=e.extend({},e.fn.affix.defaults,n),this.$window=e(window).on("scroll.affix.data-api",e.proxy(this.checkPosition,this)).on("click.affix.data-api",e.proxy(function(){setTimeout(e.proxy(this.checkPosition,this),1)},this)),this.$element=e(t),this.checkPosition()};t.prototype.checkPosition=function(){if(!this.$element.is(":visible"))return;var t=e(document).height(),n=this.$window.scrollTop(),r=this.$element.offset(),i=this.options.offset,s=i.bottom,o=i.top,u="affix affix-top affix-bottom",a;typeof i!="object"&&(s=o=i),typeof o=="function"&&(o=i.top()),typeof s=="function"&&(s=i.bottom()),a=this.unpin!=null&&n+this.unpin<=r.top?!1:s!=null&&r.top+this.$element.height()>=t-s?"bottom":o!=null&&n<=o?"top":!1;if(this.affixed===a)return;this.affixed=a,this.unpin=a=="bottom"?r.top-n:null,this.$element.removeClass(u).addClass("affix"+(a?"-"+a:""))};var n=e.fn.affix;e.fn.affix=function(n){return this.each(function(){var r=e(this),i=r.data("affix"),s=typeof n=="object"&&n;i||r.data("affix",i=new t(this,s)),typeof n=="string"&&i[n]()})},e.fn.affix.Constructor=t,e.fn.affix.defaults={offset:0},e.fn.affix.noConflict=function(){return e.fn.affix=n,this},e(window).on("load",function(){e('[data-spy="affix"]').each(function(){var t=e(this),n=t.data();n.offset=n.offset||{},n.offsetBottom&&(n.offset.bottom=n.offsetBottom),n.offsetTop&&(n.offset.top=n.offsetTop),t.affix(n)})})}(window.jQuery);

/*! Konami code plugin for jQuery - http://tommcfarlin.com */
(function(e){"use strict";e.fn.konami=function(t){var n,r,i,s;n=e.extend({},e.fn.konami.defaults,t);return this.each(function(){i=[];e(window).keyup(function(e){s=e.keyCode||e.which;if(n.code.length>i.push(s)){return}if(n.code.length<i.length){i.shift()}if(n.code.toString()!==i.toString()){return}n.cheat()})})};e.fn.konami.defaults={code:[38,38,40,40,37,39,37,39,66,65],cheat:null}})(jQuery);

/*! Magnific Popup - v0.9.9 - http://dimsemenov.com/plugins/magnific-popup */
(function(e){var t,n,i,o,r,a,s,l="Close",c="BeforeClose",d="AfterClose",u="BeforeAppend",p="MarkupParse",f="Open",m="Change",g="mfp",h="."+g,v="mfp-ready",C="mfp-removing",y="mfp-prevent-close",w=function(){},b=!!window.jQuery,I=e(window),x=function(e,n){t.ev.on(g+e+h,n)},k=function(t,n,i,o){var r=document.createElement("div");return r.className="mfp-"+t,i&&(r.innerHTML=i),o?n&&n.appendChild(r):(r=e(r),n&&r.appendTo(n)),r},T=function(n,i){t.ev.triggerHandler(g+n,i),t.st.callbacks&&(n=n.charAt(0).toLowerCase()+n.slice(1),t.st.callbacks[n]&&t.st.callbacks[n].apply(t,e.isArray(i)?i:[i]))},E=function(n){return n===s&&t.currTemplate.closeBtn||(t.currTemplate.closeBtn=e(t.st.closeMarkup.replace("%title%",t.st.tClose)),s=n),t.currTemplate.closeBtn},_=function(){e.magnificPopup.instance||(t=new w,t.init(),e.magnificPopup.instance=t)},S=function(){var e=document.createElement("p").style,t=["ms","O","Moz","Webkit"];if(void 0!==e.transition)return!0;for(;t.length;)if(t.pop()+"Transition"in e)return!0;return!1};w.prototype={constructor:w,init:function(){var n=navigator.appVersion;t.isIE7=-1!==n.indexOf("MSIE 7."),t.isIE8=-1!==n.indexOf("MSIE 8."),t.isLowIE=t.isIE7||t.isIE8,t.isAndroid=/android/gi.test(n),t.isIOS=/iphone|ipad|ipod/gi.test(n),t.supportsTransition=S(),t.probablyMobile=t.isAndroid||t.isIOS||/(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),o=e(document),t.popupsCache={}},open:function(n){i||(i=e(document.body));var r;if(n.isObj===!1){t.items=n.items.toArray(),t.index=0;var s,l=n.items;for(r=0;l.length>r;r++)if(s=l[r],s.parsed&&(s=s.el[0]),s===n.el[0]){t.index=r;break}}else t.items=e.isArray(n.items)?n.items:[n.items],t.index=n.index||0;if(t.isOpen)return t.updateItemHTML(),void 0;t.types=[],a="",t.ev=n.mainEl&&n.mainEl.length?n.mainEl.eq(0):o,n.key?(t.popupsCache[n.key]||(t.popupsCache[n.key]={}),t.currTemplate=t.popupsCache[n.key]):t.currTemplate={},t.st=e.extend(!0,{},e.magnificPopup.defaults,n),t.fixedContentPos="auto"===t.st.fixedContentPos?!t.probablyMobile:t.st.fixedContentPos,t.st.modal&&(t.st.closeOnContentClick=!1,t.st.closeOnBgClick=!1,t.st.showCloseBtn=!1,t.st.enableEscapeKey=!1),t.bgOverlay||(t.bgOverlay=k("bg").on("click"+h,function(){t.close()}),t.wrap=k("wrap").attr("tabindex",-1).on("click"+h,function(e){t._checkIfClose(e.target)&&t.close()}),t.container=k("container",t.wrap)),t.contentContainer=k("content"),t.st.preloader&&(t.preloader=k("preloader",t.container,t.st.tLoading));var c=e.magnificPopup.modules;for(r=0;c.length>r;r++){var d=c[r];d=d.charAt(0).toUpperCase()+d.slice(1),t["init"+d].call(t)}T("BeforeOpen"),t.st.showCloseBtn&&(t.st.closeBtnInside?(x(p,function(e,t,n,i){n.close_replaceWith=E(i.type)}),a+=" mfp-close-btn-in"):t.wrap.append(E())),t.st.alignTop&&(a+=" mfp-align-top"),t.fixedContentPos?t.wrap.css({overflow:t.st.overflowY,overflowX:"hidden",overflowY:t.st.overflowY}):t.wrap.css({top:I.scrollTop(),position:"absolute"}),(t.st.fixedBgPos===!1||"auto"===t.st.fixedBgPos&&!t.fixedContentPos)&&t.bgOverlay.css({height:o.height(),position:"absolute"}),t.st.enableEscapeKey&&o.on("keyup"+h,function(e){27===e.keyCode&&t.close()}),I.on("resize"+h,function(){t.updateSize()}),t.st.closeOnContentClick||(a+=" mfp-auto-cursor"),a&&t.wrap.addClass(a);var u=t.wH=I.height(),m={};if(t.fixedContentPos&&t._hasScrollBar(u)){var g=t._getScrollbarSize();g&&(m.marginRight=g)}t.fixedContentPos&&(t.isIE7?e("body, html").css("overflow","hidden"):m.overflow="hidden");var C=t.st.mainClass;return t.isIE7&&(C+=" mfp-ie7"),C&&t._addClassToMFP(C),t.updateItemHTML(),T("BuildControls"),e("html").css(m),t.bgOverlay.add(t.wrap).prependTo(t.st.prependTo||i),t._lastFocusedEl=document.activeElement,setTimeout(function(){t.content?(t._addClassToMFP(v),t._setFocus()):t.bgOverlay.addClass(v),o.on("focusin"+h,t._onFocusIn)},16),t.isOpen=!0,t.updateSize(u),T(f),n},close:function(){t.isOpen&&(T(c),t.isOpen=!1,t.st.removalDelay&&!t.isLowIE&&t.supportsTransition?(t._addClassToMFP(C),setTimeout(function(){t._close()},t.st.removalDelay)):t._close())},_close:function(){T(l);var n=C+" "+v+" ";if(t.bgOverlay.detach(),t.wrap.detach(),t.container.empty(),t.st.mainClass&&(n+=t.st.mainClass+" "),t._removeClassFromMFP(n),t.fixedContentPos){var i={marginRight:""};t.isIE7?e("body, html").css("overflow",""):i.overflow="",e("html").css(i)}o.off("keyup"+h+" focusin"+h),t.ev.off(h),t.wrap.attr("class","mfp-wrap").removeAttr("style"),t.bgOverlay.attr("class","mfp-bg"),t.container.attr("class","mfp-container"),!t.st.showCloseBtn||t.st.closeBtnInside&&t.currTemplate[t.currItem.type]!==!0||t.currTemplate.closeBtn&&t.currTemplate.closeBtn.detach(),t._lastFocusedEl&&e(t._lastFocusedEl).focus(),t.currItem=null,t.content=null,t.currTemplate=null,t.prevHeight=0,T(d)},updateSize:function(e){if(t.isIOS){var n=document.documentElement.clientWidth/window.innerWidth,i=window.innerHeight*n;t.wrap.css("height",i),t.wH=i}else t.wH=e||I.height();t.fixedContentPos||t.wrap.css("height",t.wH),T("Resize")},updateItemHTML:function(){var n=t.items[t.index];t.contentContainer.detach(),t.content&&t.content.detach(),n.parsed||(n=t.parseEl(t.index));var i=n.type;if(T("BeforeChange",[t.currItem?t.currItem.type:"",i]),t.currItem=n,!t.currTemplate[i]){var o=t.st[i]?t.st[i].markup:!1;T("FirstMarkupParse",o),t.currTemplate[i]=o?e(o):!0}r&&r!==n.type&&t.container.removeClass("mfp-"+r+"-holder");var a=t["get"+i.charAt(0).toUpperCase()+i.slice(1)](n,t.currTemplate[i]);t.appendContent(a,i),n.preloaded=!0,T(m,n),r=n.type,t.container.prepend(t.contentContainer),T("AfterChange")},appendContent:function(e,n){t.content=e,e?t.st.showCloseBtn&&t.st.closeBtnInside&&t.currTemplate[n]===!0?t.content.find(".mfp-close").length||t.content.append(E()):t.content=e:t.content="",T(u),t.container.addClass("mfp-"+n+"-holder"),t.contentContainer.append(t.content)},parseEl:function(n){var i,o=t.items[n];if(o.tagName?o={el:e(o)}:(i=o.type,o={data:o,src:o.src}),o.el){for(var r=t.types,a=0;r.length>a;a++)if(o.el.hasClass("mfp-"+r[a])){i=r[a];break}o.src=o.el.attr("data-mfp-src"),o.src||(o.src=o.el.attr("href"))}return o.type=i||t.st.type||"inline",o.index=n,o.parsed=!0,t.items[n]=o,T("ElementParse",o),t.items[n]},addGroup:function(e,n){var i=function(i){i.mfpEl=this,t._openClick(i,e,n)};n||(n={});var o="click.magnificPopup";n.mainEl=e,n.items?(n.isObj=!0,e.off(o).on(o,i)):(n.isObj=!1,n.delegate?e.off(o).on(o,n.delegate,i):(n.items=e,e.off(o).on(o,i)))},_openClick:function(n,i,o){var r=void 0!==o.midClick?o.midClick:e.magnificPopup.defaults.midClick;if(r||2!==n.which&&!n.ctrlKey&&!n.metaKey){var a=void 0!==o.disableOn?o.disableOn:e.magnificPopup.defaults.disableOn;if(a)if(e.isFunction(a)){if(!a.call(t))return!0}else if(a>I.width())return!0;n.type&&(n.preventDefault(),t.isOpen&&n.stopPropagation()),o.el=e(n.mfpEl),o.delegate&&(o.items=i.find(o.delegate)),t.open(o)}},updateStatus:function(e,i){if(t.preloader){n!==e&&t.container.removeClass("mfp-s-"+n),i||"loading"!==e||(i=t.st.tLoading);var o={status:e,text:i};T("UpdateStatus",o),e=o.status,i=o.text,t.preloader.html(i),t.preloader.find("a").on("click",function(e){e.stopImmediatePropagation()}),t.container.addClass("mfp-s-"+e),n=e}},_checkIfClose:function(n){if(!e(n).hasClass(y)){var i=t.st.closeOnContentClick,o=t.st.closeOnBgClick;if(i&&o)return!0;if(!t.content||e(n).hasClass("mfp-close")||t.preloader&&n===t.preloader[0])return!0;if(n===t.content[0]||e.contains(t.content[0],n)){if(i)return!0}else if(o&&e.contains(document,n))return!0;return!1}},_addClassToMFP:function(e){t.bgOverlay.addClass(e),t.wrap.addClass(e)},_removeClassFromMFP:function(e){this.bgOverlay.removeClass(e),t.wrap.removeClass(e)},_hasScrollBar:function(e){return(t.isIE7?o.height():document.body.scrollHeight)>(e||I.height())},_setFocus:function(){(t.st.focus?t.content.find(t.st.focus).eq(0):t.wrap).focus()},_onFocusIn:function(n){return n.target===t.wrap[0]||e.contains(t.wrap[0],n.target)?void 0:(t._setFocus(),!1)},_parseMarkup:function(t,n,i){var o;i.data&&(n=e.extend(i.data,n)),T(p,[t,n,i]),e.each(n,function(e,n){if(void 0===n||n===!1)return!0;if(o=e.split("_"),o.length>1){var i=t.find(h+"-"+o[0]);if(i.length>0){var r=o[1];"replaceWith"===r?i[0]!==n[0]&&i.replaceWith(n):"img"===r?i.is("img")?i.attr("src",n):i.replaceWith('<img src="'+n+'" class="'+i.attr("class")+'" />'):i.attr(o[1],n)}}else t.find(h+"-"+e).html(n)})},_getScrollbarSize:function(){if(void 0===t.scrollbarSize){var e=document.createElement("div");e.id="mfp-sbm",e.style.cssText="width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",document.body.appendChild(e),t.scrollbarSize=e.offsetWidth-e.clientWidth,document.body.removeChild(e)}return t.scrollbarSize}},e.magnificPopup={instance:null,proto:w.prototype,modules:[],open:function(t,n){return _(),t=t?e.extend(!0,{},t):{},t.isObj=!0,t.index=n||0,this.instance.open(t)},close:function(){return e.magnificPopup.instance&&e.magnificPopup.instance.close()},registerModule:function(t,n){n.options&&(e.magnificPopup.defaults[t]=n.options),e.extend(this.proto,n.proto),this.modules.push(t)},defaults:{disableOn:0,key:null,midClick:!1,mainClass:"",preloader:!0,focus:"",closeOnContentClick:!1,closeOnBgClick:!0,closeBtnInside:!0,showCloseBtn:!0,enableEscapeKey:!0,modal:!1,alignTop:!1,removalDelay:0,prependTo:null,fixedContentPos:"auto",fixedBgPos:"auto",overflowY:"auto",closeMarkup:'<button title="%title%" type="button" class="mfp-close">&times;</button>',tClose:"Close (Esc)",tLoading:"Loading..."}},e.fn.magnificPopup=function(n){_();var i=e(this);if("string"==typeof n)if("open"===n){var o,r=b?i.data("magnificPopup"):i[0].magnificPopup,a=parseInt(arguments[1],10)||0;r.items?o=r.items[a]:(o=i,r.delegate&&(o=o.find(r.delegate)),o=o.eq(a)),t._openClick({mfpEl:o},i,r)}else t.isOpen&&t[n].apply(t,Array.prototype.slice.call(arguments,1));else n=e.extend(!0,{},n),b?i.data("magnificPopup",n):i[0].magnificPopup=n,t.addGroup(i,n);return i};var P,O,z,M="inline",B=function(){z&&(O.after(z.addClass(P)).detach(),z=null)};e.magnificPopup.registerModule(M,{options:{hiddenClass:"hide",markup:"",tNotFound:"Content not found"},proto:{initInline:function(){t.types.push(M),x(l+"."+M,function(){B()})},getInline:function(n,i){if(B(),n.src){var o=t.st.inline,r=e(n.src);if(r.length){var a=r[0].parentNode;a&&a.tagName&&(O||(P=o.hiddenClass,O=k(P),P="mfp-"+P),z=r.after(O).detach().removeClass(P)),t.updateStatus("ready")}else t.updateStatus("error",o.tNotFound),r=e("<div>");return n.inlineElement=r,r}return t.updateStatus("ready"),t._parseMarkup(i,{},n),i}}});var F,H="ajax",L=function(){F&&i.removeClass(F)},A=function(){L(),t.req&&t.req.abort()};e.magnificPopup.registerModule(H,{options:{settings:null,cursor:"mfp-ajax-cur",tError:'<a href="%url%">The content</a> could not be loaded.'},proto:{initAjax:function(){t.types.push(H),F=t.st.ajax.cursor,x(l+"."+H,A),x("BeforeChange."+H,A)},getAjax:function(n){F&&i.addClass(F),t.updateStatus("loading");var o=e.extend({url:n.src,success:function(i,o,r){var a={data:i,xhr:r};T("ParseAjax",a),t.appendContent(e(a.data),H),n.finished=!0,L(),t._setFocus(),setTimeout(function(){t.wrap.addClass(v)},16),t.updateStatus("ready"),T("AjaxContentAdded")},error:function(){L(),n.finished=n.loadError=!0,t.updateStatus("error",t.st.ajax.tError.replace("%url%",n.src))}},t.st.ajax.settings);return t.req=e.ajax(o),""}}});var j,N=function(n){if(n.data&&void 0!==n.data.title)return n.data.title;var i=t.st.image.titleSrc;if(i){if(e.isFunction(i))return i.call(t,n);if(n.el)return n.el.attr(i)||""}return""};e.magnificPopup.registerModule("image",{options:{markup:'<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',cursor:"mfp-zoom-out-cur",titleSrc:"title",verticalFit:!0,tError:'<a href="%url%">The image</a> could not be loaded.'},proto:{initImage:function(){var e=t.st.image,n=".image";t.types.push("image"),x(f+n,function(){"image"===t.currItem.type&&e.cursor&&i.addClass(e.cursor)}),x(l+n,function(){e.cursor&&i.removeClass(e.cursor),I.off("resize"+h)}),x("Resize"+n,t.resizeImage),t.isLowIE&&x("AfterChange",t.resizeImage)},resizeImage:function(){var e=t.currItem;if(e&&e.img&&t.st.image.verticalFit){var n=0;t.isLowIE&&(n=parseInt(e.img.css("padding-top"),10)+parseInt(e.img.css("padding-bottom"),10)),e.img.css("max-height",t.wH-n)}},_onImageHasSize:function(e){e.img&&(e.hasSize=!0,j&&clearInterval(j),e.isCheckingImgSize=!1,T("ImageHasSize",e),e.imgHidden&&(t.content&&t.content.removeClass("mfp-loading"),e.imgHidden=!1))},findImageSize:function(e){var n=0,i=e.img[0],o=function(r){j&&clearInterval(j),j=setInterval(function(){return i.naturalWidth>0?(t._onImageHasSize(e),void 0):(n>200&&clearInterval(j),n++,3===n?o(10):40===n?o(50):100===n&&o(500),void 0)},r)};o(1)},getImage:function(n,i){var o=0,r=function(){n&&(n.img[0].complete?(n.img.off(".mfploader"),n===t.currItem&&(t._onImageHasSize(n),t.updateStatus("ready")),n.hasSize=!0,n.loaded=!0,T("ImageLoadComplete")):(o++,200>o?setTimeout(r,100):a()))},a=function(){n&&(n.img.off(".mfploader"),n===t.currItem&&(t._onImageHasSize(n),t.updateStatus("error",s.tError.replace("%url%",n.src))),n.hasSize=!0,n.loaded=!0,n.loadError=!0)},s=t.st.image,l=i.find(".mfp-img");if(l.length){var c=document.createElement("img");c.className="mfp-img",n.img=e(c).on("load.mfploader",r).on("error.mfploader",a),c.src=n.src,l.is("img")&&(n.img=n.img.clone()),c=n.img[0],c.naturalWidth>0?n.hasSize=!0:c.width||(n.hasSize=!1)}return t._parseMarkup(i,{title:N(n),img_replaceWith:n.img},n),t.resizeImage(),n.hasSize?(j&&clearInterval(j),n.loadError?(i.addClass("mfp-loading"),t.updateStatus("error",s.tError.replace("%url%",n.src))):(i.removeClass("mfp-loading"),t.updateStatus("ready")),i):(t.updateStatus("loading"),n.loading=!0,n.hasSize||(n.imgHidden=!0,i.addClass("mfp-loading"),t.findImageSize(n)),i)}}});var W,R=function(){return void 0===W&&(W=void 0!==document.createElement("p").style.MozTransform),W};e.magnificPopup.registerModule("zoom",{options:{enabled:!1,easing:"ease-in-out",duration:300,opener:function(e){return e.is("img")?e:e.find("img")}},proto:{initZoom:function(){var e,n=t.st.zoom,i=".zoom";if(n.enabled&&t.supportsTransition){var o,r,a=n.duration,s=function(e){var t=e.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),i="all "+n.duration/1e3+"s "+n.easing,o={position:"fixed",zIndex:9999,left:0,top:0,"-webkit-backface-visibility":"hidden"},r="transition";return o["-webkit-"+r]=o["-moz-"+r]=o["-o-"+r]=o[r]=i,t.css(o),t},d=function(){t.content.css("visibility","visible")};x("BuildControls"+i,function(){if(t._allowZoom()){if(clearTimeout(o),t.content.css("visibility","hidden"),e=t._getItemToZoom(),!e)return d(),void 0;r=s(e),r.css(t._getOffset()),t.wrap.append(r),o=setTimeout(function(){r.css(t._getOffset(!0)),o=setTimeout(function(){d(),setTimeout(function(){r.remove(),e=r=null,T("ZoomAnimationEnded")},16)},a)},16)}}),x(c+i,function(){if(t._allowZoom()){if(clearTimeout(o),t.st.removalDelay=a,!e){if(e=t._getItemToZoom(),!e)return;r=s(e)}r.css(t._getOffset(!0)),t.wrap.append(r),t.content.css("visibility","hidden"),setTimeout(function(){r.css(t._getOffset())},16)}}),x(l+i,function(){t._allowZoom()&&(d(),r&&r.remove(),e=null)})}},_allowZoom:function(){return"image"===t.currItem.type},_getItemToZoom:function(){return t.currItem.hasSize?t.currItem.img:!1},_getOffset:function(n){var i;i=n?t.currItem.img:t.st.zoom.opener(t.currItem.el||t.currItem);var o=i.offset(),r=parseInt(i.css("padding-top"),10),a=parseInt(i.css("padding-bottom"),10);o.top-=e(window).scrollTop()-r;var s={width:i.width(),height:(b?i.innerHeight():i[0].offsetHeight)-a-r};return R()?s["-moz-transform"]=s.transform="translate("+o.left+"px,"+o.top+"px)":(s.left=o.left,s.top=o.top),s}}});var Z="iframe",q="//about:blank",D=function(e){if(t.currTemplate[Z]){var n=t.currTemplate[Z].find("iframe");n.length&&(e||(n[0].src=q),t.isIE8&&n.css("display",e?"block":"none"))}};e.magnificPopup.registerModule(Z,{options:{markup:'<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',srcAction:"iframe_src",patterns:{youtube:{index:"youtube.com",id:"v=",src:"//www.youtube.com/embed/%id%?autoplay=1"},vimeo:{index:"vimeo.com/",id:"/",src:"//player.vimeo.com/video/%id%?autoplay=1"},gmaps:{index:"//maps.google.",src:"%id%&output=embed"}}},proto:{initIframe:function(){t.types.push(Z),x("BeforeChange",function(e,t,n){t!==n&&(t===Z?D():n===Z&&D(!0))}),x(l+"."+Z,function(){D()})},getIframe:function(n,i){var o=n.src,r=t.st.iframe;e.each(r.patterns,function(){return o.indexOf(this.index)>-1?(this.id&&(o="string"==typeof this.id?o.substr(o.lastIndexOf(this.id)+this.id.length,o.length):this.id.call(this,o)),o=this.src.replace("%id%",o),!1):void 0});var a={};return r.srcAction&&(a[r.srcAction]=o),t._parseMarkup(i,a,n),t.updateStatus("ready"),i}}});var K=function(e){var n=t.items.length;return e>n-1?e-n:0>e?n+e:e},Y=function(e,t,n){return e.replace(/%curr%/gi,t+1).replace(/%total%/gi,n)};e.magnificPopup.registerModule("gallery",{options:{enabled:!1,arrowMarkup:'<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',preload:[0,2],navigateByImgClick:!0,arrows:!0,tPrev:"Previous (Left arrow key)",tNext:"Next (Right arrow key)",tCounter:"%curr% of %total%"},proto:{initGallery:function(){var n=t.st.gallery,i=".mfp-gallery",r=Boolean(e.fn.mfpFastClick);return t.direction=!0,n&&n.enabled?(a+=" mfp-gallery",x(f+i,function(){n.navigateByImgClick&&t.wrap.on("click"+i,".mfp-img",function(){return t.items.length>1?(t.next(),!1):void 0}),o.on("keydown"+i,function(e){37===e.keyCode?t.prev():39===e.keyCode&&t.next()})}),x("UpdateStatus"+i,function(e,n){n.text&&(n.text=Y(n.text,t.currItem.index,t.items.length))}),x(p+i,function(e,i,o,r){var a=t.items.length;o.counter=a>1?Y(n.tCounter,r.index,a):""}),x("BuildControls"+i,function(){if(t.items.length>1&&n.arrows&&!t.arrowLeft){var i=n.arrowMarkup,o=t.arrowLeft=e(i.replace(/%title%/gi,n.tPrev).replace(/%dir%/gi,"left")).addClass(y),a=t.arrowRight=e(i.replace(/%title%/gi,n.tNext).replace(/%dir%/gi,"right")).addClass(y),s=r?"mfpFastClick":"click";o[s](function(){t.prev()}),a[s](function(){t.next()}),t.isIE7&&(k("b",o[0],!1,!0),k("a",o[0],!1,!0),k("b",a[0],!1,!0),k("a",a[0],!1,!0)),t.container.append(o.add(a))}}),x(m+i,function(){t._preloadTimeout&&clearTimeout(t._preloadTimeout),t._preloadTimeout=setTimeout(function(){t.preloadNearbyImages(),t._preloadTimeout=null},16)}),x(l+i,function(){o.off(i),t.wrap.off("click"+i),t.arrowLeft&&r&&t.arrowLeft.add(t.arrowRight).destroyMfpFastClick(),t.arrowRight=t.arrowLeft=null}),void 0):!1},next:function(){t.direction=!0,t.index=K(t.index+1),t.updateItemHTML()},prev:function(){t.direction=!1,t.index=K(t.index-1),t.updateItemHTML()},goTo:function(e){t.direction=e>=t.index,t.index=e,t.updateItemHTML()},preloadNearbyImages:function(){var e,n=t.st.gallery.preload,i=Math.min(n[0],t.items.length),o=Math.min(n[1],t.items.length);for(e=1;(t.direction?o:i)>=e;e++)t._preloadItem(t.index+e);for(e=1;(t.direction?i:o)>=e;e++)t._preloadItem(t.index-e)},_preloadItem:function(n){if(n=K(n),!t.items[n].preloaded){var i=t.items[n];i.parsed||(i=t.parseEl(n)),T("LazyLoad",i),"image"===i.type&&(i.img=e('<img class="mfp-img" />').on("load.mfploader",function(){i.hasSize=!0}).on("error.mfploader",function(){i.hasSize=!0,i.loadError=!0,T("LazyLoadError",i)}).attr("src",i.src)),i.preloaded=!0}}}});var U="retina";e.magnificPopup.registerModule(U,{options:{replaceSrc:function(e){return e.src.replace(/\.\w+$/,function(e){return"@2x"+e})},ratio:1},proto:{initRetina:function(){if(window.devicePixelRatio>1){var e=t.st.retina,n=e.ratio;n=isNaN(n)?n():n,n>1&&(x("ImageHasSize."+U,function(e,t){t.img.css({"max-width":t.img[0].naturalWidth/n,width:"100%"})}),x("ElementParse."+U,function(t,i){i.src=e.replaceSrc(i,n)}))}}}}),function(){var t=1e3,n="ontouchstart"in window,i=function(){I.off("touchmove"+r+" touchend"+r)},o="mfpFastClick",r="."+o;e.fn.mfpFastClick=function(o){return e(this).each(function(){var a,s=e(this);if(n){var l,c,d,u,p,f;s.on("touchstart"+r,function(e){u=!1,f=1,p=e.originalEvent?e.originalEvent.touches[0]:e.touches[0],c=p.clientX,d=p.clientY,I.on("touchmove"+r,function(e){p=e.originalEvent?e.originalEvent.touches:e.touches,f=p.length,p=p[0],(Math.abs(p.clientX-c)>10||Math.abs(p.clientY-d)>10)&&(u=!0,i())}).on("touchend"+r,function(e){i(),u||f>1||(a=!0,e.preventDefault(),clearTimeout(l),l=setTimeout(function(){a=!1},t),o())})})}s.on("click"+r,function(){a||o()})})},e.fn.destroyMfpFastClick=function(){e(this).off("touchstart"+r+" click"+r),n&&I.off("touchmove"+r+" touchend"+r)}}(),_()})(window.jQuery||window.Zepto); 

/*! jQuery Placeholder v2.0.8 - http://mths.be/placeholder */
(function(e,t,n){function c(e){var t={};var r=/^jQuery\d+$/;n.each(e.attributes,function(e,n){if(n.specified&&!r.test(n.name)){t[n.name]=n.value}});return t}function h(e,t){var r=this;var i=n(r);if(r.value==i.attr("placeholder")&&i.hasClass("placeholder")){if(i.data("placeholder-password")){i=i.hide().next().show().attr("id",i.removeAttr("id").data("placeholder-id"));if(e===true){return i[0].value=t}i.focus()}else{r.value="";i.removeClass("placeholder");r==d()&&r.select()}}}function p(){var e;var t=this;var r=n(t);var i=this.id;if(t.value==""){if(t.type=="password"){if(!r.data("placeholder-textinput")){try{e=r.clone().attr({type:"text"})}catch(s){e=n("<input>").attr(n.extend(c(this),{type:"text"}))}e.removeAttr("name").data({"placeholder-password":r,"placeholder-id":i}).bind("focus.placeholder",h);r.data({"placeholder-textinput":e,"placeholder-id":i}).before(e)}r=r.removeAttr("id").hide().prev().attr("id",i).show()}r.addClass("placeholder");r[0].value=r.attr("placeholder")}else{r.removeClass("placeholder")}}function d(){try{return t.activeElement}catch(e){}}var r=Object.prototype.toString.call(e.operamini)=="[object OperaMini]";var i="placeholder"in t.createElement("input")&&!r;var s="placeholder"in t.createElement("textarea")&&!r;var o=n.fn;var u=n.valHooks;var a=n.propHooks;var f;var l;if(i&&s){l=o.placeholder=function(){return this};l.input=l.textarea=true}else{l=o.placeholder=function(){var e=this;e.filter((i?"textarea":":input")+"[placeholder]").not(".placeholder").bind({"focus.placeholder":h,"blur.placeholder":p}).data("placeholder-enabled",true).trigger("blur.placeholder");return e};l.input=i;l.textarea=s;f={get:function(e){var t=n(e);var r=t.data("placeholder-password");if(r){return r[0].value}return t.data("placeholder-enabled")&&t.hasClass("placeholder")?"":e.value},set:function(e,t){var r=n(e);var i=r.data("placeholder-password");if(i){return i[0].value=t}if(!r.data("placeholder-enabled")){return e.value=t}if(t==""){e.value=t;if(e!=d()){p.call(e)}}else if(r.hasClass("placeholder")){h.call(e,true,t)||(e.value=t)}else{e.value=t}return r}};if(!i){u.input=f;a.value=f}if(!s){u.textarea=f;a.value=f}n(function(){n(t).delegate("form","submit.placeholder",function(){var e=n(".placeholder",this).each(h);setTimeout(function(){e.each(p)},10)})});n(e).bind("beforeunload.placeholder",function(){n(".placeholder").each(function(){this.value=""})})}})(this,document,jQuery)

/*! Big banner - greensock.js  -  http://www.greensock.com*/
;eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('(17(e){"4V 4U";19 t=e.5Z||e;1a(!t.5f){19 n,r,i,s,o,u=17(e){19 n,r=e.1t("."),i=t;1b(n=0;r.1c>n;n++)i[r[n]]=i=i[r[n]]||{};18 i},a=u("4Q.4R"),f=1e-10,l=[].6K,c=17(){},h=17(){19 e=9V.1y.9W,t=e.1W([]);18 17(n){18 1d!=n&&(n 2b 3N||"3U"==1j n&&!!n.2d&&e.1W(n)===t)}}(),p={},d=17(n,r,i,s){15.4C=p[n]?p[n].4C:[],p[n]=15,15.54=1d,15.7A=i;19 o=[];15.64=17(a){1b(19 f,l,c,h,v=r.1c,m=v;--v>-1;)(f=p[r[v]]||1h d(r[v],[])).54?(o[v]=f.54,m--):a&&f.4C.2d(15);1a(0===m&&i)1b(l=("4Q.4R."+n).1t("."),c=l.4q(),h=u(l.1J("."))[c]=15.54=i.35(i,o),s&&(t[c]=h,"17"==1j 63&&63.9X?63((e.8j?e.8j+"/":"")+n.1t(".").1J("/"),[],17(){18 h}):"9U"!=1j 62&&62.8f&&(62.8f=h)),v=0;15.4C.1c>v;v++)15.4C[v].64()},15.64(!0)},v=e.3u=17(e,t,n,r){18 1h d(e,t,n,r)},m=a.8k=17(e,t,n){18 t=t||17(){},v(e,[],17(){18 t},n),t};v.77=t;19 g=[0,0,1,1],y=[],b=m("2o.8h",17(e,t,n,r){15.5L=e,15.6v=n||0,15.6t=r||0,15.5H=t?g.43(t):g},!0),w=b.7C={},E=b.8l=17(e,t,n,r){1b(19 i,s,o,u,f=t.1t(","),l=f.1c,c=(n||"5k,6c,5m").1t(",");--l>-1;)1b(s=f[l],i=r?m("2o."+s,1d,!0):a.2o[s]||{},o=c.1c;--o>-1;)u=c[o],w[s+"."+u]=w[u+s]=i[u]=e.2w?e:e[u]||1h e};1b(i=b.1y,i.3s=!1,i.2w=17(e){1a(15.5L)18 15.5H[0]=e,15.5L.35(1d,15.5H);19 t=15.6v,n=15.6t,r=1===t?1-e:2===t?e:.5>e?2*e:2*(1-e);18 1===n?r*=r:2===n?r*=r*r:3===n?r*=r*r*r:4===n&&(r*=r*r*r*r),1===t?1-r:2===t?r:.5>e?r/2:1-r/2},n=["7X","7Z","9T","9Q","9R,9S"],r=n.1c;--r>-1;)i=n[r]+",9Y"+r,E(1h b(1d,1d,1,r),i,"6c",!0),E(1h b(1d,1d,2,r),i,"5k"+(0===r?",9Z":"")),E(1h b(1d,1d,3,r),i,"5m");w.a5=a.2o.7X.5k,w.a6=a.2o.7Z.5m;19 S=m("8V.8W",17(e){15.4d={},15.85=e||15});i=S.1y,i.79=17(e,t,n,r,i){i=i||0;19 u,a,f=15.4d[e],l=0;1b(1d==f&&(15.4d[e]=f=[]),a=f.1c;--a>-1;)u=f[a],u.c===t&&u.s===n?f.2Q(a,1):0===l&&i>u.2s&&(l=a+1);f.2Q(l,0,{c:t,s:n,8a:r,2s:i}),15!==s||o||s.31()},i.a7=17(e,t){19 n,r=15.4d[e];1a(r)1b(n=r.1c;--n>-1;)1a(r[n].c===t)18 r.2Q(n,1),2h 0},i.8Q=17(e){19 t,n,r,i=15.4d[e];1a(i)1b(t=i.1c,n=15.85;--t>-1;)r=i[t],r.8a?r.c.1W(r.s||n,{2p:e,2x:n}):r.c.1W(r.s||n)};19 x=e.a4,T=e.a3,N=88.a0||17(){18(1h 88).a1()},C=N();1b(n=["5t","a2","9P","o"],r=n.1c;--r>-1&&!x;)x=e[n[r]+"9O"],T=e[n[r]+"9B"]||e[n[r]+"9C"];m("4Y",17(e,t){19 n,r,i,u,a,f=15,l=N(),h=t!==!1&&x,p=17(e){C=N(),f.34=(C-l)/8L;19 t,s=f.34-a;(!n||s>0||e===!0)&&(f.3F++,a+=s+(s>=u?.9D:u-s),t=!0),e!==!0&&(i=r(p)),t&&f.8Q("4W")};S.1W(f),f.34=f.3F=0,f.4W=17(){p(!0)},f.5c=17(){1d!=i&&(h&&T?T(i):9A(i),r=c,i=1d,f===s&&(o=!1))},f.31=17(){1d!==i&&f.5c(),r=0===n?c:h&&x?x:17(e){18 5M(e,0|8L*(a-f.34)+1)},f===s&&(o=!0),p(2)},f.5G=17(e){18 2m.1c?(n=e,u=1/(n||60),a=15.34+u,f.31(),2h 0):n},f.8S=17(e){18 2m.1c?(f.5c(),h=e,f.5G(n),2h 0):h},f.5G(e),5M(17(){h&&(!i||5>f.3F)&&f.8S(!1)},9z)}),i=a.4Y.1y=1h a.8V.8W,i.2K=a.4Y;19 k=m("5C.8H",17(e,t){1a(15.1p=t=t||{},15.1C=15.2i=e||0,15.2L=1D(t.4x)||0,15.1x=1,15.2a=t.1Y===!0,15.1A=t.1A,15.2M=t.48===!0,q){o||s.31();19 n=15.1p.5K?I:q;n.1U(15,n.1n),15.1p.3e&&15.3e(!0)}});s=k.6f=1h a.4Y,i=k.1y,i.2r=i.1O=i.2z=i.1B=!1,i.1H=i.1n=0,i.1z=-1,i.1f=i.3l=i.33=i.1o=i.21=1d,i.1B=!1;19 L=17(){o&&N()-C>8J&&s.31(),5M(L,8J)};L(),i.73=17(e,t){18 1d!=e&&15.4b(e,t),15.48(!1).3e(!1)},i.5N=17(e,t){18 1d!=e&&15.4b(e,t),15.3e(!0)},i.9v=17(e,t){18 1d!=e&&15.4b(e,t),15.3e(!1)},i.4b=17(e,t){18 15.2Z(1D(e),t!==!1)},i.9w=17(e,t){18 15.48(!1).3e(!1).2Z(e?-15.2L:0,t!==!1,!0)},i.9x=17(e,t){18 1d!=e&&15.4b(e||15.23(),t),15.48(!0).3e(!1)},i.1E=17(){},i.5u=17(){18 15},i.4n=17(){19 e,t=15.1o,n=15.1i;18!t||!15.1O&&!15.1B&&t.4n()&&(e=t.3H())>=n&&n+15.23()/15.1x>e},i.1N=17(e,t){18 o||s.31(),15.1O=!e,15.2a=15.4n(),t!==!0&&(e&&!15.21?15.1o.1U(15,15.1i-15.2L):!e&&15.21&&15.1o.3K(15,!0)),!1},i.2n=17(){18 15.1N(!1,!1)},i.4f=17(e,t){18 15.2n(e,t),15},i.3c=17(e){1b(19 t=e?15:15.21;t;)t.2r=!0,t=t.21;18 15},i.5l=17(e){1b(19 t=e.1c,n=e.43();--t>-1;)"{4B}"===e[t]&&(n[t]=15);18 n},i.9y=17(e,t,n,r){1a("9E"===(e||"").1q(0,2)){19 i=15.1p;1a(1===2m.1c)18 i[e];1d==t?3E i[e]:(i[e]=t,i[e+"5Y"]=h(n)&&-1!==n.1J("").1k("{4B}")?15.5l(n):n,i[e+"5J"]=r),"5A"===e&&(15.33=t)}18 15},i.4x=17(e){18 2m.1c?(15.1o.2j&&15.8C(15.1i+e-15.2L),15.2L=e,15):15.2L},i.2C=17(e){18 2m.1c?(15.1C=15.2i=e,15.3c(!0),15.1o.2j&&15.1n>0&&15.1n<15.1C&&0!==e&&15.2Z(15.1H*(e/15.1C),!0),15):(15.2r=!1,15.1C)},i.23=17(e){18 15.2r=!1,2m.1c?15.2C(e):15.2i},i.34=17(e,t){18 2m.1c?(15.2r&&15.23(),15.2Z(e>15.1C?15.1C:e,t)):15.1n},i.2Z=17(e,t,n){1a(o||s.31(),!2m.1c)18 15.1H;1a(15.1o){1a(0>e&&!n&&(e+=15.23()),15.1o.2j){15.2r&&15.23();19 r=15.2i,i=15.1o;1a(e>r&&!n&&(e=r),15.1i=(15.1B?15.4O:i.1n)-(15.2M?r-e:e)/15.1x,i.2r||15.3c(!1),i.1o)1b(;i.1o;)i.1o.1n!==(i.1i+i.1H)/i.1x&&i.2Z(i.1H,!0),i=i.1o}15.1O&&15.1N(!0,!1),(15.1H!==e||0===15.1C)&&15.1E(e,t,!1)}18 15},i.9F=i.9L=17(e,t){18 2m.1c?15.2Z(15.2C()*e,t):15.1n/15.2C()},i.8C=17(e){18 2m.1c?(e!==15.1i&&(15.1i=e,15.21&&15.21.4K&&15.21.1U(15,e-15.2L)),15):15.1i},i.6q=17(e){1a(!2m.1c)18 15.1x;1a(e=e||f,15.1o&&15.1o.2j){19 t=15.4O,n=t||0===t?t:15.1o.2Z();15.1i=n-(n-15.1i)*15.1x/e}18 15.1x=e,15.3c(!1)},i.48=17(e){18 2m.1c?(e!=15.2M&&(15.2M=e,15.2Z(15.1o&&!15.1o.2j?15.23()-15.1H:15.1H,!0)),15):15.2M},i.3e=17(e){1a(!2m.1c)18 15.1B;1a(e!=15.1B&&15.1o){o||e||s.31();19 t=15.1o,n=t.3H(),r=n-15.4O;!e&&t.2j&&(15.1i+=r,15.3c(!1)),15.4O=e?n:1d,15.1B=e,15.2a=15.4n(),!e&&0!==r&&15.2z&&15.2C()&&15.1E(t.2j?15.1H:(n-15.1i)/15.1x,!0,!0)}18 15.1O&&!e&&15.1N(!0,!1),15};19 A=m("5C.7y",17(e){k.1W(15,0,e),15.4H=15.2j=!0});i=A.1y=1h k,i.2K=A,i.4f().1O=!1,i.26=i.3l=1d,i.4K=!1,i.1U=i.7w=17(e,t){19 n,r;1a(e.1i=1D(t||0)+e.2L,e.1B&&15!==e.1o&&(e.4O=e.1i+(15.3H()-e.1i)/e.1x),e.21&&e.21.3K(e,!0),e.21=e.1o=15,e.1O&&e.1N(!0,!0),n=15.3l,15.4K)1b(r=e.1i;n&&n.1i>r;)n=n.1l;18 n?(e.1f=n.1f,n.1f=e):(e.1f=15.26,15.26=e),e.1f?e.1f.1l=e:15.3l=e,e.1l=n,15.1o&&15.3c(!0),15},i.3K=17(e,t){18 e.21===15&&(t||e.1N(!1,!0),e.21=1d,e.1l?e.1l.1f=e.1f:15.26===e&&(15.26=e.1f),e.1f?e.1f.1l=e.1l:15.3l===e&&(15.3l=e.1l),15.1o&&15.3c(!0)),15},i.1E=17(e,t,n){19 r,i=15.26;1b(15.1H=15.1n=15.1z=e;i;)r=i.1f,(i.2a||e>=i.1i&&!i.1B)&&(i.2M?i.1E((i.2r?i.23():i.2i)-(e-i.1i)*i.1x,t,n):i.1E((e-i.1i)*i.1x,t,n)),i=r},i.3H=17(){18 o||s.31(),15.1H};19 O=m("5f",17(t,n,r){1a(k.1W(15,n,r),15.1E=O.1y.1E,1d==t)6l"7e 6R a 1d 2x.";15.2x=t="1L"!=1j t?t:O.3I(t)||t;19 i,s,o,u=t.9M||t.1c&&t!==e&&t[0]&&(t[0]===e||t[0].3n&&t[0].1w&&!t.3n),a=15.1p.4I;1a(15.6j=a=1d==a?F[O.8G]:"2y"==1j a?a>>0:F[a],(u||t 2b 3N||t.2d&&h(t))&&"2y"!=1j t[0])1b(15.2F=o=l.1W(t,0),15.3A=[],15.2T=[],i=0;o.1c>i;i++)s=o[i],s?"1L"!=1j s?s.1c&&s!==e&&s[0]&&(s[0]===e||s[0].3n&&s[0].1w&&!s.3n)?(o.2Q(i--,1),15.2F=o=o.43(l.1W(s,0))):(15.2T[i]=R(s,15,!1),1===a&&15.2T[i].1c>1&&U(s,15,1d,1,15.2T[i])):(s=o[i--]=O.3I(s),"1L"==1j s&&o.2Q(i+1,1)):o.2Q(i--,1);1m 15.3A={},15.2T=R(t,15,!1),1===a&&15.2T.1c>1&&U(t,15,1d,1,15.2T);(15.1p.1Y||0===n&&0===15.2L&&15.1p.1Y!==!1)&&15.1E(-15.2L,!1,!0)},!0),M=17(t){18 t.1c&&t!==e&&t[0]&&(t[0]===e||t[0].3n&&t[0].1w&&!t.3n)},2f=17(e,t){19 n,r={};1b(n 1u e)j[n]||n 1u t&&"x"!==n&&"y"!==n&&"3k"!==n&&"3y"!==n&&"3a"!==n&&"3X"!==n||!(!P[n]||P[n]&&P[n].9N)||(r[n]=e[n],3E e[n]);e.4P=r};i=O.1y=1h k,i.2K=O,i.4f().1O=!1,i.3j=0,i.1s=i.2F=i.3m=i.29=1d,i.4u=!1,O.3V="1.11.8",O.6r=i.2H=1h b(1d,1d,1,1),O.8G="2t",O.6f=s,O.7j=!0,O.3I=e.$||e.9K||17(t){18 e.$?(O.3I=e.$,e.$(t)):e.6h?e.6h.9J("#"===t.1v(0)?t.1q(1):t):t};19 D=O.4s={7E:h,7p:M},P=O.9G={},H=O.9H={},B=0,j=D.7k={3z:1,4x:1,4I:1,49:1,6a:1,6s:1,5K:1,4w:1,30:1,5A:1,5U:1,5T:1,4E:1,61:1,5P:1,4i:1,7J:1,7M:1,9I:1,a8:1,a9:1,5d:1,aA:1,1Y:1,5j:1,aB:1,1A:1,3e:1,48:1,6k:1},F={3t:0,41:1,2t:2,aC:3,az:4,ay:5,"av":1,"5X":0},I=k.7P=1h A,q=k.aw=1h A;q.1i=s.34,I.1i=s.3F,q.2a=I.2a=!0,k.7h=17(){1a(q.1E((s.34-q.1i)*q.1x,!1,!1),I.1E((s.3F-I.1i)*I.1x,!1,!1),!(s.3F%ax)){19 e,t,n;1b(n 1u H){1b(t=H[n].3J,e=t.1c;--e>-1;)t[e].1O&&t.2Q(e,1);0===t.1c&&3E H[n]}1a(n=q.26,(!n||n.1B)&&O.7j&&!I.26&&1===s.4d.4W.1c){1b(;n&&n.1B;)n=n.1f;n||s.5c()}}},s.79("4W",k.7h);19 R=17(e,t,n){19 r,i,s=e.7g;1a(H[s||(e.7g=s="t"+B++)]||(H[s]={2x:e,3J:[]}),t&&(r=H[s].3J,r[i=r.1c]=t,n))1b(;--i>-1;)r[i]===t&&r.2Q(i,1);18 H[s].3J},U=17(e,t,n,r,i){19 s,o,u,a;1a(1===r||r>=4){1b(a=i.1c,s=0;a>s;s++)1a((u=i[s])!==t)u.1O||u.1N(!1,!1)&&(o=!0);1m 1a(5===r)76;18 o}19 l,c=t.1i+f,h=[],p=0,d=0===t.1C;1b(s=i.1c;--s>-1;)(u=i[s])===t||u.1O||u.1B||(u.1o!==t.1o?(l=l||z(t,0,d),0===z(u,l,d)&&(h[p++]=u)):c>=u.1i&&u.1i+u.23()/u.1x>c&&((d||!u.2z)&&2e-10>=c-u.1i||(h[p++]=u)));1b(s=p;--s>-1;)u=h[s],2===r&&u.2n(n,e)&&(o=!0),(2!==r||!u.1s&&u.2z)&&u.1N(!1,!1)&&(o=!0);18 o},z=17(e,t,n){1b(19 r=e.1o,i=r.1x,s=e.1i;r.1o;){1a(s+=r.1i,i*=r.1x,r.1B)18-22;r=r.1o}18 s/=i,s>t?s-t:n&&s===t||!e.2z&&2*f>s-t?f:(s+=e.23()/e.1x/i)>t+f?0:s-t-f};i.78=17(){19 e,t,n,r,i=15.1p,s=15.3m,o=15.1C,u=i.1Y,a=i.3z;1a(i.30){1a(15.29&&15.29.1E(-1,!0),i.30.4I=0,i.30.1Y=!0,15.29=O.3Q(15.2x,0,i.30),u)1a(15.1n>0)15.29=1d;1m 1a(0!==o)18}1m 1a(i.4w&&0!==o)1a(15.29)15.29.1E(-1,!0),15.29=1d;1m{n={};1b(r 1u i)j[r]&&"6k"!==r||(n[r]=i[r]);1a(n.4I=0,n.1A="8D",15.29=O.3Q(15.2x,0,n),i.1Y){1a(0===15.1n)18}1m 15.29.1E(-1,!0)}1a(15.2H=a?a 2b b?i.5d 2b 3N?a.3o.35(a,i.5d):a:"17"==1j a?1h b(a,i.5d):w[a]||O.6r:O.6r,15.67=15.2H.6v,15.72=15.2H.6t,15.1s=1d,15.2F)1b(e=15.2F.1c;--e>-1;)15.4p(15.2F[e],15.3A[e]={},15.2T[e],s?s[e]:1d)&&(t=!0);1m t=15.4p(15.2x,15.3A,15.2T,s);1a(t&&O.5z("5D",15),s&&(15.1s||"17"!=1j 15.2x&&15.1N(!1,!1)),i.4w)1b(n=15.1s;n;)n.s+=n.c,n.c=-n.c,n=n.1f;15.33=i.5A,15.2z=!0},i.4p=17(t,n,r,i){19 s,o,u,a,f,l;1a(1d==t)18!1;15.1p.4P||t.1w&&t!==e&&t.3n&&P.4P&&15.1p.6k!==!1&&2f(15.1p,t);1b(s 1u 15.1p){1a(l=15.1p[s],j[s])l&&(l 2b 3N||l.2d&&h(l))&&-1!==l.1J("").1k("{4B}")&&(15.1p[s]=l=15.5l(l,15));1m 1a(P[s]&&(a=1h P[s]).6V(t,15.1p[s],15)){1b(15.1s=f={1f:15.1s,t:a,p:"1S",s:0,c:1,f:!0,n:s,4J:!0,2s:a.5W},o=a.2B.1c;--o>-1;)n[a.2B[o]]=15.1s;(a.5W||a.5D)&&(u=!0),(a.65||a.7L)&&(15.4u=!0)}1m 15.1s=n[s]=f={1f:15.1s,t:t,p:s,f:"17"==1j t[s],n:s,4J:!1,2s:0},f.s=f.f?t[s.1k("4h")||"17"!=1j t["71"+s.1q(3)]?s:"71"+s.1q(3)]():1r(t[s]),f.c="1L"==1j l&&"="===l.1v(1)?3v(l.1v(0)+"1",10)*1D(l.1q(2)):1D(l)-f.s||0;f&&f.1f&&(f.1f.1l=f)}18 i&&15.2n(i,t)?15.4p(t,n,r,i):15.6j>1&&15.1s&&r.1c>1&&U(t,15,n,15.6j,r)?(15.2n(n,t),15.4p(t,n,r,i)):u},i.1E=17(e,t,n){19 r,i,s,o,u=15.1n,a=15.1C;1a(e>=a)15.1H=15.1n=a,15.3j=15.2H.3s?15.2H.2w(1):1,15.2M||(r=!0,i="49"),0===a&&(o=15.1z,15.1i===15.1o.1C&&(e=0),(0===e||0>o||o===f)&&o!==e&&(n=!0,o>f&&(i="4i")),15.1z=o=!t||e||15.1z===e?e:f);1m 1a(1e-7>e)15.1H=15.1n=0,15.3j=15.2H.3s?15.2H.2w(0):0,(0!==u||0===a&&15.1z>0&&15.1z!==f)&&(i="4i",r=15.2M),0>e?(15.2a=!1,0===a&&(15.1z>=0&&(n=!0),15.1z=o=!t||e||15.1z===e?e:f)):15.2z||(n=!0);1m 1a(15.1H=15.1n=e,15.67){19 l=e/a,c=15.67,h=15.72;(1===c||3===c&&l>=.5)&&(l=1-l),3===c&&(l*=2),1===h?l*=l:2===h?l*=l*l:3===h?l*=l*l*l:4===h&&(l*=l*l*l*l),15.3j=1===c?1-l:2===c?l:.5>e/a?l/2:1-l/2}1m 15.3j=15.2H.2w(e/a);1a(15.1n!==u||n){1a(!15.2z){1a(15.78(),!15.2z||15.1O)18;15.1n&&!r?15.3j=15.2H.2w(15.1n/a):r&&15.2H.3s&&(15.3j=15.2H.2w(0===15.1n?0:1))}1b(15.2a||!15.1B&&15.1n!==u&&e>=0&&(15.2a=!0),0===u&&(15.29&&(e>=0?15.29.1E(e,t,n):i||(i="aE")),15.1p.4E&&(0!==15.1n||0===a)&&(t||15.1p.4E.35(15.1p.5P||15,15.1p.61||y))),s=15.1s;s;)s.f?s.t[s.p](s.c*15.3j+s.s):s.t[s.p]=s.c*15.3j+s.s,s=s.1f;15.33&&(0>e&&15.29&&15.1i&&15.29.1E(e,t,n),t||(15.1n!==u||r)&&15.33.35(15.1p.5T||15,15.1p.5U||y)),i&&(15.1O||(0>e&&15.29&&!15.33&&15.1i&&15.29.1E(e,t,n),r&&(15.1o.4H&&15.1N(!1,!1),15.2a=!1),!t&&15.1p[i]&&15.1p[i].35(15.1p[i+"5J"]||15,15.1p[i+"5Y"]||y),0===a&&15.1z===f&&o!==f&&(15.1z=0)))}},i.2n=17(e,t){1a("41"===e&&(e=1d),1d==e&&(1d==t||t===15.2x))18 15.1N(!1,!1);t="1L"!=1j t?t||15.2F||15.2x:O.3I(t)||t;19 n,r,i,s,o,u,a,f;1a((h(t)||M(t))&&"2y"!=1j t[0])1b(n=t.1c;--n>-1;)15.2n(e,t[n])&&(u=!0);1m{1a(15.2F){1b(n=15.2F.1c;--n>-1;)1a(t===15.2F[n]){o=15.3A[n]||{},15.3m=15.3m||[],r=15.3m[n]=e?15.3m[n]||{}:"41";76}}1m{1a(t!==15.2x)18!1;o=15.3A,r=15.3m=e?15.3m||{}:"41"}1a(o){a=e||o,f=e!==r&&"41"!==r&&e!==o&&("3U"!=1j e||!e.aK);1b(i 1u a)(s=o[i])&&(s.4J&&s.t.2n(a)&&(u=!0),s.4J&&0!==s.t.2B.1c||(s.1l?s.1l.1f=s.1f:s===15.1s&&(15.1s=s.1f),s.1f&&(s.1f.1l=s.1l),s.1f=s.1l=1d),3E o[i]),f&&(r[i]=1);!15.1s&&15.2z&&15.1N(!1,!1)}}18 u},i.5u=17(){18 15.4u&&O.5z("65",15),15.1s=1d,15.3m=1d,15.33=1d,15.29=1d,15.2z=15.2a=15.4u=!1,15.3A=15.2F?{}:[],15},i.1N=17(e,t){1a(o||s.31(),e&&15.1O){19 n,r=15.2F;1a(r)1b(n=r.1c;--n>-1;)15.2T[n]=R(r[n],15,!0);1m 15.2T=R(15.2x,15,!0)}18 k.1y.1N.1W(15,e,t),15.4u&&15.1s?O.5z(e?"7L":"65",15):!1},O.3Q=17(e,t,n){18 1h O(e,t,n)},O.66=17(e,t,n){18 n.4w=!0,n.1Y=0!=n.1Y,1h O(e,t,n)},O.6b=17(e,t,n,r){18 r.30=n,r.1Y=0!=r.1Y&&0!=n.1Y,1h O(e,t,r)},O.6o=17(e,t,n,r,i){18 1h O(t,0,{4x:e,49:t,6a:n,6s:r,4i:t,7J:n,7M:r,1Y:!1,5K:i,4I:0})},O.4h=17(e,t){18 1h O(e,0,t)},O.40=17(e,t){1a(1d==e)18[];e="1L"!=1j e?e:O.3I(e)||e;19 n,r,i,s;1a((h(e)||M(e))&&"2y"!=1j e[0]){1b(n=e.1c,r=[];--n>-1;)r=r.43(O.40(e[n],t));1b(n=r.1c;--n>-1;)1b(s=r[n],i=n;--i>-1;)s===r[i]&&r.2Q(n,1)}1m 1b(r=R(e).43(),n=r.1c;--n>-1;)(r[n].1O||t&&!r[n].4n())&&r.2Q(n,1);18 r},O.9u=O.aI=17(e,t,n){"3U"==1j t&&(n=t,t=!1);1b(19 r=O.40(e,t),i=r.1c;--i>-1;)r[i].2n(n,e)};19 W=m("4S.80",17(e,t){15.2B=(e||"").1t(","),15.4D=15.2B[0],15.5W=t||0,15.aF=W.1y},!0);1a(i=W.1y,W.3V="1.10.1",W.3w=2,i.1s=1d,i.aG=17(e,t,n,r,i,s){19 o,u;18 1d!=r&&(o="2y"==1j r||"="!==r.1v(1)?1D(r)-n:3v(r.1v(0)+"1",10)*1D(r.1q(2)))?(15.1s=u={1f:15.1s,t:e,p:t,s:n,c:o,f:"17"==1j e[t],n:i||t,r:s},u.1f&&(u.1f.1l=u),u):2h 0},i.1S=17(e){1b(19 t,n=15.1s,r=1e-6;n;)t=n.c*e+n.s,n.r?t=1g.3G(t):r>t&&t>-r&&(t=0),n.f?n.t[n.p](t):n.t[n.p]=t,n=n.1f},i.2n=17(e){19 t,n=15.2B,r=15.1s;1a(1d!=e[15.4D])15.2B=[];1m 1b(t=n.1c;--t>-1;)1d!=e[n[t]]&&n.2Q(t,1);1b(;r;)1d!=e[r.n]&&(r.1f&&(r.1f.1l=r.1l),r.1l?(r.1l.1f=r.1f,r.1l=1d):15.1s===r&&(15.1s=r.1f)),r=r.1f;18!1},i.7u=17(e,t){1b(19 n=15.1s;n;)(e[15.4D]||1d!=n.n&&e[n.n.1t(15.4D+"2f").1J("")])&&(n.r=t),n=n.1f},O.5z=17(e,t){19 n,r,i,s,o,u=t.1s;1a("5D"===e){1b(;u;){1b(o=u.1f,r=i;r&&r.2s>u.2s;)r=r.1f;(u.1l=r?r.1l:s)?u.1l.1f=u:i=u,(u.1f=r)?r.1l=u:s=u,u=o}u=t.1s=i}1b(;u;)u.4J&&"17"==1j u.t[e]&&u.t[e]()&&(n=!0),u=u.1f;18 n},W.6Q=17(e){1b(19 t=e.1c;--t>-1;)e[t].3w===W.3w&&(P[(1h e[t]).4D]=e[t]);18!0},v.2D=17(e){1a(!(e&&e.7m&&e.7o&&e.3w))6l"aH 2D au.";19 t,n=e.7m,r=e.5E||0,i=e.as,s={7o:"6V",4h:"1S",4f:"2n",3G:"7u",af:"5D"},o=m("4S."+n.1v(0).4Z()+n.1q(1)+"8u",17(){W.1W(15,n,r),15.2B=i||[]},e.ag===!0),u=o.1y=1h W(n);u.2K=o,o.3w=e.3w;1b(t 1u s)"17"==1j e[t]&&(u[s[t]]=e[t]);18 o.3V=e.3V,W.6Q([o]),o},n=e.37){1b(r=0;n.1c>r;r++)n[r]();1b(i 1u p)p[i].7A||e.6x.7z("ah ae ad aa: 4Q.4R."+i)}o=!1}})(1Q);(1Q.37||(1Q.37=[])).2d(17(){"4V 4U";1Q.3u("ab",["5C.8H","5C.7y","5f"],17(e,t,n){19 r=17(e){t.1W(15,e),15.2J={},15.4H=15.1p.4H===!0,15.2j=15.1p.2j===!0,15.4K=!0,15.33=15.1p.5A;19 n,r,i=15.1p;1b(r 1u i)n=i[r],o(n)&&-1!==n.1J("").1k("{4B}")&&(i[r]=15.5l(n));o(i.3J)&&15.1U(i.3J,0,i.ac,i.ai)},i=1e-10,s=n.4s.7p,o=n.4s.7E,u=[],a=1Q.3u.77,f=17(e){19 t,n={};1b(t 1u e)n[t]=e[t];18 n},l=17(e,t,n,r){e.1o.5N(e.1i),t&&t.35(r||e.1o,n||u)},c=u.6K,h=r.1y=1h t;18 r.3V="1.11.8",h.2K=r,h.4f().1O=!1,h.3Q=17(e,t,r,i){19 s=r.5j&&a.69||n;18 t?15.1U(1h s(e,t,r),i):15.4h(e,r,i)},h.66=17(e,t,r,i){18 15.1U((r.5j&&a.69||n).66(e,t,r),i)},h.6b=17(e,t,r,i,s){19 o=i.5j&&a.69||n;18 t?15.1U(o.6b(e,t,r,i),s):15.4h(e,i,s)},h.6u=17(e,t,i,o,u,a,l,h){19 p,d=1h r({49:a,6a:l,6s:h,2j:15.2j});1b("1L"==1j e&&(e=n.3I(e)||e),s(e)&&(e=c.1W(e,0)),o=o||0,p=0;e.1c>p;p++)i.30&&(i.30=f(i.30)),d.3Q(e[p],t,f(i),p*o);18 15.1U(d,u)},h.ao=17(e,t,n,r,i,s,o,u){18 n.1Y=0!=n.1Y,n.4w=!0,15.6u(e,t,n,r,i,s,o,u)},h.an=17(e,t,n,r,i,s,o,u,a){18 r.30=n,r.1Y=0!=r.1Y&&0!=n.1Y,15.6u(e,t,r,i,s,o,u,a)},h.1W=17(e,t,r,i){18 15.1U(n.6o(0,e,t,r),i)},h.4h=17(e,t,r){18 r=15.3h(r,0,!0),1d==t.1Y&&(t.1Y=r===15.1n&&!15.1B),15.1U(1h n(e,0,t),r)},r.ak=17(e,t){e=e||{},1d==e.2j&&(e.2j=!0);19 i,s,o=1h r(e),u=o.1o;1b(1d==t&&(t=!0),u.3K(o,!0),o.1i=0,o.1z=o.1n=o.1H=u.1n,i=u.26;i;)s=i.1f,t&&i 2b n&&i.2x===i.1p.49||o.1U(i,i.1i-i.2L),i=s;18 u.1U(o,0),o},h.1U=17(i,s,u,a){19 f,l,c,h,p,d;1a("2y"!=1j s&&(s=15.3h(s,0,!0,i)),!(i 2b e)){1a(i 2b 3N||i&&i.2d&&o(i)){1b(u=u||"al",a=a||0,f=s,l=i.1c,c=0;l>c;c++)o(h=i[c])&&(h=1h r({3J:h})),15.1U(h,f),"1L"!=1j h&&"17"!=1j h&&("am"===u?f=h.1i+h.23()/h.1x:"aN"===u&&(h.1i-=h.4x())),f+=a;18 15.3c(!0)}1a("1L"==1j i)18 15.7x(i,s);1a("17"!=1j i)6l"7e 1U "+i+" 93 8Y 21; 3D 94 8K a 6R, 21, 17, 8X 1L.";i=n.6o(0,i)}1a(t.1y.1U.1W(15,i,s),(15.1O||15.1n===15.1C)&&!15.1B&&15.1C<15.2C())1b(p=15,d=p.3H()>i.1i;p.1o;)d&&p.1o.2j?p.2Z(p.1H,!0):p.1O&&p.1N(!0,!1),p=p.1o;18 15},h.5n=17(t){1a(t 2b e)18 15.3K(t,!1);1a(t 2b 3N||t&&t.2d&&o(t)){1b(19 n=t.1c;--n>-1;)15.5n(t[n]);18 15}18"1L"==1j t?15.7n(t):15.4f(1d,t)},h.3K=17(e,n){t.1y.3K.1W(15,e,n);19 r=15.3l;18 r?15.1n>r.1i+r.2i/r.1x&&(15.1n=15.2C(),15.1H=15.2i):15.1n=15.1H=15.1C=15.2i=0,15},h.92=17(e,t){18 15.1U(e,15.3h(1d,t,!0,e))},h.7w=h.9t=17(e,t,n,r){18 15.1U(e,t||0,n,r)},h.9m=17(e,t,n,r){18 15.1U(e,15.3h(1d,t,!0,e),n,r)},h.7x=17(e,t){18 15.2J[e]=15.3h(t),15},h.9l=17(e,t,n,r){18 15.1W(l,["{4B}",t,n,r],15,e)},h.7n=17(e){18 3E 15.2J[e],15},h.9j=17(e){18 1d!=15.2J[e]?15.2J[e]:-1},h.3h=17(t,n,r,i){19 s;1a(i 2b e&&i.21===15)15.5n(i);1m 1a(i&&(i 2b 3N||i.2d&&o(i)))1b(s=i.1c;--s>-1;)i[s]2b e&&i[s].21===15&&15.5n(i[s]);1a("1L"==1j n)18 15.3h(n,r&&"2y"==1j t&&1d==15.2J[n]?t-15.2C():0,r);1a(n=n||0,"1L"!=1j t||!6d(t)&&1d==15.2J[t])1d==t&&(t=15.2C());1m{1a(s=t.1k("="),-1===s)18 1d==15.2J[t]?r?15.2J[t]=15.2C()+n:n:15.2J[t]+n;n=3v(t.1v(s-1)+"1",10)*1D(t.1q(s+1)),t=s>1?15.3h(t.1q(0,s-1),0,r):15.2C()}18 1D(t)+n},h.4b=17(e,t){18 15.2Z("2y"==1j e?e:15.3h(e),t!==!1)},h.9n=17(){18 15.3e(!0)},h.9o=17(e,t){18 15.73(e,t)},h.9s=17(e,t){18 15.5N(e,t)},h.1E=17(e,t,n){15.1O&&15.1N(!0,!1);19 r,s,o,a,f,l=15.2r?15.23():15.2i,c=15.1n,h=15.1i,p=15.1x,d=15.1B;1a(e>=l?(15.1H=15.1n=l,15.2M||15.5I()||(s=!0,a="49",0===15.1C&&(0===e||0>15.1z||15.1z===i)&&15.1z!==e&&15.26&&(f=!0,15.1z>i&&(a="4i"))),15.1z=15.1C||!t||e||15.1z===e?e:i,e=l+1e-4):1e-7>e?(15.1H=15.1n=0,(0!==c||0===15.1C&&15.1z!==i&&(15.1z>0||0>e&&15.1z>=0))&&(a="4i",s=15.2M),0>e?(15.2a=!1,0===15.1C&&15.1z>=0&&15.26&&(f=!0),15.1z=e):(15.1z=15.1C||!t||e||15.1z===e?e:i,e=0,15.2z||(f=!0))):15.1H=15.1n=15.1z=e,15.1n!==c&&15.26||n||f){1a(15.2z||(15.2z=!0),15.2a||!15.1B&&15.1n!==c&&e>0&&(15.2a=!0),0===c&&15.1p.4E&&0!==15.1n&&(t||15.1p.4E.35(15.1p.5P||15,15.1p.61||u)),15.1n>=c)1b(r=15.26;r&&(o=r.1f,!15.1B||d);)(r.2a||r.1i<=15.1n&&!r.1B&&!r.1O)&&(r.2M?r.1E((r.2r?r.23():r.2i)-(e-r.1i)*r.1x,t,n):r.1E((e-r.1i)*r.1x,t,n)),r=o;1m 1b(r=15.3l;r&&(o=r.1l,!15.1B||d);)(r.2a||c>=r.1i&&!r.1B&&!r.1O)&&(r.2M?r.1E((r.2r?r.23():r.2i)-(e-r.1i)*r.1x,t,n):r.1E((e-r.1i)*r.1x,t,n)),r=o;15.33&&(t||15.33.35(15.1p.5T||15,15.1p.5U||u)),a&&(15.1O||(h===15.1i||p!==15.1x)&&(0===15.1n||l>=15.23())&&(s&&(15.1o.4H&&15.1N(!1,!1),15.2a=!1),!t&&15.1p[a]&&15.1p[a].35(15.1p[a+"5J"]||15,15.1p[a+"5Y"]||u)))}},h.5I=17(){1b(19 e=15.26;e;){1a(e.1B||e 2b r&&e.5I())18!0;e=e.1f}18!1},h.5s=17(e,t,r,i){i=i||-59;1b(19 s=[],o=15.26,u=0;o;)i>o.1i||(o 2b n?t!==!1&&(s[u++]=o):(r!==!1&&(s[u++]=o),e!==!1&&(s=s.43(o.5s(!0,t,r)),u=s.1c))),o=o.1f;18 s},h.40=17(e,t){1b(19 r=n.40(e),i=r.1c,s=[],o=0;--i>-1;)(r[i].21===15||t&&15.8B(r[i]))&&(s[o++]=r[i]);18 s},h.8B=17(e){1b(19 t=e.21;t;){1a(t===15)18!0;t=t.21}18!1},h.8p=17(e,t,n){n=n||0;1b(19 r,i=15.26,s=15.2J;i;)i.1i>=n&&(i.1i+=e),i=i.1f;1a(t)1b(r 1u s)s[r]>=n&&(s[r]+=e);18 15.3c(!0)},h.2n=17(e,t){1a(!e&&!t)18 15.1N(!1,!1);1b(19 n=t?15.40(t):15.5s(!0,!0,!1),r=n.1c,i=!1;--r>-1;)n[r].2n(e,t)&&(i=!0);18 i},h.9a=17(e){19 t=15.5s(!1,!0,!0),n=t.1c;1b(15.1n=15.1H=0;--n>-1;)t[n].1N(!1,!1);18 e!==!1&&(15.2J={}),15.3c(!0)},h.5u=17(){1b(19 e=15.26;e;)e.5u(),e=e.1f;18 15},h.1N=17(e,n){1a(e===15.1O)1b(19 r=15.26;r;)r.1N(e,!0),r=r.1f;18 t.1y.1N.1W(15,e,n)},h.2C=17(e){18 2m.1c?(0!==15.2C()&&0!==e&&15.6q(15.1C/e),15):(15.2r&&15.23(),15.1C)},h.23=17(e){1a(!2m.1c){1a(15.2r){1b(19 t,n,r=0,i=15.3l,s=9b;i;)t=i.1l,i.2r&&i.23(),i.1i>s&&15.4K&&!i.1B?15.1U(i,i.1i-i.2L):s=i.1i,0>i.1i&&!i.1B&&(r-=i.1i,15.1o.2j&&(15.1i+=i.1i/15.1x),15.8p(-i.1i,!1,-59),s=0),n=i.1i+i.2i/i.1x,n>r&&(r=n),i=t;15.1C=15.2i=r,15.2r=!1}18 15.2i}18 0!==15.23()&&0!==e&&15.6q(15.2i/e),15},h.9d=17(){1b(19 t=15.1o;t.1o;)t=t.1o;18 t===e.7P},h.3H=17(){18 15.1B?15.1H:(15.1o.3H()-15.1i)*15.1x},r},!0)}),1Q.3u&&1Q.37.4q()();(1Q.37||(1Q.37=[])).2d(17(){"4V 4U";1Q.3u("2o.7Y",["2o.8h"],17(e){19 t,n,r,i=1Q.5Z||1Q,s=i.4Q.4R,o=2*1g.4o,u=1g.4o/2,a=s.8k,f=17(t,n){19 r=a("2o."+t,17(){},!0),i=r.1y=1h e;18 i.2K=r,i.2w=n,r},l=e.8l||17(){},c=17(e,t,n,r){19 i=a("2o."+e,{6c:1h t,5k:1h n,5m:1h r},!0);18 l(i,e),i},h=17(e,t,n){15.t=e,15.v=t,n&&(15.5w=n,n.5y=15,15.c=n.v-t,15.8v=n.t-e)},p=17(t,n){19 r=a("2o."+t,17(e){15.1T=e||0===e?e:1.aO,15.2q=1.cW*15.1T},!0),i=r.1y=1h e;18 i.2K=r,i.2w=n,i.3o=17(e){18 1h r(e)},r},d=c("7Y",p("cv",17(e){18(e-=1)*e*((15.1T+1)*e+15.1T)+1}),p("cr",17(e){18 e*e*((15.1T+1)*e-15.1T)}),p("co",17(e){18 1>(e*=2)?.5*e*e*((15.2q+1)*e-15.2q):.5*((e-=2)*e*((15.2q+1)*e+15.2q)+2)})),v=a("2o.6w",17(e,t,n){t=t||0===t?t:.7,1d==e?e=.7:e>1&&(e=1),15.89=1!==e?t:0,15.1T=(1-e)/2,15.2q=e,15.3f=15.1T+15.2q,15.3s=n===!0},!0),m=v.1y=1h e;18 m.2K=v,m.2w=17(e){19 t=e+(.5-e)*15.89;18 15.1T>e?15.3s?1-(e=1-e/15.1T)*e:t-(e=1-e/15.1T)*e*e*e*t:e>15.3f?15.3s?1-(e=(e-15.3f)/15.1T)*e:t+(e-t)*(e=(e-15.3f)/15.1T)*e*e*e:15.3s?1:t},v.3z=1h v(.7,.7),m.3o=v.3o=17(e,t,n){18 1h v(e,t,n)},t=a("2o.87",17(e){e=e||1,15.1T=1/e,15.2q=e+1},!0),m=t.1y=1h e,m.2K=t,m.2w=17(e){18 0>e?e=0:e>=1&&(e=.cG),(15.2q*e>>0)*15.1T},m.3o=t.3o=17(e){18 1h t(e)},n=a("2o.7f",17(t){t=t||{};1b(19 n,r,i,s,o,u,a=t.cE||"3t",f=[],l=0,c=0|(t.cI||20),p=c,d=t.cA!==!1,v=t.cJ===!0,m=t.8N 2b e?t.8N:1d,g="2y"==1j t.8R?.4*t.8R:.4;--p>-1;)n=d?1g.8A():1/c*p,r=m?m.2w(n):n,"3t"===a?i=g:"cK"===a?(s=1-n,i=s*s*g):"1u"===a?i=n*n*g:.5>n?(s=2*n,i=.5*s*s*g):(s=2*(1-n),i=.5*s*s*g),d?r+=1g.8A()*i-.5*i:p%2?r+=.5*i:r-=.5*i,v&&(r>1?r=1:0>r&&(r=0)),f[l++]={x:n,y:r};1b(f.cL(17(e,t){18 e.x-t.x}),u=1h h(1,1,1d),p=c;--p>-1;)o=f[p],u=1h h(o.x,o.y,u);15.1l=1h h(0,0,0!==u.t?u:u.5w)},!0),m=n.1y=1h e,m.2K=n,m.2w=17(e){19 t=15.1l;1a(e>t.t){1b(;t.5w&&e>=t.t;)t=t.5w;t=t.5y}1m 1b(;t.5y&&t.t>=e;)t=t.5y;18 15.1l=t,t.v+(e-t.t)/t.8v*t.c},m.3o=17(e){18 1h n(e)},n.3z=1h n,c("cH",f("cF",17(e){18 1/2.75>e?7.2I*e*e:2/2.75>e?7.2I*(e-=1.5/2.75)*e+.75:2.5/2.75>e?7.2I*(e-=2.25/2.75)*e+.6m:7.2I*(e-=2.6n/2.75)*e+.6p}),f("cM",17(e){18 1/2.75>(e=1-e)?1-7.2I*e*e:2/2.75>e?1-(7.2I*(e-=1.5/2.75)*e+.75):2.5/2.75>e?1-(7.2I*(e-=2.25/2.75)*e+.6m):1-(7.2I*(e-=2.6n/2.75)*e+.6p)}),f("cN",17(e){19 t=.5>e;18 e=t?1-2*e:2*e-1,e=1/2.75>e?7.2I*e*e:2/2.75>e?7.2I*(e-=1.5/2.75)*e+.75:2.5/2.75>e?7.2I*(e-=2.25/2.75)*e+.6m:7.2I*(e-=2.6n/2.75)*e+.6p,t?.5*(1-e):.5*e+.5})),c("cO",f("cP",17(e){18 1g.38(1-(e-=1)*e)}),f("cQ",17(e){18-(1g.38(1-e*e)-1)}),f("cD",17(e){18 1>(e*=2)?-.5*(1g.38(1-e*e)-1):.5*(1g.38(1-(e-=2)*e)+1)})),r=17(t,n,r){19 i=a("2o."+t,17(e,t){15.1T=e||1,15.2q=t||r,15.3f=15.2q/o*(1g.cp(1/15.1T)||0)},!0),s=i.1y=1h e;18 s.2K=i,s.2w=n,s.3o=17(e,t){18 1h i(e,t)},i},c("cn",r("cm",17(e){18 15.1T*1g.3p(2,-10*e)*1g.2k((e-15.3f)*o/15.2q)+1},.3),r("cj",17(e){18-(15.1T*1g.3p(2,10*(e-=1))*1g.2k((e-15.3f)*o/15.2q))},.3),r("ck",17(e){18 1>(e*=2)?-.5*15.1T*1g.3p(2,10*(e-=1))*1g.2k((e-15.3f)*o/15.2q):.5*15.1T*1g.3p(2,-10*(e-=1))*1g.2k((e-15.3f)*o/15.2q)+1},.45)),c("cl",f("cs",17(e){18 1-1g.3p(2,-10*e)}),f("cz",17(e){18 1g.3p(2,10*(e-1))-.cX}),f("cB",17(e){18 1>(e*=2)?.5*1g.3p(2,10*(e-1)):.5*(2-1g.3p(2,-10*(e-1)))})),c("cy",f("cx",17(e){18 1g.2k(e*u)}),f("cu",17(e){18-1g.2E(e*u)+1}),f("cw",17(e){18-.5*(1g.2E(1g.4o*e)-1)})),a("2o.dg",{df:17(t){18 e.7C[t]}},!0),l(i.6w,"6w","3z,"),l(n,"7f","3z,"),l(t,"87","3z,"),d},!0)}),1Q.3u&&1Q.37.4q()();(1Q.37||(1Q.37=[])).2d(17(){"4V 4U";1Q.3u("4S.dl",["4S.80","5f"],17(e,t){19 n,r,i,s,o=17(){e.1W(15,"4P"),15.2B.1c=0,15.1S=o.1y.1S},u={},a=o.1y=1h e("4P");a.2K=o,o.3V="1.11.8",o.3w=2,o.81=0,o.8w="cY",a="2c",o.6E={4z:a,7G:a,7F:a,4A:a,3k:a,3y:a,dk:a,6B:a,6U:a,3b:a,dd:""};19 f,l,c,h,p,d,v=/(?:\\d|\\-\\d|\\.\\d|\\-\\.\\d)+/g,m=/(?:\\d|\\-\\d|\\.\\d|\\-\\.\\d|\\+=\\d|\\-=\\d|\\+=.\\d|\\-=\\.\\d)+/g,g=/(?:\\+=|\\-=|\\-|\\b)[\\d\\-\\.]+[a-d7-dn-9]*(?:%|\\b)/3M,y=/[^\\d\\-\\.]/g,b=/(?:\\d|\\-|\\+|=|#|\\.)*/g,w=/1Z *= *([^)]*)/,E=/1Z:([^;]*)/,S=/3B\\(1Z *=.+?\\)/i,x=/^(5h|6y)/,T=/([A-Z])/g,N=/-([a-z])/3M,C=/(^(?:8t\\(\\"|8t\\())|(?:(\\"\\))$|\\)$)/3M,k=17(e,t){18 t.4Z()},L=/(?:6i|7T|7H)/i,A=/(8c|8d|8n|8m)=[\\d\\-\\.e]+/3M,O=/82\\:5Q\\.5S\\.5V\\(.+?\\)/i,M=/,(?=[^\\)]*(?:\\(|$))/3M,2f=1g.4o/36,D=36/1g.4o,P={},H=6h,B=H.6e("74"),j=H.6e("d2"),F=o.4s={dm:u},I=dc.cC,q=17(){19 e,t=I.1k("ch"),n=H.6e("74");18 c=-1!==I.1k("bj")&&-1===I.1k("bk")&&(-1===t||1D(I.1q(t+8,1))>3),p=c&&6>1D(I.1q(I.1k("bl/")+8,1)),h=-1!==I.1k("bh"),/bg ([0-9]{1,}[\\.0-9]{0,})/.bc(I)&&(d=1r(46.$1)),n.bd="<a 1w=\'4z:be;1Z:.55;\'>a</a>",e=n.bf("a")[0],e?/^0.55/.2P(e.1w.1Z):!1}(),R=17(e){18 w.2P("1L"==1j e?e:(e.2X?e.2X.2l:e.1w.2l)||"")?1r(46.$1)/22:1},U=17(e){1Q.6x&&6x.7z(e)},z="",W="",X=17(e,t){t=t||B;19 n,r,i=t.1w;1a(2h 0!==i[e])18 e;1b(e=e.1v(0).4Z()+e.1q(1),n=["O","bn","5t","bv","bw"],r=5;--r>-1&&2h 0===i[n[r]+e];);18 r>=0?(W=3===r?"5t":n[r],z="-"+W.6P()+"-",W+e):1d},V=H.7v?H.7v.bx:17(){},$=o.bu=17(e,t,n,r,i){19 s;18 q||"1Z"!==t?(!r&&e.1w[t]?s=e.1w[t]:(n=n||V(e,1d))?s=n[t]||n.42(t)||n.42(t.1I(T,"-$1").6P()):e.2X&&(s=e.2X[t]),1d==i||s&&"3t"!==s&&"2t"!==s&&"2t 2t"!==s?s:i):R(e)},J=F.bs=17(e,n,r,i,s){1a("2c"===i||!i)18 r;1a("2t"===i||!r)18 0;19 u,a,f,l=L.2P(n),c=e,h=B.1w,p=0>r;1a(p&&(r=-r),"%"===i&&-1!==n.1k("3X"))u=r/22*(l?e.bo:e.bp);1m{1a(h.3d="3X:0 5x 7i;4m:"+$(e,"4m")+";bq-3y:0;","%"!==i&&c.7r)h[l?"84":"6X"]=r+i;1m{1a(c=e.ci||H.br,a=c.6g,f=t.6f.3F,a&&l&&a.34===f)18 a.3k*r/22;h[l?"3k":"3y"]=r+i}c.7r(B),u=1r(B[l?"4N":"4M"]),c.aV(B),l&&"%"===i&&o.aW!==!1&&(a=c.6g=c.6g||{},a.34=f,a.3k=22*(u/r)),0!==u||s||(u=J(e,n,r,i,!0))}18 p?-u:u},K=F.aX=17(e,t,n){1a("83"!==$(e,"4m",n))18 0;19 r="4A"===t?"6i":"7N",i=$(e,"6U"+r,n);18 e["aY"+r]-(J(e,t,1r(i),i.1I(b,""))||0)},Q=17(e,t){19 n,r,i={};1a(t=t||V(e,1d))1a(n=t.1c)1b(;--n>-1;)i[t[n].1I(N,k)]=t.42(t[n]);1m 1b(n 1u t)i[n]=t[n];1m 1a(t=e.2X||e.1w)1b(n 1u t)"1L"==1j n&&2h 0===i[n]&&(i[n.1I(N,k)]=t[n]);18 q||(i.1Z=R(e)),r=4l(e,t,!1),i.1K=r.1K,i.1R=r.1R,i.28=r.28,i.2g=r.2g,i.x=r.x,i.y=r.y,39&&(i.z=r.z,i.1P=r.1P,i.1V=r.1V,i.2W=r.2W),i.7R&&3E i.7R,i},G=17(e,t,n,r,i){19 s,o,u,a={},f=e.1w;1b(o 1u n)"3d"!==o&&"1c"!==o&&6d(o)&&(t[o]!==(s=n[o])||i&&i[o])&&-1===o.1k("aU")&&("2y"==1j s||"1L"==1j s)&&(a[o]="2t"!==s||"4A"!==o&&"4z"!==o?""!==s&&"2t"!==s&&"3t"!==s||"1L"!=1j t[o]||""===t[o].1I(y,"")?s:0:K(e,o),2h 0!==f[o]&&(u=1h ct(f,o,f[o],u)));1a(r)1b(o 1u r)"3a"!==o&&(a[o]=r[o]);18{57:a,4e:u}},Y={3k:["6i","7T"],3y:["7N","aP"]},Z=["7K","7S","7B","7U"],3L=17(e,t,n){19 r=1r("3k"===t?e.4N:e.4M),i=Y[t],s=i.1c;1b(n=n||V(e,1d);--s>-1;)r-=1r($(e,"6B"+i[s],n,!0))||0,r-=1r($(e,"3X"+i[s]+"7H",n,!0))||0;18 r},3g=17(e,t){(1d==e||""===e||"2t"===e||"2t 2t"===e)&&(e="0 0");19 n=e.1t(" "),r=-1!==e.1k("4A")?"0%":-1!==e.1k("7G")?"22%":n[0],i=-1!==e.1k("4z")?"0%":-1!==e.1k("7F")?"22%":n[1];18 1d==i?i="0":"7l"===i&&(i="50%"),("7l"===r||6d(1r(r))&&-1===(r+"").1k("="))&&(r="50%"),t&&(t.8o=-1!==r.1k("%"),t.8r=-1!==i.1k("%"),t.aQ="="===r.1v(1),t.aR="="===i.1v(1),t.52=1r(r.1I(y,"")),t.5R=1r(i.1I(y,""))),r+" "+i+(n.1c>2?" "+n[2]:"")},3q=17(e,t){18"1L"==1j e&&"="===e.1v(1)?3v(e.1v(0)+"1",10)*1r(e.1q(2)):1r(e)-1r(t)},2A=17(e,t){18 1d==e?t:"1L"==1j e&&"="===e.1v(1)?3v(e.1v(0)+"1",10)*1D(e.1q(2))+t:1r(e)},3D=17(e,t,n,r){19 i,s,o,u,a=1e-6;18 1d==e?u=t:"2y"==1j e?u=e:(i=68,s=e.1t("2f"),o=1D(s[0].1I(y,""))*(-1===e.1k("aS")?1:D)-("="===e.1v(1)?0:t),s.1c&&(r&&(r[n]=t+o),-1!==e.1k("aZ")&&(o%=i,o!==o%(i/2)&&(o=0>o?o+i:o-i)),-1!==e.1k("b0")&&0>o?o=(o+59*i)%i-(0|o/i)*i:-1!==e.1k("b7")&&o>0&&(o=(o-59*i)%i-(0|o/i)*i)),u=t+o),a>u&&u>-a&&(u=0),u},3x={b9:[0,1F,1F],b6:[0,1F,0],b5:[58,58,58],7I:[0,0,0],b1:[2N,0,0],b2:[0,2N,2N],b3:[0,0,1F],b4:[0,0,2N],by:[1F,1F,1F],bz:[1F,0,1F],c3:[2N,2N,0],c4:[1F,1F,0],c5:[1F,c2,0],c1:[2N,2N,2N],bX:[2N,0,2N],bY:[0,2N,0],7i:[1F,0,0],c0:[1F,58,c7],ce:[0,1F,1F],4g:[1F,1F,1F,0]},5a=17(e,t,n){18 e=0>e?e+1:e>1?e-1:e,0|1F*(1>6*e?t+6*(n-t)*e:.5>e?n:2>3*e?t+6*(n-t)*(2/3-e):t)+.5},5i=17(e){19 t,n,r,i,s,o;18 e&&""!==e?"2y"==1j e?[e>>16,1F&e>>8,1F&e]:(","===e.1v(e.1c-1)&&(e=e.1q(0,e.1c-1)),3x[e]?3x[e]:"#"===e.1v(0)?(4===e.1c&&(t=e.1v(1),n=e.1v(2),r=e.1v(3),e="#"+t+t+n+n+r+r),e=3v(e.1q(1),16),[e>>16,1F&e>>8,1F&e]):"6y"===e.1q(0,3)?(e=e.2G(v),i=1D(e[0])%68/68,s=1D(e[1])/22,o=1D(e[2])/22,n=.5>=o?o*(s+1):o+s-o*s,t=2*o-n,e.1c>3&&(e[3]=1D(e[3])),e[0]=5a(i+1/3,t,n),e[1]=5a(i,t,n),e[2]=5a(i-1/3,t,n),e):(e=e.2G(v)||3x.4g,e[0]=1D(e[0]),e[1]=1D(e[1]),e[2]=1D(e[2]),e.1c>3&&(e[3]=1D(e[3])),e)):3x.7I},at="(?:\\\\b(?:(?:5h|6Z|6y|cc)\\\\(.+?\\\\))|\\\\B#.+?\\\\b";1b(a 1u 3x)at+="|"+a+"\\\\b";at=46(at+")","3M");19 6H=17(e,t,n,r){1a(1d==e)18 17(e){18 e};19 i,s=t?(e.2G(at)||[""])[0]:"",o=e.1t(s).1J("").2G(g)||[],u=e.1q(0,e.1k(o[0])),a=")"===e.1v(e.1c-1)?")":"",f=-1!==e.1k(" ")?" ":",",l=o.1c,c=l>0?o[0].1I(v,""):"";18 l?i=t?17(e){19 t,h,p,d;1a("2y"==1j e)e+=c;1m 1a(r&&M.2P(e)){1b(d=e.1I(M,"|").1t("|"),p=0;d.1c>p;p++)d[p]=i(d[p]);18 d.1J(",")}1a(t=(e.2G(at)||[s])[0],h=e.1t(t).1J("").2G(g)||[],p=h.1c,l>p--)1b(;l>++p;)h[p]=n?h[0|(p-1)/2]:o[p];18 u+h.1J(f)+f+t+a+(-1!==e.1k("6Y")?" 6Y":"")}:17(e){19 t,s,h;1a("2y"==1j e)e+=c;1m 1a(r&&M.2P(e)){1b(s=e.1I(M,"|").1t("|"),h=0;s.1c>h;h++)s[h]=i(s[h]);18 s.1J(",")}1a(t=e.2G(g)||[],h=t.1c,l>h--)1b(;l>++h;)t[h]=n?t[0|(h-1)/2]:o[h];18 u+t.1J(f)+a}:17(e){18 e}},5g=17(e){18 e=e.1t(","),17(t,n,r,i,s,o,u){19 a,f=(n+"").1t(" ");1b(u={},a=0;4>a;a++)u[e[a]]=f[a]=f[a]||f[(a-1)/2>>0];18 i.2O(t,u,s,o)}},ct=(F.c8=17(e){15.2D.1S(e);1b(19 t,n,r,i,s=15.1A,o=s.7W,u=s.4e,a=1e-6;u;)t=o[u.v],u.r?t=1g.3G(t):a>t&&t>-a&&(t=0),u.t[u.p]=t,u=u.1f;1a(s.8F&&(s.8F.1K=o.1K),1===e)1b(u=s.4e;u;){1a(n=u.t,n.2p){1a(1===n.2p){1b(i=n.1X+n.s+n.4a,r=1;n.l>r;r++)i+=n["3i"+r]+n["2R"+(r+1)];n.e=i}}1m n.e=n.s+n.1X;u=u.1f}},17(e,t,n,r,i){15.t=e,15.p=t,15.v=n,15.r=i,r&&(r.1l=15,15.1f=r)}),27=(F.c9=17(e,t,n,r,i,s){19 o,u,a,f,l,c=r,h={},p={},d=n.3r,v=P;1b(n.3r=1d,P=t,r=l=n.2O(e,t,r,i),P=v,s&&(n.3r=d,c&&(c.1l=1d,c.1l&&(c.1l.1f=1d)));r&&r!==c;){1a(1>=r.2p&&(u=r.p,p[u]=r.s+r.c,h[u]=r.s,s||(f=1h ct(r,"s",u,f,r.r),r.c=0),1===r.2p))1b(o=r.l;--o>0;)a="3i"+o,u=r.p+"2f"+a,p[u]=r.1A[a],h[u]=r[a],s||(f=1h ct(r,a,u,f,r.56[a]));r=r.1f}18{7W:h,ca:p,4e:f,4j:l}},F.cb=17(e,t,r,i,o,u,a,f,l,c,h){15.t=e,15.p=t,15.s=r,15.c=i,15.n=a||t,e 2b 27||s.2d(15.n),15.r=f,15.2p=u||0,l&&(15.2s=l,n=!0),15.b=2h 0===c?r:c,15.e=2h 0===h?r+i:h,o&&(15.1f=o,o.1l=15)}),4j=o.3Y=17(e,t,n,r,i,s,o,u,a,l){n=n||s||"",o=1h 27(e,t,0,0,o,l?2:1,1d,!1,u,n,r),r+="";19 c,h,p,d,g,y,b,w,E,S,T,N,C=n.1t(", ").1J(",").1t(" "),k=r.1t(", ").1J(",").1t(" "),L=C.1c,A=f!==!1;1b((-1!==r.1k(",")||-1!==n.1k(","))&&(C=C.1J(" ").1I(M,", ").1t(" "),k=k.1J(" ").1I(M,", ").1t(" "),L=C.1c),L!==k.1c&&(C=(s||"").1t(" "),L=C.1c),o.2D=a,o.1S=l,c=0;L>c;c++)1a(d=C[c],g=k[c],w=1r(d),w||0===w)o.3S("",w,3q(g,w),g.1I(m,""),A&&-1!==g.1k("2c"),!0);1m 1a(i&&("#"===d.1v(0)||3x[d]||x.2P(d)))N=","===g.1v(g.1c-1)?"),":")",d=5i(d),g=5i(g),E=d.1c+g.1c>6,E&&!q&&0===g[3]?(o["2R"+o.l]+=o.l?" 4g":"4g",o.e=o.e.1t(k[c]).1J("4g")):(q||(E=!1),o.3S(E?"6Z(":"5h(",d[0],g[0]-d[0],",",!0,!0).3S("",d[1],g[1]-d[1],",",!0).3S("",d[2],g[2]-d[2],E?",":N,!0),E&&(d=4>d.1c?1:d[3],o.3S("",d,(4>g.1c?1:g[3])-d,N,!1)));1m 1a(y=d.2G(v)){1a(b=g.2G(m),!b||b.1c!==y.1c)18 o;1b(p=0,h=0;y.1c>h;h++)T=y[h],S=d.1k(T,p),o.3S(d.1q(p,S-p),1D(T),3q(b[h],T),"",A&&"2c"===d.1q(S+T.1c,2),0===h),p=S+T.1c;o["2R"+o.l]+=d.1q(p)}1m o["2R"+o.l]+=o.l?" "+d:d;1a(-1!==r.1k("=")&&o.1A){1b(N=o.1X+o.1A.s,c=1;o.l>c;c++)N+=o["2R"+c]+o.1A["3i"+c];o.e=N+o["2R"+c]}18 o.l||(o.2p=-1,o.1X=o.e),o.3C||o},2u=9;1b(a=27.1y,a.l=a.2s=0;--2u>0;)a["3i"+2u]=0,a["2R"+2u]="";a.1X="",a.1f=a.1l=a.3C=a.1A=a.2D=a.1S=a.56=1d,a.3S=17(e,t,n,r,i,s){19 o=15,u=o.l;18 o["2R"+u]+=s&&u?" "+e:e||"",n||0===u||o.2D?(o.l++,o.2p=o.1S?2:1,o["2R"+o.l]=r||"",u>0?(o.1A["3i"+u]=t+n,o.56["3i"+u]=i,o["3i"+u]=t,o.2D||(o.3C=1h 27(o,"3i"+u,t,n,o.3C||o,0,o.n,i,o.2s),o.3C.1X=0),o):(o.1A={s:t+n},o.56={},o.s=t,o.c=n,o.r=i,o)):(o["2R"+u]+=t+(r||""),o)};19 5O=17(e,t){t=t||{},15.p=t.2V?X(e)||e:e,u[e]=u[15.p]=15,15.2U=t.4v||6H(t.2v,t.4c,t.bW,t.3W),t.24&&(15.2O=t.24),15.8T=t.4c,15.3W=t.3W,15.5b=t.5b,15.3Z=t.2v,15.2s=t.5E||0},1G=F.bV=17(e,t,n){"3U"!=1j t&&(t={24:n});19 r,i,s=e.1t(","),o=t.2v;1b(n=n||[o],r=0;s.1c>r;r++)t.2V=0===r&&t.2V,t.2v=n[r]||o,i=1h 5O(s[r],t)},7a=17(e){1a(!u[e]){19 t=e.1v(0).4Z()+e.1q(1)+"8u";1G(e,{24:17(e,n,r,i,s,o,a){19 f=(1Q.5Z||1Q).4Q.4R.4S[t];18 f?(f.bG(),u[r].2O(e,n,r,i,s,o,a)):(U("bH: "+t+" bI bJ 8K bF."),s)}})}};a=5O.1y,a.3Y=17(e,t,n,r,i,s){19 o,u,a,f,l,c,h=15.5b;1a(15.3W&&(M.2P(n)||M.2P(t)?(u=t.1I(M,"|").1t("|"),a=n.1I(M,"|").1t("|")):h&&(u=[t],a=[n])),a){1b(f=a.1c>u.1c?a.1c:u.1c,o=0;f>o;o++)t=u[o]=u[o]||15.3Z,n=a[o]=a[o]||15.3Z,h&&(l=t.1k(h),c=n.1k(h),l!==c&&(n=-1===c?a:u,n[o]+=" "+h));t=u.1J(", "),n=a.1J(", ")}18 4j(e,15.p,t,n,15.8T,15.3Z,r,15.2s,i,s)},a.2O=17(e,t,n,r,s,o){18 15.3Y(e.1w,15.2U($(e,15.p,i,!1,15.3Z)),15.2U(t),s,o)},o.bE=17(e,t,n){1G(e,{24:17(e,r,i,s,o,u){19 a=1h 27(e,i,0,0,o,2,i,!1,n);18 a.2D=u,a.1S=t(e,r,s.2Y,i),a},5E:n})};19 6O="28,2g,2W,x,y,z,1R,32,1K,1P,1V,3b".1t(","),bt=X("3R"),8O=z+"3R",5q=X("4G"),39=1d!==X("3b"),5F=F.bB=17(){15.32=0},4l=F.bC=17(e,t,n,r){1a(e.3T&&n&&!r)18 e.3T;19 i,s,u,a,f,l,c,h,p,d,v,m,g,y=n?e.3T||1h 5F:1h 5F,b=0>y.28,w=2e-5,E=4X,S=bD.99,x=S*2f,T=39?1r($(e,5q,t,!1,"0 0 0").1t(" ")[2])||y.2S||0:0;1b(bt?i=$(e,8O,t,!0):e.2X&&(i=e.2X.2l.2G(A),i=i&&4===i.1c?[i[0].1q(4),1D(i[2].1q(4)),1D(i[1].1q(4)),i[3].1q(4),y.x||0,y.y||0].1J(","):""),s=(i||"").2G(/(?:\\-|\\b)[\\d\\-\\.e]+\\b/3M)||[],u=s.1c;--u>-1;)a=1D(s[u]),s[u]=(f=a-(a|=0))?(0|f*E+(0>f?-.5:.5))/E+a:a;1a(16===s.1c){19 N=s[8],C=s[9],k=s[10],L=s[12],O=s[13],M=s[14];1a(y.2S&&(M=-y.2S,L=N*M-s[12],O=C*M-s[13],M=k*M+y.2S-s[14]),!n||r||1d==y.1P){19 P,H,B,j,F,I,q,R=s[0],U=s[1],z=s[2],W=s[3],X=s[4],V=s[5],J=s[6],K=s[7],Q=s[11],G=1g.4F(J,k),Y=-x>G||G>x;y.1P=G*D,G&&(j=1g.2E(-G),F=1g.2k(-G),P=X*j+N*F,H=V*j+C*F,B=J*j+k*F,N=X*-F+N*j,C=V*-F+C*j,k=J*-F+k*j,Q=K*-F+Q*j,X=P,V=H,J=B),G=1g.4F(N,R),y.1V=G*D,G&&(I=-x>G||G>x,j=1g.2E(-G),F=1g.2k(-G),P=R*j-N*F,H=U*j-C*F,B=z*j-k*F,C=U*F+C*j,k=z*F+k*j,Q=W*F+Q*j,R=P,U=H,z=B),G=1g.4F(U,V),y.1K=G*D,G&&(q=-x>G||G>x,j=1g.2E(-G),F=1g.2k(-G),R=R*j+X*F,H=U*j+V*F,V=U*-F+V*j,J=z*-F+J*j,U=H),q&&Y?y.1K=y.1P=0:q&&I?y.1K=y.1V=0:I&&Y&&(y.1V=y.1P=0),y.28=(0|1g.38(R*R+U*U)*E+.5)/E,y.2g=(0|1g.38(V*V+C*C)*E+.5)/E,y.2W=(0|1g.38(J*J+k*k)*E+.5)/E,y.1R=0,y.3b=Q?1/(0>Q?-Q:Q):0,y.x=L,y.y=O,y.z=M}}1m 1a(!(39&&!r&&s.1c&&y.x===s[4]&&y.y===s[5]&&(y.1P||y.1V)||2h 0!==y.x&&"3t"===$(e,"6S",t))){19 Z=s.1c>=6,3L=Z?s[0]:1,3g=s[1]||0,3q=s[2]||0,2A=Z?s[3]:1;y.x=s[4]||0,y.y=s[5]||0,l=1g.38(3L*3L+3g*3g),c=1g.38(2A*2A+3q*3q),h=3L||3g?1g.4F(3g,3L)*D:y.1K||0,p=3q||2A?1g.4F(3q,2A)*D+h:y.1R||0,d=l-1g.51(y.28||0),v=c-1g.51(y.2g||0),1g.51(p)>90&&bS>1g.51(p)&&(b?(l*=-1,p+=0>=h?36:-36,h+=0>=h?36:-36):(c*=-1,p+=0>=p?36:-36)),m=(h-y.1K)%36,g=(p-y.1R)%36,(2h 0===y.1R||d>w||-w>d||v>w||-w>v||m>-S&&S>m&&5X|m*E||g>-S&&S>g&&5X|g*E)&&(y.28=l,y.2g=c,y.1K=h,y.1R=p),39&&(y.1P=y.1V=y.z=0,y.3b=1r(o.81)||0,y.2W=1)}y.2S=T;1b(u 1u y)w>y[u]&&y[u]>-w&&(y[u]=0);18 n&&(e.3T=y),y},7q=17(e){19 t,n,r=15.1A,i=-r.1K*2f,s=i+r.1R*2f,o=4X,u=(0|1g.2E(i)*r.28*o)/o,a=(0|1g.2k(i)*r.28*o)/o,f=(0|1g.2k(s)*-r.2g*o)/o,l=(0|1g.2E(s)*r.2g*o)/o,c=15.t.1w,h=15.t.2X;1a(h){n=a,a=-f,f=-n,t=h.2l,c.2l="";19 p,v,m=15.t.4N,g=15.t.4M,y="83"!==h.4m,E="82:5Q.5S.5V(8c="+u+", 8d="+a+", 8n="+f+", 8m="+l,S=r.x,x=r.y;1a(1d!=r.52&&(p=(r.8o?.8q*m*r.52:r.52)-m/2,v=(r.8r?.8q*g*r.5R:r.5R)-g/2,S+=p-(p*u+v*a),x+=v-(p*f+v*l)),y?(p=m/2,v=g/2,E+=", 8e="+(p-(p*u+v*a)+S)+", 8g="+(v-(p*f+v*l)+x)+")"):E+=", bU=\'2t bR\')",c.2l=-1!==t.1k("5Q.5S.5V(")?t.1I(O,E):E+" "+t,(0===e||1===e)&&1===u&&0===a&&0===f&&1===l&&(y&&-1===E.1k("8e=0, 8g=0")||w.2P(t)&&22!==1r(46.$1)||-1===t.1k("bN("&&t.1k("bO"))&&c.6W("2l")),!y){19 T,N,C,k=8>d?1:-1;1b(p=r.53||0,v=r.5p||0,r.53=1g.3G((m-((0>u?-u:u)*m+(0>a?-a:a)*g))/2+S),r.5p=1g.3G((g-((0>l?-l:l)*g+(0>f?-f:f)*m))/2+x),2u=0;4>2u;2u++)N=Z[2u],T=h[N],n=-1!==T.1k("2c")?1r(T):J(15.t,N,1r(T),T.1I(b,""))||0,C=n!==r[N]?2>2u?-r.53:-r.5p:2>2u?p-r.53:v-r.5p,c[N]=(r[N]=1g.3G(n-C*(0===2u||2===2u?1:k)))+"2c"}}},5o=F.d3=17(){19 e,t,n,r,i,s,o,u,a,f,l,c,p,d,v,m,g,y,b,w,E,S,x,T=15.1A,N=15.t.1w,C=T.1K*2f,k=T.28,L=T.2g,A=T.2W,O=T.3b;1a(h){19 M=1e-4;M>k&&k>-M&&(k=A=2e-5),M>L&&L>-M&&(L=A=2e-5),!O||T.z||T.1P||T.1V||(O=0)}1a(C||T.1R)y=1g.2E(C),b=1g.2k(C),e=y,i=b,T.1R&&(C-=T.1R*2f,y=1g.2E(C),b=1g.2k(C),"bP"===T.4r&&(w=1g.bM(T.1R*2f),w=1g.38(1+w*w),y*=w,b*=w)),t=-b,s=y;1m{1a(!(T.1V||T.1P||1!==A||O))18 N[bt]="bQ("+T.x+"2c,"+T.y+"2c,"+T.z+"2c)"+(1!==k||1!==L?" 4y("+k+","+L+")":""),2h 0;e=s=1,t=i=0}l=1,n=r=o=u=a=f=c=p=d=0,v=O?-1/O:0,m=T.2S,g=4X,C=T.1V*2f,C&&(y=1g.2E(C),b=1g.2k(C),a=l*-b,p=v*-b,n=e*b,o=i*b,l*=y,v*=y,e*=y,i*=y),C=T.1P*2f,C&&(y=1g.2E(C),b=1g.2k(C),w=t*y+n*b,E=s*y+o*b,S=f*y+l*b,x=d*y+v*b,n=t*-b+n*y,o=s*-b+o*y,l=f*-b+l*y,v=d*-b+v*y,t=w,s=E,f=S,d=x),1!==A&&(n*=A,o*=A,l*=A,v*=A),1!==L&&(t*=L,s*=L,f*=L,d*=L),1!==k&&(e*=k,i*=k,a*=k,p*=k),m&&(c-=m,r=n*c,u=o*c,c=l*c+m),r=(w=(r+=T.x)-(r|=0))?(0|w*g+(0>w?-.5:.5))/g+r:r,u=(w=(u+=T.y)-(u|=0))?(0|w*g+(0>w?-.5:.5))/g+u:u,c=(w=(c+=T.z)-(c|=0))?(0|w*g+(0>w?-.5:.5))/g+c:c,N[bt]="bT("+[(0|e*g)/g,(0|i*g)/g,(0|a*g)/g,(0|p*g)/g,(0|t*g)/g,(0|s*g)/g,(0|f*g)/g,(0|d*g)/g,(0|n*g)/g,(0|o*g)/g,(0|l*g)/g,(0|v*g)/g,r,u,c,O?1+ -c/O:1].1J(",")+")"},7t=F.bL=17(e){19 t,n,r,i,s,o=15.1A,u=15.t,a=u.1w;18 o.1P||o.1V||o.z||o.44?(15.1S=5o,5o.1W(15,e),2h 0):(o.1K||o.1R?(t=o.1K*2f,n=t-o.1R*2f,r=4X,i=o.28*r,s=o.2g*r,a[bt]="8P("+(0|1g.2E(t)*i)/r+","+(0|1g.2k(t)*i)/r+","+(0|1g.2k(n)*-s)/r+","+(0|1g.2E(n)*s)/r+","+o.x+","+o.y+")"):a[bt]="8P("+o.28+",0,0,"+o.2g+","+o.x+","+o.y+")",2h 0)};1G("3R,4y,28,2g,2W,x,y,z,1K,1P,1V,6M,1R,32,6N,6G,6A,bK,4G,8U,8x,8M,44,4r",{24:17(e,t,n,r,s,u,a){1a(r.3r)18 s;19 f,l,c,h,p,d,v,m=r.3r=4l(e,i,!0,a.8M),g=e.1w,y=1e-6,b=6O.1c,w=a,E={};1a("1L"==1j w.3R&&bt)c=g.3d,g[bt]=w.3R,g.6S="bA",f=4l(e,1d,!1),g.3d=c;1m 1a("3U"==1j w){1a(f={28:2A(1d!=w.28?w.28:w.4y,m.28),2g:2A(1d!=w.2g?w.2g:w.4y,m.2g),2W:2A(w.2W,m.2W),x:2A(w.x,m.x),y:2A(w.y,m.y),z:2A(w.z,m.z),3b:2A(w.8U,m.3b)},v=w.8x,1d!=v)1a("3U"==1j v)1b(c 1u v)w[c]=v[c];1m w.1K=v;f.1K=3D("1K"1u w?w.1K:"6N"1u w?w.6N+"6F":"6M"1u w?w.6M:m.1K,m.1K,"1K",E),39&&(f.1P=3D("1P"1u w?w.1P:"6G"1u w?w.6G+"6F":m.1P||0,m.1P,"1P",E),f.1V=3D("1V"1u w?w.1V:"6A"1u w?w.6A+"6F":m.1V||0,m.1V,"1V",E)),f.1R=1d==w.1R?m.1R:3D(w.1R,m.1R),f.32=1d==w.32?m.32:3D(w.32,m.32),(l=f.32-m.32)&&(f.1R+=l,f.1K+=l)}1b(39&&1d!=w.44&&(m.44=w.44,d=!0),m.4r=w.4r||m.4r||o.8w,p=m.44||m.z||m.1P||m.1V||f.z||f.1P||f.1V||f.3b,p||1d==w.4y||(f.2W=1);--b>-1;)n=6O[b],h=f[n]-m[n],(h>y||-y>h||1d!=P[n])&&(d=!0,s=1h 27(m,n,m[n],h,s),n 1u E&&(s.e=E[n]),s.1X=0,s.2D=u,r.2B.2d(s.n));18 h=w.4G,(h||39&&p&&m.2S)&&(bt?(d=!0,n=5q,h=(h||$(e,n,i,!1,"50% 50%"))+"",s=1h 27(g,n,0,0,s,-1,"4G"),s.b=g[n],s.2D=u,39?(c=m.2S,h=h.1t(" "),m.2S=(h.1c>2&&(0===c||"1M"!==h[2])?1r(h[2]):c)||0,s.1X=s.e=g[n]=h[0]+" "+(h[1]||"50%")+" 1M",s=1h 27(m,"2S",0,0,s,-1,s.n),s.b=c,s.1X=s.e=m.2S):s.1X=s.e=g[n]=h):3g(h+"",m)),d&&(r.47=p||3===15.47?3:2),s},2V:!0}),1G("cd",{2v:"1M 1M 1M 1M #8i",2V:!0,4c:!0,3W:!0,5b:"6Y"}),1G("cg",{2v:"1M",24:17(e,t,n,s,o){t=15.2U(t);19 u,a,f,l,c,h,p,d,v,m,g,y,b,w,E,S,x=["cf","c6","bZ","b8"],T=e.1w;1b(v=1r(e.4N),m=1r(e.4M),u=t.1t(" "),a=0;x.1c>a;a++)15.p.1k("3X")&&(x[a]=X(x[a])),c=l=$(e,x[a],i,!1,"1M"),-1!==c.1k(" ")&&(l=c.1t(" "),c=l[0],l=l[1]),h=f=u[a],p=1r(c),y=c.1q((p+"").1c),b="="===h.1v(1),b?(d=3v(h.1v(0)+"1",10),h=h.1q(2),d*=1r(h),g=h.1q((d+"").1c-(0>d?1:0))||""):(d=1r(h),g=h.1q((d+"").1c)),""===g&&(g=r[n]||y),g!==y&&(w=J(e,"7Q",p,y),E=J(e,"aT",p,y),"%"===g?(c=22*(w/v)+"%",l=22*(E/m)+"%"):"4k"===g?(S=J(e,"7Q",1,"4k"),c=w/S+"4k",l=E/S+"4k"):(c=w+"2c",l=E+"2c"),b&&(h=1r(c)+d+g,f=1r(l)+d+g)),o=4j(T,x[a],c+" "+l,h+" "+f,!1,"1M",o);18 o},2V:!0,4v:6H("1M 1M 1M 1M",!1,!0)}),1G("ba",{2v:"0 0",24:17(e,t,n,r,s,o){19 u,a,f,l,c,h,p="bb-4m",v=i||V(e,1d),m=15.2U((v?d?v.42(p+"-x")+" "+v.42(p+"-y"):v.42(p):e.2X.bm+" "+e.2X.bi)||"0 0"),g=15.2U(t);1a(-1!==m.1k("%")!=(-1!==g.1k("%"))&&(h=$(e,"d1").1I(C,""),h&&"3t"!==h)){1b(u=m.1t(" "),a=g.1t(" "),j.d9("d5",h),f=2;--f>-1;)m=u[f],l=-1!==m.1k("%"),l!==(-1!==a[f].1k("%"))&&(c=0===f?e.4N-j.3k:e.4M-j.3y,u[f]=l?1r(m)/22*c+"2c":22*(1r(m)/c)+"%");m=u.1J(" ")}18 15.3Y(e.1w,m,g,s,o)},4v:3g}),1G("db",{2v:"0 0",4v:3g}),1G("3b",{2v:"1M",2V:!0}),1G("dj",{2v:"50% 50%",2V:!0}),1G("di",{2V:!0}),1G("dh",{2V:!0}),1G("de",{2V:!0}),1G("6U",{24:5g("7B,7S,7U,7K")}),1G("6B",{24:5g("cq,cR,cV,cU")}),1G("cT",{2v:"8y(1M,1M,1M,1M)",24:17(e,t,n,r,s,o){19 u,a,f;18 9>d?(a=e.2X,f=8>d?" ":",",u="8y("+a.cS+f+a.d4+f+a.da+f+a.cZ+")",t=15.2U(t).1t(",").1J(f)):(u=15.2U($(e,15.p,i,!1,15.3Z)),t=15.2U(t)),15.3Y(e.1w,u,t,s,o)}}),1G("d6",{2v:"1M 1M 1M #8i",4c:!0,3W:!0}),1G("7c,8b",{24:17(e,t,n,r,i){18 i}}),1G("3X",{2v:"1M 5x #6T",24:17(e,t,n,r,s,o){18 15.3Y(e.1w,15.2U($(e,"6X",i,!1,"1M")+" "+$(e,"d8",i,!1,"5x")+" "+$(e,"d0",i,!1,"#6T")),15.2U(t),s,o)},4c:!0,4v:17(e){19 t=e.1t(" ");18 t[0]+" "+(t[1]||"5x")+" "+(e.2G(at)||["#6T"])[0]}}),1G("9e",{24:5g("6X,9f,9g,84")}),1G("9c,6L,86",{24:17(e,t,n,r,i){19 s=e.1w,o="6L"1u s?"6L":"86";18 1h 27(s,o,0,0,i,-1,n,!1,0,s[o],t)}});19 8s=17(e){19 t,n=15.t,r=n.2l||$(15.1A,"2l"),i=0|15.s+15.c*e;22===i&&(-1===r.1k("96(")&&-1===r.1k("97(")&&-1===r.1k("98(")?(n.6W("2l"),t=!$(15.1A,"2l")):(n.2l=r.1I(S,""),t=!0)),t||(15.3P&&(n.2l=r=r||"3B(1Z="+i+")"),-1===r.1k("1Z")?0===i&&15.3P||(n.2l=r+" 3B(1Z="+i+")"):n.2l=r.1I(w,"1Z="+i))};1G("1Z,3B,4T",{2v:"1",24:17(e,t,n,r,s,o){19 u=1r($(e,"1Z",i,!1,"1")),a=e.1w,f="4T"===n;18"1L"==1j t&&"="===t.1v(1)&&(t=("-"===t.1v(0)?-1:1)*1r(t.1q(2))+u),f&&1===u&&"5v"===$(e,"6z",i)&&0!==t&&(u=0),q?s=1h 27(a,"1Z",u,t-u,s):(s=1h 27(a,"1Z",22*u,22*(t-u),s),s.3P=f?1:0,a.7s=1,s.2p=2,s.b="3B(1Z="+s.s+")",s.e="3B(1Z="+(s.s+s.c)+")",s.1A=e,s.2D=o,s.1S=8s),f&&(s=1h 27(a,"6z",0,0,s,-1,1d,!1,0,0!==u?"6I":"5v",0===t?"5v":"6I"),s.1X="6I",r.2B.2d(s.n),r.2B.2d(n)),s}});19 5r=17(e,t){t&&(e.8z?("5t"===t.1q(0,2)&&(t="M"+t.1q(1)),e.8z(t.1I(T,"-$1").6P())):e.6W(t))},8I=17(e){1a(15.t.4L=15,1===e||0===e){15.t.3a=0===e?15.b:15.e;1b(19 t=15.1A,n=15.t.1w;t;)t.v?n[t.p]=t.v:5r(n,t.p),t=t.1f;1===e&&15.t.4L===15&&(15.t.4L=1d)}1m 15.t.3a!==15.e&&(15.t.3a=15.e)};1G("3a",{24:17(e,t,r,s,o,u,a){19 f,l,c,h,p,d=e.3a,v=e.1w.3d;1a(o=s.6J=1h 27(e,r,0,0,o,2),o.1S=8I,o.2s=-11,n=!0,o.b=d,l=Q(e,i),c=e.4L){1b(h={},p=c.1A;p;)h[p.p]=1,p=p.1f;c.1S(1)}18 e.4L=o,o.e="="!==t.1v(1)?t:d.1I(46("\\\\s*\\\\b"+t.1q(2)+"\\\\b"),"")+("+"===t.1v(0)?" "+t.1q(2):""),s.2Y.1C&&(e.3a=o.e,f=G(e,l,Q(e),a,h),e.3a=d,o.1A=f.4e,e.1w.3d=v,o=o.3C=s.2O(e,f.57,o,u)),o}});19 8E=17(e){1a((1===e||0===e)&&15.1A.1H===15.1A.2i&&"8D"!==15.1A.1A){19 t,n,r,i,s=15.t.1w,o=u.3R.2O;1a("41"===15.e)s.3d="",i=!0;1m 1b(t=15.e.1t(","),r=t.1c;--r>-1;)n=t[r],u[n]&&(u[n].2O===o?i=!0:n="4G"===n?5q:u[n].p),5r(s,n);i&&(5r(s,bt),15.t.3T&&3E 15.t.3T)}};1b(1G("9h",{24:17(e,t,r,i,s){18 s=1h 27(e,r,0,0,s,2),s.1S=8E,s.e=t,s.2s=-10,s.1A=i.2Y,n=!0,s}}),a="9i,9p,9q,95".1t(","),2u=a.1c;2u--;)7a(a[2u]);a=o.1y,a.1s=1d,a.6V=17(e,t,u){1a(!e.3n)18!1;15.7V=e,15.2Y=u,15.7d=t,f=t.7c,n=!1,r=t.6E||o.6E,i=V(e,""),s=15.2B;19 a,h,d,v,m,g,y,b,w,S=e.1w;1a(l&&""===S.3O&&(a=$(e,"3O",i),("2t"===a||""===a)&&(S.3O=0)),"1L"==1j t&&(v=S.3d,a=Q(e,i),S.3d=v+";"+t,a=G(e,a,Q(e)).57,!q&&E.2P(t)&&(a.1Z=1r(46.$1)),t=a,S.3d=v),15.1s=h=15.2O(e,t,1d),15.47){1b(w=3===15.47,bt?c&&(l=!0,""===S.3O&&(y=$(e,"3O",i),("2t"===y||""===y)&&(S.3O=0)),p&&(S.7b=15.7d.7b||(w?"9r":"5v"))):S.7s=1,d=h;d&&d.1f;)d=d.1f;b=1h 27(e,"3R",0,0,1d,2),15.5e(b,1d,d),b.1S=w&&39?5o:bt?7t:7q,b.1A=15.3r||4l(e,i,!0),s.4q()}1a(n){1b(;h;){1b(g=h.1f,d=v;d&&d.2s>h.2s;)d=d.1f;(h.1l=d?d.1l:m)?h.1l.1f=h:v=h,(h.1f=d)?d.1l=h:m=h,h=g}15.1s=v}18!0},a.2O=17(e,t,n,s){19 o,a,l,c,h,p,d,v,m,g,y=e.1w;1b(o 1u t)p=t[o],a=u[o],a?n=a.2O(e,p,o,15,n,s,t):(h=$(e,o,i)+"",m="1L"==1j p,"4c"===o||"9k"===o||"91"===o||-1!==o.1k("8Z")||m&&x.2P(p)?(m||(p=5i(p),p=(p.1c>3?"6Z(":"5h(")+p.1J(",")+")"),n=4j(y,o,h,p,!0,"4g",n,0,s)):!m||-1===p.1k(" ")&&-1===p.1k(",")?(l=1r(h),d=l||0===l?h.1q((l+"").1c):"",(""===h||"2t"===h)&&("3k"===o||"3y"===o?(l=3L(e,o,i),d="2c"):"4A"===o||"4z"===o?(l=K(e,o,i),d="2c"):(l="1Z"!==o?0:1,d="")),g=m&&"="===p.1v(1),g?(c=3v(p.1v(0)+"1",10),p=p.1q(2),c*=1r(p),v=p.1I(b,"")):(c=1r(p),v=m?p.1q((c+"").1c)||"":""),""===v&&(v=o 1u r?r[o]:d),p=c||0===c?(g?c+l:c)+v:t[o],d!==v&&""!==v&&(c||0===c)&&l&&(l=J(e,o,l,d),"%"===v?(l/=J(e,o,22,"%")/22,t.8b!==!0&&(h=l+"%")):"4k"===v?l/=J(e,o,1,"4k"):"2c"!==v&&(c=J(e,o,c,v),v="2c"),g&&(c||0===c)&&(p=c+l+v)),g&&(c+=l),!l&&0!==l||!c&&0!==c?2h 0!==y[o]&&(p||"ar"!=p+""&&1d!=p)?(n=1h 27(y,o,c||l||0,0,n,-1,o,!1,0,h,p),n.1X="3t"!==p||"6S"!==o&&-1===o.1k("aq")?p:h):U("ap "+o+" 6R aj: "+t[o]):(n=1h 27(y,o,l,c-l,n,0,o,f!==!1&&("2c"===v||"3O"===o),0,h,p),n.1X=v)):n=4j(y,o,h,p,!0,1d,n,0,s)),s&&n&&!n.2D&&(n.2D=s);18 n},a.1S=17(e){19 t,n,r,i=15.1s,s=1e-6;1a(1!==e||15.2Y.1n!==15.2Y.1C&&0!==15.2Y.1n)1a(e||15.2Y.1n!==15.2Y.1C&&0!==15.2Y.1n||15.2Y.1z===-1e-6)1b(;i;){1a(t=i.c*e+i.s,i.r?t=1g.3G(t):s>t&&t>-s&&(t=0),i.2p)1a(1===i.2p)1a(r=i.l,2===r)i.t[i.p]=i.1X+t+i.4a+i.3P+i.5B;1m 1a(3===r)i.t[i.p]=i.1X+t+i.4a+i.3P+i.5B+i.6D+i.6C;1m 1a(4===r)i.t[i.p]=i.1X+t+i.4a+i.3P+i.5B+i.6D+i.6C+i.7D+i.7O;1m 1a(5===r)i.t[i.p]=i.1X+t+i.4a+i.3P+i.5B+i.6D+i.6C+i.7D+i.7O+i.aJ+i.aM;1m{1b(n=i.1X+t+i.4a,r=1;i.l>r;r++)n+=i["3i"+r]+i["2R"+(r+1)];i.t[i.p]=n}1m-1===i.2p?i.t[i.p]=i.1X:i.1S&&i.1S(e);1m i.t[i.p]=t+i.1X;i=i.1f}1m 1b(;i;)2!==i.2p?i.t[i.p]=i.b:i.1S(e),i=i.1f;1m 1b(;i;)2!==i.2p?i.t[i.p]=i.e:i.1S(e),i=i.1f},a.aL=17(e){15.47=e||3===15.47?3:2,15.3r=15.3r||4l(15.7V,i,!0)},a.5e=17(e,t,n,r){18 e&&(t&&(t.1l=e),e.1f&&(e.1f.1l=e.1l),e.1l?e.1l.1f=e.1f:15.1s===e&&(15.1s=e.1f,r=!0),n?n.1f=e:r||1d!==15.1s||(15.1s=e),e.1f=t,e.1l=n),e},a.2n=17(t){19 n,r,i,s=t;1a(t.4T||t.3B){s={};1b(r 1u t)s[r]=t[r];s.1Z=1,s.4T&&(s.6z=1)}18 t.3a&&(n=15.6J)&&(i=n.3C,i&&i.1l?15.5e(i.1l,n.1f,i.1l.1l):i===15.1s&&(15.1s=n.1f),n.1f&&15.5e(n.1f,n.1f.1f,i.1l),15.6J=1d),e.1y.2n.1W(15,s)};19 4t=17(e,t,n){19 r,i,s,o;1a(e.6K)1b(i=e.1c;--i>-1;)4t(e[i],t,n);1m 1b(r=e.70,i=r.1c;--i>-1;)s=r[i],o=s.2p,s.1w&&(t.2d(Q(s)),n&&n.2d(s)),1!==o&&9!==o&&11!==o||!s.70.1c||4t(s,t,n)};18 o.aD=17(e,n,r){19 i,s,o,u=t.3Q(e,n,r),a=[u],f=[],l=[],c=[],h=t.4s.7k;1b(e=u.2F||u.2x,4t(e,f,c),u.1E(n,!0),4t(e,l),u.1E(0,!0),u.1N(!0),i=c.1c;--i>-1;)1a(s=G(c[i],f[i],l[i]),s.4e){s=s.57;1b(o 1u r)h[o]&&(s[o]=r[o]);a.2d(t.3Q(c[i],n,s))}18 a},e.6Q([o]),o},!0)}),1Q.3u&&1Q.37.4q()()',62,830,'|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||this||function|return|var|if|for|length|null||_next|Math|new|_startTime|typeof|indexOf|_prev|else|_time|_timeline|vars|substr|parseFloat|_firstPT|split|in|charAt|style|_timeScale|prototype|_rawPrevTime|data|_paused|_duration|Number|render|255|mt|_totalTime|replace|join|rotation|string|0px|_enabled|_gc|rotationX|window|skewX|setRatio|_p1|add|rotationY|call|xs0|immediateRender|opacity||timeline|100|totalDuration|parser||_first|ht|scaleX|_startAt|_active|instanceof|px|push||_|scaleY|void|_totalDuration|smoothChildTiming|sin|filter|arguments|_kill|easing|type|_p2|_dirty|pr|auto|dt|defaultValue|getRatio|target|number|_initted|rt|_overwriteProps|duration|plugin|cos|_targets|match|_ease|5625|_labels|constructor|_delay|_reversed|128|parse|test|splice|xs|zOrigin|_siblings|format|prefix|scaleZ|currentStyle|_tween|totalTime|startAt|wake|skewY|_onUpdate|time|apply|180|_gsQueue|sqrt|St|className|perspective|_uncache|cssText|paused|_p3|tt|_parseTimeOrLabel|xn|ratio|width|_last|_overwrittenProps|nodeType|config|pow|nt|_transform|_calcEnd|none|_gsDefine|parseInt|API|st|height|ease|_propLookup|alpha|xfirst|it|delete|frame|round|rawTime|selector|tweens|_remove|et|gi|Array|zIndex|xn1|to|transform|appendXtra|_gsTransform|object|version|multi|border|parseComplex|dflt|getTweensOf|all|getPropertyValue|concat|force3D||RegExp|_transformType|reversed|onComplete|xs1|seek|color|_listeners|firstMPT|kill|transparent|set|onReverseComplete|pt|em|Tt|position|isActive|PI|_initProps|pop|skewType|_internals|_t|_notifyPluginsOfEnabled|formatter|runBackwards|delay|scale|top|left|self|sc|_propName|onStart|atan2|transformOrigin|autoRemoveChildren|overwrite|pg|_sortChildren|_gsClassPT|offsetHeight|offsetWidth|_pauseTime|css|com|greensock|plugins|autoAlpha|strict|use|tick|1e5|Ticker|toUpperCase||abs|ox|ieOffsetX|gsClass||rxp|difs|192|9999999999|ot|keyword|sleep|easeParams|_linkCSSP|TweenLite|lt|rgb|ut|repeat|easeIn|_swapSelfInParams|easeInOut|remove|Ct|ieOffsetY|Et|At|getChildren|ms|invalidate|hidden|next|solid|prev|_onPluginEvent|onUpdate|xs2|core|_onInitAllProps|priority|xt|fps|_params|_hasPausedChild|Scope|useFrames|_func|setTimeout|pause|vt|onStartScope|DXImageTransform|oy|Microsoft|onUpdateScope|onUpdateParams|Matrix|_priority|false|Params|GreenSockGlobals||onStartParams|module|define|check|_onDisable|from|_easeType|360|TweenMax|onCompleteParams|fromTo|easeOut|isNaN|createElement|ticker|_gsCache|document|Left|_overwrite|autoCSS|throw|9375|625|delayedCall|984375|timeScale|defaultEase|onCompleteScope|_power|staggerTo|_type|SlowMo|console|hsl|visibility|shortRotationY|padding|xs3|xn2|suffixMap|_short|shortRotationX|ft|inherit|_classNamePT|slice|cssFloat|rotationZ|shortRotation|yt|toLowerCase|activate|tween|display|000|margin|_onInitTween|removeAttribute|borderTopWidth|inset|rgba|childNodes|get|_easePower|play|div||break|globals|_init|addEventListener|gt|WebkitBackfaceVisibility|autoRound|_vars|Cannot|RoughEase|_gsTweenID|_updateRoot|red|autoSleep|reservedProps|center|propName|removeLabel|init|isSelector|Nt|appendChild|zoom|kt|_roundProps|defaultView|insert|addLabel|SimpleTimeline|log|func|marginTop|map|xn3|isArray|bottom|right|Width|black|onReverseCompleteParams|marginLeft|_onEnable|onReverseCompleteScope|Top|xs4|_rootFramesTimeline|borderLeft|filters|marginRight|Right|marginBottom|_target|proxy|Linear|Back|Quad|TweenPlugin|defaultTransformPerspective|progid|absolute|borderLeftWidth|_eventTarget|styleFloat|SteppedEase|Date|_p|up|strictUnits|M11|M12|Dx|exports|Dy|Ease|999|GreenSockAMDPath|_class|register|M22|M21|oxp|shiftChildren|01|oyp|Lt|url|Plugin|gap|defaultSkewType|directionalRotation|rect|removeProperty|random|_contains|startTime|isFromStart|Mt|autoRotate|defaultOverwrite|Animation|Ot|2e3|not|1e3|parseTransform|template|wt|matrix|dispatchEvent|strength|useRAF|clrs|transformPerspective|events|EventDispatcher|or|the|Color||stroke|append|into|is|physics2D|atrix|radient|oader||clear|999999999999|float|usesFrames|borderWidth|borderRightWidth|borderBottomWidth|clearProps|bezier|getLabelTime|fill|addPause|appendMultiple|stop|gotoAndPlay|throwProps|physicsProps|visible|gotoAndStop|insertMultiple|killTweensOf|resume|restart|reverse|eventCallback|1500|clearTimeout|CancelAnimationFrame|CancelRequestAnimationFrame|004|on|progress|_plugins|_tweenLookup|onRepeat|getElementById|jQuery|totalProgress|jquery|_autoCSS|RequestAnimationFrame|webkit|Quart|Quint|Strong|Cubic|undefined|Object|toString|amd|Power|easeNone|now|getTime|moz|cancelAnimationFrame|requestAnimationFrame|linear|swing|removeEventListener|onRepeatParams|onRepeatScope|dependency|TimelineLite|align|missing|encountered|initAll|global|GSAP|stagger|value|exportRoot|normal|sequence|staggerFromTo|staggerFrom|invalid|Style|NaN|overwriteProps||definition|true|_rootTimeline|120|preexisting|allOnStart|yoyo|repeatDelay|concurrent|cascadeTo|_dummyGS|_super|_addTween|illegal|killDelayedCallsTo|xn4|_tempKill|_enableTransforms|xs5|start|70158|Bottom|oxr|oyr|rad|borderTop|Origin|removeChild|cacheWidths|calculateOffset|offset|short|_cw|maroon|teal|blue|navy|silver|lime|ccw|borderBottomLeftRadius|aqua|backgroundPosition|background|exec|innerHTML|1px|getElementsByTagName|MSIE|Firefox|backgroundPositionY|Safari|Chrome|Version|backgroundPositionX|Moz|clientWidth|clientHeight|line|body|convertToPixels||getStyle|Ms|Webkit|getComputedStyle|white|fuchsia|block|Transform|getTransform|179|registerSpecialProp|loaded|_cssRegister|Error|js|file|shortRotationZ|set2DTransformRatio|tan|gradient|Alpha|simple|translate3d|expand|270|matrix3d|sizingMethod|_registerComplexSpecialProp|collapsible|purple|green|borderBottomRightRadius|pink|gray|165|olive|yellow|orange|borderTopRightRadius|203|_setPluginRatio|_parseToProxy|end|CSSPropTween|hsla|boxShadow|cyan|borderTopLeftRadius|borderRadius|Android|parentNode|ElasticIn|ElasticInOut|Expo|ElasticOut|Elastic|BackInOut|asin|paddingTop|BackIn|ExpoOut||SineIn|BackOut|SineInOut|SineOut|Sine|ExpoIn|randomize|ExpoInOut|userAgent|CircInOut|taper|BounceOut|999999999|Bounce|points|clamp|out|sort|BounceIn|BounceInOut|Circ|CircOut|CircIn|paddingRight|clipTop|clip|paddingLeft|paddingBottom|525|001|compensated|clipLeft|borderTopColor|backgroundImage|img|set3DTransformRatio|clipRight|src|textShadow|zA|borderTopStyle|setAttribute|clipBottom|backgroundSize|navigator|lineHeight|userSelect|find|EaseLookup|backfaceVisibility|transformStyle|perspectiveOrigin|fontSize|CSSPlugin|_specialProps|Z0'.split('|'),0,{}));

/*! Big banner - 2D & 3D Transitions for LayerSlider  -  http://kreaturamedia.com*/
;eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('20 1Z={27:[{j:"13 N E",d:1,g:1,f:{e:0,i:"o"},c:{n:"W",b:"1e",a:G,h:"r"}},{j:"13 N r",d:1,g:1,f:{e:0,i:"o"},c:{n:"W",b:"1e",a:G,h:"E"}},{j:"13 N L",d:1,g:1,f:{e:0,i:"o"},c:{n:"W",b:"1e",a:G,h:"J"}},{j:"13 N J",d:1,g:1,f:{e:0,i:"o"},c:{n:"W",b:"1e",a:G,h:"L"}},{j:"26",d:1,g:1,f:{e:0,i:"o"},c:{n:"14",b:"1e",a:G,h:"r"}},{j:"Z R o",d:[2,4],g:[4,7],f:{e:1k,i:"o"},c:{n:"14",b:"z",a:G,h:"r"}},{j:"Z R D",d:[2,4],g:[4,7],f:{e:1k,i:"D"},c:{n:"14",b:"z",a:G,h:"r"}},{j:"Z R 1j-o",d:[2,4],g:[4,7],f:{e:1k,i:"1j-o"},c:{n:"14",b:"z",a:G,h:"r"}},{j:"Z R 1j-D",d:[2,4],g:[4,7],f:{e:1k,i:"1j-D"},c:{n:"14",b:"z",a:G,h:"r"}},{j:"Z R (k)",d:[2,4],g:[4,7],f:{e:1k,i:"k"},c:{n:"14",b:"z",a:G,h:"r"}},{j:"1y 1H N E",d:1,g:1s,f:{e:25,i:"D"},c:{n:"14",b:"1X",a:V,h:"r"}},{j:"1y 1H N r",d:1,g:1s,f:{e:25,i:"o"},c:{n:"14",b:"w",a:V,h:"r"}},{j:"1y 1H N L",d:1s,g:1,f:{e:25,i:"1j-D"},c:{n:"14",b:"w",a:V,h:"r"}},{j:"1y 1H N J",d:1s,g:1,f:{e:25,i:"1j-o"},c:{n:"14",b:"w",a:V,h:"r"}},{j:"1y Y N E",d:1,g:25,f:{e:1k,i:"D"},c:{n:"W",b:"w",a:1g,h:"r"}},{j:"1y Y N r",d:1,g:25,f:{e:1k,i:"o"},c:{n:"W",b:"w",a:1g,h:"E"}},{j:"1y 1W N L",d:25,g:1,f:{e:1k,i:"1j-D"},c:{n:"W",b:"w",a:1g,h:"J"}},{j:"1y Y N J",d:25,g:1,f:{e:1k,i:"1j-o"},c:{n:"W",b:"w",a:1g,h:"L"}},{j:"13 R m E (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"W",b:"z",a:1m,h:"E"}},{j:"13 R m r (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"W",b:"z",a:1m,h:"r"}},{j:"13 R m L (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"W",b:"z",a:1m,h:"L"}},{j:"13 R m J (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"W",b:"z",a:1m,h:"J"}},{j:"13 k R m k 1S",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"W",b:"z",a:1m,h:"k"}},{j:"13 d m E (o)",d:[7,11],g:1,f:{e:1d,i:"o"},c:{n:"W",b:"w",a:p,h:"E"}},{j:"13 d m E (D)",d:[7,11],g:1,f:{e:1d,i:"D"},c:{n:"W",b:"w",a:p,h:"E"}},{j:"13 d m E (k)",d:[7,11],g:1,f:{e:1d,i:"k"},c:{n:"W",b:"w",a:p,h:"E"}},{j:"13 d m r (o)",d:[7,11],g:1,f:{e:1d,i:"o"},c:{n:"W",b:"w",a:p,h:"r"}},{j:"13 d m r (D)",d:[7,11],g:1,f:{e:1d,i:"D"},c:{n:"W",b:"w",a:p,h:"r"}},{j:"13 d m r (k)",d:[7,11],g:1,f:{e:1d,i:"k"},c:{n:"W",b:"w",a:p,h:"r"}},{j:"13 d N J m L (o)",d:[7,11],g:1,f:{e:1d,i:"o"},c:{n:"W",b:"w",a:p,h:"L"}},{j:"13 d N J m L (k)",d:[7,11],g:1,f:{e:1d,i:"k"},c:{n:"W",b:"w",a:p,h:"L"}},{j:"13 d N L m J (D)",d:[7,11],g:1,f:{e:1d,i:"D"},c:{n:"W",b:"w",a:p,h:"J"}},{j:"13 d N L m J (k)",d:[7,11],g:1,f:{e:1d,i:"k"},c:{n:"W",b:"w",a:p,h:"J"}},{j:"13 P m L (o)",d:1,g:[12,16],f:{e:q,i:"o"},c:{n:"W",b:"w",a:p,h:"L"}},{j:"13 P m L (D)",d:1,g:[12,16],f:{e:q,i:"D"},c:{n:"W",b:"w",a:p,h:"L"}},{j:"13 P m L (k)",d:1,g:[12,16],f:{e:q,i:"k"},c:{n:"W",b:"w",a:p,h:"L"}},{j:"13 P m J (o)",d:1,g:[12,16],f:{e:q,i:"o"},c:{n:"W",b:"w",a:p,h:"J"}},{j:"13 P m J (D)",d:1,g:[12,16],f:{e:q,i:"D"},c:{n:"W",b:"w",a:p,h:"J"}},{j:"13 P m J (k)",d:1,g:[12,16],f:{e:q,i:"k"},c:{n:"W",b:"w",a:p,h:"J"}},{j:"13 P N r m E (o)",d:1,g:[12,16],f:{e:q,i:"o"},c:{n:"W",b:"w",a:p,h:"E"}},{j:"13 P N r m E (k)",d:1,g:[12,16],f:{e:q,i:"k"},c:{n:"W",b:"w",a:p,h:"E"}},{j:"13 P N E m r (D)",d:1,g:[12,16],f:{e:q,i:"D"},c:{n:"W",b:"w",a:p,h:"r"}},{j:"13 P N E m r (k)",d:1,g:[12,16],f:{e:q,i:"k"},c:{n:"W",b:"w",a:p,h:"r"}},{j:"Z v Y R m E (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"Q",b:"z",a:1m,h:"E"}},{j:"Z v Y R m r (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"Q",b:"z",a:1m,h:"r"}},{j:"Z v Y R m L (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"Q",b:"z",a:1m,h:"L"}},{j:"Z v Y R m J (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"Q",b:"z",a:1m,h:"J"}},{j:"Z v Y k R m k 1S",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"Q",b:"z",a:1m,h:"k"}},{j:"Z v Y R N J-r (o)",d:[2,4],g:[4,7],f:{e:1f,i:"o"},c:{n:"Q",b:"z",a:1m,h:"1V"}},{j:"Z v Y R N L-E (D)",d:[2,4],g:[4,7],f:{e:1f,i:"D"},c:{n:"Q",b:"z",a:1m,h:"21"}},{j:"Z v Y R N J-E (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"Q",b:"z",a:1m,h:"1T"}},{j:"Z v Y R N L-r (k)",d:[2,4],g:[4,7],f:{e:1f,i:"k"},c:{n:"Q",b:"z",a:1m,h:"1U"}},{j:"Z v Y d m E (o)",d:[7,11],g:1,f:{e:1d,i:"o"},c:{n:"Q",b:"w",a:p,h:"E"}},{j:"Z v Y d m E (D)",d:[7,11],g:1,f:{e:1d,i:"D"},c:{n:"Q",b:"w",a:p,h:"E"}},{j:"Z v Y d m E (k)",d:[7,11],g:1,f:{e:1d,i:"k"},c:{n:"Q",b:"w",a:p,h:"E"}},{j:"Z v Y d m r (o)",d:[7,11],g:1,f:{e:1d,i:"o"},c:{n:"Q",b:"w",a:p,h:"r"}},{j:"Z v Y d m r (D)",d:[7,11],g:1,f:{e:1d,i:"D"},c:{n:"Q",b:"w",a:p,h:"r"}},{j:"Z v Y d m r (k)",d:[7,11],g:1,f:{e:1d,i:"k"},c:{n:"Q",b:"w",a:p,h:"r"}},{j:"Z v Y d N J m L (o)",d:[7,11],g:1,f:{e:1d,i:"o"},c:{n:"Q",b:"w",a:p,h:"L"}},{j:"Z v Y d N J m L (k)",d:[7,11],g:1,f:{e:1d,i:"k"},c:{n:"Q",b:"w",a:p,h:"L"}},{j:"Z v Y d N L m J (D)",d:[7,11],g:1,f:{e:1d,i:"D"},c:{n:"Q",b:"w",a:p,h:"J"}},{j:"Z v Y d N L m J (k)",d:[7,11],g:1,f:{e:1d,i:"k"},c:{n:"Q",b:"w",a:p,h:"J"}},{j:"Z v Y P m L (o)",d:1,g:[12,16],f:{e:q,i:"o"},c:{n:"Q",b:"w",a:p,h:"L"}},{j:"Z v Y P m L (D)",d:1,g:[12,16],f:{e:q,i:"D"},c:{n:"Q",b:"w",a:p,h:"L"}},{j:"Z v Y P m L (k)",d:1,g:[12,16],f:{e:q,i:"k"},c:{n:"Q",b:"w",a:p,h:"L"}},{j:"Z v Y P m J (o)",d:1,g:[12,16],f:{e:q,i:"o"},c:{n:"Q",b:"w",a:p,h:"J"}},{j:"Z v Y P m J (D)",d:1,g:[12,16],f:{e:q,i:"D"},c:{n:"Q",b:"w",a:p,h:"J"}},{j:"Z v Y P m J (k)",d:1,g:[12,16],f:{e:q,i:"k"},c:{n:"Q",b:"w",a:p,h:"J"}},{j:"Z v Y P N r m E (o)",d:1,g:[12,16],f:{e:q,i:"o"},c:{n:"Q",b:"w",a:p,h:"E"}},{j:"Z v Y P N r m E (k)",d:1,g:[12,16],f:{e:q,i:"k"},c:{n:"Q",b:"w",a:p,h:"E"}},{j:"Z v Y P N E m r (D)",d:1,g:[12,16],f:{e:q,i:"D"},c:{n:"Q",b:"w",a:p,h:"r"}},{j:"Z v Y P N E m r (k)",d:1,g:[12,16],f:{e:q,i:"k"},c:{n:"Q",b:"w",a:p,h:"r"}},{j:"1u",d:1,g:1,f:{e:0,i:"o"},c:{n:"Q",b:"1e",a:V,h:"r",1h:.5}},{j:"1u d",d:4,g:1,f:{e:1f,i:"o"},c:{n:"Q",b:"1e",a:V,h:"r",1h:.5}},{j:"1u g",d:1,g:4,f:{e:1f,i:"o"},c:{n:"Q",b:"1e",a:V,h:"r",1h:.5}},{j:"1u R A",d:3,g:4,f:{e:1s,i:"o"},c:{n:"Q",b:"1e",a:V,h:"r",1h:.5,y:x}},{j:"1u R F",d:3,g:4,f:{e:1s,i:"o"},c:{n:"Q",b:"1e",a:V,h:"J",1h:.5,u:-x}},{j:"1u-1I R A",d:3,g:4,f:{e:15,i:"o"},c:{n:"Q",b:"1e",a:V,h:"r",1h:.5,y:x}},{j:"1u-1I R F",d:3,g:4,f:{e:15,i:"o"},c:{n:"Q",b:"1e",a:V,h:"J",1h:.5,u:-x}},{j:"1u 1I d",d:4,g:1,f:{e:1f,i:"o"},c:{n:"Q",b:"1e",a:V,h:"E",1h:.5}},{j:"1u 1I g",d:1,g:4,f:{e:1f,i:"o"},c:{n:"Q",b:"1e",a:V,h:"r",1h:.5}},{j:"1c f N r",d:1,g:1,f:{e:0,i:"o"},c:{n:"W",b:"z",a:V,h:"E",y:x}},{j:"1c f N E",d:1,g:1,f:{e:0,i:"o"},c:{n:"W",b:"z",a:V,h:"r",y:-x}},{j:"1c f N J",d:1,g:1,f:{e:0,i:"o"},c:{n:"W",b:"z",a:V,h:"L",u:-x}},{j:"1c f N L",d:1,g:1,f:{e:0,i:"o"},c:{n:"W",b:"z",a:V,h:"J",u:x}},{j:"1c R N r",d:[3,4],g:[3,4],f:{e:19,i:"o"},c:{n:"14",b:"z",a:V,h:"r",y:x}},{j:"1c R N E",d:[3,4],g:[3,4],f:{e:19,i:"D"},c:{n:"14",b:"z",a:V,h:"r",y:-x}},{j:"1c R N J",d:[3,4],g:[3,4],f:{e:19,i:"o"},c:{n:"14",b:"z",a:V,h:"r",u:-x}},{j:"1c R N L",d:[3,4],g:[3,4],f:{e:19,i:"D"},c:{n:"14",b:"z",a:V,h:"r",u:x}},{j:"1c d N J",d:[6,12],g:1,f:{e:19,i:"o"},c:{n:"14",b:"z",a:V,h:"r",u:x}},{j:"1c d N L",d:[6,12],g:1,f:{e:19,i:"D"},c:{n:"14",b:"z",a:V,h:"r",u:-x}},{j:"1c g N r",d:1,g:[6,12],f:{e:19,i:"o"},c:{n:"14",b:"z",a:V,h:"r",y:-x}},{j:"1c g N E",d:1,g:[6,12],f:{e:19,i:"D"},c:{n:"14",b:"z",a:V,h:"r",y:x}},{j:"1v d N r",d:[3,10],g:1,f:{e:19,i:"o"},c:{n:"14",b:"z",a:V,h:"r",y:x}},{j:"1v d N E",d:[3,10],g:1,f:{e:19,i:"D"},c:{n:"14",b:"z",a:V,h:"r",y:-x}},{j:"1v g N J",d:1,g:[3,10],f:{e:19,i:"o"},c:{n:"14",b:"z",a:V,h:"r",u:-x}},{j:"1v g N L",d:1,g:[3,10],f:{e:19,i:"D"},c:{n:"14",b:"z",a:V,h:"r",u:x}},{j:"1v v 1z f N r",d:1,g:1,f:{e:q,i:"o"},c:{n:"Q",b:"z",a:V,h:"E",1h:.1,1r:-x,y:x}},{j:"1v v 1z f N E",d:1,g:1,f:{e:q,i:"o"},c:{n:"Q",b:"z",a:V,h:"r",1h:.1,1r:x,y:-x}},{j:"1v v 1z R N r",d:[3,4],g:[3,4],f:{e:19,i:"o"},c:{n:"Q",b:"z",a:V,h:"E",1r:-1w}},{j:"1v v 1z R N E",d:[3,4],g:[3,4],f:{e:19,i:"o"},c:{n:"Q",b:"z",a:V,h:"r",1r:-1w}},{j:"1v v 1z R N k",d:[3,4],g:[3,4],f:{e:19,i:"k"},c:{n:"Q",b:"z",a:V,h:"k",1r:-1w}},{j:"B f 1O",d:1,g:1,f:{e:0,i:"o"},c:{n:"14",b:"z",a:1a,h:"r",1h:.8}},{j:"B f N 1L",d:1,g:1,f:{e:0,i:"o"},c:{n:"14",b:"w",a:1a,h:"r",1h:1.2}},{j:"B R k",d:[3,4],g:[3,4],f:{e:1s,i:"k"},c:{n:"14",b:"z",a:V,h:"r",1h:.1}},{j:"B R N 1L k",d:[3,4],g:[3,4],f:{e:1s,i:"k"},c:{n:"14",b:"z",a:V,h:"r",1h:2}},{j:"B 1O v 1z R k",d:[3,4],g:[3,4],f:{e:1s,i:"k"},c:{n:"14",b:"z",a:V,h:"r",1h:.1,1r:x}},{j:"B v 1z R N 1L k",d:[3,4],g:[3,4],f:{e:1s,i:"k"},c:{n:"14",b:"z",a:V,h:"r",1h:2,1r:-x}},{j:"1D-Y R 24",d:3,g:4,f:{e:15,i:"o"},c:{n:"W",b:"w",a:1Y,h:"1T"}},{j:"1D-Y d A",d:6,g:1,f:{e:0,i:"o"},c:{n:"Q",b:"z",a:V,h:"r"}},{j:"1D-Y d F",d:6,g:1,f:{e:0,i:"o"},c:{n:"Q",b:"z",a:V,h:"J"}},{j:"1D-Y g A",d:1,g:8,f:{e:0,i:"o"},c:{n:"Q",b:"z",a:V,h:"r"}},{j:"1D-Y g F",d:1,g:8,f:{e:0,i:"o"},c:{n:"Q",b:"z",a:V,h:"J"}}],23:[{j:"1b f m E (l&#t;)",d:1,g:1,f:{e:q,i:"o"},s:{c:{y:1E},b:"1F",a:G,h:"A"},C:{c:{y:l},b:"z",a:G,h:"A"}},{j:"1b f m r (l&#t;)",d:1,g:1,f:{e:q,i:"o"},s:{c:{y:-1E},b:"1F",a:G,h:"A"},C:{c:{y:-l},b:"z",a:G,h:"A"}},{j:"1b f m L (l&#t;)",d:1,g:1,f:{e:q,i:"o"},s:{c:{u:-1E},b:"1F",a:1x,h:"F"},C:{c:{u:-l},b:"z",a:1x,h:"F"}},{j:"1b f m J (l&#t;)",d:1,g:1,f:{e:q,i:"o"},s:{c:{u:1E},b:"1F",a:1x,h:"F"},C:{c:{u:l},b:"z",a:1x,h:"F"}},{j:"1b R m E (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"o"},s:{c:{y:l},b:"w",a:G,h:"A"}},{j:"1b R m r (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"D"},s:{c:{y:-l},b:"w",a:G,h:"A"}},{j:"1b R m L (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"1j-o"},s:{c:{u:-l},b:"w",a:G,h:"F"}},{j:"1b R m J (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"1j-D"},s:{c:{u:l},b:"w",a:G,h:"F"}},{j:"1B S R k (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"k"},s:{c:{y:l},b:"w",a:1G,h:"A"}},{j:"1C S R k (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"k"},s:{c:{u:l},b:"w",a:1G,h:"F"}},{j:"B v S R m E (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"o"},M:{c:{I:.1A},a:1l,b:"18"},s:{c:{y:l},b:"H",a:G,h:"A"},C:{a:1g,b:"H"}},{j:"B v S R m r (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"D"},M:{c:{I:.1A},a:1l,b:"18"},s:{c:{y:-l},b:"H",a:G,h:"A"},C:{a:1g,b:"H"}},{j:"B v S R m L (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"1j-o"},M:{c:{I:.1A},a:1l,b:"18"},s:{c:{u:-l},b:"H",a:G,h:"F"},C:{a:1g,b:"H"}},{j:"B v S R m J (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"1j-D"},M:{c:{I:.1A},a:1l,b:"18"},s:{c:{u:l},b:"H",a:G,h:"F"},C:{a:1g,b:"H"}},{j:"B v A S R k (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"k"},M:{c:{I:.1A,u:1k},a:1l,b:"18"},s:{c:{y:l,u:-1k},b:"H",a:1G,h:"A"},C:{c:{u:0},a:1g,b:"H"}},{j:"B v F S R k (l&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"k"},M:{c:{I:.1A,y:-15},a:1l,b:"18"},s:{c:{u:l,y:15},b:"H",a:1G,h:"F"},C:{c:{y:0},a:1g,b:"H"}},{j:"1b d m E (l&#t;)",d:[5,9],g:1,f:{e:q,i:"o"},s:{c:{y:l},b:"w",a:1a,h:"A"}},{j:"1b d m r (l&#t;)",d:[5,9],g:1,f:{e:q,i:"o"},s:{c:{y:-l},b:"w",a:1a,h:"A"}},{j:"1b d m L (l&#t;)",d:[5,9],g:1,f:{e:q,i:"o"},s:{c:{u:-l},b:"w",a:G,h:"F"}},{j:"1b d m J (l&#t;)",d:[5,9],g:1,f:{e:q,i:"D"},s:{c:{u:l},b:"w",a:G,h:"F"}},{j:"1B S d k (l&#t;)",d:[5,9],g:1,f:{e:q,i:"k"},s:{c:{y:l},b:"w",a:1a,h:"A"}},{j:"1C S d k (l&#t;)",d:[5,9],g:1,f:{e:q,i:"k"},s:{c:{u:-l},b:"w",a:1a,h:"F"}},{j:"1C S d k (1J&#t;)",d:[3,7],g:1,f:{e:1Q,i:"k"},s:{c:{u:-1J},b:"w",a:1R,h:"F"}},{j:"B v S d m E (l&#t;)",d:[5,9],g:1,f:{e:19,i:"o"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:l},b:"H",a:1p,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v S d m r (l&#t;)",d:[5,9],g:1,f:{e:19,i:"D"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:-l},b:"H",a:1p,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v S d m L (l&#t;)",d:[5,9],g:1,f:{e:19,i:"o"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-l},b:"w",a:p,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v S d m J (l&#t;)",d:[5,9],g:1,f:{e:19,i:"D"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:l},b:"w",a:p,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v A S d k (l&#t;)",d:[5,9],g:1,f:{e:19,i:"k"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:l},b:"H",a:1p,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v F S d k (l&#t;)",d:[5,9],g:1,f:{e:19,i:"k"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-l},b:"H",a:p,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"1b P m E (l&#t;)",d:1,g:[5,9],f:{e:q,i:"o"},s:{c:{y:l},b:"w",a:1a,h:"A"}},{j:"1b P m r (l&#t;)",d:1,g:[5,9],f:{e:q,i:"o"},s:{c:{y:-l},b:"w",a:1a,h:"A"}},{j:"1b P m L (l&#t;)",d:1,g:[5,9],f:{e:q,i:"o"},s:{c:{u:-l},b:"w",a:G,h:"F"}},{j:"1b P m J (l&#t;)",d:1,g:[5,9],f:{e:q,i:"D"},s:{c:{u:l},b:"w",a:G,h:"F"}},{j:"1B S P k (l&#t;)",d:1,g:[5,9],f:{e:q,i:"k"},s:{c:{y:l},b:"w",a:1a,h:"A"}},{j:"1C S P k (l&#t;)",d:1,g:[5,9],f:{e:q,i:"k"},s:{c:{u:-l},b:"w",a:1a,h:"F"}},{j:"1B S P k (1J&#t;)",d:1,g:[4,9],f:{e:1Q,i:"k"},s:{c:{y:1J},b:"w",a:1R,h:"A"}},{j:"B v S P m E (l&#t;)",d:1,g:[7,11],f:{e:19,i:"o"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:l},b:"w",a:p,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v S P m r (l&#t;)",d:1,g:[7,11],f:{e:19,i:"D"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:-l},b:"w",a:p,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v S P m L (l&#t;)",d:1,g:[7,11],f:{e:19,i:"o"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-l},b:"H",a:1p,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v S P m J (l&#t;)",d:1,g:[7,11],f:{e:q,i:"D"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:l},b:"H",a:1p,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v A S P k (l&#t;)",d:1,g:[7,11],f:{e:q,i:"k"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:l},b:"H",a:p,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v F S P k (l&#t;)",d:1,g:[7,11],f:{e:q,i:"k"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-l},b:"H",a:1p,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"1N 1P 1M v S m E (l&#t;)",d:1,g:[7,11],f:{e:q,i:"o"},M:{c:{I:.O,u:-1k},a:p,b:"z"},s:{c:{u:-1k,y:l},b:"w",a:G,h:"A"},C:{c:{u:0,e:X},b:"z",a:p}},{j:"1N 1P 1M v S m r (l&#t;)",d:1,g:[7,11],f:{e:q,i:"D"},M:{c:{I:.O,u:-1k},a:p,b:"z"},s:{c:{u:1k,y:-l},b:"w",a:G,h:"A"},C:{c:{u:0,e:X},b:"z",a:p}},{j:"1c 1t m E (x&#t;)",d:1,g:1,f:{e:q,i:"o"},s:{c:{y:x},b:"w",a:1a,h:"A"}},{j:"1c 1t m r (x&#t;)",d:1,g:1,f:{e:q,i:"o"},s:{c:{y:-x},b:"w",a:1a,h:"A"}},{j:"1c 1t m L (x&#t;)",d:1,g:1,f:{e:q,i:"o"},s:{c:{u:-x},b:"w",a:1a,h:"F"}},{j:"1c 1t m J (x&#t;)",d:1,g:1,f:{e:q,i:"o"},s:{c:{u:x},b:"w",a:1a,h:"F"}},{j:"B v 17 1t m E (x&#t;)",d:1,g:1,f:{e:q,i:"k"},s:{c:{I:.8,1r:7,u:10,y:1w},b:"1e",a:1x,h:"A"},C:{c:{1r:0,u:0,y:x},a:1x,b:"1e"}},{j:"B v 17 1t m r (x&#t;)",d:1,g:1,f:{e:q,i:"k"},s:{c:{I:.8,1r:-7,u:10,y:-1w},b:"1e",a:1x,h:"A"},C:{c:{1r:0,u:0,y:-x},a:1x,b:"1e"}},{j:"B v 17 1n m E (x&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"o"},M:{c:{I:.O},a:1l,b:"18"},s:{c:{y:x},b:"H",a:G,h:"A"},C:{a:1g,b:"H"}},{j:"B v 17 1n m r (x&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"D"},M:{c:{I:.O},a:1l,b:"18"},s:{c:{y:-x},b:"H",a:G,h:"A"},C:{a:1g,b:"H"}},{j:"B v 17 1n m L (x&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"1j-o"},M:{c:{I:.O},a:1l,b:"18"},s:{c:{u:-x},b:"H",a:G,h:"F"},C:{a:1g,b:"H"}},{j:"B v 17 1n m J (x&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"1j-D"},M:{c:{I:.O},a:1l,b:"18"},s:{c:{u:x},b:"H",a:G,h:"F"},C:{a:1g,b:"H"}},{j:"B v A 17 1n k (x&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"k"},M:{c:{I:.1i,u:-15},a:1o,b:"18"},s:{c:{y:q,u:15},b:"H",a:1o,h:"A"},C:{c:{y:x,u:0},a:1o,b:"H"}},{j:"B v F 17 1n k (x&#t;)",d:[2,4],g:[4,7],f:{e:q,i:"k"},M:{c:{I:.1i,y:15},a:1o,b:"18"},s:{c:{u:q,y:-15},b:"H",a:1o,h:"F"},C:{c:{u:x,y:0},a:1o,b:"H"}},{j:"1c d m E (x&#t;)",d:[5,9],g:1,f:{e:q,i:"o"},s:{c:{y:x},b:"w",a:1a,h:"A"}},{j:"1c d m r (x&#t;)",d:[5,9],g:1,f:{e:q,i:"o"},s:{c:{y:-x},b:"w",a:1a,h:"A"}},{j:"1B 17 d k (x&#t;)",d:[5,9],g:1,f:{e:q,i:"k"},s:{c:{y:x},b:"w",a:1a,h:"A"}},{j:"B v 17 d m E (x&#t;)",d:[5,9],g:1,f:{e:q,i:"o"},M:{c:{I:.O,u:3},a:p,b:"K"},s:{c:{y:22,u:0},b:"H",a:G,h:"A"},C:{c:{e:X,y:x},b:"K",a:p}},{j:"B v 17 d m r (x&#t;)",d:[5,9],g:1,f:{e:q,i:"D"},M:{c:{I:.O,u:3},a:p,b:"K"},s:{c:{y:-x,u:0},b:"H",a:G,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v 17 d m L (x&#t;)",d:[5,9],g:1,f:{e:q,i:"o"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-x},b:"H",a:G,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v 17 d m J (x&#t;)",d:[5,9],g:1,f:{e:q,i:"D"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:x},b:"H",a:G,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v A 17 d k (x&#t;)",d:[5,9],g:1,f:{e:q,i:"k"},M:{c:{I:.O,u:3},a:p,b:"K"},s:{c:{y:x,u:0},b:"H",a:G,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v F 17 d k (x&#t;)",d:[5,9],g:1,f:{e:q,i:"k"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-x},b:"H",a:G,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v A 17 1K d m E (x&#t;)",d:[7,11],g:1,f:{e:q,i:"o"},s:{c:{I:.O,u:5,y:1w},b:"18",a:G,h:"A"},C:{c:{u:0,y:x},b:"18",a:G}},{j:"B v A 17 1K d m r (x&#t;)",d:[7,11],g:1,f:{e:q,i:"D"},s:{c:{I:.O,u:5,y:-1w},b:"18",a:G,h:"A"},C:{c:{u:0,y:-x},b:"18",a:G}},{j:"1c P m L (x&#t;)",d:1,g:[5,9],f:{e:q,i:"o"},s:{c:{u:-x},b:"w",a:G,h:"F"}},{j:"1c P m J (x&#t;)",d:1,g:[5,9],f:{e:q,i:"D"},s:{c:{u:x},b:"w",a:G,h:"F"}},{j:"1C 17 P k (x&#t;)",d:1,g:[5,9],f:{e:q,i:"k"},s:{c:{u:-x},b:"w",a:G,h:"F"}},{j:"B v 17 P m L (x&#t;)",d:1,g:[7,11],f:{e:q,i:"o"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-x},b:"H",a:G,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v 17 P m J (x&#t;)",d:1,g:[7,11],f:{e:q,i:"D"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:x},b:"H",a:G,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v 17 P m E (x&#t;)",d:1,g:[7,11],f:{e:q,i:"o"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:x},b:"H",a:G,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v 17 P m r (x&#t;)",d:1,g:[7,11],f:{e:q,i:"D"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:-x},b:"H",a:G,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v A 17 P k (x&#t;)",d:1,g:[7,11],f:{e:q,i:"k"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:x},b:"H",a:G,h:"A"},C:{c:{e:X},b:"K",a:p}},{j:"B v F 17 P k (x&#t;)",d:1,g:[7,11],f:{e:q,i:"k"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-x},b:"H",a:G,h:"F"},C:{c:{e:X},b:"K",a:p}},{j:"B v F 17 1K P m E (x&#t;)",d:1,g:[7,11],f:{e:q,i:"o"},s:{c:{I:.O,u:1w,y:-5},b:"18",a:G,h:"F"},C:{c:{u:x,y:0},b:"18",a:G}},{j:"B v F 17 1K P m r (x&#t;)",d:1,g:[7,11],f:{e:q,i:"D"},s:{c:{I:.O,u:-1w,y:-5},b:"18",a:G,h:"F"},C:{c:{u:-x,y:0},b:"18",a:G}},{j:"1b 1t m E (l&#t;, T U)",d:1,g:1,f:{e:q,i:"o",U:"T"},s:{c:{y:l},b:"w",a:1a,h:"A"}},{j:"1b 1t m r (l&#t;, T U)",d:1,g:1,f:{e:q,i:"o",U:"T"},s:{c:{y:-l},b:"w",a:1a,h:"A"}},{j:"1b 1t m L (l&#t;, T U)",d:1,g:1,f:{e:q,i:"o",U:"T"},s:{c:{u:-l},b:"w",a:1a,h:"F"}},{j:"1b 1t m J (l&#t;, T U)",d:1,g:1,f:{e:q,i:"o",U:"T"},s:{c:{u:l},b:"w",a:1a,h:"F"}},{j:"B v S 1n m E (l&#t;, T U)",d:[2,4],g:[4,7],f:{e:q,i:"o",U:"T"},M:{c:{I:.O},a:1l,b:"18"},s:{c:{y:l},b:"H",a:G,h:"A"},C:{a:1g,b:"H"}},{j:"B v S 1n m r (l&#t;, T U)",d:[2,4],g:[4,7],f:{e:q,i:"D",U:"T"},M:{c:{I:.O},a:1l,b:"18"},s:{c:{y:-l},b:"H",a:G,h:"A"},C:{a:1g,b:"H"}},{j:"B v S 1n m L (l&#t;, T U)",d:[2,4],g:[4,7],f:{e:q,i:"1j-o",U:"T"},M:{c:{I:.O},a:1l,b:"18"},s:{c:{u:-l},b:"H",a:G,h:"F"},C:{a:1g,b:"H"}},{j:"B v S 1n m J (l&#t;, T U)",d:[2,4],g:[4,7],f:{e:q,i:"1j-D",U:"T"},M:{c:{I:.O},a:1l,b:"18"},s:{c:{u:l},b:"H",a:G,h:"F"},C:{a:1g,b:"H"}},{j:"B v A S 1n k (l&#t;, T U)",d:[2,4],g:[4,7],f:{e:q,i:"k",U:"T"},M:{c:{I:.1i},a:1o,b:"18"},s:{c:{y:l},b:"H",a:1o,h:"A"},C:{a:1o,b:"H"}},{j:"B v F S 1n k (l&#t;, T U)",d:[2,4],g:[4,7],f:{e:q,i:"k",U:"T"},M:{c:{I:.1i},a:1o,b:"18"},s:{c:{u:l},b:"H",a:1o,h:"F"},C:{a:1o,b:"H"}},{j:"B v S d m E (l&#t;, T U)",d:[5,9],g:1,f:{e:1i,i:"o",U:"T"},M:{c:{I:.O,u:3},a:p,b:"K"},s:{c:{y:l,u:-3},b:"w",a:1p,h:"A"},C:{c:{e:X,u:0},b:"z",a:1q}},{j:"B v S d m r (l&#t;, T U)",d:[5,9],g:1,f:{e:1i,i:"D",U:"T"},M:{c:{I:.O,u:3},a:p,b:"K"},s:{c:{y:-l,u:-3},b:"w",a:1p,h:"A"},C:{c:{e:X,u:0},b:"z",a:1q}},{j:"B v S d m L (l&#t;, T U)",d:[5,9],g:1,f:{e:1i,i:"o",U:"T"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-l},b:"H",a:G,h:"F"},C:{c:{e:X},b:"z",a:1q}},{j:"B v S d m J (l&#t;, T U)",d:[5,9],g:1,f:{e:1i,i:"D",U:"T"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:l},b:"H",a:G,h:"F"},C:{c:{e:X},b:"z",a:1q}},{j:"B v A S d k (l&#t;, T U)",d:[5,9],g:1,f:{e:1i,i:"k",U:"T"},M:{c:{I:.O,u:3},a:p,b:"K"},s:{c:{y:l,u:-3},b:"w",a:1p,h:"A"},C:{c:{e:X,u:0},b:"z",a:1q}},{j:"B v F S d k (l&#t;, T U)",d:[5,9],g:1,f:{e:1i,i:"k",U:"T"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-l},b:"H",a:G,h:"F"},C:{c:{e:X},b:"z",a:1q}},{j:"B v S P m L (l&#t;, T U)",d:1,g:[7,11],f:{e:1i,i:"o",U:"T"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-l},b:"w",a:1p,h:"F"},C:{c:{e:X},b:"z",a:1q}},{j:"B v S P m J (l&#t;, T U)",d:1,g:[7,11],f:{e:1i,i:"D",U:"T"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:l},b:"w",a:1p,h:"F"},C:{c:{e:X},b:"z",a:1q}},{j:"B v S P m E (l&#t;, T U)",d:1,g:[7,11],f:{e:1i,i:"o",U:"T"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:l},b:"H",a:G,h:"A"},C:{c:{e:X},b:"z",a:1q}},{j:"B v S P m r (l&#t;, T U)",d:1,g:[7,11],f:{e:1i,i:"D",U:"T"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:-l},b:"H",a:G,h:"A"},C:{c:{e:X},b:"z",a:1q}},{j:"B v A S P k (l&#t;, T U)",d:1,g:[7,11],f:{e:1i,i:"k",U:"T"},M:{c:{I:.O},a:p,b:"K"},s:{c:{y:l},b:"H",a:G,h:"A"},C:{c:{e:X},b:"z",a:1q}},{j:"B v F S P k (l&#t;, T U)",d:1,g:[7,11],f:{e:1i,i:"k",U:"T"},M:{c:{I:.O},a:p,b:"K"},s:{c:{u:-l},b:"w",a:1p,h:"F"},C:{c:{e:X},b:"z",a:1q}}]}',62,132,'||||||||||duration|easing|transition|rows|delay|tile|cols|direction|sequence|name|random|180|to|type|forward|600|75|left|animation|176|rotateX|and|easeInOutQuart|90|rotateY|easeOutQuart|horizontal|Scaling|after|reverse|right|vertical|1e3|easeInOutBack|scale3d|top|easeOutBack|bottom|before|from|85|columns|mixed|tiles|spinning|large|depth|750|slide|200|sliding|Fading||||Sliding|fade|||turning|easeInOutQuint|55|1500|Spinning|Turning|100|easeInOutQuad|50|350|scale|65|col|30|450|500|cuboids|700|1200|400|rotate|35|cuboid|Carousel|Flying|45|800|Smooth|rotating|95|Horizontal|Vertical|Mirror|91|easeInQuart|1300|fading|mirror|540|drunk|out|scaling|Drunk|in|colums|150|2e3|directions|topright|bottomleft|topleft|sliging|linear|850|layerSliderTransitions|var|bottomright|87|t3d|diagonal||Crossfading|t2d'.split('|')));

/*! Big banner - LayerSlider  -  http://kreaturamedia.com*/
function lsShowNotice(e,t,n){var r;if(typeof e=="string"){r=jQuery("#"+e)}else if(typeof e=="object"){r=e}var i,s;switch(t){case"jquery":i="multiple jQuery issue";s='It looks like that another plugin or your theme loads an extra copy of the jQuery library causing problems for LayerSlider to show your sliders. <strong>Please navigate on your WordPress admin area to the main page of LayerSlider and enable the "Put JS includes to body" option within the Troubleshooting & Advanced Settings box.</strong>';break;case"oldjquery":i="old jQuery issue";s="It looks like you are using an old version ("+n+') of the jQuery library. LayerSlider requires at least version 1.7.0 or newer. Please update jQuery to 1.10.x or higher. Important: Please do not use the jQuery Updater plugin on WordPress and do not update to 2.x version of jQuery because it is not compatible with older browsers like IE 7 & 8. <a href="http://support.kreaturamedia.com/faq/4/layerslider-for-wordpress/#group-13&entry-60">You can read more about updating jQuery by clicking here.</a>';break}r.addClass("ls-error");r.append('<p class="ls-exclam">!</p>');r.append('<p class="ls-error-title">LayerSlider: '+i+"</p>");r.append('<p class="ls-error-text">'+s+"</p>")}(function(e){e.fn.layerSlider=function(n){var r="1.7.0";var i=e.fn.jquery;var s=e(this);var o=function(e,t){var n=e.split(".");var r=t.split(".");for(var i=0;i<n.length;++i){if(r.length==i){return false}if(parseInt(n[i])==parseInt(r[i])){continue}else if(parseInt(n[i])>parseInt(r[i])){return false}else{return true}}if(n.length!=r.length){return true}return true};if(!o("1.8.0",i)){s.addClass("ls-norotate")}if(!o(r,i)){lsShowNotice(s,"oldjquery",i)}else{if((typeof n).match("object|undefined")){return this.each(function(e){new t(this,n)})}else{if(n==="data"){var u=e(this).data("LayerSlider").g;if(u){return u}}else if(n==="userInitData"){var a=e(this).data("LayerSlider").o;if(a){return a}}else if(n==="defaultInitData"){var a=e(this).data("LayerSlider").defaults;if(a){return a}}else{return this.each(function(t){var r=e(this).data("LayerSlider");if(r){if(!r.g.isAnimating&&!r.g.isLoading){if(typeof n=="number"){if(n>0&&n<r.g.layersNum+1&&n!=r.g.curLayerIndex){r.change(n)}}else{switch(n){case"prev":r.o.cbPrev(r.g);r.prev("clicked");break;case"next":r.o.cbNext(r.g);r.next("clicked");break;case"start":if(!r.g.autoSlideshow){r.o.cbStart(r.g);r.g.originalAutoSlideshow=true;r.start()}break}}}if(n=="debug"){r.d.show()}if((r.g.autoSlideshow||!r.g.autoSlideshow&&r.g.originalAutoSlideshow)&&n=="stop"){r.o.cbStop(r.g);r.g.originalAutoSlideshow=false;r.g.curLayer.find('iframe[src*="youtube.com"], iframe[src*="youtu.be"], iframe[src*="player.vimeo"]').each(function(){clearTimeout(e(this).data("videoTimer"))});r.stop()}if(n=="forceStop"){r.forcestop()}}})}}}};var t=function(u,a){var f=this;f.$el=e(u).addClass("ls-container");f.$el.data("LayerSlider",f);f.load=function(){f.defaults=t.options;f.o=e.extend({},f.defaults,a);f.g=e.extend({},t.global);f.lt=e.extend({},t.layerTransitions);f.st=e.extend({},t.slideTransitions);f.g.enableCSS3=e(u).hasClass("ls-norotate")?false:true;f.g.originalMarkup=e(u).html();if(f.g.ie78){f.o.lazyLoad=false}if(f.o.autoPauseSlideshow==="enabled"){f.o.autoPauseSlideshow=true}if(f.o.autoPauseSlideshow==="disabled"){f.o.autoPauseSlideshow=false}if(typeof layerSliderTransitions!=="undefined"){f.t=e.extend({},layerSliderTransitions)}if(typeof layerSliderCustomTransitions!=="undefined"){f.ct=e.extend({},layerSliderCustomTransitions)}if(!f.g.initialized){f.g.initialized=true;f.debug();if(e("html").find('meta[content*="WordPress"]').length){f.g.wpVersion=e("html").find('meta[content*="WordPress"]').attr("content").split("WordPress")[1]}if(e("html").find('script[src*="layerslider"]').length){if(e("html").find('script[src*="layerslider"]').attr("src").indexOf("?")!=-1){f.g.lswpVersion=e("html").find('script[src*="layerslider"]').attr("src").split("?")[1].split("=")[1]}}f.d.aT("LayerSlider controls");f.d.aU('<a href="#">prev</a> | <a href="#">next</a> | <a href="#">start</a> | <a href="#">stop</a> | <a href="#">force stop</a>');f.d.history.find("a").each(function(){e(this).click(function(t){t.preventDefault();e(u).layerSlider(e(this).text())})});f.d.aT("LayerSlider version information");f.d.aU("JS version: <strong>"+f.g.version+"</strong>");if(f.g.lswpVersion){f.d.aL("WP version: <strong>"+f.g.lswpVersion+"</strong>")}if(f.g.wpVersion){f.d.aL("WordPress version: <strong>"+f.g.wpVersion+"</strong>")}f.d.aL("jQuery version: <strong>"+e().jquery+"</strong>");if(e(u).attr("id")){f.d.aT("LayerSlider container");f.d.aU("#"+e(u).attr("id"))}if(!f.o.skin||f.o.skin==""||!f.o.skinsPath||f.o.skinsPath==""){f.d.aT("Loading without skin. Possibilities: mistyped skin and / or skinsPath.");f.init()}else{f.d.aT("Trying to load with skin: "+f.o.skin,true);e(u).addClass("ls-"+f.o.skin);var n=f.o.skinsPath+f.o.skin+"/skin.css";cssContainer=e("head");if(!e("head").length){cssContainer=e("body")}if(e('link[href="'+n+'"]').length){f.d.aU('Skin "'+f.o.skin+'" is already loaded.');r=e('link[href="'+n+'"]');if(!f.g.loaded){f.g.loaded=true;f.g.t1=setTimeout(function(){f.init()},150)}}else{if(document.createStyleSheet){document.createStyleSheet(n);var r=e('link[href="'+n+'"]')}else{var r=e('<link rel="stylesheet" href="'+n+'" type="text/css" />').appendTo(cssContainer)}}r.load(function(){if(!f.g.loaded){f.d.aU("curSkin.load(); fired");f.g.loaded=true;f.g.t2=setTimeout(function(){f.init()},150)}});e(window).load(function(){if(!f.g.loaded){f.d.aU("$(window).load(); fired");f.g.loaded=true;f.g.t3=setTimeout(function(){f.init()},150)}});f.g.t4=setTimeout(function(){if(!f.g.loaded){f.d.aT("Fallback mode: Neither curSkin.load(); or $(window).load(); were fired");f.g.loaded=true;f.init()}},1e3)}}};f.init=function(){e(u).prependTo(e(f.o.appendTo));if(!e("html").attr("id")){e("html").attr("id","ls-global")}else if(!e("body").attr("id")){e("body").attr("id","ls-global")}if(f.g.isMobile()===true&&f.o.hideOnMobile===true){e(u).addClass("ls-forcehide");e(u).closest(".ls-wp-fullwidth-container").addClass("ls-forcehide")}var t=function(){if(f.o.hideOnMobile===true&&f.g.isMobile()===true){e(u).addClass("ls-forcehide");e(u).closest(".ls-wp-fullwidth-container").addClass("ls-forcehide");f.o.autoStart=false}else{if(e(window).width()<f.o.hideUnder||e(window).width()>f.o.hideOver){e(u).addClass("ls-forcehide");e(u).closest(".ls-wp-fullwidth-container").addClass("ls-forcehide")}else{e(u).removeClass("ls-forcehide");e(u).closest(".ls-wp-fullwidth-container").removeClass("ls-forcehide")}}};e(window).resize(function(){t()});t();f.g.sliderWidth=function(){return e(u).width()};f.g.sliderHeight=function(){return e(u).height()};e(u).find(".ls-layer").removeClass("ls-layer").addClass("ls-slide");e(u).find('.ls-slide > *[class*="ls-s"]').each(function(){var t=e(this).attr("class").split("ls-s")[1].split(" ")[0];e(this).removeClass("ls-s"+t).addClass("ls-l"+t)});if(f.o.firstLayer){f.o.firstSlide=f.o.firstLayer}if(f.o.animateFirstLayer===false){f.o.animateFirstSlide=false}if(e(u).find(".ls-slide").length==1){f.o.autoStart=false;f.o.navPrevNext=false;f.o.navStartStop=false;f.o.navButtons=false;f.o.loops=0;f.o.forceLoopNum=false;f.o.autoPauseSlideshow=true;f.o.firstSlide=1;f.o.thumbnailNavigation="disabled"}if(e(u).parent().hasClass("ls-wp-fullwidth-helper")){e(u)[0].style.width="100%"}if(f.o.width){f.g.sliderOriginalWidthRU=f.g.sliderOriginalWidth=""+f.o.width}else{f.g.sliderOriginalWidthRU=f.g.sliderOriginalWidth=e(u)[0].style.width}if(f.o.height){f.g.sliderOriginalHeight=""+f.o.height}else{f.g.sliderOriginalHeight=e(u)[0].style.height}if(f.g.sliderOriginalWidth.indexOf("%")==-1&&f.g.sliderOriginalWidth.indexOf("px")==-1){f.g.sliderOriginalWidth+="px"}if(f.g.sliderOriginalHeight.indexOf("%")==-1&&f.g.sliderOriginalHeight.indexOf("px")==-1){f.g.sliderOriginalHeight+="px"}if(f.o.responsive&&f.g.sliderOriginalWidth.indexOf("px")!=-1&&f.g.sliderOriginalHeight.indexOf("px")!=-1){f.g.responsiveMode=true}else{f.g.responsiveMode=false}if(f.o.fullScreen===true){f.o.responsiveUnder=0;f.g.responsiveMode=true;if(f.g.sliderOriginalWidth.indexOf("%")!=-1){f.g.sliderOriginalWidth=parseInt(f.g.sliderOriginalWidth)+"px"}if(f.g.sliderOriginalHeight.indexOf("%")!=-1){f.g.sliderOriginalHeight=parseInt(f.g.sliderOriginalHeight)+"px"}}e(u).find('*[class*="ls-l"], *[class*="ls-bg"]').each(function(){if(!e(this).parent().hasClass("ls-slide")){e(this).insertBefore(e(this).parent())}});e(u).find(".ls-slide").each(function(){e(this).children(':not([class*="ls-"])').each(function(){e(this).remove()});var t=e("<div>").addClass("ls-gpuhack");if(e(this).find(".ls-bg").length){t.insertAfter(e(this).find(".ls-bg").eq("0"))}else{t.prependTo(e(this))}});e(u).find('.ls-slide, *[class*="ls-l"]').each(function(){if(e(this).data("ls")||e(this).attr("rel")||e(this).attr("style")){if(e(this).data("ls")){var t=e(this).data("ls").toLowerCase().split(";")}else if(e(this).attr("rel")&&e(this).attr("rel").indexOf(":")!=-1&&e(this).attr("rel").indexOf(";")!=-1){var t=e(this).attr("rel").toLowerCase().split(";")}else{var t=e(this).attr("style").toLowerCase().split(";")}for(x=0;x<t.length;x++){param=t[x].split(":");if(param[0].indexOf("easing")!=-1){param[1]=f.ieEasing(param[1])}var n="";if(param[2]){n=":"+e.trim(param[2])}if(param[0]!=" "&&param[0]!=""){e(this).data(e.trim(param[0]),e.trim(param[1])+n)}}}if(f.o.startInViewport===true&&f.o.autoStart===true){f.o.autoStart=false;f.g.originalAutoStart=true}var r=e(this);r.data("originalLeft",r[0].style.left);r.data("originalTop",r[0].style.top);if(e(this).is("a")&&e(this).children().length>0){r=e(this).children()}var i=r.width();var s=r.height();if(r[0].style.width&&r[0].style.width.indexOf("%")!=-1){i=r[0].style.width}if(r[0].style.height&&r[0].style.height.indexOf("%")!=-1){s=r[0].style.height}r.data("originalWidth",i);r.data("originalHeight",s);r.data("originalPaddingLeft",r.css("padding-left"));r.data("originalPaddingRight",r.css("padding-right"));r.data("originalPaddingTop",r.css("padding-top"));r.data("originalPaddingBottom",r.css("padding-bottom"));var o=typeof parseFloat(r.css("opacity"))=="number"?Math.round(parseFloat(r.css("opacity"))*100)/100:1;e(this).data("originalOpacity",o);if(r.css("border-left-width").indexOf("px")==-1){r.data("originalBorderLeft",r[0].style.borderLeftWidth)}else{r.data("originalBorderLeft",r.css("border-left-width"))}if(r.css("border-right-width").indexOf("px")==-1){r.data("originalBorderRight",r[0].style.borderRightWidth)}else{r.data("originalBorderRight",r.css("border-right-width"))}if(r.css("border-top-width").indexOf("px")==-1){r.data("originalBorderTop",r[0].style.borderTopWidth)}else{r.data("originalBorderTop",r.css("border-top-width"))}if(r.css("border-bottom-width").indexOf("px")==-1){r.data("originalBorderBottom",r[0].style.borderBottomWidth)}else{r.data("originalBorderBottom",r.css("border-bottom-width"))}r.data("originalFontSize",r.css("font-size"));r.data("originalLineHeight",r.css("line-height"))});if(document.location.hash){for(var n=0;n<e(u).find(".ls-slide").length;n++){if(e(u).find(".ls-slide").eq(n).data("deeplink")==document.location.hash.split("#")[1]){f.o.firstSlide=n+1}}}e(u).find('*[class*="ls-linkto-"]').each(function(){var t=e(this).attr("class").split(" ");for(var n=0;n<t.length;n++){if(t[n].indexOf("ls-linkto-")!=-1){var r=parseInt(t[n].split("ls-linkto-")[1]);e(this).css({cursor:"pointer"}).click(function(t){t.preventDefault();e(u).layerSlider(r)})}}});f.g.layersNum=e(u).find(".ls-slide").length;if(f.o.randomSlideshow&&f.g.layersNum>2){f.o.firstSlide=="random";f.o.twoWaySlideshow=false}else{f.o.randomSlideshow=false}if(f.o.firstSlide=="random"){f.o.firstSlide=Math.floor(Math.random()*f.g.layersNum+1)}f.o.fisrtSlide=f.o.fisrtSlide<f.g.layersNum+1?f.o.fisrtSlide:1;f.o.fisrtSlide=f.o.fisrtSlide<1?1:f.o.fisrtSlide;f.g.nextLoop=1;if(f.o.animateFirstSlide){f.g.nextLoop=0}var r=document.location.href.indexOf("file:")===-1?"":"http:";e(u).find('iframe[src*="youtube.com"], iframe[src*="youtu.be"]').each(function(){e(this).parent().addClass("ls-video-layer");if(e(this).parent('[class*="ls-l"]')){var t=e(this);var n=r;e.getJSON(n+"//gdata.youtube.com/feeds/api/videos/"+e(this).attr("src").split("embed/")[1].split("?")[0]+"?v=2&alt=json&callback=?",function(e){t.data("videoDuration",parseInt(e["entry"]["media$group"]["yt$duration"]["seconds"])*1e3)});var i=e("<div>").addClass("ls-vpcontainer").appendTo(e(this).parent());e("<img>").appendTo(i).addClass("ls-videopreview").attr("alt","Play video").attr("src",n+"//img.youtube.com/vi/"+e(this).attr("src").split("embed/")[1].split("?")[0]+"/"+f.o.youtubePreview);e("<div>").appendTo(i).addClass("ls-playvideo");e(this).parent().css({width:e(this).width(),height:e(this).height()}).click(function(){if(e(this).data("showuntil")>0&&e(this).data("showUntilTimer")){clearTimeout(e(this).data("showUntilTimer"))}f.g.isAnimating=true;if(f.g.paused){if(f.o.autoPauseSlideshow!=false){f.g.paused=false}f.g.originalAutoSlideshow=true}else{f.g.originalAutoSlideshow=f.g.autoSlideshow}if(f.o.autoPauseSlideshow!=false){f.stop()}f.g.pausedByVideo=true;n=e(this).find("iframe").data("videoSrc").indexOf("http")===-1?r:"";e(this).find("iframe").attr("src",n+e(this).find("iframe").data("videoSrc"));e(this).find(".ls-vpcontainer").delay(f.g.v.d).fadeOut(f.g.v.fo,function(){if(f.o.autoPauseSlideshow=="auto"&&f.g.originalAutoSlideshow==true){var e=setTimeout(function(){f.start()},t.data("videoDuration")-f.g.v.d);t.data("videoTimer",e)}f.g.isAnimating=false;if(f.g.resize==true){f.makeResponsive(f.g.curLayer,function(){f.g.resize=false})}})});var s="&";if(e(this).attr("src").indexOf("?")==-1){s="?"}var o="&wmode=opaque&html5=1";if(e(this).attr("src").indexOf("autoplay")==-1){e(this).data("videoSrc",e(this).attr("src")+s+"autoplay=1"+o)}else{e(this).data("videoSrc",e(this).attr("src").replace("autoplay=0","autoplay=1")+o)}e(this).data("originalWidth",e(this).attr("width"));e(this).data("originalHeight",e(this).attr("height"));e(this).attr("src","")}});e(u).find('iframe[src*="player.vimeo"]').each(function(){e(this).parent().addClass("ls-video-layer");if(e(this).parent('[class*="ls-l"]')){var t=e(this);var n=r;var i=e("<div>").addClass("ls-vpcontainer").appendTo(e(this).parent());e.getJSON(n+"//vimeo.com/api/v2/video/"+e(this).attr("src").split("video/")[1].split("?")[0]+".json?callback=?",function(n){e("<img>").appendTo(i).addClass("ls-videopreview").attr("alt","Play video").attr("src",n[0]["thumbnail_large"]);t.data("videoDuration",parseInt(n[0]["duration"])*1e3);e("<div>").appendTo(i).addClass("ls-playvideo")});e(this).parent().css({width:e(this).width(),height:e(this).height()}).click(function(){if(e(this).data("showuntil")>0&&e(this).data("showUntilTimer")){clearTimeout(e(this).data("showUntilTimer"))}f.g.isAnimating=true;if(f.g.paused){if(f.o.autoPauseSlideshow!=false){f.g.paused=false}f.g.originalAutoSlideshow=true}else{f.g.originalAutoSlideshow=f.g.autoSlideshow}if(f.o.autoPauseSlideshow!=false){f.stop()}f.g.pausedByVideo=true;n=e(this).find("iframe").data("videoSrc").indexOf("http")===-1?r:"";e(this).find("iframe").attr("src",n+e(this).find("iframe").data("videoSrc"));e(this).find(".ls-vpcontainer").delay(f.g.v.d).fadeOut(f.g.v.fo,function(){if(f.o.autoPauseSlideshow=="auto"&&f.g.originalAutoSlideshow==true){var e=setTimeout(function(){f.start()},t.data("videoDuration")-f.g.v.d);t.data("videoTimer",e)}f.g.isAnimating=false;if(f.g.resize==true){f.makeResponsive(f.g.curLayer,function(){f.g.resize=false})}})});var s="&";if(e(this).attr("src").indexOf("?")==-1){s="?"}var o="&wmode=opaque";if(e(this).attr("src").indexOf("autoplay")==-1){e(this).data("videoSrc",e(this).attr("src")+s+"autoplay=1"+o)}else{e(this).data("videoSrc",e(this).attr("src").replace("autoplay=0","autoplay=1")+o)}e(this).data("originalWidth",e(this).attr("width"));e(this).data("originalHeight",e(this).attr("height"));e(this).attr("src","")}});e(u).find("video, audio").each(function(){var t=typeof e(this).attr("width")!=="undefined"?e(this).attr("width"):"640";var n=typeof e(this).attr("height")!=="undefined"?e(this).attr("height"):""+e(this).height();if(t.indexOf("%")===-1){t=parseInt(t)}if(n.indexOf("%")===-1){n=parseInt(n)}if(t==="100%"&&(n===0||n==="0"||n==="100%")){e(this).attr("height","100%");n="auto"}e(this).parent().addClass("ls-video-layer").css({width:t,height:n}).data({originalWidth:t,originalHeight:n});var r=e(this);e(this).removeAttr("width").removeAttr("height").css({width:"100%",height:"100%"}).click(function(){this.play();f.g.isAnimating=true;if(f.g.paused){if(f.o.autoPauseSlideshow!==false){f.g.paused=false}f.g.originalAutoSlideshow=true}else{f.g.originalAutoSlideshow=f.g.autoSlideshow}if(f.o.autoPauseSlideshow!==false){f.stop()}f.g.pausedByVideo=true;e(this).on("ended",function(){if(f.o.autoPauseSlideshow==="auto"&&f.g.originalAutoSlideshow===true){f.start()}});f.g.isAnimating=false;if(f.g.resize===true){f.makeResponsive(f.g.curLayer,function(){f.g.resize=false})}})});if(f.o.animateFirstSlide){f.o.firstSlide=f.o.firstSlide-1===0?f.g.layersNum:f.o.firstSlide-1}f.g.curLayerIndex=f.o.firstSlide;f.g.curLayer=e(u).find(".ls-slide:eq("+(f.g.curLayerIndex-1)+")");e(u).find(".ls-slide").wrapAll('<div class="ls-inner"></div>');if(f.o.showBarTimer){f.g.barTimer=e("<div>").addClass("ls-bar-timer").appendTo(e(u).find(".ls-inner"))}if(f.o.showCircleTimer&&!f.g.ie78){f.g.circleTimer=e("<div>").addClass("ls-circle-timer").appendTo(e(u).find(".ls-inner"));f.g.circleTimer.append(e('<div class="ls-ct-left"><div class="ls-ct-rotate"><div class="ls-ct-hider"><div class="ls-ct-half"></div></div></div></div><div class="ls-ct-right"><div class="ls-ct-rotate"><div class="ls-ct-hider"><div class="ls-ct-half"></div></div></div></div><div class="ls-ct-center"></div>'))}f.g.li=e("<div>").css({zIndex:-1,display:"none"}).addClass("ls-loading-container").appendTo(e(u));e("<div>").addClass("ls-loading-indicator").appendTo(f.g.li);if(e(u).css("position")=="static"){e(u).css("position","relative")}if(f.o.globalBGImage){e(u).find(".ls-inner").css({backgroundImage:"url("+f.o.globalBGImage+")"})}else{e(u).find(".ls-inner").css({backgroundColor:f.o.globalBGColor})}if(f.o.globalBGColor=="transparent"&&f.o.globalBGImage==false){e(u).find(".ls-inner").css({background:"none transparent !important"})}e(u).find(".ls-slide img").each(function(){e(this).removeAttr("width").removeAttr("height");if(f.o.imgPreload===true&&f.o.lazyLoad===true){if(typeof e(this).data("src")!=="string"){e(this).data("src",e(this).attr("src"));var t=f.o.skinsPath+"../img/blank.gif";e(this).attr("src",t)}}else{if(typeof e(this).data("src")==="string"){e(this).attr("src",e(this).data("src"));e(this).removeAttr("data-src")}}});e(u).find(".ls-slide").on("mouseenter",function(t){f.g.parallaxStartX=t.pageX-e(this).parent().offset().left;f.g.parallaxStartY=t.pageY-e(this).parent().offset().top});e(u).find(".ls-slide").on("mousemove",function(t){var n=e(this).parent().offset().left+f.g.parallaxStartX;var r=e(this).parent().offset().top+f.g.parallaxStartY;var i=t.pageX-n;var s=t.pageY-r;e(this).find("> *:not(.ls-bg)").each(function(){if(typeof e(this).data("parallaxlevel")!=="undefined"&&parseInt(e(this).data("parallaxlevel"))!==0){e(this).css({marginLeft:-i/100*parseInt(e(this).data("parallaxlevel")),marginTop:-s/100*parseInt(e(this).data("parallaxlevel"))})}})});e(u).find(".ls-slide").on("mouseleave",function(){e(this).find("> *:not(.ls-bg)").each(function(){if(typeof e(this).data("parallaxlevel")!=="undefined"&&parseInt(e(this).data("parallaxlevel"))!==0){TweenLite.to(this,.4,{css:{marginLeft:0,marginTop:0}})}})});if(f.o.navPrevNext){e('<a class="ls-nav-prev" href="#" />').click(function(t){t.preventDefault();e(u).layerSlider("prev")}).appendTo(e(u));e('<a class="ls-nav-next" href="#" />').click(function(t){t.preventDefault();e(u).layerSlider("next")}).appendTo(e(u));if(f.o.hoverPrevNext){e(u).find(".ls-nav-prev, .ls-nav-next").css({display:"none"});e(u).hover(function(){if(!f.g.forceHideControls){if(f.g.ie78){e(u).find(".ls-nav-prev, .ls-nav-next").css("display","block")}else{e(u).find(".ls-nav-prev, .ls-nav-next").stop(true,true).fadeIn(300)}}},function(){if(f.g.ie78){e(u).find(".ls-nav-prev, .ls-nav-next").css("display","none")}else{e(u).find(".ls-nav-prev, .ls-nav-next").stop(true,true).fadeOut(300)}})}}if(f.o.navStartStop||f.o.navButtons){var i=e('<div class="ls-bottom-nav-wrapper" />').appendTo(e(u));f.g.bottomWrapper=i;if(f.o.thumbnailNavigation=="always"){i.addClass("ls-above-thumbnails")}if(f.o.navButtons&&f.o.thumbnailNavigation!="always"){e('<span class="ls-bottom-slidebuttons" />').appendTo(e(u).find(".ls-bottom-nav-wrapper"));if(f.o.thumbnailNavigation=="hover"){var s=e('<div class="ls-thumbnail-hover"><div class="ls-thumbnail-hover-inner"><div class="ls-thumbnail-hover-bg"></div><div class="ls-thumbnail-hover-img"><img></div><span></span></div></div>').appendTo(e(u).find(".ls-bottom-slidebuttons"))}for(x=1;x<f.g.layersNum+1;x++){var o=e('<a href="#" />').appendTo(e(u).find(".ls-bottom-slidebuttons")).click(function(t){t.preventDefault();e(u).layerSlider(e(this).index()+1)});if(f.o.thumbnailNavigation=="hover"){e(u).find(".ls-thumbnail-hover, .ls-thumbnail-hover-img").css({width:f.o.tnWidth,height:f.o.tnHeight});var a=e(u).find(".ls-thumbnail-hover");var l=a.find("img").css({height:f.o.tnHeight});var c=e(u).find(".ls-thumbnail-hover-inner").css({visibility:"hidden",display:"block"});o.hover(function(){var t=e(u).find(".ls-slide").eq(e(this).index());var n;if(f.o.imgPreload===true&&f.o.lazyLoad===true){if(t.find(".ls-tn").length){n=t.find(".ls-tn").data("src")}else if(t.find(".ls-videopreview").length){n=t.find(".ls-videopreview").attr("src")}else if(t.find(".ls-bg").length){n=t.find(".ls-bg").data("src")}else{n=f.o.skinsPath+f.o.skin+"/nothumb.png"}}else{if(t.find(".ls-tn").length){n=t.find(".ls-tn").attr("src")}else if(t.find(".ls-videopreview").length){n=t.find(".ls-videopreview").attr("src")}else if(t.find(".ls-bg").length){n=t.find(".ls-bg").attr("src")}else{n=f.o.skinsPath+f.o.skin+"/nothumb.png"}}e(u).find(".ls-thumbnail-hover-img").css({left:parseInt(a.css("padding-left")),top:parseInt(a.css("padding-top"))});l.load(function(){if(e(this).width()==0){l.css({position:"relative",margin:"0 auto",left:"auto"})}else{l.css({position:"absolute",marginLeft:-e(this).width()/2,left:"50%"})}}).attr("src",n);a.css({display:"block"}).stop().animate({left:e(this).position().left+(e(this).width()-a.outerWidth())/2},250);c.css({display:"none",visibility:"visible"}).stop().fadeIn(250)},function(){c.stop().fadeOut(250,function(){a.css({visibility:"hidden",display:"block"})})})}}if(f.o.thumbnailNavigation=="hover"){s.appendTo(e(u).find(".ls-bottom-slidebuttons"))}e(u).find(".ls-bottom-slidebuttons a:eq("+(f.o.firstSlide-1)+")").addClass("ls-nav-active")}if(f.o.navStartStop){var h=e('<a class="ls-nav-start" href="#" />').click(function(t){t.preventDefault();e(u).layerSlider("start")}).prependTo(e(u).find(".ls-bottom-nav-wrapper"));var p=e('<a class="ls-nav-stop" href="#" />').click(function(t){t.preventDefault();e(u).layerSlider("stop")}).appendTo(e(u).find(".ls-bottom-nav-wrapper"))}else if(f.o.thumbnailNavigation!="always"){e('<span class="ls-nav-sides ls-nav-sideleft" />').prependTo(e(u).find(".ls-bottom-nav-wrapper"));e('<span class="ls-nav-sides ls-nav-sideright" />').appendTo(e(u).find(".ls-bottom-nav-wrapper"))}if(f.o.hoverBottomNav&&f.o.thumbnailNavigation!="always"){i.css({display:"none"});e(u).hover(function(){if(!f.g.forceHideControls){if(f.g.ie78){i.css("display","block")}else{i.stop(true,true).fadeIn(300)}}},function(){if(f.g.ie78){i.css("display","none")}else{i.stop(true,true).fadeOut(300)}})}}if(f.o.thumbnailNavigation=="always"){f.g.thumbsWrapper=e('<div class="ls-thumbnail-wrapper"></div>').appendTo(e(u));var s=e('<div class="ls-thumbnail"><div class="ls-thumbnail-inner"><div class="ls-thumbnail-slide-container"><div class="ls-thumbnail-slide"></div></div></div></div>').appendTo(f.g.thumbsWrapper);f.g.thumbnails=e(u).find(".ls-thumbnail-slide-container");if(!("ontouchstart"in window)){f.g.thumbnails.hover(function(){e(this).addClass("ls-thumbnail-slide-hover")},function(){e(this).removeClass("ls-thumbnail-slide-hover");f.scrollThumb()}).mousemove(function(t){var n=parseInt(t.pageX-e(this).offset().left)/e(this).width()*(e(this).width()-e(this).find(".ls-thumbnail-slide").width());e(this).find(".ls-thumbnail-slide").stop().css({marginLeft:n})})}else{f.g.thumbnails.addClass("ls-touchscroll")}e(u).find(".ls-slide").each(function(){var t=e(this).index()+1;var n;if(f.o.imgPreload===true&&f.o.lazyLoad===true){if(e(this).find(".ls-tn").length){n=e(this).find(".ls-tn").data("src")}else if(e(this).find(".ls-videopreview").length){n=e(this).find(".ls-videopreview").attr("src")}else if(e(this).find(".ls-bg").length){n=e(this).find(".ls-bg").data("src")}else{n=f.o.skinsPath+f.o.skin+"/nothumb.png"}}else{if(e(this).find(".ls-tn").length){n=e(this).find(".ls-tn").attr("src")}else if(e(this).find(".ls-videopreview").length){n=e(this).find(".ls-videopreview").attr("src")}else if(e(this).find(".ls-bg").length){n=e(this).find(".ls-bg").attr("src")}else{n=f.o.skinsPath+f.o.skin+"/nothumb.png"}}var r=e('<a href="#" class="ls-thumb-'+t+'"><img src="'+n+'"></a>');r.appendTo(e(u).find(".ls-thumbnail-slide"));if(!("ontouchstart"in window)){r.hover(function(){e(this).children().stop().fadeTo(300,f.o.tnActiveOpacity/100)},function(){if(!e(this).children().hasClass("ls-thumb-active")){e(this).children().stop().fadeTo(300,f.o.tnInactiveOpacity/100)}})}r.click(function(n){n.preventDefault();e(u).layerSlider(t)})});if(h&&p){var d=f.g.bottomWrapper=e('<div class="ls-bottom-nav-wrapper ls-below-thumbnails"></div>').appendTo(e(u));h.clone().click(function(t){t.preventDefault();e(u).layerSlider("start")}).appendTo(d);p.clone().click(function(t){t.preventDefault();e(u).layerSlider("stop")}).appendTo(d)}if(f.o.hoverBottomNav){f.g.thumbsWrapper.css("display","none");if(d){f.g.bottomWrapper=d.css("display")=="block"?d:e(u).find(".ls-above-thumbnails");f.g.bottomWrapper.css("display","none")}e(u).hover(function(){e(u).addClass("ls-hover");if(!f.g.forceHideControls){if(f.g.ie78){f.g.thumbsWrapper.css("display","block");if(f.g.bottomWrapper){f.g.bottomWrapper.css("display","block")}}else{f.g.thumbsWrapper.stop(true,true).fadeIn(300);if(f.g.bottomWrapper){f.g.bottomWrapper.stop(true,true).fadeIn(300)}}}},function(){e(u).removeClass("ls-hover");if(f.g.ie78){f.g.thumbsWrapper.css("display","none");if(f.g.bottomWrapper){f.g.bottomWrapper.css("display","none")}}else{f.g.thumbsWrapper.stop(true,true).fadeOut(300);if(f.g.bottomWrapper){f.g.bottomWrapper.stop(true,true).fadeOut(300)}}})}}f.g.shadow=e('<div class="ls-shadow"></div>').appendTo(e(u));if(f.g.shadow.css("display")=="block"&&!f.g.shadow.find("img").length){f.g.showShadow=function(){f.g.shadow.css({display:"none",visibility:"visible"}).fadeIn(500,function(){f.g.showShadow=false})};f.g.shadowImg=e("<img>").attr("src",f.o.skinsPath+f.o.skin+"/shadow.png").appendTo(f.g.shadow);f.g.shadowBtmMod=typeof parseInt(e(u).css("padding-bottom"))=="number"?parseInt(e(u).css("padding-bottom")):0}f.resizeShadow();if(f.o.keybNav&&e(u).find(".ls-slide").length>1){e("body").bind("keydown",function(e){if(!f.g.isAnimating&&!f.g.isLoading){if(e.which==37){f.o.cbPrev(f.g);f.prev("clicked")}else if(e.which==39){f.o.cbNext(f.g);f.next("clicked")}}})}if("ontouchstart"in window&&e(u).find(".ls-slide").length>1&&f.o.touchNav){e(u).find(".ls-inner").bind("touchstart",function(e){var t=e.touches?e.touches:e.originalEvent.touches;if(t.length==1){f.g.touchStartX=f.g.touchEndX=t[0].clientX}});e(u).find(".ls-inner").bind("touchmove",function(e){var t=e.touches?e.touches:e.originalEvent.touches;if(t.length==1){f.g.touchEndX=t[0].clientX}if(Math.abs(f.g.touchStartX-f.g.touchEndX)>45){e.preventDefault()}});e(u).find(".ls-inner").bind("touchend",function(t){if(Math.abs(f.g.touchStartX-f.g.touchEndX)>45){if(f.g.touchStartX-f.g.touchEndX>0){f.o.cbNext(f.g);e(u).layerSlider("next")}else{f.o.cbPrev(f.g);e(u).layerSlider("prev")}}})}if(f.o.pauseOnHover==true&&e(u).find(".ls-slide").length>1){e(u).find(".ls-inner").hover(function(){f.o.cbPause(f.g);if(f.g.autoSlideshow){f.g.paused=true;f.stop();if(f.g.barTimer){f.g.barTimer.stop()}if(f.g.circleTimer){if(f.g.cttl){f.g.cttl.pause()}}f.g.pausedSlideTime=(new Date).getTime()}},function(){if(f.g.paused==true){f.start();f.g.paused=false}})}f.resizeSlider();if(f.o.yourLogo){f.g.yourLogo=e("<img>").addClass("ls-yourlogo").appendTo(e(u)).attr("style",f.o.yourLogoStyle).css({visibility:"hidden",display:"bock"}).load(function(){var t=0;if(!f.g.yourLogo){t=1e3}setTimeout(function(){f.g.yourLogo.data("originalWidth",f.g.yourLogo.width());f.g.yourLogo.data("originalHeight",f.g.yourLogo.height());if(f.g.yourLogo.css("left")!="auto"){f.g.yourLogo.data("originalLeft",f.g.yourLogo[0].style.left)}if(f.g.yourLogo.css("right")!="auto"){f.g.yourLogo.data("originalRight",f.g.yourLogo[0].style.right)}if(f.g.yourLogo.css("top")!="auto"){f.g.yourLogo.data("originalTop",f.g.yourLogo[0].style.top)}if(f.g.yourLogo.css("bottom")!="auto"){f.g.yourLogo.data("originalBottom",f.g.yourLogo[0].style.bottom)}if(f.o.yourLogoLink!=false){e("<a>").appendTo(e(u)).attr("href",f.o.yourLogoLink).attr("target",f.o.yourLogoTarget).css({textDecoration:"none",outline:"none"}).append(f.g.yourLogo)}f.g.yourLogo.css({display:"none",visibility:"visible"});f.resizeYourLogo()},t)}).attr("src",f.o.yourLogo)}e(window).resize(function(){f.g.resize=true;if(!f.g.isAnimating){f.makeResponsive(f.g.curLayer,function(){if(f.g.ltContainer){f.g.ltContainer.empty()}f.g.resize=false});if(f.g.yourLogo){f.resizeYourLogo()}}});f.g.showSlider=true;if(f.o.animateFirstSlide==true){if(f.o.autoStart){f.g.autoSlideshow=true;e(u).find(".ls-nav-start").addClass("ls-nav-start-active")}else{e(u).find(".ls-nav-stop").addClass("ls-nav-stop-active")}f.next()}else{f.imgPreload(f.g.curLayer,function(){f.g.curLayer.fadeIn(f.o.sliderFadeInDuration,function(){f.g.isLoading=false;e(this).addClass("ls-active");if(f.o.autoPlayVideos){e(this).delay(e(this).data("delayin")+25).queue(function(){e(this).find(".ls-videopreview").click();e(this).find("video, audio").each(function(){if(typeof e(this)[0].currentTime!==0){e(this)[0].currentTime=0}e(this).click()});e(this).dequeue()})}f.g.curLayer.find(' > *[class*="ls-l"]').each(function(){var t=e(this);if((!t.hasClass("ls-video-layer")||t.hasClass("ls-video-layer")&&f.o.autoPlayVideos===false)&&t.data("showuntil")>0){t.data("showUntilTimer",setTimeout(function(){f.sublayerShowUntil(t)},t.data("showuntil")))}})});f.changeThumb(f.g.curLayerIndex);if(f.o.autoStart){f.g.isLoading=false;f.start()}else{e(u).find(".ls-nav-stop").addClass("ls-nav-stop-active")}})}f.o.cbInit(e(u))};f.start=function(){if(f.g.autoSlideshow){if(f.g.prevNext=="prev"&&f.o.twoWaySlideshow){f.prev()}else{f.next()}}else{f.g.autoSlideshow=true;if(!f.g.isAnimating&&!f.g.isLoading){f.timer()}}e(u).find(".ls-nav-start").addClass("ls-nav-start-active");e(u).find(".ls-nav-stop").removeClass("ls-nav-stop-active")};f.timer=function(){if(e(u).find(".ls-active").data("ls")){var t=f.st.slideDelay}else{var t=f.o.slideDelay}var n=e(u).find(".ls-active").data("slidedelay")?parseInt(e(u).find(".ls-active").data("slidedelay")):t;if(!f.o.animateFirstSlide&&!e(u).find(".ls-active").data("slidedelay")){var r=e(u).find(".ls-slide:eq("+(f.o.firstSlide-1)+")").data("slidedelay");n=r?r:t}clearTimeout(f.g.slideTimer);if(f.g.pausedSlideTime){if(!f.g.startSlideTime){f.g.startSlideTime=(new Date).getTime()}if(f.g.startSlideTime>f.g.pausedSlideTime){f.g.pausedSlideTime=(new Date).getTime()}if(!f.g.curSlideTime){f.g.curSlideTime=n}f.g.curSlideTime-=f.g.pausedSlideTime-f.g.startSlideTime;f.g.pausedSlideTime=false;f.g.startSlideTime=(new Date).getTime()}else{f.g.curSlideTime=n;f.g.startSlideTime=(new Date).getTime()}f.g.curSlideTime=parseInt(f.g.curSlideTime);f.g.slideTimer=setTimeout(function(){f.g.startSlideTime=f.g.pausedSlideTime=f.g.curSlideTime=false;f.start()},f.g.curSlideTime);if(f.g.barTimer){f.g.barTimer.animate({width:f.g.sliderWidth()},f.g.curSlideTime,"linear",function(){e(this).css({width:0})})}if(f.g.circleTimer){var i=f.g.circleTimer.find(".ls-ct-right .ls-ct-rotate");var s=f.g.circleTimer.find(".ls-ct-left .ls-ct-rotate");if(f.g.circleTimer.css("display")=="none"){i.css({rotate:0});s.css({rotate:0});f.g.circleTimer.fadeIn(350)}if(!f.g.cttl){f.g.cttl=new TimelineLite;f.g.cttl.add(TweenLite.fromTo(i[0],n/2e3,{rotation:0},{ease:Linear.easeNone,rotation:180,onReverseComplete:function(){f.g.cttl=false}}));f.g.cttl.add(TweenLite.fromTo(s[0],n/2e3,{rotation:0},{ease:Linear.easeNone,rotation:180}))}else{f.g.cttl.resume()}}};f.stop=function(){f.g.pausedSlideTime=(new Date).getTime();if(f.g.barTimer){f.g.barTimer.stop()}if(f.g.circleTimer){if(f.g.cttl){f.g.cttl.pause()}}if(!f.g.paused&&!f.g.originalAutoSlideshow){e(u).find(".ls-nav-stop").addClass("ls-nav-stop-active");e(u).find(".ls-nav-start").removeClass("ls-nav-start-active")}clearTimeout(f.g.slideTimer);f.g.autoSlideshow=false};f.forcestop=function(){clearTimeout(f.g.slideTimer);f.g.autoSlideshow=false;clearTimeout(f.g.t1);clearTimeout(f.g.t2);clearTimeout(f.g.t3);clearTimeout(f.g.t4);clearTimeout(f.g.t5);if(f.g.barTimer){f.g.barTimer.stop()}if(f.g.circleTimer){if(f.g.cttl){f.g.cttl.pause()}}e(u).find("*").stop(true,false).dequeue();e(u).find(".ls-slide >").each(function(){if(e(this).data("tr")){e(this).data("tr").pause()}});if(!f.g.paused&&!f.g.originalAutoSlideshow){e(u).find(".ls-nav-stop").addClass("ls-nav-stop-active");e(u).find(".ls-nav-start").removeClass("ls-nav-start-active")}};f.restart=function(){e(u).find("*").stop();clearTimeout(f.g.slideTimer);f.change(f.g.curLayerIndex,f.g.prevNext)};f.ieEasing=function(t){if(e.trim(t.toLowerCase())=="swing"||e.trim(t.toLowerCase())=="linear"){return t.toLowerCase()}else{return t.replace("easeinout","easeInOut").replace("easein","easeIn").replace("easeout","easeOut").replace("quad","Quad").replace("quart","Quart").replace("cubic","Cubic").replace("quint","Quint").replace("sine","Sine").replace("expo","Expo").replace("circ","Circ").replace("elastic","Elastic").replace("back","Back").replace("bounce","Bounce")}};f.prev=function(e){if(f.g.curLayerIndex<2){f.g.nextLoop+=1}if(f.g.nextLoop>f.o.loops&&f.o.loops>0&&!e){f.g.nextLoop=0;f.stop();if(f.o.forceLoopNum==false){f.o.loops=0}}else{var t=f.g.curLayerIndex<2?f.g.layersNum:f.g.curLayerIndex-1;f.g.prevNext="prev";f.change(t,f.g.prevNext)}};f.next=function(e){if(!f.o.randomSlideshow){if(!(f.g.curLayerIndex<f.g.layersNum)){f.g.nextLoop+=1}if(f.g.nextLoop>f.o.loops&&f.o.loops>0&&!e){f.g.nextLoop=0;f.stop();if(f.o.forceLoopNum==false){f.o.loops=0}}else{var t=f.g.curLayerIndex<f.g.layersNum?f.g.curLayerIndex+1:1;f.g.prevNext="next";f.change(t,f.g.prevNext)}}else if(!e){var t=f.g.curLayerIndex;var n=function(){t=Math.floor(Math.random()*f.g.layersNum)+1;if(t==f.g.curLayerIndex){n()}else{f.g.prevNext="next";f.change(t,f.g.prevNext)}};n()}else if(e){var t=f.g.curLayerIndex<f.g.layersNum?f.g.curLayerIndex+1:1;f.g.prevNext="next";f.change(t,f.g.prevNext)}};f.change=function(t,n){f.g.startSlideTime=f.g.pausedSlideTime=f.g.curSlideTime=false;if(f.g.barTimer){f.g.barTimer.stop().delay(300).animate({width:0},450)}if(f.g.circleTimer){f.g.circleTimer.fadeOut(500);if(f.g.cttl){f.g.cttl.reverse().duration(.35)}}if(f.g.pausedByVideo==true){f.g.pausedByVideo=false;f.g.autoSlideshow=f.g.originalAutoSlideshow;f.g.curLayer.find('iframe[src*="youtube.com"], iframe[src*="youtu.be"], iframe[src*="player.vimeo"]').each(function(){e(this).parent().find(".ls-vpcontainer").fadeIn(f.g.v.fi,function(){e(this).parent().find("iframe").attr("src","")})});f.g.curLayer.find("video, audio").each(function(){this.pause()})}e(u).find('iframe[src*="youtube.com"], iframe[src*="youtu.be"], iframe[src*="player.vimeo"]').each(function(){clearTimeout(e(this).data("videoTimer"))});clearTimeout(f.g.slideTimer);f.g.nextLayerIndex=t;f.g.nextLayer=e(u).find(".ls-slide:eq("+(f.g.nextLayerIndex-1)+")");if(!n){if(f.g.curLayerIndex<f.g.nextLayerIndex){f.g.prevNext="next"}else{f.g.prevNext="prev"}}var r=0;if(e(u).find('iframe[src*="youtube.com"], iframe[src*="youtu.be"], iframe[src*="player.vimeo"]').length>0){r=f.g.v.fi}f.imgPreload(f.g.nextLayer,function(){f.animate()})};f.imgPreload=function(t,n){f.g.isLoading=true;if(f.g.showSlider){e(u).css({visibility:"visible"})}if(f.o.imgPreload){var r=[];var i=0;if(t.css("background-image")!="none"&&t.css("background-image").indexOf("url")!=-1&&!t.hasClass("ls-preloaded")&&!t.hasClass("ls-not-preloaded")){var s=t.css("background-image");s=s.match(/url\((.*)\)/)[1].replace(/"/gi,"");r[r.length]=[s,t]}t.find("img:not(.ls-preloaded, .ls-not-preloaded)").each(function(){if(f.o.lazyLoad===true){e(this).attr("src",e(this).data("src"))}r[r.length]=[e(this).attr("src"),e(this)]});t.find("*").each(function(){if(e(this).css("background-image")!="none"&&e(this).css("background-image").indexOf("url")!=-1&&!e(this).hasClass("ls-preloaded")&&!e(this).hasClass("ls-not-preloaded")){var t=e(this).css("background-image");t=t.match(/url\((.*)\)/)[1].replace(/"/gi,"");r[r.length]=[t,e(this)]}});if(r.length==0){e(".ls-thumbnail-wrapper, .ls-nav-next, .ls-nav-prev, .ls-bottom-nav-wrapper").css({visibility:"visible"});f.makeResponsive(t,n)}else{if(f.g.ie78){f.g.li.css("display","block")}else{f.g.li.delay(400).fadeIn(300)}var o=function(){f.g.li.stop(true,true).css({display:"none"});e(".ls-thumbnail-wrapper, .ls-nav-next, .ls-nav-prev, .ls-bottom-nav-wrapper").css({visibility:"visible"});if(navigator.userAgent.indexOf("Trident/7")!==-1||f.g.ie78){setTimeout(function(){f.makeResponsive(t,n)},50)}else{f.makeResponsive(t,n)}};for(x=0;x<r.length;x++){e("<img>").data("el",r[x]).load(function(){e(this).data("el")[1].addClass("ls-preloaded");if(++i==r.length){o()}}).error(function(){var t=e(this).data("el")[0].substring(e(this).data("el")[0].lastIndexOf("/")+1,e(this).data("el")[0].length);if(window.console){console.log('LayerSlider error:\r\n\r\nIt seems like the URL of the image or background image "'+t+'" is pointing to a wrong location and it cannot be loaded. Please check the URLs of all your images used in the slider.')}else{alert('LayerSlider error:\r\n\r\nIt seems like the URL of the image or background image "'+t+'" is pointing to a wrong location and it cannot be loaded. Please check the URLs of all your images used in the slider.')}e(this).addClass("ls-not-preloaded");if(++i==r.length){o()}}).attr("src",r[x][0])}}}else{e(".ls-thumbnail-wrapper, .ls-nav-next, .ls-nav-prev, .ls-bottom-nav-wrapper").css({visibility:"visible"});f.makeResponsive(t,n)}};f.makeResponsive=function(t,n){t.css({visibility:"hidden",display:"block"});if(f.g.showShadow){f.g.showShadow()}f.resizeSlider();if(f.o.thumbnailNavigation=="always"){f.resizeThumb()}t.children().each(function(){var t=e(this);var n=t.data("originalLeft")?t.data("originalLeft"):"0";var r=t.data("originalTop")?t.data("originalTop"):"0";if(t.is("a")&&t.children().length>0){t.css({display:"block"});t=t.children()}var i="auto";var s="auto";if(t.data("originalWidth")){if(typeof t.data("originalWidth")=="number"){i=parseInt(t.data("originalWidth"))*f.g.ratio}else if(t.data("originalWidth").indexOf("%")!=-1){i=t.data("originalWidth")}}if(t.data("originalHeight")){if(typeof t.data("originalHeight")=="number"){s=parseInt(t.data("originalHeight"))*f.g.ratio}else if(t.data("originalHeight").indexOf("%")!=-1){s=t.data("originalHeight")}}var o=t.data("originalPaddingLeft")?parseInt(t.data("originalPaddingLeft"))*f.g.ratio:0;var a=t.data("originalPaddingRight")?parseInt(t.data("originalPaddingRight"))*f.g.ratio:0;var l=t.data("originalPaddingTop")?parseInt(t.data("originalPaddingTop"))*f.g.ratio:0;var c=t.data("originalPaddingBottom")?parseInt(t.data("originalPaddingBottom"))*f.g.ratio:0;var h=t.data("originalBorderLeft")?parseInt(t.data("originalBorderLeft"))*f.g.ratio:0;var p=t.data("originalBorderRight")?parseInt(t.data("originalBorderRight"))*f.g.ratio:0;var d=t.data("originalBorderTop")?parseInt(t.data("originalBorderTop"))*f.g.ratio:0;var v=t.data("originalBorderBottom")?parseInt(t.data("originalBorderBottom"))*f.g.ratio:0;var m=t.data("originalFontSize");var g=t.data("originalLineHeight");if(f.g.responsiveMode||f.o.responsiveUnder>0){if(t.is("img")&&!t.hasClass("ls-bg")&&t.attr("src")){t.css({width:"auto",height:"auto"});if((i==0||i=="auto")&&typeof s=="number"&&s!=0){i=s/t.height()*t.width()}if((s==0||s=="auto")&&typeof i=="number"&&i!=0){s=i/t.width()*t.height()}if(i=="auto"){i=t.width()*f.g.ratio}if(s=="auto"){s=t.height()*f.g.ratio}t.css({width:i,height:s})}if(!t.is("img")){t.css({width:i,height:s,"font-size":parseInt(m)*f.g.ratio+"px","line-height":parseInt(g)*f.g.ratio+"px"})}if(t.is("div")&&t.find("iframe").data("videoSrc")){var y=t.find("iframe");y.attr("width",parseInt(y.data("originalWidth"))*f.g.ratio).attr("height",parseInt(y.data("originalHeight"))*f.g.ratio);t.css({width:parseInt(y.data("originalWidth"))*f.g.ratio,height:parseInt(y.data("originalHeight"))*f.g.ratio})}t.css({padding:l+"px "+a+"px "+c+"px "+o+"px ",borderLeftWidth:h+"px",borderRightWidth:p+"px",borderTopWidth:d+"px",borderBottomWidth:v+"px"})}if(!t.hasClass("ls-bg")){var b=t;if(t.parent().is("a")){t=t.parent()}var w=0;if(f.o.layersContainer){w=f.o.layersContainer>0?(f.g.sliderWidth()-f.o.layersContainer)/2:0}else if(f.o.sublayerContainer){w=f.o.sublayerContainer>0?(f.g.sliderWidth()-f.o.sublayerContainer)/2:0}w=w<0?0:w;if(n.indexOf("%")!=-1){t.css({left:f.g.sliderWidth()/100*parseInt(n)-b.width()/2-o-h})}else if(w>0||f.g.responsiveMode||f.o.responsiveUnder>0){t.css({left:w+parseInt(n)*f.g.ratio})}if(r.indexOf("%")!=-1){t.css({top:f.g.sliderHeight()/100*parseInt(r)-b.height()/2-l-d})}else if(f.g.responsiveMode||f.o.responsiveUnder>0){t.css({top:parseInt(r)*f.g.ratio})}}else{var E=e(u).find(".ls-inner");t.css({width:"auto",height:"auto"});i=t.width();s=t.height();var S=f.g.ratio;if(f.g.sliderOriginalWidth.indexOf("%")!=-1){if(f.g.sliderWidth()>i){S=f.g.sliderWidth()/i;if(f.g.sliderHeight()>s*S){S=f.g.sliderHeight()/s}}else if(f.g.sliderHeight()>s){S=f.g.sliderHeight()/s;if(f.g.sliderWidth()>i*S){S=f.g.sliderWidth()/i}}}t.css({width:i*S,height:s*S,marginLeft:E.width()/2-i*S/2,marginTop:E.height()/2-s*S/2})}});t.css({display:"none",visibility:"visible"});f.resizeShadow();n();e(this).dequeue()};f.resizeShadow=function(){if(f.g.shadowImg){var e=function(){if(f.g.shadowImg.height()>0){if(f.g.shadowBtmMod>0){f.g.shadow.css({height:f.g.shadowImg.height()/2})}else{f.g.shadow.css({height:f.g.shadowImg.height(),marginTop:-f.g.shadowImg.height()/2})}}else{setTimeout(function(){e()},50)}};e()}};f.resizeSlider=function(){if(f.o.responsiveUnder>0){if(e(window).width()<f.o.responsiveUnder){f.g.responsiveMode=true;f.g.sliderOriginalWidth=f.o.responsiveUnder+"px"}else{f.g.responsiveMode=false;f.g.sliderOriginalWidth=f.g.sliderOriginalWidthRU;f.g.ratio=1}}if(f.g.responsiveMode){var t=e(u).parent();if(f.o.fullScreen===true){e(u).css({width:"100%",height:e(window).height()})}else{e(u).css({width:t.width()-parseInt(e(u).css("padding-left"))-parseInt(e(u).css("padding-right"))});f.g.ratio=e(u).width()/parseInt(f.g.sliderOriginalWidth);e(u).css({height:f.g.ratio*parseInt(f.g.sliderOriginalHeight)})}}else{f.g.ratio=1;e(u).css({width:f.g.sliderOriginalWidth,height:f.g.sliderOriginalHeight})}if(e(u).closest(".ls-wp-fullwidth-container").length){e(u).closest(".ls-wp-fullwidth-helper").css({height:e(u).outerHeight(true)});e(u).closest(".ls-wp-fullwidth-container").css({height:e(u).outerHeight(true)});e(u).closest(".ls-wp-fullwidth-helper").css({width:e(window).width(),left:-e(u).closest(".ls-wp-fullwidth-container").offset().left});if(f.g.sliderOriginalWidth.indexOf("%")!=-1){var n=parseInt(f.g.sliderOriginalWidth);var r=e("body").width()/100*n-(e(u).outerWidth()-e(u).width());e(u).width(r)}}e(u).find(".ls-inner, .ls-lt-container").css({width:f.g.sliderWidth(),height:f.g.sliderHeight()});if(f.g.curLayer&&f.g.nextLayer){f.g.curLayer.css({width:f.g.sliderWidth(),height:f.g.sliderHeight()});f.g.nextLayer.css({width:f.g.sliderWidth(),height:f.g.sliderHeight()})}else{e(u).find(".ls-slide").css({width:f.g.sliderWidth(),height:f.g.sliderHeight()})}};f.resizeYourLogo=function(){f.g.yourLogo.css({width:f.g.yourLogo.data("originalWidth")*f.g.ratio,height:f.g.yourLogo.data("originalHeight")*f.g.ratio});if(f.g.ie78){f.g.yourLogo.css("display","block")}else{f.g.yourLogo.fadeIn(300)}var t=oR=oT=oB="auto";if(f.g.yourLogo.data("originalLeft")&&f.g.yourLogo.data("originalLeft").indexOf("%")!=-1){t=f.g.sliderWidth()/100*parseInt(f.g.yourLogo.data("originalLeft"))-f.g.yourLogo.width()/2+parseInt(e(u).css("padding-left"))}else{t=parseInt(f.g.yourLogo.data("originalLeft"))*f.g.ratio}if(f.g.yourLogo.data("originalRight")&&f.g.yourLogo.data("originalRight").indexOf("%")!=-1){oR=f.g.sliderWidth()/100*parseInt(f.g.yourLogo.data("originalRight"))-f.g.yourLogo.width()/2+parseInt(e(u).css("padding-right"))}else{oR=parseInt(f.g.yourLogo.data("originalRight"))*f.g.ratio}if(f.g.yourLogo.data("originalTop")&&f.g.yourLogo.data("originalTop").indexOf("%")!=-1){oT=f.g.sliderHeight()/100*parseInt(f.g.yourLogo.data("originalTop"))-f.g.yourLogo.height()/2+parseInt(e(u).css("padding-top"))}else{oT=parseInt(f.g.yourLogo.data("originalTop"))*f.g.ratio}if(f.g.yourLogo.data("originalBottom")&&f.g.yourLogo.data("originalBottom").indexOf("%")!=-1){oB=f.g.sliderHeight()/100*parseInt(f.g.yourLogo.data("originalBottom"))-f.g.yourLogo.height()/2+parseInt(e(u).css("padding-bottom"))}else{oB=parseInt(f.g.yourLogo.data("originalBottom"))*f.g.ratio}f.g.yourLogo.css({left:t,right:oR,top:oT,bottom:oB})};f.resizeThumb=function(){f.bottomNavSizeHelper("on");var t=f.g.sliderOriginalWidth.indexOf("%")==-1?parseInt(f.g.sliderOriginalWidth):f.g.sliderWidth();e(u).find(".ls-thumbnail-slide a").css({width:parseInt(f.o.tnWidth*f.g.ratio),height:parseInt(f.o.tnHeight*f.g.ratio)});e(u).find(".ls-thumbnail-slide a:last").css({margin:0});e(u).find(".ls-thumbnail-slide").css({height:parseInt(f.o.tnHeight*f.g.ratio)});var n=e(u).find(".ls-thumbnail");var r=f.o.tnContainerWidth.indexOf("%")==-1?parseInt(f.o.tnContainerWidth):parseInt(t/100*parseInt(f.o.tnContainerWidth));n.css({width:r*Math.floor(f.g.ratio*100)/100});if(n.width()>e(u).find(".ls-thumbnail-slide").width()){n.css({width:e(u).find(".ls-thumbnail-slide").width()})}f.bottomNavSizeHelper("off")};f.changeThumb=function(t){var n=t?t:f.g.nextLayerIndex;e(u).find(".ls-thumbnail-slide a:not(.ls-thumb-"+n+")").children().each(function(){e(this).removeClass("ls-thumb-active").stop().fadeTo(750,f.o.tnInactiveOpacity/100)});e(u).find(".ls-thumbnail-slide a.ls-thumb-"+n).children().addClass("ls-thumb-active").stop().fadeTo(750,f.o.tnActiveOpacity/100)};f.scrollThumb=function(){if(!e(u).find(".ls-thumbnail-slide-container").hasClass("ls-thumbnail-slide-hover")){var t=e(u).find(".ls-thumb-active").length?e(u).find(".ls-thumb-active").parent():false;if(t){var n=t.position().left+t.width()/2;var r=e(u).find(".ls-thumbnail-slide-container").width()/2-n;r=r<e(u).find(".ls-thumbnail-slide-container").width()-e(u).find(".ls-thumbnail-slide").width()?e(u).find(".ls-thumbnail-slide-container").width()-e(u).find(".ls-thumbnail-slide").width():r;r=r>0?0:r;e(u).find(".ls-thumbnail-slide").animate({marginLeft:r},600)}}};f.bottomNavSizeHelper=function(t){if(f.o.hoverBottomNav&&!e(u).hasClass("ls-hover")){switch(t){case"on":f.g.thumbsWrapper.css({visibility:"hidden",display:"block"});break;case"off":f.g.thumbsWrapper.css({visibility:"visible",display:"none"});break}}};f.animate=function(){if(e(u).find(".ls-slide").length>1){f.g.isAnimating=true}f.g.isLoading=false;clearTimeout(f.g.slideTimer);clearTimeout(f.g.changeTimer);f.g.stopLayer=f.g.curLayer;f.o.cbAnimStart(f.g);if(f.o.thumbnailNavigation=="always"){f.changeThumb();if(!("ontouchstart"in window)){f.scrollThumb()}}f.g.nextLayer.addClass("ls-animating");var t=curLayerRight=curLayerTop=curLayerBottom=nextLayerLeft=nextLayerRight=nextLayerTop=nextLayerBottom=layerMarginLeft=layerMarginRight=layerMarginTop=layerMarginBottom="auto";var a=nextLayerWidth=f.g.sliderWidth();var l=nextLayerHeight=f.g.sliderHeight();var c=f.g.prevNext=="prev"?f.g.curLayer:f.g.nextLayer;var h=c.data("slidedirection")?c.data("slidedirection"):f.o.slideDirection;var p=f.g.slideDirections[f.g.prevNext][h];if(p=="left"||p=="right"){a=curLayerTop=nextLayerWidth=nextLayerTop=0;layerMarginTop=0}if(p=="top"||p=="bottom"){l=t=nextLayerHeight=nextLayerLeft=0;layerMarginLeft=0}switch(p){case"left":curLayerRight=nextLayerLeft=0;layerMarginLeft=-f.g.sliderWidth();break;case"right":t=nextLayerRight=0;layerMarginLeft=f.g.sliderWidth();break;case"top":curLayerBottom=nextLayerTop=0;layerMarginTop=-f.g.sliderHeight();break;case"bottom":curLayerTop=nextLayerBottom=0;layerMarginTop=f.g.sliderHeight();break}f.g.curLayer.css({left:t,right:curLayerRight,top:curLayerTop,bottom:curLayerBottom});f.g.nextLayer.css({width:nextLayerWidth,height:nextLayerHeight,left:nextLayerLeft,right:nextLayerRight,top:nextLayerTop,bottom:nextLayerBottom});var d=f.g.curLayer.data("delayout")?parseInt(f.g.curLayer.data("delayout")):f.o.delayOut;var v=f.g.curLayer.data("durationout")?parseInt(f.g.curLayer.data("durationout")):f.o.durationOut;var m=f.g.curLayer.data("easingout")?f.g.curLayer.data("easingout"):f.o.easingOut;var g=f.g.nextLayer.data("delayin")?parseInt(f.g.nextLayer.data("delayin")):f.o.delayIn;var y=f.g.nextLayer.data("durationin")?parseInt(f.g.nextLayer.data("durationin")):f.o.durationIn;if(y===0){y=1}var b=f.g.nextLayer.data("easingin")?f.g.nextLayer.data("easingin"):f.o.easingIn;var w=function(){f.g.curLayer.delay(d+v/15).animate({width:a,height:l},v,m,function(){E()})};var E=function(){f.g.stopLayer.find(' > *[class*="ls-l"]').each(function(){if(e(this).data("tr")){e(this).data("tr").kill()}e(this).css({filter:"none"})});f.g.curLayer=f.g.nextLayer;f.g.prevLayerIndex=f.g.curLayerIndex;f.g.curLayerIndex=f.g.nextLayerIndex;f.o.cbAnimStop(f.g);if(f.o.imgPreload&&f.o.lazyLoad){var t=f.g.curLayerIndex==f.g.layersNum?1:f.g.curLayerIndex+1;e(u).find(".ls-slide").eq(t-1).find("img:not(.ls-preloaded)").each(function(){e(this).load(function(){e(this).addClass("ls-preloaded")}).error(function(){var t=e(this).data("src").substring(e(this).data("src").lastIndexOf("/")+1,e(this).data("src").length);if(window.console){console('LayerSlider error:\r\n\r\nIt seems like the URL of the image or background image "'+t+'" is pointing to a wrong location and it cannot be loaded. Please check the URLs of all your images used in the slider.')}else{alert('LayerSlider error:\r\n\r\nIt seems like the URL of the image or background image "'+t+'" is pointing to a wrong location and it cannot be loaded. Please check the URLs of all your images used in the slider.')}e(this).addClass("ls-not-preloaded")}).attr("src",e(this).data("src"))})}e(u).find(".ls-slide").removeClass("ls-active");e(u).find(".ls-slide:eq("+(f.g.curLayerIndex-1)+")").addClass("ls-active").removeClass("ls-animating");e(u).find(".ls-bottom-slidebuttons a").removeClass("ls-nav-active");e(u).find(".ls-bottom-slidebuttons a:eq("+(f.g.curLayerIndex-1)+")").addClass("ls-nav-active");if(f.g.autoSlideshow){f.timer()}f.g.isAnimating=false;if(f.g.resize==true){f.makeResponsive(f.g.curLayer,function(){f.g.resize=false})}};var S=function(t){f.g.curLayer.find(' > *[class*="ls-l"]').each(function(){if(!e(this).data("transitiontype")){f.transitionType(e(this))}e(this).removeClass("ls-videohack");var r=e(this).data("slidedirection")?e(this).data("slidedirection"):p;var i,s;switch(r){case"left":i=-f.g.sliderWidth();s=0;break;case"right":i=f.g.sliderWidth();s=0;break;case"top":s=-f.g.sliderHeight();i=0;break;case"bottom":s=f.g.sliderHeight();i=0;break;case"fade":s=0;i=0;break}if(e(this).data("transitiontype")==="new"){var o="new"}else{var o=e(this).data("slideoutdirection")?e(this).data("slideoutdirection"):false}switch(o){case"left":i=f.g.sliderWidth();s=0;break;case"right":i=-f.g.sliderWidth();s=0;break;case"top":s=f.g.sliderHeight();i=0;break;case"bottom":s=-f.g.sliderHeight();i=0;break;case"fade":s=0;i=0;break;case"new":if(e(this).data("offsetxout")){if(e(this).data("offsetxout")==="left"){i=f.g.sliderWidth()}else if(e(this).data("offsetxout")==="right"){i=-f.g.sliderWidth()}else{i=-parseInt(e(this).data("offsetxout"))}}else{i=-f.lt.offsetXOut}if(e(this).data("offsetyout")){if(e(this).data("offsetyout")==="top"){s=f.g.sliderHeight()}else if(e(this).data("offsetyout")==="bottom"){s=-f.g.sliderHeight()}else{s=-parseInt(e(this).data("offsetyout"))}}else{s=-f.lt.offsetYOut}break}var u=curSubRotateX=curSubRotateY=curSubScale=curSubSkewX=curSubSkewY=curSubScaleX=curSubScaleY="none";u=e(this).data("rotateout")?e(this).data("rotateout"):f.lt.rotateOut;curSubRotateX=e(this).data("rotatexout")?e(this).data("rotatexout"):f.lt.rotateXOut;curSubRotateY=e(this).data("rotateyout")?e(this).data("rotateyout"):f.lt.rotateYOut;curSubScale=e(this).data("scaleout")?e(this).data("scaleout"):f.lt.scaleOut;curSubSkewX=e(this).data("skewxout")?e(this).data("skewxout"):f.lt.skewXOut;curSubSkewY=e(this).data("skewyout")?e(this).data("skewyout"):f.lt.skewYOut;if(curSubScale===1){curSubScaleX=e(this).data("scalexout")?e(this).data("scalexout"):f.lt.scaleXOut;curSubScaleY=e(this).data("scaleyout")?e(this).data("scaleyout"):f.lt.scaleYOut}else{curSubScaleX=curSubScaleY=curSubScale}var a=e(this).data("transformoriginout")?e(this).data("transformoriginout").split(" "):f.lt.transformOriginOut;for(var l=0;l<a.length;l++){if(a[l].indexOf("%")===-1&&a[l].indexOf("left")!==-1&&a[l].indexOf("right")!==-1&&a[l].indexOf("top")!==-1&&a[l].indexOf("bottom")!==-1){a[l]=""+parseInt(a[l])*f.g.ratio+"px"}}var c=a.join(" ");var h=e(this).data("perspectiveout")?e(this).data("perspectiveout"):f.lt.perspectiveOut;var d=parseInt(e(this).css("left"));var v=parseInt(e(this).css("top"));var m=parseInt(e(this).attr("class").split("ls-l")[1]);var g=e(this).outerWidth()>e(this).outerHeight()?e(this).outerWidth():e(this).outerHeight();var y=parseInt(u)===0?e(this).outerWidth():g;var b=parseInt(u)===0?e(this).outerHeight():g;if(m===-1&&o!=="new"||e(this).data("offsetxout")==="left"||e(this).data("offsetxout")==="right"){if(i<0){i=-(f.g.sliderWidth()-d+(curSubScaleX/2-.5)*y+100)}else if(i>0){i=d+(curSubScaleX/2+.5)*y+100}}else{i=i*f.g.ratio}if(m===-1&&o!=="new"||e(this).data("offsetyout")==="top"||e(this).data("offsetyout")==="bottom"){if(s<0){s=-(f.g.sliderHeight()-v+(curSubScaleY/2-.5)*b+100)}else if(s>0){s=v+(curSubScaleY/2+.5)*b+100}}else{s=s*f.g.ratio}if(m===-1||o==="new"){var w=1}else{var E=f.g.curLayer.data("parallaxout")?parseInt(f.g.curLayer.data("parallaxout")):f.o.parallaxOut;var w=m*E}if(e(this).data("transitiontype")==="new"){var S=f.lt.delayOut;var x=f.lt.durationOut;var T=f.lt.easingOut}else{var S=f.o.delayOut;var x=f.o.durationOut;var T=f.o.easingOut}var N=e(this).data("delayout")?parseInt(e(this).data("delayout")):S;var C=e(this).data("durationout")?parseInt(e(this).data("durationout")):x;if(C===0){C=1}var k=e(this).data("easingout")?e(this).data("easingout"):T;if(t){N=0;C=t}if(e(this).data("showUntilTimer")){clearTimeout(e(this).data("showUntilTimer"))}var L={visibility:"hidden"};var A=e(this);var O={rotation:u,rotationX:curSubRotateX,rotationY:curSubRotateY,skewX:curSubSkewX,skewY:curSubSkewY,scaleX:curSubScaleX,scaleY:curSubScaleY,x:-i*w,y:-s*w,delay:N/1e3,ease:n(k),onComplete:function(){A.css(L)}};if(o=="fade"||!o&&r==="fade"||e(this).data("fadeout")!=="false"&&e(this).data("transitiontype")==="new"){O["opacity"]=0;L["opacity"]=e(this).data("originalOpacity")}if(e(this).data("tr")){e(this).data("tr").kill()}TweenLite.set(e(this)[0],{transformOrigin:c,transformPerspective:h});e(this).data("tr",TweenLite.to(e(this)[0],C/1e3,O))})};var x=function(){f.g.nextLayer.delay(d+g).animate({width:f.g.sliderWidth(),height:f.g.sliderHeight()},y,b)};var T=function(){if(f.g.totalDuration){d=0}if(typeof f.o.cbTimeLineStart==="function"){f.o.cbTimeLineStart(f.g,d+g)}f.g.nextLayer.find(' > *[class*="ls-l"]').each(function(){if(!e(this).data("transitiontype")){f.transitionType(e(this))}if(e(this).data("transitiontype")==="new"){var t="new"}else{var t=e(this).data("slidedirection")?e(this).data("slidedirection"):p}var r,i;switch(t){case"left":r=-f.g.sliderWidth();i=0;break;case"right":r=f.g.sliderWidth();i=0;break;case"top":i=-f.g.sliderHeight();r=0;break;case"bottom":i=f.g.sliderHeight();r=0;break;case"fade":i=0;r=0;break;case"new":if(e(this).data("offsetxin")){if(e(this).data("offsetxin")==="left"){r=-f.g.sliderWidth()}else if(e(this).data("offsetxin")==="right"){r=f.g.sliderWidth()}else{r=parseInt(e(this).data("offsetxin"))}}else{r=f.lt.offsetXIn}if(e(this).data("offsetyin")){if(e(this).data("offsetyin")==="top"){i=-f.g.sliderHeight()}else if(e(this).data("offsetyin")==="bottom"){i=f.g.sliderHeight()}else{i=parseInt(e(this).data("offsetyin"))}}else{i=f.lt.offsetYIn}break}var s=nextSubRotateX=nextSubRotateY=nextSubScale=nextSubSkewX=nextSubSkewY=nextSubScaleX=nextSubScaleY="none";s=e(this).data("rotatein")?e(this).data("rotatein"):f.lt.rotateIn;nextSubRotateX=e(this).data("rotatexin")?e(this).data("rotatexin"):f.lt.rotateXIn;nextSubRotateY=e(this).data("rotateyin")?e(this).data("rotateyin"):f.lt.rotateYIn;nextSubScale=e(this).data("scalein")?e(this).data("scalein"):f.lt.scaleIn;nextSubSkewX=e(this).data("skewxin")?e(this).data("skewxin"):f.lt.skewXIn;nextSubSkewY=e(this).data("skewyin")?e(this).data("skewyin"):f.lt.skewYIn;if(nextSubScale===1){nextSubScaleX=e(this).data("scalexin")?e(this).data("scalexin"):f.lt.scaleXIn;nextSubScaleY=e(this).data("scaleyin")?e(this).data("scaleyin"):f.lt.scaleYIn}else{nextSubScaleX=nextSubScaleY=nextSubScale}var o=e(this).data("transformoriginin")?e(this).data("transformoriginin").split(" "):f.lt.transformOriginIn;for(var u=0;u<o.length;u++){if(o[u].indexOf("%")===-1&&o[u].indexOf("left")!==-1&&o[u].indexOf("right")!==-1&&o[u].indexOf("top")!==-1&&o[u].indexOf("bottom")!==-1){o[u]=""+parseInt(o[u])*f.g.ratio+"px"}}var a=o.join(" ");var l=e(this).data("perspectivein")?e(this).data("perspectivein"):f.lt.perspectiveIn;var c=parseInt(e(this).css("left"));var h=parseInt(e(this).css("top"));var d=parseInt(e(this).attr("class").split("ls-l")[1]);if(e(this)[0].style.width.indexOf("%")!==-1){e(this).css({width:f.g.sliderWidth()/100*parseInt(e(this)[0].style.width)})}var v=e(this).outerWidth()>e(this).outerHeight()?e(this).outerWidth():e(this).outerHeight();var m=parseInt(s)===0?e(this).outerWidth():v;var g=parseInt(s)===0?e(this).outerHeight():v;if(d===-1&&t!=="new"||e(this).data("offsetxin")==="left"||e(this).data("offsetxin")==="right"){if(r<0){r=-(c+(nextSubScaleX/2+.5)*m+100)}else if(r>0){r=f.g.sliderWidth()-c+(nextSubScaleX/2-.5)*m+100}}else{r=r*f.g.ratio}if(d===-1&&t!=="new"||e(this).data("offsetyin")==="top"||e(this).data("offsetyin")==="bottom"){if(i<0){i=-(h+(nextSubScaleY/2+.5)*g+100)}else if(i>0){i=f.g.sliderHeight()-h+(nextSubScaleY/2-.5)*g+100}}else{i=i*f.g.ratio}if(d===-1||t==="new"){var y=1}else{var b=f.g.nextLayer.data("parallaxin")?parseInt(f.g.nextLayer.data("parallaxin")):f.o.parallaxIn;var y=d*b}if(e(this).data("transitiontype")==="new"){var w=f.lt.delayIn;var E=f.lt.durationIn;var S=f.lt.easingIn}else{var w=f.o.delayIn;var E=f.o.durationIn;var S=f.o.easingIn}var x=e(this).data("delayin")?parseInt(e(this).data("delayin")):w;var T=e(this).data("durationin")?parseInt(e(this).data("durationin")):E;var N=e(this).data("easingin")?e(this).data("easingin"):S;var C=e(this);var k=function(){if(C.hasClass("ls-video-layer")){C.addClass("ls-videohack")}if(f.o.autoPlayVideos==true){C.find(".ls-videopreview").click();C.find("video, audio").each(function(){if(typeof e(this)[0].currentTime!==0){e(this)[0].currentTime=0}e(this).click()})}if((!C.hasClass("ls-video-layer")||C.hasClass("ls-video-layer")&&f.o.autoPlayVideos===false)&&C.data("showuntil")>0){C.data("showUntilTimer",setTimeout(function(){f.sublayerShowUntil(C)},C.data("showuntil")))}};e(this).css({marginLeft:0,marginTop:0});var L={scaleX:nextSubScaleX,scaleY:nextSubScaleY,skewX:nextSubSkewX,skewY:nextSubSkewY,rotation:s,rotationX:nextSubRotateX,rotationY:nextSubRotateY,visibility:"visible",x:r*y,y:i*y};var A={rotation:0,rotationX:0,rotationY:0,skewX:0,skewY:0,scaleX:1,scaleY:1,ease:n(N),delay:x/1e3,x:0,y:0,onComplete:function(){k()}};if(t.indexOf("fade")!=-1||e(this).data("fadein")!=="false"&&e(this).data("transitiontype")==="new"){L["opacity"]=0;A["opacity"]=e(this).data("originalOpacity")}if(e(this).data("tr")){e(this).data("tr").kill()}TweenLite.set(e(this)[0],{transformPerspective:l,transformOrigin:a});e(this).data("tr",TweenLite.fromTo(e(this)[0],T/1e3,L,A))})};var N=function(){if(i(e(u))&&(f.g.nextLayer.data("transition3d")||f.g.nextLayer.data("customtransition3d"))){if(f.g.nextLayer.data("transition3d")&&f.g.nextLayer.data("customtransition3d")){var t=Math.floor(Math.random()*2);var n=[["3d",f.g.nextLayer.data("transition3d")],["custom3d",f.g.nextLayer.data("customtransition3d")]];k(n[t][0],n[t][1])}else if(f.g.nextLayer.data("transition3d")){k("3d",f.g.nextLayer.data("transition3d"))}else{k("custom3d",f.g.nextLayer.data("customtransition3d"))}}else{if(f.g.nextLayer.data("transition2d")&&f.g.nextLayer.data("customtransition2d")){var t=Math.floor(Math.random()*2);var n=[["2d",f.g.nextLayer.data("transition2d")],["custom2d",f.g.nextLayer.data("customtransition2d")]];k(n[t][0],n[t][1])}else if(f.g.nextLayer.data("transition2d")){k("2d",f.g.nextLayer.data("transition2d"))}else if(f.g.nextLayer.data("customtransition2d")){k("custom2d",f.g.nextLayer.data("customtransition2d"))}else{k("2d","1")}}};var C=function(){if(i(e(u))&&LSCustomTransition.indexOf("3d")!=-1){k("3d",LSCustomTransition.split(":")[1])}else{if(LSCustomTransition.indexOf("3d")!=-1){k("2d","all")}else{k("2d",LSCustomTransition.split(":")[1])}}};var k=function(e,t){var n=e.indexOf("custom")==-1?f.t:f.ct;var r="3d",i,s;if(e.indexOf("2d")!=-1){r="2d"}if(t.indexOf("last")!=-1){s=n["t"+r].length-1;i="last"}else if(t.indexOf("all")!=-1){s=Math.floor(Math.random()*o(n["t"+r]));i="random from all"}else{var u=t.split(",");var a=u.length;s=parseInt(u[Math.floor(Math.random()*a)])-1;i="random from specified"}L(r,n["t"+r][s])};var L=function(t,i){var o=e(u).find(".ls-inner");var a=f.g.curLayer.find('*[class*="ls-l"]').length>0?1e3:0;var l=i.name.toLowerCase().indexOf("carousel")==-1?false:true;var c=i.name.toLowerCase().indexOf("crossfad")==-1?false:true;var h=typeof i.cols;var p=typeof i.rows;switch(h){case"number":h=i.cols;break;case"string":h=Math.floor(Math.random()*(parseInt(i.cols.split(",")[1])-parseInt(i.cols.split(",")[0])+1))+parseInt(i.cols.split(",")[0]);break;default:h=Math.floor(Math.random()*(i.cols[1]-i.cols[0]+1))+i.cols[0];break}switch(p){case"number":p=i.rows;break;case"string":p=Math.floor(Math.random()*(parseInt(i.rows.split(",")[1])-parseInt(i.rows.split(",")[0])+1))+parseInt(i.rows.split(",")[0]);break;default:p=Math.floor(Math.random()*(i.rows[1]-i.rows[0]+1))+i.rows[0];break}if(f.g.isMobile()==true&&f.o.optimizeForMobile==true||f.g.ie78&&f.o.optimizeForIE78==true){if(h>=15){h=7}else if(h>=5){h=4}else if(h>=4){h=3}else if(h>2){h=2}if(p>=15){p=7}else if(p>=5){p=4}else if(p>=4){p=3}else if(p>2){p=2}if(p>2&&h>2){p=2;if(h>4){h=4}}}var d=e(u).find(".ls-inner").width()/h;var v=e(u).find(".ls-inner").height()/p;if(!f.g.ltContainer){f.g.ltContainer=e("<div>").addClass("ls-lt-container").addClass("ls-overflow-hidden").css({width:o.width(),height:o.height()}).prependTo(o)}else{f.g.ltContainer.stop(true,true).empty().css({display:"block",width:o.width(),height:o.height()})}var m=o.width()-Math.floor(d)*h;var g=o.height()-Math.floor(v)*p;var y=[];y.randomize=function(){var e=this.length,t,n,r;if(e==0)return false;while(--e){t=Math.floor(Math.random()*(e+1));n=this[e];r=this[t];this[e]=r;this[t]=n}return this};for(var b=0;b<h*p;b++){y.push(b)}switch(i.tile.sequence){case"reverse":y.reverse();break;case"col-forward":y=s(p,h,"forward");break;case"col-reverse":y=s(p,h,"reverse");break;case"random":y.randomize();break}var w=f.g.curLayer.find(".ls-bg");var x=f.g.nextLayer.find(".ls-bg");if(w.length==0&&x.length==0){t="2d";i=e.extend(true,{},f.t["t2d"][0]);i.transition.duration=1;i.tile.delay=0}if(t=="3d"){f.g.totalDuration=(h*p-1)*i.tile.delay;var N=0;if(i.before&&i.before.duration){N+=i.before.duration}if(i.animation&&i.animation.duration){N+=i.animation.duration}if(i.after&&i.after.duration){N+=i.after.duration}f.g.totalDuration+=N;var C=0;if(i.before&&i.before.delay){C+=i.before.delay}if(i.animation&&i.animation.delay){C+=i.animation.delay}if(i.after&&i.after.delay){C+=i.after.delay}f.g.totalDuration+=C}else{f.g.totalDuration=(h*p-1)*i.tile.delay+i.transition.duration;f.g.curTiles=e("<div>").addClass("ls-curtiles").appendTo(f.g.ltContainer);f.g.nextTiles=e("<div>").addClass("ls-nexttiles").appendTo(f.g.ltContainer)}var k=f.g.prevNext;for(var L=0;L<h*p;L++){var A=L%h==0?m:0;var O=L>(p-1)*h-1?g:0;var M=e("<div>").addClass("ls-lt-tile").css({width:Math.floor(d)+A,height:Math.floor(v)+O}).appendTo(f.g.ltContainer);var _,D;if(t=="3d"){M.addClass("ls-3d-container");var P=Math.floor(d)+A;var H=Math.floor(v)+O;var B;if(i.animation.direction=="horizontal"){if(Math.abs(i.animation.transition.rotateY)>90&&i.tile.depth!="large"){B=Math.floor(P/7)+A}else{B=P}}else{if(Math.abs(i.animation.transition.rotateX)>90&&i.tile.depth!="large"){B=Math.floor(H/7)+O}else{B=H}}var j=P/2;var F=H/2;var I=B/2;var q=function(t,n,r,i,s,o,u,a,f){e("<div>").addClass(t).css({width:r,height:i,"-o-transform":"translate3d("+s+"px, "+o+"px, "+u+"px) rotateX("+a+"deg) rotateY("+f+"deg) rotateZ(0deg) scale3d(1, 1, 1)","-ms-transform":"translate3d("+s+"px, "+o+"px, "+u+"px) rotateX("+a+"deg) rotateY("+f+"deg) rotateZ(0deg) scale3d(1, 1, 1)","-moz-transform":"translate3d("+s+"px, "+o+"px, "+u+"px) rotateX("+a+"deg) rotateY("+f+"deg) rotateZ(0deg) scale3d(1, 1, 1)","-webkit-transform":"translate3d("+s+"px, "+o+"px, "+u+"px) rotateX("+a+"deg) rotateY("+f+"deg) rotateZ(0deg) scale3d(1, 1, 1)",transform:"translate3d("+s+"px, "+o+"px, "+u+"px) rotateX("+a+"deg) rotateY("+f+"deg) rotateZ(0deg) scale3d(1, 1, 1)"}).appendTo(n)};q("ls-3d-box",M,0,0,0,0,-I,0,0);var R=0;var U=0;var z=0;if(i.animation.direction=="vertical"&&Math.abs(i.animation.transition.rotateX)>90){q("ls-3d-back",M.find(".ls-3d-box"),P,H,-j,-F,-I,180,0)}else{q("ls-3d-back",M.find(".ls-3d-box"),P,H,-j,-F,-I,0,180)}q("ls-3d-bottom",M.find(".ls-3d-box"),P,B,-j,F-I,0,-90,0);q("ls-3d-top",M.find(".ls-3d-box"),P,B,-j,-F-I,0,90,0);q("ls-3d-front",M.find(".ls-3d-box"),P,H,-j,-F,I,0,0);q("ls-3d-left",M.find(".ls-3d-box"),B,H,-j-I,-F,0,0,-90);q("ls-3d-right",M.find(".ls-3d-box"),B,H,j-I,-F,0,0,90);_=M.find(".ls-3d-front");if(i.animation.direction=="horizontal"){if(Math.abs(i.animation.transition.rotateY)>90){D=M.find(".ls-3d-back")}else{D=M.find(".ls-3d-left, .ls-3d-right")}}else{if(Math.abs(i.animation.transition.rotateX)>90){D=M.find(".ls-3d-back")}else{D=M.find(".ls-3d-top, .ls-3d-bottom")}}var W=y[L]*i.tile.delay;var X=f.g.ltContainer.find(".ls-3d-container:eq("+L+") .ls-3d-box");var V=new TimelineLite;if(i.before&&i.before.transition){i.before.transition.delay=i.before.transition.delay?(i.before.transition.delay+W)/1e3:W/1e3;V.to(X[0],i.before.duration/1e3,r(i.before.transition,i.before.easing))}else{i.animation.transition.delay=i.animation.transition.delay?(i.animation.transition.delay+W)/1e3:W/1e3}V.to(X[0],i.animation.duration/1e3,r(i.animation.transition,i.animation.easing));if(i.after){if(!i.after.transition){i.after.transition={}}V.to(X[0],i.after.duration/1e3,r(i.after.transition,i.after.easing,"after"))}}else{var $=L1=T2=L2="auto";var J=O2=1;if(i.transition.direction=="random"){var K=["top","bottom","right","left"];var Q=K[Math.floor(Math.random()*K.length)]}else{var Q=i.transition.direction}if(i.name.toLowerCase().indexOf("mirror")!=-1&&L%2==0){if(k=="prev"){k="next"}else{k="prev"}}if(k=="prev"){switch(Q){case"top":Q="bottom";break;case"bottom":Q="top";break;case"left":Q="right";break;case"right":Q="left";break;case"topleft":Q="bottomright";break;case"topright":Q="bottomleft";break;case"bottomleft":Q="topright";break;case"bottomright":Q="topleft";break}}switch(Q){case"top":$=T2=-M.height();L1=L2=0;break;case"bottom":$=T2=M.height();L1=L2=0;break;case"left":$=T2=0;L1=L2=-M.width();break;case"right":$=T2=0;L1=L2=M.width();break;case"topleft":$=M.height();T2=0;L1=M.width();L2=0;break;case"topright":$=M.height();T2=0;L1=-M.width();L2=0;break;case"bottomleft":$=-M.height();T2=0;L1=M.width();L2=0;break;case"bottomright":$=-M.height();T2=0;L1=-M.width();L2=0;break}f.g.scale2D=i.transition.scale?i.transition.scale:1;if(l==true&&f.g.scale2D!=1){$=$/2;T2=T2/2;L1=L1/2;L2=L2/2}switch(i.transition.type){case"fade":$=T2=L1=L2=0;J=0;O2=1;break;case"mixed":J=0;O2=1;if(f.g.scale2D==1){T2=L2=0}break}if((i.transition.rotate||i.transition.rotateX||i.transition.rotateY||f.g.scale2D!=1)&&!f.g.ie78&&i.transition.type!="slide"){M.css({overflow:"visible"})}else{M.css({overflow:"hidden"})}if(l==true){f.g.curTiles.css({overflow:"visible"})}else{f.g.curTiles.css({overflow:"hidden"})}if(c==true||i.transition.type=="slide"||l==true){var G=M.appendTo(f.g.curTiles);var Y=M.clone().appendTo(f.g.nextTiles);_=e("<div>").addClass("ls-curtile").appendTo(G)}else{var Y=M.appendTo(f.g.nextTiles)}D=e("<div>").addClass("ls-nexttile").appendTo(Y).css({top:-$,left:-L1,dispay:"block",opacity:J});var Z=y[L]*i.tile.delay;var et=i.transition.rotate?i.transition.rotate:0;var tt=i.transition.rotateX?i.transition.rotateX:0;var nt=i.transition.rotateY?i.transition.rotateY:0;if(k=="prev"){et=-et;tt=-tt;nt=-nt}TweenLite.fromTo(D[0],i.transition.duration/1e3,{rotation:et,rotationX:tt,rotationY:nt,scale:f.g.scale2D},{delay:Z/1e3,top:0,left:0,opacity:O2,rotation:0,rotationX:0,rotationY:0,scale:1,ease:n(i.transition.easing)});if(c==true&&(x.length<1||x.length>0&&(x.attr("src").toLowerCase().indexOf("png")!=-1||x.width()<f.g.sliderWidth()||x.height()<f.g.sliderHeight()))){TweenLite.to(_[0],i.transition.duration/1e3,{delay:Z/1e3,opacity:0,ease:n(i.transition.easing)})}if((i.transition.type=="slide"||l==true)&&i.name.toLowerCase().indexOf("mirror")==-1){var rt=0;if(et!=0){rt=-et}TweenLite.to(_[0],i.transition.duration/1e3,{delay:Z/1e3,top:T2,left:L2,rotation:rt,scale:f.g.scale2D,opacity:J,ease:n(i.transition.easing)})}}if(w.length){if(t=="3d"||t=="2d"&&(c==true||i.transition.type=="slide"||l==true)){_.append(e("<img>").attr("src",w.attr("src")).css({width:w[0].style.width,height:w[0].style.height,marginLeft:parseFloat(w.css("margin-left"))-parseFloat(M.position().left),marginTop:parseFloat(w.css("margin-top"))-parseFloat(M.position().top)}))}else if(f.g.curTiles.children().length==0){f.g.curTiles.append(e("<img>").attr("src",w.attr("src")).css({width:w[0].style.width,height:w[0].style.height,marginLeft:parseFloat(w.css("margin-left")),marginTop:parseFloat(w.css("margin-top"))}))}}if(x.length){D.append(e("<img>").attr("src",x.attr("src")).css({width:x[0].style.width,height:x[0].style.height,marginLeft:parseFloat(x.css("margin-left"))-parseFloat(M.position().left),marginTop:parseFloat(x.css("margin-top"))-parseFloat(M.position().top)}))}}var it=f.g.curLayer;var st=f.g.nextLayer;setTimeout(function(){it.find(".ls-bg").css({visibility:"hidden"})},50);st.find(".ls-bg").css({visibility:"hidden"});f.g.ltContainer.removeClass("ls-overflow-hidden");S(a);if(a===0){a=10}setTimeout(function(){it.css({width:0})},a);var ot=parseInt(st.data("timeshift"))?parseInt(st.data("timeshift")):0;var ut=f.g.totalDuration+ot>0?f.g.totalDuration+ot:0;setTimeout(function(){if(f.g.resize==true){f.g.ltContainer.empty();it.removeClass("ls-active");f.makeResponsive(st,function(){f.g.resize=false})}T();if(st.find(".ls-bg").length<1||st.find(".ls-bg").length>0&&st.find(".ls-bg").attr("src").toLowerCase().indexOf("png")!=-1){f.g.ltContainer.delay(350).fadeOut(300,function(){e(this).empty().show()})}st.css({width:f.g.sliderWidth(),height:f.g.sliderHeight()})},ut);if(f.g.totalDuration<300){f.g.totalDuration=1e3}setTimeout(function(){f.g.ltContainer.addClass("ls-overflow-hidden");st.addClass("ls-active");if(st.find(".ls-bg").length){st.find(".ls-bg").css({display:"none",visibility:"visible"});if(f.g.ie78){st.find(".ls-bg").css("display","block");setTimeout(function(){E()},500)}else{st.find(".ls-bg").fadeIn(500,function(){E()})}}else{E()}},f.g.totalDuration)};var A=function(){f.g.nextLayer.find(' > *[class*="ls-l"]').each(function(){e(this).css({visibility:"hidden"})});f.g.sliderTop=e(u).offset().top;e(window).load(function(){setTimeout(function(){f.g.sliderTop=e(u).offset().top},20)});var t=function(){if(e(window).scrollTop()+e(window).height()-f.g.sliderHeight()/2>f.g.sliderTop){f.g.firstSlideAnimated=true;if(f.g.originalAutoStart===true){f.o.autoStart=true;f.start()}T()}};e(window).scroll(function(){if(!f.g.firstSlideAnimated){t()}});t()};var O=(f.g.nextLayer.data("transition3d")||f.g.nextLayer.data("transition2d"))&&f.t||(f.g.nextLayer.data("customtransition3d")||f.g.nextLayer.data("customtransition2d"))&&f.ct?"new":"old";if(!f.g.nextLayer.data("transitiontype")){f.transitionType(f.g.nextLayer)}if(f.g.nextLayer.data("transitiontype")==="new"){O="new"}if(f.o.slideTransition){O="forced"}if(f.o.animateFirstSlide&&!f.g.firstSlideAnimated){if(f.g.layersNum==1){var d=0;f.o.cbAnimStop(f.g)}else{var M=parseInt(f.g.nextLayer.data("timeshift"))?parseInt(f.g.nextLayer.data("timeshift")):0;var _=O=="new"?0:v;f.g.t5=setTimeout(function(){E()},_+Math.abs(M))}f.g.totalDuration=true;if(f.o.startInViewport===true){A()}else{f.g.firstSlideAnimated=true;T()}f.g.nextLayer.css({width:f.g.sliderWidth(),height:f.g.sliderHeight()});if(!f.g.ie78){f.g.nextLayer.find(".ls-bg").css({display:"none"}).fadeIn(f.o.sliderFadeInDuration)}f.g.isLoading=false}else{switch(O){case"old":f.g.totalDuration=false;if(f.g.ltContainer){f.g.ltContainer.empty()}w();S();x();T();break;case"new":if(typeof LSCustomTransition!="undefined"){C()}else{N()}break;case"forced":L(f.o.slideTransition.type,f.o.slideTransition.obj);break}}};f.transitionType=function(e){var t=e.data("ls")||!e.data("ls")&&!e.data("slidedelay")&&!e.data("slidedirection")&&!e.data("slideoutdirection")&&!e.data("delayin")&&!e.data("delayout")&&!e.data("durationin")&&!e.data("durationout")&&!e.data("showuntil")&&!e.data("easingin")&&!e.data("easingout")&&!e.data("scalein")&&!e.data("scaleout")&&!e.data("rotatein")&&!e.data("rotateout")?"new":"old";e.data("transitiontype",t)};f.sublayerShowUntil=function(e){if(!e.data("transitiontype")){f.transitionType(e)}e.removeClass("ls-videohack");var t=f.g.curLayer;if(f.g.prevNext!="prev"&&f.g.nextLayer){t=f.g.nextLayer}var r=t.data("slidedirection")?t.data("slidedirection"):f.o.slideDirection;var i=f.g.slideDirections[f.g.prevNext][r];var s=e.data("slidedirection")?e.data("slidedirection"):i;var o,u;switch(s){case"left":o=-f.g.sliderWidth();u=0;break;case"right":o=f.g.sliderWidth();u=0;break;case"top":u=-f.g.sliderHeight();o=0;break;case"bottom":u=f.g.sliderHeight();o=0;break;case"fade":u=0;o=0;break}if(e.data("transitiontype")==="new"){var a="new"}else{var a=e.data("slideoutdirection")?e.data("slideoutdirection"):false}switch(a){case"left":o=f.g.sliderWidth();u=0;break;case"right":o=-f.g.sliderWidth();u=0;break;case"top":u=f.g.sliderHeight();o=0;break;case"bottom":u=-f.g.sliderHeight();o=0;break;case"fade":u=0;o=0;break;case"new":if(e.data("offsetxout")){if(e.data("offsetxout")==="left"){o=f.g.sliderWidth()}else if(e.data("offsetxout")==="right"){o=-f.g.sliderWidth()}else{o=-parseInt(e.data("offsetxout"))}}else{o=-f.lt.offsetXOut}if(e.data("offsetyout")){if(e.data("offsetyout")==="top"){u=f.g.sliderHeight()}else if(e.data("offsetyout")==="bottom"){u=-f.g.sliderHeight()}else{u=-parseInt(e.data("offsetyout"))}}else{u=-f.lt.offsetYOut}break}var l=curSubRotateX=curSubRotateY=curSubScale=curSubSkewX=curSubSkewY=curSubScaleX=curSubScaleY="none";l=e.data("rotateout")?e.data("rotateout"):f.lt.rotateOut;curSubRotateX=e.data("rotatexout")?e.data("rotatexout"):f.lt.rotateXOut;curSubRotateY=e.data("rotateyout")?e.data("rotateyout"):f.lt.rotateYOut;curSubScale=e.data("scaleout")?e.data("scaleout"):f.lt.scaleOut;curSubSkewX=e.data("skewxout")?e.data("skewxout"):f.lt.skewXOut;curSubSkewY=e.data("skewyout")?e.data("skewyout"):f.lt.skewYOut;if(curSubScale===1){curSubScaleX=e.data("scalexout")?e.data("scalexout"):f.lt.scaleXOut;curSubScaleY=e.data("scaleyout")?e.data("scaleyout"):f.lt.scaleYOut}else{curSubScaleX=curSubScaleY=curSubScale}var c=e.data("transformoriginout")?e.data("transformoriginout").split(" "):f.lt.transformOriginOut;for(var h=0;h<c.length;h++){if(c[h].indexOf("%")===-1&&c[h].indexOf("left")!==-1&&c[h].indexOf("right")!==-1&&c[h].indexOf("top")!==-1&&c[h].indexOf("bottom")!==-1){c[h]=""+parseInt(c[h])*f.g.ratio+"px"}}var p=c.join(" ");var d=e.data("perspectiveout")?e.data("perspectiveout"):f.lt.perspectiveOut;var v=parseInt(e.css("left"));var m=parseInt(e.css("top"));var g=parseInt(e.attr("class").split("ls-l")[1]);var y=e.outerWidth()>e.outerHeight()?e.outerWidth():e.outerHeight();var b=parseInt(l)===0?e.outerWidth():y;var w=parseInt(l)===0?e.outerHeight():y;if(g===-1&&a!=="new"||e.data("offsetxout")==="left"||e.data("offsetxout")==="right"){if(o<0){o=-(f.g.sliderWidth()-v+(curSubScaleX/2-.5)*b+100)}else if(o>0){o=v+(curSubScaleX/2+.5)*b+100}}else{o=o*f.g.ratio}if(g===-1&&a!=="new"||e.data("offsetyout")==="top"||e.data("offsetyout")==="bottom"){if(u<0){u=-(f.g.sliderHeight()-m+(curSubScaleY/2-.5)*w+100)}else if(u>0){u=m+(curSubScaleY/2+.5)*w+100}}else{u=u*f.g.ratio}if(g===-1||a==="new"){var E=1}else{var S=f.g.curLayer.data("parallaxout")?parseInt(f.g.curLayer.data("parallaxout")):f.o.parallaxOut;var E=g*S}if(e.data("transitiontype")==="new"){var x=f.lt.durationOut;var T=f.lt.easingOut}else{var x=f.o.durationOut;var T=f.o.easingOut}var N=e.data("durationout")?parseInt(e.data("durationout")):x;if(N===0){N=1}var C=e.data("easingout")?e.data("easingout"):T;var k={visibility:"hidden"};var L={rotation:l,rotationX:curSubRotateX,rotationY:curSubRotateY,skewX:curSubSkewX,skewY:curSubSkewY,scaleX:curSubScaleX,scaleY:curSubScaleY,x:-o*E,y:-u*E,ease:n(C),onComplete:function(){e.css(k)}};if(a=="fade"||!a&&s=="fade"||e.data("fadeout")!=="false"&&e.data("transitiontype")==="new"){L["opacity"]=0;k["opacity"]=e.data("originalOpacity")}TweenLite.set(e[0],{transformPerspective:d,transformOrigin:p});TweenLite.to(e[0],N/1e3,L)};f.debug=function(){f.d={history:e("<div>"),aT:function(t){e("<h1>"+t+"</h1>").appendTo(f.d.history)},aeU:function(){e("<ul>").appendTo(f.d.history)},aU:function(t){e("<ul><li>"+t+"</li></ul>").appendTo(f.d.history)},aL:function(t){e("<li>"+t+"</li>").appendTo(f.d.history.find("ul:last"))},aUU:function(t){e("<ul>").appendTo(f.d.history.find("ul:last li:last"))},aF:function(e){f.d.history.find("ul:last li:last").hover(function(){e.css({border:"2px solid red",marginTop:parseInt(e.css("margin-top"))-2,marginLeft:parseInt(e.css("margin-left"))-2})},function(){e.css({border:"0px",marginTop:parseInt(e.css("margin-top"))+2,marginLeft:parseInt(e.css("margin-left"))+2})})},show:function(){if(!e("body").find(".ls-debug-console").length){if(!f.d.putData){f.d.aT("Init code");f.d.aeU();for(var t in f.o){f.d.aL(t+": <strong>"+f.o[t]+"</strong>")}f.d.aT("LayerSlider Content");f.d.aU("Number of slides found: <strong>"+e(u).find(".ls-slide").length+"</strong>");e(u).find('.ls-inner .ls-slide, .ls-inner *[class*="ls-l"]').each(function(){if(e(this).hasClass("ls-slide")){f.d.aU("<strong>SLIDE "+(e(this).index()+1)+"</strong>");f.d.aUU();f.d.aL("<strong>SLIDE "+(e(this).index()+1)+" properties:</strong><br><br>")}else{f.d.aU("&nbsp;&nbsp;&nbsp;&nbsp;Layer ( "+e(this).prop("tagName")+" )");f.d.aF(e(this));f.d.aUU();f.d.aL("<strong>"+e(this).prop("tagName")+" layer properties:</strong><br><br>");f.d.aL("distance / class: <strong>"+e(this).attr("class")+"</strong>")}e.each(e(this).data(),function(e,t){f.d.aL(e+": <strong>"+t+"</strong>")})});f.d.putData=true}var n=e("<div>").addClass("ls-debug-console").css({position:"fixed",zIndex:"10000000000",top:"10px",right:"10px",width:"300px",padding:"20px",background:"black","border-radius":"10px",height:e(window).height()-60,opacity:0,marginRight:150}).appendTo(e("body")).css({marginRight:0,opacity:.9}).click(function(t){if(t.shiftKey&&t.altKey){e(this).remove()}});var r=e("<div>").css({width:"100%",height:"100%",overflow:"auto"}).appendTo(n);var i=e("<div>").css({width:"100%"}).appendTo(r).append(f.d.history)}},hide:function(){e("body").find(".ls-debug-console").remove()}};e(u).click(function(e){if(e.shiftKey&&e.altKey){f.d.show()}})};f.load()};var n=function(e){var t;if(e.toLowerCase().indexOf("swing")!==-1||e.toLowerCase().indexOf("linear")!==-1){t=Linear.easeNone}else if(e.toLowerCase().indexOf("easeinout")!==-1){var n=e.toLowerCase().split("easeinout")[1];t=window[n.charAt(0).toUpperCase()+n.slice(1)].easeInOut}else if(e.toLowerCase().indexOf("easeout")!==-1){var n=e.toLowerCase().split("easeout")[1];t=window[n.charAt(0).toUpperCase()+n.slice(1)].easeOut}else if(e.toLowerCase().indexOf("easein")!==-1){var n=e.toLowerCase().split("easein")[1];t=window[n.charAt(0).toUpperCase()+n.slice(1)].easeIn}return t};var r=function(e,t,r,i){if(typeof t==="undefined"){var t="easeInOutQuart"}var s={};if(e.rotate!==i){s.rotation=e.rotate}if(e.rotateY!==i){s.rotationY=e.rotateY}if(e.rotateX!==i){s.rotationX=e.rotateX}if(r==="after"){s.scaleX=s.scaleY=s.scaleZ=1}else if(e.scale3d!==i){s.scaleX=s.scaleY=s.scaleZ=e.scale3d}if(e.delay){s.delay=r==="after"?e.delay/1e3:e.delay}s.ease=n(t);return s};var i=function(t){var n=e("<div>"),r=false,i=false,s=["perspective","OPerspective","msPerspective","MozPerspective","WebkitPerspective"];transform=["transformStyle","OTransformStyle","msTransformStyle","MozTransformStyle","WebkitTransformStyle"];for(var o=s.length-1;o>=0;o--){r=r?r:n[0].style[s[o]]!=undefined}for(var o=transform.length-1;o>=0;o--){n.css("transform-style","preserve-3d");i=i?i:n[0].style[transform[o]]=="preserve-3d"}if(r&&n[0].style[s[4]]!=undefined){n.attr("id","ls-test3d").appendTo(t);r=n[0].offsetHeight===3&&n[0].offsetLeft===9;n.remove()}return r&&i};var s=function(e,t,n){var r=[];if(n=="forward"){for(var i=0;i<e;i++){for(var s=0;s<t;s++){r.push(i+s*e)}}}else{for(var i=e-1;i>-1;i--){for(var s=t-1;s>-1;s--){r.push(i+s*e)}}}return r};var o=function(e){var t=0;for(var n in e){if(e.hasOwnProperty(n)){++t}}return t};var u=function(){uaMatch=function(e){e=e.toLowerCase();var t=/(chrome)[ \/]([\w.]+)/.exec(e)||/(webkit)[ \/]([\w.]+)/.exec(e)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e)||/(msie) ([\w.]+)/.exec(e)||e.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e)||[];return{browser:t[1]||"",version:t[2]||"0"}};var e=uaMatch(navigator.userAgent),t={};if(e.browser){t[e.browser]=true;t.version=e.version}if(t.chrome){t.webkit=true}else if(t.webkit){t.safari=true}return t};lsPrefixes=function(e,t){var n=["webkit","khtml","moz","ms","o",""];var r=0,i,s;while(r<n.length&&!e[i]){i=t;if(n[r]==""){i=i.substr(0,1).toLowerCase()+i.substr(1)}i=n[r]+i;s=typeof e[i];if(s!="undefined"){n=[n[r]];return s=="function"?e[i]():e[i]}r++}};t.global={version:"5.2.0",isMobile:function(){if(navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i)||navigator.userAgent.match(/BlackBerry/i)||navigator.userAgent.match(/Windows Phone/i)){return true}else{return false}},isHideOn3D:function(e){if(e.css("padding-bottom")=="auto"||e.css("padding-bottom")=="none"||e.css("padding-bottom")==0||e.css("padding-bottom")=="0px"){return true}else{return false}},ie78:u().msie&&u().version<9?true:false,originalAutoStart:false,paused:false,pausedByVideo:false,autoSlideshow:false,isAnimating:false,layersNum:null,prevNext:"next",slideTimer:null,sliderWidth:null,sliderHeight:null,slideDirections:{prev:{left:"right",right:"left",top:"bottom",bottom:"top"},next:{left:"left",right:"right",top:"top",bottom:"bottom"}},v:{d:500,fo:750,fi:500}};t.layerTransitions={offsetXIn:80,offsetYIn:0,durationIn:1e3,delayIn:0,easingIn:"easeInOutQuint",fadeIn:true,rotateIn:0,rotateXIn:0,rotateYIn:0,scaleIn:1,scaleXIn:1,scaleYIn:1,skewXIn:0,skewYIn:0,transformOriginIn:["50%","50%","0"],perspectiveIn:500,offsetXOut:-80,offsetYOut:0,durationOut:400,showUntil:0,easingOut:"easeInOutQuint",fadeOut:true,rotateOut:0,rotateXOut:0,rotateYOut:0,scaleOut:1,scaleXOut:1,scaleYOut:1,skewXOut:0,skewYOut:0,transformOriginOut:["50%","50%","0"],perspectiveOut:500};t.slideTransitions={slideDelay:4e3};t.options={responsive:true,responsiveUnder:0,layersContainer:0,fullScreen:false,appendTo:"",autoStart:true,startInViewport:true,pauseOnHover:true,firstSlide:1,animateFirstSlide:true,sliderFadeInDuration:350,loops:0,forceLoopNum:true,twoWaySlideshow:false,randomSlideshow:false,skin:"v5",skinsPath:"/layerslider/skins/",globalBGColor:"transparent",globalBGImage:false,navPrevNext:true,navStartStop:true,navButtons:true,keybNav:true,touchNav:true,hoverPrevNext:true,hoverBottomNav:false,showBarTimer:false,showCircleTimer:true,thumbnailNavigation:"hover",tnContainerWidth:"60%",tnWidth:100,tnHeight:60,tnActiveOpacity:35,tnInactiveOpacity:100,autoPlayVideos:true,autoPauseSlideshow:"auto",youtubePreview:"maxresdefault.jpg",imgPreload:true,lazyLoad:true,yourLogo:false,yourLogoStyle:"left: -10px; top: -10px;",yourLogoLink:false,yourLogoTarget:"_self",optimizeForMobile:true,optimizeForIE78:true,hideOnMobile:false,hideUnder:0,hideOver:1e6,staticImage:"",cbInit:function(e){},cbStart:function(e){},cbStop:function(e){},cbPause:function(e){},cbAnimStart:function(e){},cbAnimStop:function(e){},cbPrev:function(e){},cbNext:function(e){},slideDelay:4e3,slideDirection:"right",parallaxIn:.45,parallaxOut:.45,durationIn:1e3,durationOut:1e3,easingIn:"easeInOutQuint",easingOut:"easeInOutQuint",delayIn:0,delayOut:0}})(jQuery)

/*! Carrusels - jQuery OwlCarousel v1.3.3  -   http://www.owlgraphic.com/owlcarousel*/
if(typeof Object.create!=="function"){Object.create=function(e){function t(){}t.prototype=e;return new t}}(function(e,t,n){var r={init:function(t,n){var r=this;r.$elem=e(n);r.options=e.extend({},e.fn.owlCarousel.options,r.$elem.data(),t);r.userOptions=t;r.loadContent()},loadContent:function(){function r(e){var n,r="";if(typeof t.options.jsonSuccess==="function"){t.options.jsonSuccess.apply(this,[e])}else{for(n in e.owl){if(e.owl.hasOwnProperty(n)){r+=e.owl[n].item}}t.$elem.html(r)}t.logIn()}var t=this,n;if(typeof t.options.beforeInit==="function"){t.options.beforeInit.apply(this,[t.$elem])}if(typeof t.options.jsonPath==="string"){n=t.options.jsonPath;e.getJSON(n,r)}else{t.logIn()}},logIn:function(){var e=this;e.$elem.data("owl-originalStyles",e.$elem.attr("style"));e.$elem.data("owl-originalClasses",e.$elem.attr("class"));e.$elem.css({opacity:0});e.orignalItems=e.options.items;e.checkBrowser();e.wrapperWidth=0;e.checkVisible=null;e.setVars()},setVars:function(){var e=this;if(e.$elem.children().length===0){return false}e.baseClass();e.eventTypes();e.$userItems=e.$elem.children();e.itemsAmount=e.$userItems.length;e.wrapItems();e.$owlItems=e.$elem.find(".owl-item");e.$owlWrapper=e.$elem.find(".owl-wrapper");e.playDirection="next";e.prevItem=0;e.prevArr=[0];e.currentItem=0;e.customEvents();e.onStartup()},onStartup:function(){var e=this;e.updateItems();e.calculateAll();e.buildControls();e.updateControls();e.response();e.moveEvents();e.stopOnHover();e.owlStatus();if(e.options.transitionStyle!==false){e.transitionTypes(e.options.transitionStyle)}if(e.options.autoPlay===true){e.options.autoPlay=5e3}e.play();e.$elem.find(".owl-wrapper").css("display","block");if(!e.$elem.is(":visible")){e.watchVisibility()}else{e.$elem.css("opacity",1)}e.onstartup=false;e.eachMoveUpdate();if(typeof e.options.afterInit==="function"){e.options.afterInit.apply(this,[e.$elem])}},eachMoveUpdate:function(){var e=this;if(e.options.lazyLoad===true){e.lazyLoad()}if(e.options.autoHeight===true){e.autoHeight()}e.onVisibleItems();if(typeof e.options.afterAction==="function"){e.options.afterAction.apply(this,[e.$elem])}},updateVars:function(){var e=this;if(typeof e.options.beforeUpdate==="function"){e.options.beforeUpdate.apply(this,[e.$elem])}e.watchVisibility();e.updateItems();e.calculateAll();e.updatePosition();e.updateControls();e.eachMoveUpdate();if(typeof e.options.afterUpdate==="function"){e.options.afterUpdate.apply(this,[e.$elem])}},reload:function(){var e=this;t.setTimeout(function(){e.updateVars()},0)},watchVisibility:function(){var e=this;if(e.$elem.is(":visible")===false){e.$elem.css({opacity:0});t.clearInterval(e.autoPlayInterval);t.clearInterval(e.checkVisible)}else{return false}e.checkVisible=t.setInterval(function(){if(e.$elem.is(":visible")){e.reload();e.$elem.animate({opacity:1},200);t.clearInterval(e.checkVisible)}},500)},wrapItems:function(){var e=this;e.$userItems.wrapAll('<div class="owl-wrapper">').wrap('<div class="owl-item"></div>');e.$elem.find(".owl-wrapper").wrap('<div class="owl-wrapper-outer">');e.wrapperOuter=e.$elem.find(".owl-wrapper-outer");e.$elem.css("display","block")},baseClass:function(){var e=this,t=e.$elem.hasClass(e.options.baseClass),n=e.$elem.hasClass(e.options.theme);if(!t){e.$elem.addClass(e.options.baseClass)}if(!n){e.$elem.addClass(e.options.theme)}},updateItems:function(){var t=this,n,r;if(t.options.responsive===false){return false}if(t.options.singleItem===true){t.options.items=t.orignalItems=1;t.options.itemsCustom=false;t.options.itemsDesktop=false;t.options.itemsDesktopSmall=false;t.options.itemsTablet=false;t.options.itemsTabletSmall=false;t.options.itemsMobile=false;return false}n=e(t.options.responsiveBaseWidth).width();if(n>(t.options.itemsDesktop[0]||t.orignalItems)){t.options.items=t.orignalItems}if(t.options.itemsCustom!==false){t.options.itemsCustom.sort(function(e,t){return e[0]-t[0]});for(r=0;r<t.options.itemsCustom.length;r+=1){if(t.options.itemsCustom[r][0]<=n){t.options.items=t.options.itemsCustom[r][1]}}}else{if(n<=t.options.itemsDesktop[0]&&t.options.itemsDesktop!==false){t.options.items=t.options.itemsDesktop[1]}if(n<=t.options.itemsDesktopSmall[0]&&t.options.itemsDesktopSmall!==false){t.options.items=t.options.itemsDesktopSmall[1]}if(n<=t.options.itemsTablet[0]&&t.options.itemsTablet!==false){t.options.items=t.options.itemsTablet[1]}if(n<=t.options.itemsTabletSmall[0]&&t.options.itemsTabletSmall!==false){t.options.items=t.options.itemsTabletSmall[1]}if(n<=t.options.itemsMobile[0]&&t.options.itemsMobile!==false){t.options.items=t.options.itemsMobile[1]}}if(t.options.items>t.itemsAmount&&t.options.itemsScaleUp===true){t.options.items=t.itemsAmount}},response:function(){var n=this,r,i;if(n.options.responsive!==true){return false}i=e(t).width();n.resizer=function(){if(e(t).width()!==i){if(n.options.autoPlay!==false){t.clearInterval(n.autoPlayInterval)}t.clearTimeout(r);r=t.setTimeout(function(){i=e(t).width();n.updateVars()},n.options.responsiveRefreshRate)}};e(t).resize(n.resizer)},updatePosition:function(){var e=this;e.jumpTo(e.currentItem);if(e.options.autoPlay!==false){e.checkAp()}},appendItemsSizes:function(){var t=this,n=0,r=t.itemsAmount-t.options.items;t.$owlItems.each(function(i){var s=e(this);s.css({width:t.itemWidth}).data("owl-item",Number(i));if(i%t.options.items===0||i===r){if(!(i>r)){n+=1}}s.data("owl-roundPages",n)})},appendWrapperSizes:function(){var e=this,t=e.$owlItems.length*e.itemWidth;e.$owlWrapper.css({width:t*2,left:0});e.appendItemsSizes()},calculateAll:function(){var e=this;e.calculateWidth();e.appendWrapperSizes();e.loops();e.max()},calculateWidth:function(){var e=this;e.itemWidth=Math.round(e.$elem.width()/e.options.items)},max:function(){var e=this,t=(e.itemsAmount*e.itemWidth-e.options.items*e.itemWidth)*-1;if(e.options.items>e.itemsAmount){e.maximumItem=0;t=0;e.maximumPixels=0}else{e.maximumItem=e.itemsAmount-e.options.items;e.maximumPixels=t}return t},min:function(){return 0},loops:function(){var t=this,n=0,r=0,i,s,o;t.positionsInArray=[0];t.pagesInArray=[];for(i=0;i<t.itemsAmount;i+=1){r+=t.itemWidth;t.positionsInArray.push(-r);if(t.options.scrollPerPage===true){s=e(t.$owlItems[i]);o=s.data("owl-roundPages");if(o!==n){t.pagesInArray[n]=t.positionsInArray[i];n=o}}}},buildControls:function(){var t=this;if(t.options.navigation===true||t.options.pagination===true){t.owlControls=e('<div class="owl-controls"/>').toggleClass("clickable",!t.browser.isTouch).appendTo(t.$elem)}if(t.options.pagination===true){t.buildPagination()}if(t.options.navigation===true){t.buildButtons()}},buildButtons:function(){var t=this,n=e('<div class="owl-buttons"/>');t.owlControls.append(n);t.buttonPrev=e("<div/>",{"class":"owl-prev",html:t.options.navigationText[0]||""});t.buttonNext=e("<div/>",{"class":"owl-next",html:t.options.navigationText[1]||""});n.append(t.buttonPrev).append(t.buttonNext);n.on("touchstart.owlControls mousedown.owlControls",'div[class^="owl"]',function(e){e.preventDefault()});n.on("touchend.owlControls mouseup.owlControls",'div[class^="owl"]',function(n){n.preventDefault();if(e(this).hasClass("owl-next")){t.next()}else{t.prev()}})},buildPagination:function(){var t=this;t.paginationWrapper=e('<div class="owl-pagination"/>');t.owlControls.append(t.paginationWrapper);t.paginationWrapper.on("touchend.owlControls mouseup.owlControls",".owl-page",function(n){n.preventDefault();if(Number(e(this).data("owl-page"))!==t.currentItem){t.goTo(Number(e(this).data("owl-page")),true)}})},updatePagination:function(){var t=this,n,r,i,s,o,u;if(t.options.pagination===false){return false}t.paginationWrapper.html("");n=0;r=t.itemsAmount-t.itemsAmount%t.options.items;for(s=0;s<t.itemsAmount;s+=1){if(s%t.options.items===0){n+=1;if(r===s){i=t.itemsAmount-t.options.items}o=e("<div/>",{"class":"owl-page"});u=e("<span></span>",{text:t.options.paginationNumbers===true?n:"","class":t.options.paginationNumbers===true?"owl-numbers":""});o.append(u);o.data("owl-page",r===s?i:s);o.data("owl-roundPages",n);t.paginationWrapper.append(o)}}t.checkPagination()},checkPagination:function(){var t=this;if(t.options.pagination===false){return false}t.paginationWrapper.find(".owl-page").each(function(){if(e(this).data("owl-roundPages")===e(t.$owlItems[t.currentItem]).data("owl-roundPages")){t.paginationWrapper.find(".owl-page").removeClass("active");e(this).addClass("active")}})},checkNavigation:function(){var e=this;if(e.options.navigation===false){return false}if(e.options.rewindNav===false){if(e.currentItem===0&&e.maximumItem===0){e.buttonPrev.addClass("disabled");e.buttonNext.addClass("disabled")}else if(e.currentItem===0&&e.maximumItem!==0){e.buttonPrev.addClass("disabled");e.buttonNext.removeClass("disabled")}else if(e.currentItem===e.maximumItem){e.buttonPrev.removeClass("disabled");e.buttonNext.addClass("disabled")}else if(e.currentItem!==0&&e.currentItem!==e.maximumItem){e.buttonPrev.removeClass("disabled");e.buttonNext.removeClass("disabled")}}},updateControls:function(){var e=this;e.updatePagination();e.checkNavigation();if(e.owlControls){if(e.options.items>=e.itemsAmount){e.owlControls.hide()}else{e.owlControls.show()}}},destroyControls:function(){var e=this;if(e.owlControls){e.owlControls.remove()}},next:function(e){var t=this;if(t.isTransition){return false}t.currentItem+=t.options.scrollPerPage===true?t.options.items:1;if(t.currentItem>t.maximumItem+(t.options.scrollPerPage===true?t.options.items-1:0)){if(t.options.rewindNav===true){t.currentItem=0;e="rewind"}else{t.currentItem=t.maximumItem;return false}}t.goTo(t.currentItem,e)},prev:function(e){var t=this;if(t.isTransition){return false}if(t.options.scrollPerPage===true&&t.currentItem>0&&t.currentItem<t.options.items){t.currentItem=0}else{t.currentItem-=t.options.scrollPerPage===true?t.options.items:1}if(t.currentItem<0){if(t.options.rewindNav===true){t.currentItem=t.maximumItem;e="rewind"}else{t.currentItem=0;return false}}t.goTo(t.currentItem,e)},goTo:function(e,n,r){var i=this,s;if(i.isTransition){return false}if(typeof i.options.beforeMove==="function"){i.options.beforeMove.apply(this,[i.$elem])}if(e>=i.maximumItem){e=i.maximumItem}else if(e<=0){e=0}i.currentItem=i.owl.currentItem=e;if(i.options.transitionStyle!==false&&r!=="drag"&&i.options.items===1&&i.browser.support3d===true){i.swapSpeed(0);if(i.browser.support3d===true){i.transition3d(i.positionsInArray[e])}else{i.css2slide(i.positionsInArray[e],1)}i.afterGo();i.singleItemTransition();return false}s=i.positionsInArray[e];if(i.browser.support3d===true){i.isCss3Finish=false;if(n===true){i.swapSpeed("paginationSpeed");t.setTimeout(function(){i.isCss3Finish=true},i.options.paginationSpeed)}else if(n==="rewind"){i.swapSpeed(i.options.rewindSpeed);t.setTimeout(function(){i.isCss3Finish=true},i.options.rewindSpeed)}else{i.swapSpeed("slideSpeed");t.setTimeout(function(){i.isCss3Finish=true},i.options.slideSpeed)}i.transition3d(s)}else{if(n===true){i.css2slide(s,i.options.paginationSpeed)}else if(n==="rewind"){i.css2slide(s,i.options.rewindSpeed)}else{i.css2slide(s,i.options.slideSpeed)}}i.afterGo()},jumpTo:function(e){var t=this;if(typeof t.options.beforeMove==="function"){t.options.beforeMove.apply(this,[t.$elem])}if(e>=t.maximumItem||e===-1){e=t.maximumItem}else if(e<=0){e=0}t.swapSpeed(0);if(t.browser.support3d===true){t.transition3d(t.positionsInArray[e])}else{t.css2slide(t.positionsInArray[e],1)}t.currentItem=t.owl.currentItem=e;t.afterGo()},afterGo:function(){var e=this;e.prevArr.push(e.currentItem);e.prevItem=e.owl.prevItem=e.prevArr[e.prevArr.length-2];e.prevArr.shift(0);if(e.prevItem!==e.currentItem){e.checkPagination();e.checkNavigation();e.eachMoveUpdate();if(e.options.autoPlay!==false){e.checkAp()}}if(typeof e.options.afterMove==="function"&&e.prevItem!==e.currentItem){e.options.afterMove.apply(this,[e.$elem])}},stop:function(){var e=this;e.apStatus="stop";t.clearInterval(e.autoPlayInterval)},checkAp:function(){var e=this;if(e.apStatus!=="stop"){e.play()}},play:function(){var e=this;e.apStatus="play";if(e.options.autoPlay===false){return false}t.clearInterval(e.autoPlayInterval);e.autoPlayInterval=t.setInterval(function(){e.next(true)},e.options.autoPlay)},swapSpeed:function(e){var t=this;if(e==="slideSpeed"){t.$owlWrapper.css(t.addCssSpeed(t.options.slideSpeed))}else if(e==="paginationSpeed"){t.$owlWrapper.css(t.addCssSpeed(t.options.paginationSpeed))}else if(typeof e!=="string"){t.$owlWrapper.css(t.addCssSpeed(e))}},addCssSpeed:function(e){return{"-webkit-transition":"all "+e+"ms ease","-moz-transition":"all "+e+"ms ease","-o-transition":"all "+e+"ms ease",transition:"all "+e+"ms ease"}},removeTransition:function(){return{"-webkit-transition":"","-moz-transition":"","-o-transition":"",transition:""}},doTranslate:function(e){return{"-webkit-transform":"translate3d("+e+"px, 0px, 0px)","-moz-transform":"translate3d("+e+"px, 0px, 0px)","-o-transform":"translate3d("+e+"px, 0px, 0px)","-ms-transform":"translate3d("+e+"px, 0px, 0px)",transform:"translate3d("+e+"px, 0px,0px)"}},transition3d:function(e){var t=this;t.$owlWrapper.css(t.doTranslate(e))},css2move:function(e){var t=this;t.$owlWrapper.css({left:e})},css2slide:function(e,t){var n=this;n.isCssFinish=false;n.$owlWrapper.stop(true,true).animate({left:e},{duration:t||n.options.slideSpeed,complete:function(){n.isCssFinish=true}})},checkBrowser:function(){var e=this,r="translate3d(0px, 0px, 0px)",i=n.createElement("div"),s,o,u,a;i.style.cssText="  -moz-transform:"+r+"; -ms-transform:"+r+"; -o-transform:"+r+"; -webkit-transform:"+r+"; transform:"+r;s=/translate3d\(0px, 0px, 0px\)/g;o=i.style.cssText.match(s);u=o!==null&&o.length===1;a="ontouchstart"in t||t.navigator.msMaxTouchPoints;e.browser={support3d:u,isTouch:a}},moveEvents:function(){var e=this;if(e.options.mouseDrag!==false||e.options.touchDrag!==false){e.gestures();e.disabledEvents()}},eventTypes:function(){var e=this,t=["s","e","x"];e.ev_types={};if(e.options.mouseDrag===true&&e.options.touchDrag===true){t=["touchstart.owl mousedown.owl","touchmove.owl mousemove.owl","touchend.owl touchcancel.owl mouseup.owl"]}else if(e.options.mouseDrag===false&&e.options.touchDrag===true){t=["touchstart.owl","touchmove.owl","touchend.owl touchcancel.owl"]}else if(e.options.mouseDrag===true&&e.options.touchDrag===false){t=["mousedown.owl","mousemove.owl","mouseup.owl"]}e.ev_types.start=t[0];e.ev_types.move=t[1];e.ev_types.end=t[2]},disabledEvents:function(){var t=this;t.$elem.on("dragstart.owl",function(e){e.preventDefault()});t.$elem.on("mousedown.disableTextSelect",function(t){return e(t.target).is("input, textarea, select, option")})},gestures:function(){function s(e){if(e.touches!==undefined){return{x:e.touches[0].pageX,y:e.touches[0].pageY}}if(e.touches===undefined){if(e.pageX!==undefined){return{x:e.pageX,y:e.pageY}}if(e.pageX===undefined){return{x:e.clientX,y:e.clientY}}}}function o(t){if(t==="on"){e(n).on(r.ev_types.move,a);e(n).on(r.ev_types.end,f)}else if(t==="off"){e(n).off(r.ev_types.move);e(n).off(r.ev_types.end)}}function u(n){var u=n.originalEvent||n||t.event,a;if(u.which===3){return false}if(r.itemsAmount<=r.options.items){return}if(r.isCssFinish===false&&!r.options.dragBeforeAnimFinish){return false}if(r.isCss3Finish===false&&!r.options.dragBeforeAnimFinish){return false}if(r.options.autoPlay!==false){t.clearInterval(r.autoPlayInterval)}if(r.browser.isTouch!==true&&!r.$owlWrapper.hasClass("grabbing")){r.$owlWrapper.addClass("grabbing")}r.newPosX=0;r.newRelativeX=0;e(this).css(r.removeTransition());a=e(this).position();i.relativePos=a.left;i.offsetX=s(u).x-a.left;i.offsetY=s(u).y-a.top;o("on");i.sliding=false;i.targetElement=u.target||u.srcElement}function a(o){var u=o.originalEvent||o||t.event,a,f;r.newPosX=s(u).x-i.offsetX;r.newPosY=s(u).y-i.offsetY;r.newRelativeX=r.newPosX-i.relativePos;if(typeof r.options.startDragging==="function"&&i.dragging!==true&&r.newRelativeX!==0){i.dragging=true;r.options.startDragging.apply(r,[r.$elem])}if((r.newRelativeX>8||r.newRelativeX<-8)&&r.browser.isTouch===true){if(u.preventDefault!==undefined){u.preventDefault()}else{u.returnValue=false}i.sliding=true}if((r.newPosY>10||r.newPosY<-10)&&i.sliding===false){e(n).off("touchmove.owl")}a=function(){return r.newRelativeX/5};f=function(){return r.maximumPixels+r.newRelativeX/5};r.newPosX=Math.max(Math.min(r.newPosX,a()),f());if(r.browser.support3d===true){r.transition3d(r.newPosX)}else{r.css2move(r.newPosX)}}function f(n){var s=n.originalEvent||n||t.event,u,a,f;s.target=s.target||s.srcElement;i.dragging=false;if(r.browser.isTouch!==true){r.$owlWrapper.removeClass("grabbing")}if(r.newRelativeX<0){r.dragDirection=r.owl.dragDirection="left"}else{r.dragDirection=r.owl.dragDirection="right"}if(r.newRelativeX!==0){u=r.getNewPosition();r.goTo(u,false,"drag");if(i.targetElement===s.target&&r.browser.isTouch!==true){e(s.target).on("click.disable",function(t){t.stopImmediatePropagation();t.stopPropagation();t.preventDefault();e(t.target).off("click.disable")});a=e._data(s.target,"events").click;f=a.pop();a.splice(0,0,f)}}o("off")}var r=this,i={offsetX:0,offsetY:0,baseElWidth:0,relativePos:0,position:null,minSwipe:null,maxSwipe:null,sliding:null,dargging:null,targetElement:null};r.isCssFinish=true;r.$elem.on(r.ev_types.start,".owl-wrapper",u)},getNewPosition:function(){var e=this,t=e.closestItem();if(t>e.maximumItem){e.currentItem=e.maximumItem;t=e.maximumItem}else if(e.newPosX>=0){t=0;e.currentItem=0}return t},closestItem:function(){var t=this,n=t.options.scrollPerPage===true?t.pagesInArray:t.positionsInArray,r=t.newPosX,i=null;e.each(n,function(s,o){if(r-t.itemWidth/20>n[s+1]&&r-t.itemWidth/20<o&&t.moveDirection()==="left"){i=o;if(t.options.scrollPerPage===true){t.currentItem=e.inArray(i,t.positionsInArray)}else{t.currentItem=s}}else if(r+t.itemWidth/20<o&&r+t.itemWidth/20>(n[s+1]||n[s]-t.itemWidth)&&t.moveDirection()==="right"){if(t.options.scrollPerPage===true){i=n[s+1]||n[n.length-1];t.currentItem=e.inArray(i,t.positionsInArray)}else{i=n[s+1];t.currentItem=s+1}}});return t.currentItem},moveDirection:function(){var e=this,t;if(e.newRelativeX<0){t="right";e.playDirection="next"}else{t="left";e.playDirection="prev"}return t},customEvents:function(){var e=this;e.$elem.on("owl.next",function(){e.next()});e.$elem.on("owl.prev",function(){e.prev()});e.$elem.on("owl.play",function(t,n){e.options.autoPlay=n;e.play();e.hoverStatus="play"});e.$elem.on("owl.stop",function(){e.stop();e.hoverStatus="stop"});e.$elem.on("owl.goTo",function(t,n){e.goTo(n)});e.$elem.on("owl.jumpTo",function(t,n){e.jumpTo(n)})},stopOnHover:function(){var e=this;if(e.options.stopOnHover===true&&e.browser.isTouch!==true&&e.options.autoPlay!==false){e.$elem.on("mouseover",function(){e.stop()});e.$elem.on("mouseout",function(){if(e.hoverStatus!=="stop"){e.play()}})}},lazyLoad:function(){var t=this,n,r,i,s,o;if(t.options.lazyLoad===false){return false}for(n=0;n<t.itemsAmount;n+=1){r=e(t.$owlItems[n]);if(r.data("owl-loaded")==="loaded"){continue}i=r.data("owl-item");s=r.find(".lazyOwl");if(typeof s.data("src")!=="string"){r.data("owl-loaded","loaded");continue}if(r.data("owl-loaded")===undefined){s.hide();r.addClass("loading").data("owl-loaded","checked")}if(t.options.lazyFollow===true){o=i>=t.currentItem}else{o=true}if(o&&i<t.currentItem+t.options.items&&s.length){t.lazyPreload(r,s)}}},lazyPreload:function(e,n){function o(){e.data("owl-loaded","loaded").removeClass("loading");n.removeAttr("data-src");if(r.options.lazyEffect==="fade"){n.fadeIn(400)}else{n.show()}if(typeof r.options.afterLazyLoad==="function"){r.options.afterLazyLoad.apply(this,[r.$elem])}}function u(){i+=1;if(r.completeImg(n.get(0))||s===true){o()}else if(i<=100){t.setTimeout(u,100)}else{o()}}var r=this,i=0,s;if(n.prop("tagName")==="DIV"){n.css("background-image","url("+n.data("src")+")");s=true}else{n[0].src=n.data("src")}u()},autoHeight:function(){function s(){var r=e(n.$owlItems[n.currentItem]).height();n.wrapperOuter.css("height",r+"px");if(!n.wrapperOuter.hasClass("autoHeight")){t.setTimeout(function(){n.wrapperOuter.addClass("autoHeight")},0)}}function o(){i+=1;if(n.completeImg(r.get(0))){s()}else if(i<=100){t.setTimeout(o,100)}else{n.wrapperOuter.css("height","")}}var n=this,r=e(n.$owlItems[n.currentItem]).find("img"),i;if(r.get(0)!==undefined){i=0;o()}else{s()}},completeImg:function(e){var t;if(!e.complete){return false}t=typeof e.naturalWidth;if(t!=="undefined"&&e.naturalWidth===0){return false}return true},onVisibleItems:function(){var t=this,n;if(t.options.addClassActive===true){t.$owlItems.removeClass("active")}t.visibleItems=[];for(n=t.currentItem;n<t.currentItem+t.options.items;n+=1){t.visibleItems.push(n);if(t.options.addClassActive===true){e(t.$owlItems[n]).addClass("active")}}t.owl.visibleItems=t.visibleItems},transitionTypes:function(e){var t=this;t.outClass="owl-"+e+"-out";t.inClass="owl-"+e+"-in"},singleItemTransition:function(){function a(e){return{position:"relative",left:e+"px"}}var e=this,t=e.outClass,n=e.inClass,r=e.$owlItems.eq(e.currentItem),i=e.$owlItems.eq(e.prevItem),s=Math.abs(e.positionsInArray[e.currentItem])+e.positionsInArray[e.prevItem],o=Math.abs(e.positionsInArray[e.currentItem])+e.itemWidth/2,u="webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend";e.isTransition=true;e.$owlWrapper.addClass("owl-origin").css({"-webkit-transform-origin":o+"px","-moz-perspective-origin":o+"px","perspective-origin":o+"px"});i.css(a(s,10)).addClass(t).on(u,function(){e.endPrev=true;i.off(u);e.clearTransStyle(i,t)});r.addClass(n).on(u,function(){e.endCurrent=true;r.off(u);e.clearTransStyle(r,n)})},clearTransStyle:function(e,t){var n=this;e.css({position:"",left:""}).removeClass(t);if(n.endPrev&&n.endCurrent){n.$owlWrapper.removeClass("owl-origin");n.endPrev=false;n.endCurrent=false;n.isTransition=false}},owlStatus:function(){var e=this;e.owl={userOptions:e.userOptions,baseElement:e.$elem,userItems:e.$userItems,owlItems:e.$owlItems,currentItem:e.currentItem,prevItem:e.prevItem,visibleItems:e.visibleItems,isTouch:e.browser.isTouch,browser:e.browser,dragDirection:e.dragDirection}},clearEvents:function(){var r=this;r.$elem.off(".owl owl mousedown.disableTextSelect");e(n).off(".owl owl");e(t).off("resize",r.resizer)},unWrap:function(){var e=this;if(e.$elem.children().length!==0){e.$owlWrapper.unwrap();e.$userItems.unwrap().unwrap();if(e.owlControls){e.owlControls.remove()}}e.clearEvents();e.$elem.attr("style",e.$elem.data("owl-originalStyles")||"").attr("class",e.$elem.data("owl-originalClasses"))},destroy:function(){var e=this;e.stop();t.clearInterval(e.checkVisible);e.unWrap();e.$elem.removeData()},reinit:function(t){var n=this,r=e.extend({},n.userOptions,t);n.unWrap();n.init(r,n.$elem)},addItem:function(e,t){var n=this,r;if(!e){return false}if(n.$elem.children().length===0){n.$elem.append(e);n.setVars();return false}n.unWrap();if(t===undefined||t===-1){r=-1}else{r=t}if(r>=n.$userItems.length||r===-1){n.$userItems.eq(-1).after(e)}else{n.$userItems.eq(r).before(e)}n.setVars()},removeItem:function(e){var t=this,n;if(t.$elem.children().length===0){return false}if(e===undefined||e===-1){n=-1}else{n=e}t.unWrap();t.$userItems.eq(n).remove();t.setVars()}};e.fn.owlCarousel=function(t){return this.each(function(){if(e(this).data("owl-init")===true){return false}e(this).data("owl-init",true);var n=Object.create(r);n.init(t,this);e.data(this,"owlCarousel",n)})};e.fn.owlCarousel.options={items:5,itemsCustom:false,itemsDesktop:[1199,4],itemsDesktopSmall:[979,3],itemsTablet:[768,2],itemsTabletSmall:false,itemsMobile:[479,1],singleItem:false,itemsScaleUp:false,slideSpeed:200,paginationSpeed:800,rewindSpeed:1e3,autoPlay:false,stopOnHover:false,navigation:false,navigationText:["prev","next"],rewindNav:true,scrollPerPage:false,pagination:true,paginationNumbers:false,responsive:true,responsiveRefreshRate:200,responsiveBaseWidth:t,baseClass:"owl-carousel",theme:"owl-theme",lazyLoad:false,lazyFollow:true,lazyEffect:"fade",autoHeight:false,jsonPath:false,jsonSuccess:false,dragBeforeAnimFinish:true,mouseDrag:true,touchDrag:true,addClassActive:false,transitionStyle:false,beforeUpdate:false,afterUpdate:false,beforeInit:false,afterInit:false,beforeMove:false,afterMove:false,afterAction:false,startDragging:false,afterLazyLoad:false}})(jQuery,window,document)

/*! By http://jquery-manual.blogspot.com Copyright © 2013 */
$.fn.limitChar=function(e,t){if(t==undefined){t=true}t==true?_dat=" ...":_dat="";element=this;element.each(function(){text=$(this).text();getText=text.substr(0,e)+_dat;$(this).text(getText)})}


/*! flipclock 2015-01-19  CountDown */
var Base=function(){};Base.extend=function(t,i){"use strict";var e=Base.prototype.extend;Base._prototyping=!0;var n=new this;e.call(n,t),n.base=function(){},delete Base._prototyping;var s=n.constructor,o=n.constructor=function(){if(!Base._prototyping)if(this._constructing||this.constructor==o)this._constructing=!0,s.apply(this,arguments),delete this._constructing;else if(null!==arguments[0])return(arguments[0].extend||e).call(arguments[0],n)};return o.ancestor=this,o.extend=this.extend,o.forEach=this.forEach,o.implement=this.implement,o.prototype=n,o.toString=this.toString,o.valueOf=function(t){return"object"==t?o:s.valueOf()},e.call(o,i),"function"==typeof o.init&&o.init(),o},Base.prototype={extend:function(t,i){if(arguments.length>1){var e=this[t];if(e&&"function"==typeof i&&(!e.valueOf||e.valueOf()!=i.valueOf())&&/\bbase\b/.test(i)){var n=i.valueOf();i=function(){var t=this.base||Base.prototype.base;this.base=e;var i=n.apply(this,arguments);return this.base=t,i},i.valueOf=function(t){return"object"==t?i:n},i.toString=Base.toString}this[t]=i}else if(t){var s=Base.prototype.extend;Base._prototyping||"function"==typeof this||(s=this.extend||s);for(var o={toSource:null},a=["constructor","toString","valueOf"],c=Base._prototyping?0:1;r=a[c++];)t[r]!=o[r]&&s.call(this,r,t[r]);for(var r in t)o[r]||s.call(this,r,t[r])}return this}},Base=Base.extend({constructor:function(){this.extend(arguments[0])}},{ancestor:Object,version:"1.1",forEach:function(t,i,e){for(var n in t)void 0===this.prototype[n]&&i.call(e,t[n],n,t)},implement:function(){for(var t=0;t<arguments.length;t++)"function"==typeof arguments[t]?arguments[t](this.prototype):this.prototype.extend(arguments[t]);return this},toString:function(){return String(this.valueOf())}});var FlipClock;!function(t){"use strict";FlipClock=function(t,i,e){return i instanceof Object&&i instanceof Date==0&&(e=i,i=0),new FlipClock.Factory(t,i,e)},FlipClock.Lang={},FlipClock.Base=Base.extend({buildDate:"2014-12-12",version:"0.7.7",constructor:function(i,e){"object"!=typeof i&&(i={}),"object"!=typeof e&&(e={}),this.setOptions(t.extend(!0,{},i,e))},callback:function(t){if("function"==typeof t){for(var i=[],e=1;e<=arguments.length;e++)arguments[e]&&i.push(arguments[e]);t.apply(this,i)}},log:function(t){window.console&&console.log&&console.log(t)},getOption:function(t){return this[t]?this[t]:!1},getOptions:function(){return this},setOption:function(t,i){this[t]=i},setOptions:function(t){for(var i in t)"undefined"!=typeof t[i]&&this.setOption(i,t[i])}})}(jQuery),function(t){"use strict";FlipClock.Face=FlipClock.Base.extend({autoStart:!0,dividers:[],factory:!1,lists:[],constructor:function(t,i){this.dividers=[],this.lists=[],this.base(i),this.factory=t},build:function(){this.autoStart&&this.start()},createDivider:function(i,e,n){"boolean"!=typeof e&&e||(n=e,e=i);var s=['<span class="'+this.factory.classes.dot+' top"></span>','<span class="'+this.factory.classes.dot+' bottom"></span>'].join("");n&&(s=""),i=this.factory.localize(i);var o=['<span class="'+this.factory.classes.divider+" "+(e?e:"").toLowerCase()+'">','<span class="'+this.factory.classes.label+'">'+(i?i:"")+"</span>",s,"</span>"],a=t(o.join(""));return this.dividers.push(a),a},createList:function(t,i){"object"==typeof t&&(i=t,t=0);var e=new FlipClock.List(this.factory,t,i);return this.lists.push(e),e},reset:function(){this.factory.time=new FlipClock.Time(this.factory,this.factory.original?Math.round(this.factory.original):0,{minimumDigits:this.factory.minimumDigits}),this.flip(this.factory.original,!1)},appendDigitToClock:function(t){t.$el.append(!1)},addDigit:function(t){var i=this.createList(t,{classes:{active:this.factory.classes.active,before:this.factory.classes.before,flip:this.factory.classes.flip}});this.appendDigitToClock(i)},start:function(){},stop:function(){},autoIncrement:function(){this.factory.countdown?this.decrement():this.increment()},increment:function(){this.factory.time.addSecond()},decrement:function(){0==this.factory.time.getTimeSeconds()?this.factory.stop():this.factory.time.subSecond()},flip:function(i,e){var n=this;t.each(i,function(t,i){var s=n.lists[t];s?(e||i==s.digit||s.play(),s.select(i)):n.addDigit(i)})}})}(jQuery),function(t){"use strict";FlipClock.Factory=FlipClock.Base.extend({animationRate:1e3,autoStart:!0,callbacks:{destroy:!1,create:!1,init:!1,interval:!1,start:!1,stop:!1,reset:!1},classes:{active:"flip-clock-active",before:"flip-clock-before",divider:"flip-clock-divider",dot:"flip-clock-dot",label:"flip-clock-label",flip:"flip",play:"play",wrapper:"flip-clock-wrapper"},clockFace:"HourlyCounter",countdown:!1,defaultClockFace:"HourlyCounter",defaultLanguage:"english",$el:!1,face:!0,lang:!1,language:"english",minimumDigits:0,original:!1,running:!1,time:!1,timer:!1,$wrapper:!1,constructor:function(i,e,n){n||(n={}),this.lists=[],this.running=!1,this.base(n),this.$el=t(i).addClass(this.classes.wrapper),this.$wrapper=this.$el,this.original=e instanceof Date?e:e?Math.round(e):0,this.time=new FlipClock.Time(this,this.original,{minimumDigits:this.minimumDigits,animationRate:this.animationRate}),this.timer=new FlipClock.Timer(this,n),this.loadLanguage(this.language),this.loadClockFace(this.clockFace,n),this.autoStart&&this.start()},loadClockFace:function(t,i){var e,n="Face",s=!1;return t=t.ucfirst()+n,this.face.stop&&(this.stop(),s=!0),this.$el.html(""),this.time.minimumDigits=this.minimumDigits,e=FlipClock[t]?new FlipClock[t](this,i):new FlipClock[this.defaultClockFace+n](this,i),e.build(),this.face=e,s&&this.start(),this.face},loadLanguage:function(t){var i;return i=FlipClock.Lang[t.ucfirst()]?FlipClock.Lang[t.ucfirst()]:FlipClock.Lang[t]?FlipClock.Lang[t]:FlipClock.Lang[this.defaultLanguage],this.lang=i},localize:function(t,i){var e=this.lang;if(!t)return null;var n=t.toLowerCase();return"object"==typeof i&&(e=i),e&&e[n]?e[n]:t},start:function(t){var i=this;i.running||i.countdown&&!(i.countdown&&i.time.time>0)?i.log("Trying to start timer when countdown already at 0"):(i.face.start(i.time),i.timer.start(function(){i.flip(),"function"==typeof t&&t()}))},stop:function(t){this.face.stop(),this.timer.stop(t);for(var i in this.lists)this.lists.hasOwnProperty(i)&&this.lists[i].stop()},reset:function(t){this.timer.reset(t),this.face.reset()},setTime:function(t){this.time.time=t,this.flip(!0)},getTime:function(){return this.time},setCountdown:function(t){var i=this.running;this.countdown=t?!0:!1,i&&(this.stop(),this.start())},flip:function(t){this.face.flip(!1,t)}})}(jQuery),function(t){"use strict";FlipClock.List=FlipClock.Base.extend({digit:0,classes:{active:"flip-clock-active",before:"flip-clock-before",flip:"flip"},factory:!1,$el:!1,$obj:!1,items:[],lastDigit:0,constructor:function(t,i){this.factory=t,this.digit=i,this.lastDigit=i,this.$el=this.createList(),this.$obj=this.$el,i>0&&this.select(i),this.factory.$el.append(this.$el)},select:function(t){if("undefined"==typeof t?t=this.digit:this.digit=t,this.digit!=this.lastDigit){var i=this.$el.find("."+this.classes.before).removeClass(this.classes.before);this.$el.find("."+this.classes.active).removeClass(this.classes.active).addClass(this.classes.before),this.appendListItem(this.classes.active,this.digit),i.remove(),this.lastDigit=this.digit}},play:function(){this.$el.addClass(this.factory.classes.play)},stop:function(){var t=this;setTimeout(function(){t.$el.removeClass(t.factory.classes.play)},this.factory.timer.interval)},createListItem:function(t,i){return['<li class="'+(t?t:"")+'">','<a href="#">','<div class="up">','<div class="shadow"></div>','<div class="inn">'+(i?i:"")+"</div>","</div>",'<div class="down">','<div class="shadow"></div>','<div class="inn">'+(i?i:"")+"</div>","</div>","</a>","</li>"].join("")},appendListItem:function(t,i){var e=this.createListItem(t,i);this.$el.append(e)},createList:function(){var i=this.getPrevDigit()?this.getPrevDigit():this.digit,e=t(['<ul class="'+this.classes.flip+" "+(this.factory.running?this.factory.classes.play:"")+'">',this.createListItem(this.classes.before,i),this.createListItem(this.classes.active,this.digit),"</ul>"].join(""));return e},getNextDigit:function(){return 9==this.digit?0:this.digit+1},getPrevDigit:function(){return 0==this.digit?9:this.digit-1}})}(jQuery),function(t){"use strict";String.prototype.ucfirst=function(){return this.substr(0,1).toUpperCase()+this.substr(1)},t.fn.FlipClock=function(i,e){return new FlipClock(t(this),i,e)},t.fn.flipClock=function(i,e){return t.fn.FlipClock(i,e)}}(jQuery),function(t){"use strict";FlipClock.Time=FlipClock.Base.extend({time:0,factory:!1,minimumDigits:0,constructor:function(t,i,e){"object"!=typeof e&&(e={}),e.minimumDigits||(e.minimumDigits=t.minimumDigits),this.base(e),this.factory=t,i&&(this.time=i)},convertDigitsToArray:function(t){var i=[];t=t.toString();for(var e=0;e<t.length;e++)t[e].match(/^\d*$/g)&&i.push(t[e]);return i},digit:function(t){var i=this.toString(),e=i.length;return i[e-t]?i[e-t]:!1},digitize:function(i){var e=[];if(t.each(i,function(t,i){i=i.toString(),1==i.length&&(i="0"+i);for(var n=0;n<i.length;n++)e.push(i.charAt(n))}),e.length>this.minimumDigits&&(this.minimumDigits=e.length),this.minimumDigits>e.length)for(var n=e.length;n<this.minimumDigits;n++)e.unshift("0");return e},getDateObject:function(){return this.time instanceof Date?this.time:new Date((new Date).getTime()+1e3*this.getTimeSeconds())},getDayCounter:function(t){var i=[this.getDays(),this.getHours(!0),this.getMinutes(!0)];return t&&i.push(this.getSeconds(!0)),this.digitize(i)},getDays:function(t){var i=this.getTimeSeconds()/60/60/24;return t&&(i%=7),Math.floor(i)},getHourCounter:function(){var t=this.digitize([this.getHours(),this.getMinutes(!0),this.getSeconds(!0)]);return t},getHourly:function(){return this.getHourCounter()},getHours:function(t){var i=this.getTimeSeconds()/60/60;return t&&(i%=24),Math.floor(i)},getMilitaryTime:function(t,i){"undefined"==typeof i&&(i=!0),t||(t=this.getDateObject());var e=[t.getHours(),t.getMinutes()];return i===!0&&e.push(t.getSeconds()),this.digitize(e)},getMinutes:function(t){var i=this.getTimeSeconds()/60;return t&&(i%=60),Math.floor(i)},getMinuteCounter:function(){var t=this.digitize([this.getMinutes(),this.getSeconds(!0)]);return t},getTimeSeconds:function(t){return t||(t=new Date),this.time instanceof Date?this.factory.countdown?Math.max(this.time.getTime()/1e3-t.getTime()/1e3,0):t.getTime()/1e3-this.time.getTime()/1e3:this.time},getTime:function(t,i){"undefined"==typeof i&&(i=!0),t||(t=this.getDateObject()),console.log(t);var e=t.getHours(),n=[e>12?e-12:0===e?12:e,t.getMinutes()];return i===!0&&n.push(t.getSeconds()),this.digitize(n)},getSeconds:function(t){var i=this.getTimeSeconds();return t&&(60==i?i=0:i%=60),Math.ceil(i)},getWeeks:function(t){var i=this.getTimeSeconds()/60/60/24/7;return t&&(i%=52),Math.floor(i)},removeLeadingZeros:function(i,e){var n=0,s=[];return t.each(e,function(t){i>t?n+=parseInt(e[t],10):s.push(e[t])}),0===n?s:e},addSeconds:function(t){this.time instanceof Date?this.time.setSeconds(this.time.getSeconds()+t):this.time+=t},addSecond:function(){this.addSeconds(1)},subSeconds:function(t){this.time instanceof Date?this.time.setSeconds(this.time.getSeconds()-t):this.time-=t},subSecond:function(){this.subSeconds(1)},toString:function(){return this.getTimeSeconds().toString()}})}(jQuery),function(){"use strict";FlipClock.Timer=FlipClock.Base.extend({callbacks:{destroy:!1,create:!1,init:!1,interval:!1,start:!1,stop:!1,reset:!1},count:0,factory:!1,interval:1e3,animationRate:1e3,constructor:function(t,i){this.base(i),this.factory=t,this.callback(this.callbacks.init),this.callback(this.callbacks.create)},getElapsed:function(){return this.count*this.interval},getElapsedTime:function(){return new Date(this.time+this.getElapsed())},reset:function(t){clearInterval(this.timer),this.count=0,this._setInterval(t),this.callback(this.callbacks.reset)},start:function(t){this.factory.running=!0,this._createTimer(t),this.callback(this.callbacks.start)},stop:function(t){this.factory.running=!1,this._clearInterval(t),this.callback(this.callbacks.stop),this.callback(t)},_clearInterval:function(){clearInterval(this.timer)},_createTimer:function(t){this._setInterval(t)},_destroyTimer:function(t){this._clearInterval(),this.timer=!1,this.callback(t),this.callback(this.callbacks.destroy)},_interval:function(t){this.callback(this.callbacks.interval),this.callback(t),this.count++},_setInterval:function(t){var i=this;i._interval(t),i.timer=setInterval(function(){i._interval(t)},this.interval)}})}(jQuery),function(t){FlipClock.TwentyFourHourClockFace=FlipClock.Face.extend({constructor:function(t,i){this.base(t,i)},build:function(i){var e=this,n=this.factory.$el.find("ul");this.factory.time.time||(this.factory.original=new Date,this.factory.time=new FlipClock.Time(this.factory,this.factory.original));var i=i?i:this.factory.time.getMilitaryTime(!1,this.showSeconds);i.length>n.length&&t.each(i,function(t,i){e.createList(i)}),this.createDivider(),this.createDivider(),t(this.dividers[0]).insertBefore(this.lists[this.lists.length-2].$el),t(this.dividers[1]).insertBefore(this.lists[this.lists.length-4].$el),this.base()},flip:function(t,i){this.autoIncrement(),t=t?t:this.factory.time.getMilitaryTime(!1,this.showSeconds),this.base(t,i)}})}(jQuery),function(t){FlipClock.CounterFace=FlipClock.Face.extend({shouldAutoIncrement:!1,constructor:function(t,i){"object"!=typeof i&&(i={}),t.autoStart=i.autoStart?!0:!1,i.autoStart&&(this.shouldAutoIncrement=!0),t.increment=function(){t.countdown=!1,t.setTime(t.getTime().getTimeSeconds()+1)},t.decrement=function(){t.countdown=!0;var i=t.getTime().getTimeSeconds();i>0&&t.setTime(i-1)},t.setValue=function(i){t.setTime(i)},t.setCounter=function(i){t.setTime(i)},this.base(t,i)},build:function(){var i=this,e=this.factory.$el.find("ul"),n=this.factory.getTime().digitize([this.factory.getTime().time]);n.length>e.length&&t.each(n,function(t,e){var n=i.createList(e);n.select(e)}),t.each(this.lists,function(t,i){i.play()}),this.base()},flip:function(t,i){this.shouldAutoIncrement&&this.autoIncrement(),t||(t=this.factory.getTime().digitize([this.factory.getTime().time])),this.base(t,i)},reset:function(){this.factory.time=new FlipClock.Time(this.factory,this.factory.original?Math.round(this.factory.original):0),this.flip()}})}(jQuery),function(t){FlipClock.DailyCounterFace=FlipClock.Face.extend({showSeconds:!0,constructor:function(t,i){this.base(t,i)},build:function(i){var e=this,n=this.factory.$el.find("ul"),s=0;i=i?i:this.factory.time.getDayCounter(this.showSeconds),i.length>n.length&&t.each(i,function(t,i){e.createList(i)}),this.showSeconds?t(this.createDivider("Seconds")).insertBefore(this.lists[this.lists.length-2].$el):s=2,t(this.createDivider("Minutes")).insertBefore(this.lists[this.lists.length-4+s].$el),t(this.createDivider("Hours")).insertBefore(this.lists[this.lists.length-6+s].$el),t(this.createDivider("Days",!0)).insertBefore(this.lists[0].$el),this.base()},flip:function(t,i){t||(t=this.factory.time.getDayCounter(this.showSeconds)),this.autoIncrement(),this.base(t,i)}})}(jQuery),function(t){FlipClock.HourlyCounterFace=FlipClock.Face.extend({constructor:function(t,i){this.base(t,i)},build:function(i,e){var n=this,s=this.factory.$el.find("ul");e=e?e:this.factory.time.getHourCounter(),e.length>s.length&&t.each(e,function(t,i){n.createList(i)}),t(this.createDivider("Seconds")).insertBefore(this.lists[this.lists.length-2].$el),t(this.createDivider("Minutes")).insertBefore(this.lists[this.lists.length-4].$el),i||t(this.createDivider("Hours",!0)).insertBefore(this.lists[0].$el),this.base()},flip:function(t,i){t||(t=this.factory.time.getHourCounter()),this.autoIncrement(),this.base(t,i)},appendDigitToClock:function(t){this.base(t),this.dividers[0].insertAfter(this.dividers[0].next())}})}(jQuery),function(){FlipClock.MinuteCounterFace=FlipClock.HourlyCounterFace.extend({clearExcessDigits:!1,constructor:function(t,i){this.base(t,i)},build:function(){this.base(!0,this.factory.time.getMinuteCounter())},flip:function(t,i){t||(t=this.factory.time.getMinuteCounter()),this.base(t,i)}})}(jQuery),function(t){FlipClock.TwelveHourClockFace=FlipClock.TwentyFourHourClockFace.extend({meridium:!1,meridiumText:"AM",build:function(){var i=this.factory.time.getTime(!1,this.showSeconds);this.base(i),this.meridiumText=this.getMeridium(),this.meridium=t(['<ul class="flip-clock-meridium">',"<li>",'<a href="#">'+this.meridiumText+"</a>","</li>","</ul>"].join("")),this.meridium.insertAfter(this.lists[this.lists.length-1].$el)},flip:function(t,i){this.meridiumText!=this.getMeridium()&&(this.meridiumText=this.getMeridium(),this.meridium.find("a").html(this.meridiumText)),this.base(this.factory.time.getTime(!1,this.showSeconds),i)},getMeridium:function(){return(new Date).getHours()>=12?"PM":"AM"},isPM:function(){return"PM"==this.getMeridium()?!0:!1},isAM:function(){return"AM"==this.getMeridium()?!0:!1}})}(jQuery),function(){FlipClock.Lang.Arabic={years:"سنوات",months:"شهور",days:"أيام",hours:"ساعات",minutes:"دقائق",seconds:"ثواني"},FlipClock.Lang.ar=FlipClock.Lang.Arabic,FlipClock.Lang["ar-ar"]=FlipClock.Lang.Arabic,FlipClock.Lang.arabic=FlipClock.Lang.Arabic}(jQuery),function(){FlipClock.Lang.Danish={years:"År",months:"Måneder",days:"Dage",hours:"Timer",minutes:"Minutter",seconds:"Sekunder"},FlipClock.Lang.da=FlipClock.Lang.Danish,FlipClock.Lang["da-dk"]=FlipClock.Lang.Danish,FlipClock.Lang.danish=FlipClock.Lang.Danish}(jQuery),function(){FlipClock.Lang.German={years:"Jahre",months:"Monate",days:"Tage",hours:"Stunden",minutes:"Minuten",seconds:"Sekunden"},FlipClock.Lang.de=FlipClock.Lang.German,FlipClock.Lang["de-de"]=FlipClock.Lang.German,FlipClock.Lang.german=FlipClock.Lang.German}(jQuery),function(){FlipClock.Lang.English={years:"Years",months:"Months",days:"Days",hours:"Hours",minutes:"Minutes",seconds:"Seconds"},FlipClock.Lang.en=FlipClock.Lang.English,FlipClock.Lang["en-us"]=FlipClock.Lang.English,FlipClock.Lang.english=FlipClock.Lang.English}(jQuery),function(){FlipClock.Lang.Spanish={years:"A&#241;os",months:"Meses",days:"D&#237;as",hours:"Horas",minutes:"Minutos",seconds:"Segundos"},FlipClock.Lang.es=FlipClock.Lang.Spanish,FlipClock.Lang["es-es"]=FlipClock.Lang.Spanish,FlipClock.Lang.spanish=FlipClock.Lang.Spanish}(jQuery),function(){FlipClock.Lang.Finnish={years:"Vuotta",months:"Kuukautta",days:"Päivää",hours:"Tuntia",minutes:"Minuuttia",seconds:"Sekuntia"},FlipClock.Lang.fi=FlipClock.Lang.Finnish,FlipClock.Lang["fi-fi"]=FlipClock.Lang.Finnish,FlipClock.Lang.finnish=FlipClock.Lang.Finnish}(jQuery),function(){FlipClock.Lang.French={years:"Ans",months:"Mois",days:"Jours",hours:"Heures",minutes:"Minutes",seconds:"Secondes"},FlipClock.Lang.fr=FlipClock.Lang.French,FlipClock.Lang["fr-ca"]=FlipClock.Lang.French,FlipClock.Lang.french=FlipClock.Lang.French}(jQuery),function(){FlipClock.Lang.Italian={years:"Anni",months:"Mesi",days:"Giorni",hours:"Ore",minutes:"Minuti",seconds:"Secondi"},FlipClock.Lang.it=FlipClock.Lang.Italian,FlipClock.Lang["it-it"]=FlipClock.Lang.Italian,FlipClock.Lang.italian=FlipClock.Lang.Italian}(jQuery),function(){FlipClock.Lang.Latvian={years:"Gadi",months:"Mēneši",days:"Dienas",hours:"Stundas",minutes:"Minūtes",seconds:"Sekundes"},FlipClock.Lang.lv=FlipClock.Lang.Latvian,FlipClock.Lang["lv-lv"]=FlipClock.Lang.Latvian,FlipClock.Lang.latvian=FlipClock.Lang.Latvian}(jQuery),function(){FlipClock.Lang.Dutch={years:"Jaren",months:"Maanden",days:"Dagen",hours:"Uren",minutes:"Minuten",seconds:"Seconden"},FlipClock.Lang.nl=FlipClock.Lang.Dutch,FlipClock.Lang["nl-be"]=FlipClock.Lang.Dutch,FlipClock.Lang.dutch=FlipClock.Lang.Dutch}(jQuery),function(){FlipClock.Lang.Norwegian={years:"År",months:"Måneder",days:"Dager",hours:"Timer",minutes:"Minutter",seconds:"Sekunder"},FlipClock.Lang.no=FlipClock.Lang.Norwegian,FlipClock.Lang.nb=FlipClock.Lang.Norwegian,FlipClock.Lang["no-nb"]=FlipClock.Lang.Norwegian,FlipClock.Lang.norwegian=FlipClock.Lang.Norwegian}(jQuery),function(){FlipClock.Lang.Portuguese={years:"Anos",months:"Meses",days:"Dias",hours:"Horas",minutes:"Minutos",seconds:"Segundos"},FlipClock.Lang.pt=FlipClock.Lang.Portuguese,FlipClock.Lang["pt-br"]=FlipClock.Lang.Portuguese,FlipClock.Lang.portuguese=FlipClock.Lang.Portuguese}(jQuery),function(){FlipClock.Lang.Russian={years:"лет",months:"месяцев",days:"дней",hours:"часов",minutes:"минут",seconds:"секунд"},FlipClock.Lang.ru=FlipClock.Lang.Russian,FlipClock.Lang["ru-ru"]=FlipClock.Lang.Russian,FlipClock.Lang.russian=FlipClock.Lang.Russian}(jQuery),function(){FlipClock.Lang.Swedish={years:"År",months:"Månader",days:"Dagar",hours:"Timmar",minutes:"Minuter",seconds:"Sekunder"},FlipClock.Lang.sv=FlipClock.Lang.Swedish,FlipClock.Lang["sv-se"]=FlipClock.Lang.Swedish,FlipClock.Lang.swedish=FlipClock.Lang.Swedish}(jQuery),function(){FlipClock.Lang.Chinese={years:"年",months:"月",days:"日",hours:"时",minutes:"分",seconds:"秒"},FlipClock.Lang.zh=FlipClock.Lang.Chinese,FlipClock.Lang["zh-cn"]=FlipClock.Lang.Chinese,FlipClock.Lang.chinese=FlipClock.Lang.Chinese}(jQuery);


	/* @end 	- Libraries */
	/********************************************************************************************************************/
	/* @Start 	- Variables globales */

/**************************************************************************/
/***************** Configuración del Folleto Nacional *********************/
/**************************************************************************/
/**/																	/**/
/**/// 		Oferta Folleto											  ///**/
/**/		var FNP1_FechaInicio 	= "13/03/2016, 21:00:00";			/**/
/**/		var FNP1_FechaFin 		= "19/03/2016, 20:59:59";			/**/
/**/																	/**/
/**/// 		Ofertas Folleto											  ///**/
/**/		var FNP2_FechaInicio 	= "02/03/2016, 21:00:00";			/**/
/**/		var FNP2_FechaFin 		= "13/03/2016, 21:00:00";			/**/
/**/																	/**/
/**************************************************************************/
/**************************************************************************/

/**************************************************************************/
/***************** Configuración de Campañas Periódicas *******************/
/**************************************************************************/
/**/																	/**/
/**///		Martes Night Fever										  ///**/
/**/		var MNS_Dia 			= 2;								/**/
/**/		var MNS_HoraInicio		= "10:00:00";						/**/
/**/		var MNS_HoraFin 		= "10:00:00";						/**/
/**/																	/**/
/**************************************************************************/
/**************************************************************************/

/**************************************************************************/
/****************** Configuración de la CutOver Layer *********************/
/**************************************************************************/
/**/																	/**/
/**///		CutOver Layer											  ///**/
/**/		var CUT_FechaInicio		= "29/02/2016, 01:00:00";			/**/
/**/		var CUT_FechaFin 		= "29/02/2016, 03:00:00";			/**/
/**/																	/**/
/**************************************************************************/
/**************************************************************************/

/*************** Configuración de Campañas diferentes a OF1/2**************/
/**************************************************************************/
/**/																	/**/
/**///		Semana Santa 											  ///**/
/**/		var DDM_FechaInicio 	= "01/02/2016, 09:00:00";			/**/
/**/		var DDM_FechaFin 		= "14/02/2016, 23:59:50";			/**/
/**///		dia del padre 											  ///**/
/**/		var DDP_FechaInicio 	= "13/03/2016, 21:00:00";			/**/
/**/		var DDP_FechaFin 		= "19/03/2016, 20:59:59";			/**/
/**/																	/**/
/**************************************************************************/
/**************************************************************************/
/**************************************************************************/

/******************** Configuración del Intersitial ***********************/
/**************************************************************************/
/**/																	/**/
/**///		Interstitial Layer										  ///**/
/**/		var INT_FechaInicio		= "13/03/2016, 21:00:00";			/**/
/**/		var INT_FechaFin 		= "19/03/2016, 20:59:59";			/**/
/**/		var INT2_FechaInicio	= "25/11/2015, 22:00:00";			/**/
/**/		var INT2_FechaFin 		= "26/11/2015, 15:00:00";			/**/
/**/																	/**/
/**************************************************************************/
/**************************************************************************/

/**************************************************************************/
/**************************************************************************/
/**/// 		Black Friday											  ///**/
/**/		var BFR_FechaInicio		= "25/11/2015, 21:59:00";			/**/
/**/		var BFR_FechaFin 		= "29/11/2015, 21:59:59";			/**/
/**/																	/**/
/**************************************************************************/
/**************************************************************************/
/************************ ConfiguraciÃ³n de Skins **************************/
/**************************************************************************/
/**/																	/**/
/**///		Skin Alternativo (CampaÃ±as)								  ///**/
/**/		var ALT_FechaInicio		= "12/01/2016, 10:00:00";			/**/
/**/		var ALT_FechaFin 		= "13/01/2016, 10:00:00";			/**/
/**/		var ALT_Class	 		= "alt";  							/**/
/**/																	/**/
/**************************************************************************/
/**************************************************************************/


/*************** Configuración  de Galaxy Night Show   ********************/
/**************************************************************************/
/**/																	/**/
/**/																	/**/
/**///		Galaxy Night show										  ///**/
/**/		var XHD_FechaInicio 	= "12/01/2016, 10:00:00";			/**/
/**/		var XHD_FechaFin 		= "13/01/2016, 10:00:00";			/**/
/**/		var XHD_FechaActCamp	= "12/01/2016, 22:00:00";			/**/
/**/																	/**/
/**************************************************************************/
/**************************************************************************/


/**************************************************************************/
/********************* Configuración de Test A-B **************************/
/**************************************************************************/
/**/																	/**/
/**/		var TestAB_FechaInicio		= "19/01/2015, 00:00:01";		/**/
/**/		var TestAB_FechaFin 		= "25/01/2015, 23:59:59";		/**/
/**/		                                    						/**/
/**************************************************************************/
/**************************************************************************/

/**************************************************************************/
/********************* Configuración de Renovator **************************/
/**************************************************************************/
/**/																	/**/
/**/		var Financiacion_FechaInicio 	= "27/05/2015, 21:00:00";	/**/
/**/		var Financiacion_FechaFin		= "13/09/2015, 23:59:59";	/**/
/**/		                                    						/**/
/**************************************************************************/
/**************************************************************************/

/**************************************************************************/
/**************************************************************************/
/**/// 		¡Entrypoint Dia laboral Lunes a viernes!        			/**/
/**/		var LbEntry_DiaInicial		= 1;	// Lunes				/**/
/**/		var LbEntry_DiaFinal 		= 1;	// Viernes				/**/
/**/		var LbEntry_HoraInicial		= "00:00:00";					/**/
/**/		var LbEntry_HoraFinal 		= "00:00:00";					/**/
/**/																	/**/
/**************************************************************************/
/**************************************************************************/

/**************************************************************************/
/**************************************************************************/
/**/// 		¡Entrypoint Dia Fijo!        								/**/
/**/		var Fijo_FechaInicio  	= "02/03/2016, 21:00:00";			/**/
/**/		var Fijo_FechaFin 		= "13/03/2016, 23:59:59";			/**/
/**/																/**/
/**************************************************************************/
/**************************************************************************/

var DateNow 		= new Date();
var pricesPath 		= '//d243u7pon29hni.cloudfront.net/price2.0/';
var path 			= '//tiendas.mediamarkt.es'; 
var mm_cdnImgURL 	= "//d243u7pon29hni.cloudfront.net";
var mm_cdnCssURL 	= "//d243u7pon29hni.cloudfront.net";
var mm_cdnJsURL 	= "//d243u7pon29hni.cloudfront.net";
customMMES.load 	= true;
var environment		= window.location.hostname.split(".")[0];
var contenido;
var search;
var timer;

/* @end 	- Variables globales */
/********************************************************************************************************************/
/* @Start 	- Custom Body */

$('body').attr('id', 'mm-wrapper');
$('body').addClass('mm-body');

/* @End  	- Custom Body */
/********************************************************************************************************************/
/* @Start 	- Funciones referentes a la URL */

customMMES.mmDomain = {
	queries: function() {

		this.IsPROD = false;
		this.IsPERF = false;
		this.IsTEST = false;

		// Devuelve Objetos TRUE/FALSE realizando una comparación con el dominio la página actual.

		this.mm_protocol 	= window.location.protocol;
		this.getEnvironment = window.location.hostname.split(".")[0];
		this.path_name 		= window.location.pathname;

		if ((this.getEnvironment == "www") || (this.getEnvironment == "mcstest") || (this.getEnvironment == "perf")) {
			this.IsCMS = true;
		}
		else {
			this.IsCMS = false;
		};

		if ((this.getEnvironment == "tiendas") || (this.getEnvironment == "tiendas-test") || (this.getEnvironment == "tiendas-pre")) {
			this.IsShop = true;
		}
		else {
			this.IsShop = false;
		};

		if ((window.location.hostname.split(".")[1] == "mediamarkt") && (this.IsCMS == false)) {
			this.IsWLS = true;
		}
		else {
			this.IsWLS = false;
		};

		if (this.path_name == "/") {
			this.IsHome = true;
		}
		else {
			this.IsHome = false;
		};

		if ((this.path_name.split("/")[1] == "mcs") && (this.path_name.split("/")[2] == "shop")) {
			this.IsLanding = true;
		}
		else {
			this.IsLanding = false;
		};

		if ((this.IsCMS == false) && (window.location.pathname.split("/")[1] == "ecommerce") && (window.location.pathname.split("/")[2] == "checkout")) {
			this.IsCheckout = true;
		}
		else {
			this.IsCheckout = false;
		};	
		if ((this.IsCMS == false) && (window.location.pathname.split("/")[1] == "index.cfm") || (window.location.href == "http://shop.mediamarkt.es/")|| (window.location.href == "http://tiendas-test.mediamarkt.es/") || (window.location.href == "http://tiendas-pre.mediamarkt.es/") || (window.location.href == "http://tiendas.mediamarkt.es/")) {
			this.IsHomeTiendas = true;
		}
		else {
			this.IsHomeTiendas = false;
		}

		if ((this.getEnvironment == "www") || (this.getEnvironment == "tiendas")) {
			this.IsPROD = true;
		}
		if ((this.getEnvironment == "perf") || (this.getEnvironment == "tiendas-pre")) {
			this.IsPERF = true;
		}
		if ((this.getEnvironment == "mcstest") || (this.getEnvironment == "tiendas-test")) {
			this.IsTEST = true;
		}
	},

	check: function(domain) {

		// Devuelve TRUE/FALSE en base a la comparación entre el dominio del parámetro y el de la página actual

		var Domain = window.location.hostname.split(".")[0];
		if (Domain == domain) {
			return true;
		}
		else {
			return false;
		}
	},

	contain: function(string) {

		// Evalúa si el 'pathname' de una URL contiene la cadena pasada por parámetro, devolviendo TRUE/FALSE en cada caso.

		if (window.location.pathname.indexOf(string) > -1) {
			return true;
		}
		else {
			return false;
		}
	},

	set: function() {

		// Establece 'www' en rutas relativas del visor del FFNN cuando se visualiza en 'tiendas', 
		// y respeta el subdominio correspondiente cuando se visualiza en 'perf' o 'mcstest'.

		var getEnvironment = window.location.hostname.split(".")[0];
		if (getEnvironment == "www" || getEnvironment == "mcstest" || getEnvironment == "perf") {
			return true;
		}
		else {
			return false;
		}
	},
	getTiendaDomain: function(){
		this.entorno = 'tiendas';

		if (customMMES.mmDomain.query.IsPERF === true){
			this.entorno = 'tiendas-pre';
		}else if(customMMES.mmDomain.query.IsTEST === true){
			this.entorno = 'tiendas-test';
		}
		return this.entorno;
	}
};



// Instanciamos la función customMMES.mmDomain.queries() para utilizarla en otras funciones de este documento JS.

customMMES.mmDomain.query = new customMMES.mmDomain.queries();

if (customMMES.mmDomain.set() == true) {
	var domain = environment;
}
else {
	var domain = "www";
};

customMMES.getParmFromHash = function(url, parm) {

	var re = new RegExp("#.*[/]" + parm + "=([^&]+)(&|$)");
	var match = url.match(re);
	return(match ? match[1] : "");
};

customMMES.normalize = function(str) {

	/* Se utiliza en la función:
	 *	customMMES.ga.contentValue();
	 */

	 var ltr = ['[àáâãä]','[èéêë]','[ìíîï]','[òóôõö]','[ùúûü]','ñ','ç','[ýÿ]','_'];
	 var rpl = ['a','e','i','o','u','n','c','y','-'];
	 var str = String(str.toLowerCase());

	 for (var i = 0, c = ltr.length; i < c; i++) {
	 	var rgx = new RegExp(ltr[i],'g');
	 	str = str.replace(rgx,rpl[i]);
	 };
	 str = str.replace(/\s+/g,'-');
	 return str;
	};

customMMES.normalizeGA = function(str) {

/**
 *This function is used in the object 
 *customMMES.menu
 */


	var ltr = ['[àáâãä]','[èéêë]','[ìíîï]','[òóôõö]','[ùúûü]','ñ','ç','[ýÿ]','_'];
    var rpl = ['a','e','i','o','u','n','c','y','-'];
    var str = String(str.toLowerCase());

    for (var i = 0, c = ltr.length; i < c; i++) {
    	var rgx = new RegExp(ltr[i],'g');
    	str = str.replace(rgx,rpl[i]);
    };
    str = str.replace(/\s+/g,' ');
    return str;
};

customMMES.URLparam = function(name) {
 
	/* Se utiliza en las funciones:
	 *	customMMES.visorFNP();
	 *  customMMES.visorFNC();
	 *  customMMES.ga.contentValue();
	 *  customMMES.flyer.show();
	 */

	 var regexS = "[&#92;?&]" + name + "=([^&#]*)";
	 var regex = new RegExp( regexS );
	 var tmpURL = window.location.href;
	 var results = regex.exec( tmpURL );
	 if (results == null) {
	 	return "";
	 }
	 else {
	 	return results[1];
	 }
	};





/* @end		- Funciones referentes a la URL */
/********************************************************************************************************************/
/* @start 	- Funciones referentes a fechas */

Date.prototype.addDays = function(days){

	// Nuevo método para el objeto Date(), que devuelve una fecha futura o pasada N días, en base a una fecha dada.
	// Parámetro: [INTEGER] Pasar por parámetro un número positivo (para fecha futura), o negativo (para fecha pasada).

	this.date = new Date(this.valueOf());
	this.date.setDate(this.date.getDate() + days);	     
	var newDate = new Array();
	newDate[0] 	= this.date.getDate();
	newDate[1] 	= this.date.getMonth();
	newDate[2] 	= this.date.getFullYear();
	var updatedDate = new Date(newDate[2], newDate[1], newDate[0]);
	return updatedDate;
};

customMMES.mmDate = {

	formatDate: function(fecha) {

		// Parámetro: [STRING] Pasar un string de fecha, en el formato: "dd/mm/aaaa, hh:mm:ss".
		// La función devuelve un objeto Date(aaaa, mm, dd, hh, mm, ss).

		var fullDatetime 		= fecha.split(", ");
		var fullDate 			= fullDatetime[0].split("/");
		var fullTime 			= fullDatetime[1].split(":");	
		var day 				= parseFloat(fullDate[0]);
		var month 				= parseFloat(fullDate[1]) -1;
		var year 				= parseFloat(fullDate[2]);
		var hour 				= parseFloat(fullTime[0]);
		var minutes 			= parseFloat(fullTime[1]);
		var seconds 			= parseFloat(fullTime[2]);
		var newDate				= new Date(year, month, day, hour, minutes, seconds);
		return newDate;
	},

	getMonthName: function(mes) {

		// Parámetro: [INTEGER] Pasar por parámetro el número correspondiente al nº REAL del mes. Ejemplo: Enero = 1, (...), Diciembre = 12.
		// Devuelve un STRING con el mes pasado por parámetro.

		this.monthName 			= new Array(12);
		this.monthName[0] 	= "Enero";
		this.monthName[1] 	= "Febrero";
		this.monthName[2] 	= "Marzo";
		this.monthName[3] 	= "Abril";
		this.monthName[4] 	= "Mayo";
		this.monthName[5] 	= "Junio";
		this.monthName[6] 	= "Julio";
		this.monthName[7] 	= "Agosto";
		this.monthName[8] 	= "Septiembre";
		this.monthName[9] 	= "Octubre";
		this.monthName[10] 	= "Noviembre";
		this.monthName[11] 	= "Diciembre";
		return this.monthName[mes-1];
	},

	collate: function(fechaInicio, fechaFin) {

		// Parámetros: [STRING] Pasar un string de fecha en ambos parámetros, en el formato: "dd/mm/aaaa, hh:mm:ss".
		// Devuelve TRUE/FALSE si la fecha Actual está comprendida entre la Fecha Inicio y la Fecha Fin.

		var DateIni = customMMES.mmDate.formatDate(fechaInicio);
		var DateEnd = customMMES.mmDate.formatDate(fechaFin);
		if ((DateIni <= DateNow && DateNow <= DateEnd) == true) {
			return true;
		}
		else {
			return false;
		};
	},

	getmonthday: function(day) {

		// Parámetro: [INTEGER 0-6 (D,L,M,X,J,V,S)] Establece el último día (L-M-X-J-V-S-D) del mes en curso. 
		// Por defecto '2' (martes) si el parámetro es NULL.

		var dia;
		var n;
		var lastmonthday;
		var inihour;
		if (day !== null && day <= 6) {
			dia = day;
		}

		if (day == null) {
			dia = 2;
		}

		if (day > 6) {
			dia = 2; 
			console.log("Error: check 'day' value in 'getmonthday' configuration.");
		}

		var d = new Date(),
		month = d.getMonth(),
		monthday = [];
		d.setDate(1); 

		while (d.getDay() !== dia) {
			d.setDate(d.getDate() + 1);
		};

		while (d.getMonth() === month) {
			monthday.push(new Date(d.getTime()));
			d.setDate(d.getDate() + 7);
		};

		n = monthday.length;
		lastmonthday = monthday[n-1];
		return lastmonthday; 
	},

	MNSCampaignType: function(day, iniTime, endTime) {

		// Parámetro 1: [INTEGER 0-6 (D,L,M,X,J,V,S)] Establece el último día (L-M-X-J-V-S-D) del mes en curso. Por defecto '2' (martes) si el parámetro es NULL.
		// Parámetro 2: [STRING 'hh:mm:ss'] Establece manualmente la hora en la fecha de Inicio. Por defecto '22:00:00' si el parámetro es NULL.
		// Parámetro 3: [STRING 'hh:mm:ss'] Establece manualmente la hora en la fecha de Fin. Por defecto '10:00:00' si el parámetro es NULL.

		var mnsday =  customMMES.mmDate.getmonthday(day);
		var MNSpre = "22:00:00";
		var MNSstart;
		var MNSend;

		// Inicio Pre-Campaña MNS (Martes 10am)
		
		iniTime == null ? MNSstart = "10:00:00" : MNSstart = iniTime;

		if (DateNow.getDate() == 1 && DateNow.getDay == MNS_Dia + 1) { // Ajuste de fechas si el MNS cae el último día del mes

			this.inidate = new Date(DateNow.addDays(-1).getFullYear(), DateNow.addDays(-1).getMonth(), DateNow.addDays(-1).getDate(), parseFloat(MNSstart.split(":")[0]), parseFloat(MNSstart.split(":")[1]), parseFloat(MNSstart.split(":")[2])); 
			this.inidatestring = DateNow.addDays(-1).getDate() + "/" + (DateNow.addDays(-1).getMonth() + 1) + "/" + DateNow.addDays(-1).getFullYear() + ", " + parseFloat(MNSstart.split(":")[0]) + ":" + parseFloat(MNSstart.split(":")[1]) + ":" + parseFloat(MNSstart.split(":")[2]);

			this.folderName = DateNow.addDays(-1).getFullYear().toString() + ("0" + (DateNow.addDays(-1).getMonth() + 1)).slice(-2).toString() + ("0" + DateNow.addDays(-1).getDate()).slice(-2).toString();
		}
		else {
			this.inidate = new Date(mnsday.getFullYear(), mnsday.getMonth(), mnsday.getDate(), parseFloat(MNSstart.split(":")[0]), parseFloat(MNSstart.split(":")[1]), parseFloat(MNSstart.split(":")[2])); 
			this.inidatestring = mnsday.getDate() + "/" + (mnsday.getMonth() + 1) + "/" + mnsday.getFullYear() + ", " + parseFloat(MNSstart.split(":")[0]) + ":" + parseFloat(MNSstart.split(":")[1]) + ":" + parseFloat(MNSstart.split(":")[2]);

			this.folderName = mnsday.getFullYear().toString() + ("0" + (mnsday.getMonth() + 1)).slice(-2).toString() + ("0" + mnsday.getDate()).slice(-2).toString();
		}

		// Inicio Campaña MNS (Martes 22pm)

		if (DateNow.getDate() == 1 && DateNow.getDay == MNS_Dia + 1) { // Ajuste de fechas si el MNS cae el último día del mes

			this.predate = new Date(DateNow.addDays(-1).getFullYear(), DateNow.addDays(-1).getMonth(), DateNow.addDays(-1).getDate(), parseFloat(MNSpre.split(":")[0]), parseFloat(MNSpre.split(":")[1]), parseFloat(MNSpre.split(":")[2])); 
			this.predatestring = DateNow.addDays(-1).getDate() + "/" + (DateNow.addDays(-1).getMonth() + 1) + "/" + DateNow.addDays(-1).getFullYear() + ", " + parseFloat(MNSpre.split(":")[0]) + ":" + parseFloat(MNSpre.split(":")[1]) + ":" + parseFloat(MNSpre.split(":")[2]);
		}
		else {
			this.predate = new Date(mnsday.getFullYear(), mnsday.getMonth(), mnsday.getDate(), parseFloat(MNSpre.split(":")[0]), parseFloat(MNSpre.split(":")[1]), parseFloat(MNSpre.split(":")[2])); 
			this.predatestring = mnsday.getDate() + "/" + (mnsday.getMonth() + 1) + "/" + mnsday.getFullYear() + ", " + parseFloat(MNSpre.split(":")[0]) + ":" + parseFloat(MNSpre.split(":")[1]) + ":" + parseFloat(MNSpre.split(":")[2]);
		}

		// Fin Campaña MNS (Miércoles 10am)
		
		endTime == null ? MNSend = "10:00:00" : MNSend = endTime;

		if (DateNow.getDate() == 1 && DateNow.getDay == MNS_Dia + 1) { // Ajuste de fechas si el MNS cae el último día del mes

			this.enddate = new Date(DateNow.getFullYear(), DateNow.getMonth(), DateNow.getDate(), parseFloat(MNSend.split(":")[0]), parseFloat(MNSend.split(":")[1]), parseFloat(MNSend.split(":")[2]));
			this.enddatestring = DateNow.getDate() + "/" + (DateNow.getMonth() + 1) + "/" + DateNow.getFullYear() + ", " + parseFloat(MNSend.split(":")[0]) + ":" + parseFloat(MNSend.split(":")[1]) + ":" + parseFloat(MNSend.split(":")[2]);
		}
		else {
			this.enddate = new Date(mnsday.addDays(1).getFullYear(), mnsday.addDays(1).getMonth(), mnsday.addDays(1).getDate(), parseFloat(MNSend.split(":")[0]), parseFloat(MNSend.split(":")[1]), parseFloat(MNSend.split(":")[2]));
			this.enddatestring = mnsday.addDays(1).getDate() + "/" + (mnsday.addDays(1).getMonth() + 1) + "/" + mnsday.addDays(1).getFullYear() + ", " + parseFloat(MNSend.split(":")[0]) + ":" + parseFloat(MNSend.split(":")[1]) + ":" + parseFloat(MNSend.split(":")[2]);
		}
	},

	weeklyCampaign: function(diaInicio, diaFin, horaInicio, horaFin) {

		// Parámetro 1: [INTEGER 0-6 (D,L,M,X,J,V,S)] Establece el dia inicial de la campaña dentro del ciclo semanal.
		// Parámetro 2: [INTEGER 0-6 (D,L,M,X,J,V,S)] Establece el dia final de la campaña dentro del ciclo semanal.
		// Parámetro 3: [STRING (HH:MM:SS)] Establece la hora de inicio de la campaña. Por defecto '10:00:00' si el parámetro es NULL.
		// Parámetro 4: [STRING (HH:MM:SS)] Establece la hora de fin de la campaña. Por defecto '23:59:59' si el parámetro es NULL.
		
		this.DSA = DateNow.getDay();	// DSA: Día de la Semana Actual
		this.DSI = diaInicio;			// DSI: Día de la Semana Inicial	
		this.DSF = diaFin;				// DSF: Día de la Semana Final
		var initime;
		var endtime;
		horaInicio == null ? initime = "10:00:00" : initime = horaInicio;
		horaFin    == null ? endtime = "23:59:59" : endtime = horaFin;
		this.TSI = customMMES.mmDate.formatDate("01/01/2000, " + parseFloat(initime.split(":")[0]) + ":" + parseFloat(initime.split(":")[1]) + ":" + parseFloat(initime.split(":")[2]));
		this.TSF = customMMES.mmDate.formatDate("01/01/2000, " + parseFloat(endtime.split(":")[0]) + ":" + parseFloat(endtime.split(":")[1]) + ":" + parseFloat(endtime.split(":")[2]));
		this.TSA = customMMES.mmDate.formatDate("01/01/2000, " + DateNow.getHours() + ":" + DateNow.getMinutes() + ":" + DateNow.getSeconds());
		this.IsActive = false;


	  	// DSI = DSF

	  	if (this.DSI == this.DSF && this.DSI == this.DSA && this.TSA >= this.TSI && this.TSA <= this.TSF) {
	  		this.IsActive = true;
	  	}

		// DSA = DSI & DSI != DSF

		if (this.DSA == this.DSI && this.TSA >= this.TSI && this.DSI != this.DSF) {	
			this.IsActive = true;
		}

		// DSA = DSF & DSI != DSF 

		if (this.DSA == this.DSF && this.TSA <= this.TSF && this.DSI != this.DSF) {	
			this.IsActive = true;
		}

		// DSI < DSA < DSF

		if (this.DSI < this.DSA && this.DSA < this.DSF && this.DSA <= this.DSF && this.TSI <= this.TSA && this.TSA <= this.TSF) {	
			this.IsActive = true;
		}

		// DSI > DSF (DSI -> 6; 0 -> DSF)

		if (this.DSI > this.DSF) {

			// DSI > DSF (DSI -> 6)

			if (this.DSA > this.DSI && this.DSA <= 6 && this.TSI <= this.TSA) {
				this.IsActive = true;
			}

			// DSI > DSF (0 -> DSF)

			if (this.DSA < this.DSF && this.DSA >= 0 && this.TSA <= this.TSF) {
				this.IsActive = true;
			}
		}
	}
};

/* @end		- Funciones referentes a fechas */
/********************************************************************************************************************/
/* @start 	- Funciones de control de visibilidad en el Left Navigation */

customMMES.leftNavigation = {

	Campaign: function(id, fechaInicio, fechaFin) {

		// Parámetro 1: [STRING] La ID del ítem, la cual será usada como 'data-submenu-id' para el Left Navi y como ID en la landing de 'Folleto y Ofertas Especiales'.
		// Parámetro 2: [STRING] Pasar un string de Fecha, con el siguiente formato: "dd/mm/aaaa, hh:mm:ss". Define la fecha de inicio de la campaña.
		// Parámetro 3: [STRING] Pasar un string de Fecha, con el siguiente formato: "dd/mm/aaaa, hh:mm:ss". Define la fecha fin de la campaña.

		var Active_period 	= customMMES.mmDate.collate(fechaInicio, fechaFin);
		this.active_period 	= Active_period; 

		// En el Left Navigation
		
		this.dataID = $("nav#left-nav [data-submenu-id='ctg-" + id + "']");
		if (this.dataID !== null) { 
			if (this.active_period == true) {
				this.dataID.show();
			}
			else {
				this.dataID.hide();
			}
		}

		// En la landing 'Folleto y Ofertas Especiales'

		this.ID = $("#mm-main #mm-" + id);
		if (this.ID !== null && (customMMES.mmDomain.contain("/es/shop/ofertas-mediamarkt") == true)) {
			if (this.active_period == true) {
				this.ID.removeClass("hidden");
			}
			else {
				this.ID.addClass("hidden");
			}
		}
	},

	weeklyCampaign: function(id, diaInicio, diaFin, horaInicio, horaFin) { 

		// Parámetro 1: [STRING] La urlSEO, sin 'backslashes' del ítem del Left Navigation a controlar. Dicho valor será la ID en 'www' y el HREF en 'tiendas'
		// Parámetro 2: [INTEGER 0-6 (D,L,M,X,J,V,S)] Establece el dia inicial de la campaña dentro del ciclo semanal.
		// Parámetro 3: [INTEGER 0-6 (D,L,M,X,J,V,S)] Establece el dia final de la campaña dentro del ciclo semanal.
		// Parámetro 4: [STRING (HH:MM:SS)] Establece la hora de inicio de la campaña. Por defecto '10:00:00', si el parámetro es NULL.
		// Parámetro 5: [STRING (HH:MM:SS)] Establece la hora de fin de la campaña. Por defecto '23:59:59', si el parámetro es NULL.

		var Active_period = new customMMES.mmDate.weeklyCampaign(diaInicio, diaFin, horaInicio, horaFin);
		this.active_period 	= Active_period;

		// En el Left Navigation
		
		this.dataID = $("nav#left-nav [data-submenu-id='" + id + "']");
		if (this.dataID !== null) { 
			if (this.active_period == true) {
				this.dataID.show();
			}
			else {
				this.dataID.hide();
			}
		}

		// En la landing 'Folleto y Ofertas Especiales'

		this.ID = $("#mm-main #mm-" + id);
		if((this.ID !== null) && (customMMES.mmDomain.contain("/es/shop/ofertas-mediamarkt") == true)) {
			if(this.active_period == true) {
				this.ID.removeClass("hidden");
			}
			else {
				this.ID.addClass("hidden");
			}
		}
	}
};

/* @end		- Funciones de control de visibilidad en el Left Navigation */
/********************************************************************************************************************/
/* @start 	- Funciones de Folleto Nacional (Landing, Visor Folleto) */


/* 	Por defecto, la Fecha Inicio del FN se establece al día siguiente de la
 *	Fecha Inicio de publicación (FNP1_FechaInicio ó FNP2_FechaInicio, según 
 * 	sea el caso).
 * 
 * 	Modificando la variable DateStartDelay se puede modificar esta funcionalidad,
 *	incrementando en N días la Fecha Inicio del FN respecto la Fecha Inicio 
 *	de publicación. 
 *
 *	La Fecha de Inicio (FNPx_FechaInicio + DateStartDelay) deben sumar el número correspondiente
 *	al día de inicio de campaña, indicado en la primera y/o última página del Folleto Nacional.
 */

 var DateStartDelay 		= 1;

//  Folleto Nacional Península

var FNP1_DateStartDelay	= DateStartDelay;
var FNP1_DateStart		= customMMES.mmDate.formatDate(FNP1_FechaInicio); 
var FNP1_DateEnd		= customMMES.mmDate.formatDate(FNP1_FechaFin); 
var FNP1_Version		= "P";
	//var FNP1_Name			= "";
  	var FNP1_Name			= ""; //Default: Ej: "Folleto Nacional XX de Diciembre";

//  Folleto Nacional Península (Alternativo)

var FNP2_DateStartDelay	= DateStartDelay; 
var FNP2_DateStart		= customMMES.mmDate.formatDate(FNP2_FechaInicio); 
var FNP2_DateEnd		= customMMES.mmDate.formatDate(FNP2_FechaFin);
var FNP2_Version		= "P";
var FNP2_Name			= "";
  //var FNP2_Name			= ""; Default: Ej: "Folleto Nacional XX de Diciembre";

//  Folleto Nacional Canarias

var FNC1_DateStartDelay	= DateStartDelay;
var FNC1_DateStart		= FNP1_DateStart;
var FNC1_DateEnd		= FNP1_DateEnd;
var FNC1_Version		= "C";
	//var FNC1_Name			= "";
  	var FNC1_Name			= ""; //Default: Ej: "Folleto Nacional Versión Canarias XX de Diciembre";

//  Folleto Nacional Canarias (Alternativo)

var FNC2_DateStartDelay	= DateStartDelay;
var FNC2_DateStart		= FNP2_DateStart;
var FNC2_DateEnd		= FNP2_DateEnd;
var FNC2_Version		= "C";
var FNC2_Name			= "";
  //var FNC2_Name			= ""; Default: Ej: "Folleto Nacional Versión Canarias XX de Diciembre";


//  Folleto Black Friday (Peninsula y Canarias)

var BFR_DateStartDelay	= 1; 
var BFR_DateStart		= customMMES.mmDate.formatDate(BFR_FechaInicio); 
var BFR_DateEnd		= customMMES.mmDate.formatDate(BFR_FechaFin);
var BFR_Version		= "P";
var BFR_Name			= "";

customMMES.FN = function(FechaInicio, FechaFin, NombreFolleto, Version, Delay) {

	this.iniDate 			= FechaInicio;
	this.endDate 			= FechaFin;

	this.delay 				= Delay;
	this.iniDayReal 		= this.iniDate.getDate();
	this.iniDayFlyer 		= this.iniDate.addDays(this.delay).getDate()
	this.iniMonth 			= this.iniDate.addDays(this.delay).getMonth() + 1;
	this.iniMonthName 		= customMMES.mmDate.getMonthName(this.iniDate.addDays(this.delay).getMonth() + 1);
	this.iniYear 			= this.iniDate.addDays(this.delay).getFullYear();
	
	this.endDay 			= this.endDate.getDate();
	this.endMonth 			= (this.endDate.getMonth() ) + 1;
	this.endMonthName 		= customMMES.mmDate.getMonthName(this.endDate.getMonth() + 1);
	this.endYear 			= this.endDate.getFullYear();

	this.folderDay			= (this.iniDayFlyer < 10 ? "0" : "") + this.iniDayFlyer;
	this.folderMonth		= (this.iniMonth < 10 ? "0" : "") + this.iniMonth;
	this.folderYear			= this.iniYear;
	this.folderName 		= "" + this.folderYear + this.folderMonth + this.folderDay + "";

	// Composición del título del FN dinámicamente 	

	if (NombreFolleto == "" || NombreFolleto == null) {
		this.FlyerName = "Folleto Nacional ";
		if (Version == "C") {
			this.FlyerName = this.FlyerName + "versión Canarias ";
		}
		this.FlyerName = this.FlyerName + this.iniDayFlyer + " de" + " " + this.iniMonthName;
	} 
	else {
		this.FlyerName = NombreFolleto;
	};
};

	// Inicializamos los Objetos de tipo FN, según fecha y localización

	customMMES.FNP1	= new customMMES.FN(FNP1_DateStart, FNP1_DateEnd, FNP1_Name, "P", FNP1_DateStartDelay);
	customMMES.FNP2	= new customMMES.FN(FNP2_DateStart, FNP2_DateEnd, FNP2_Name, "P", FNP2_DateStartDelay);
	customMMES.FNC1	= new customMMES.FN(FNC1_DateStart, FNC1_DateEnd, FNC1_Name, "C", FNC1_DateStartDelay);
	customMMES.FNC2	= new customMMES.FN(FNC2_DateStart, FNC2_DateEnd, FNC2_Name, "C", FNC2_DateStartDelay);
	customMMES.BFR	= new customMMES.FN(BFR_DateStart, BFR_DateEnd, BFR_Name, "P", BFR_DateStartDelay);
	
	// Definición y determinación del FN Actual

		// Península

		if ((DateNow > customMMES.FNP1.iniDate) && (DateNow <= customMMES.FNP1.endDate)) {
			customMMES.FNPA = new customMMES.FN(FNP1_DateStart, FNP1_DateEnd, FNP1_Name, "P", FNP1_DateStartDelay);
		}
		else {
			customMMES.FNPA = new customMMES.FN(FNP2_DateStart, FNP2_DateEnd, FNP2_Name, "P", FNP2_DateStartDelay);
		};

		// Canarias

		if ((DateNow > customMMES.FNC1.iniDate) && (DateNow <= customMMES.FNC1.endDate)) {
			customMMES.FNCA = new customMMES.FN(FNC1_DateStart, FNC1_DateEnd, FNC1_Name, "C", FNC1_DateStartDelay);
		}
		else {
			customMMES.FNCA = new customMMES.FN(FNC2_DateStart, FNC2_DateEnd, FNC2_Name, "C", FNC2_DateStartDelay);
		};

	// Definición y determinación del FN más reciente, CUANDO NO HAY NINGUNO ACTIVO, para mostrar fechas por defecto en landings de FN

		// Península

		if ((DateNow > customMMES.FNP1.endDate) && (DateNow > customMMES.FNP2.endDate)) {
			if (customMMES.FNP1.endDate > customMMES.FNP2.endDate) {
				customMMES.FNPA = new customMMES.FN(FNP1_DateStart, FNP1_DateEnd, FNP1_Name, "P", FNP1_DateStartDelay);
			}
			else {
				customMMES.FNPA = new customMMES.FN(FNP2_DateStart, FNP2_DateEnd, FNP2_Name, "P", FNP2_DateStartDelay);
			};
		};

		// Canarias

		if ((DateNow > customMMES.FNC1.endDate) && (DateNow > customMMES.FNC2.endDate)) {
			if (customMMES.FNC1.endDate > customMMES.FNC2.endDate) {
				customMMES.FNCA = new customMMES.FN(FNC1_DateStart, FNC1_DateEnd, FNC1_Name, "C", FNC1_DateStartDelay);
			}
			else {
				customMMES.FNCA = new customMMES.FN(FNC2_DateStart, FNC2_DateEnd, FNC2_Name, "C", FNC2_DateStartDelay);
			};
		};

// Landing FN dinámica 

customMMES.FNDateVariables = function() {

	// Península

	if ($("#landing-FNP").length == 1) {
		$("#landing-FNP iframe").attr("src", "//" + domain + ".mediamarkt.es/static/layer/folleto-nacional/" + customMMES.FNPA.folderName + "-index.html");
	}

	if ($(".mm-info-flyer.mm-nac").length > 0) {
		$(".mm-info-flyer.mm-nac iframe").attr("src", "//" + domain + ".mediamarkt.es/static/layer/folleto-nacional/" + customMMES.FNPA.folderName + "-index.html");
		$(".mm-info-flyer.mm-nac .ffnn-start-day").text(customMMES.FNPA.iniDayFlyer);
		$(".mm-info-flyer.mm-nac .ffnn-start-month").text(customMMES.FNPA.iniMonthName);
		$(".mm-info-flyer.mm-nac .ffnn-start-year").text(customMMES.FNPA.iniYear);
		$(".mm-info-flyer.mm-nac .ffnn-end-day").text(customMMES.FNPA.endDay);
		$(".mm-info-flyer.mm-nac .ffnn-end-month").text(customMMES.FNPA.endMonthName);
		$(".mm-info-flyer.mm-nac .ffnn-end-year").text(customMMES.FNPA.endYear);

		if (customMMES.FNPA.iniYear == customMMES.FNPA.endYear) {
			$(".mm-info-flyer.mm-nac .year-prefix").remove();
		}

		if ((customMMES.FNPA.iniMonth == customMMES.FNPA.endMonth) && (customMMES.FNPA.iniYear == customMMES.FNPA.endYear)) {
			$(".mm-info-flyer.mm-nac .month-prefix").remove();
		}
	}

	// Canarias

	if ($("#landing-FNC").length == 1) {
		$("#landing-FNC iframe").attr("src", "//" + domain + ".mediamarkt.es/static/layer/folleto-nacional-canarias/" + customMMES.FNCA.folderName + "-index.html");
	}

	if ($(".mm-info-flyer.mm-can").length > 0) {
		$(".mm-info-flyer.mm-can iframe").attr("src", "//" + domain + ".mediamarkt.es/static/layer/folleto-nacional-canarias/" + customMMES.FNCA.folderName + "-index.html");
		$(".mm-info-flyer.mm-can .ffnn-start-day").text(customMMES.FNCA.iniDayFlyer);
		$(".mm-info-flyer.mm-can .ffnn-start-month").text(customMMES.FNCA.iniMonthName);
		$(".mm-info-flyer.mm-can .ffnn-start-year").text(customMMES.FNCA.iniYear);

		$(".mm-info-flyer.mm-can .ffnn-end-day").text(customMMES.FNCA.endDay);
		$(".mm-info-flyer.mm-can .ffnn-end-month").text(customMMES.FNCA.endMonthName);
		$(".mm-info-flyer.mm-can .ffnn-end-year").text(customMMES.FNCA.endYear);

		if (customMMES.FNCA.iniYear == customMMES.FNCA.endYear) {
			$(".mm-info-flyer.mm-can .year-prefix").remove();
		}

		if ((customMMES.FNCA.iniMonth == customMMES.FNCA.endMonth) && (customMMES.FNCA.iniYear == customMMES.FNCA.endYear)) {
			$(".mm-info-flyer.mm-can .month-prefix").remove();
		}
	}
};

//  Nueva Ventana modal FFNN

customMMES.visorFNP = function() {
	var pageFlyer = '#/page/' + customMMES.URLparam('page').replace("/","");
	customMMES.modal.show(995,775,"//" + domain + ".mediamarkt.es/static/layer/folleto-nacional/" + customMMES.FNPA.folderName + "-index.html" + pageFlyer, null,"ffnn");
};

customMMES.visorFNC = function() {
	var pageFlyer = '#/page/' + customMMES.URLparam('page').replace("/","");
	customMMES.modal.show(995,775,"//" + domain + ".mediamarkt.es/static/layer/folleto-nacional-canarias/" + customMMES.FNCA.folderName + "-index.html" + pageFlyer, null, "ffnn");
};

// Llamada a la nueva Ventana modal FFNN según el Hash de la URL

customMMES.visorFN = function() {

	if (customMMES.getParmFromHash(window.location.href,"ffnn") == "peninsula") {
		customMMES.visorFNP();
	}

	if (customMMES.getParmFromHash(window.location.href,"ffnn") == "canarias") {
		customMMES.visorFNC();	
	}
};

/* @end		- Funciones de Folleto Nacional (Landing, Visor Folleto) */
/********************************************************************************************************************/
/* @start 	- Funciones de inyección de precios para landings */

customMMES.landing = {

/**
* @param idProducto {string}: referencia del producto del cual queremos coger el precio.
* @param claseDiv {string}: class de CSS donde irá insertado el precio.
* @param textoDesde {string}: Se debe reportar 'Desde' o 'Cadauno' si se quiere añadir estos textos al precio. En caso contrario, string vacío. ("").
* @param colorTexto {string}: Se debe reportar 'black', 'red' o 'white' si se quiere añadir estos colores al texto del parámetro anterior. En caso contrario, string vacío. ("").
*/

insert: function(idProducto, claseDiv, textoDesde, colorTexto){

		// function traer(idProducto, claseDiv){
			$.ajax({
	            url: '//tiendas.mediamarkt.es/ecommerce/processes/PriceByProductId/PriceByProductId.cfm?pId=' + idProducto,//1217318
	            beforeSend: function() {
	            	$('.' + claseDiv).show(); 
	            }
	        })
			.done(function(data) {
				$('.' + claseDiv).html(data);
				var precioRecibido = $('#precio').text();
				$('.'+ claseDiv + ' #precio').remove();
				$('.' + claseDiv).text(precioRecibido);
				$('.' + claseDiv).preciosMM().css('position','relative');
				if (textoDesde == "Desde") {
					$('.' + claseDiv).addClass('price-from-'+ colorTexto);
				}else{
					if (textoDesde == "Cadauno") {
						$('.' + claseDiv).addClass('price-to-'+ colorTexto);
					}	
				}
			})
			.fail(function(){
	            // console.log('error en la conexión ajax');
	        })
			.complete(function() {

	            //console.log('petición correcta');

	        });
	    // }

	},
	insertPriceCuote: function(idProducto, claseDiv, textoDesde, colorTexto, claseCuote, cuote){

/*!
* @param idProducto {string}: referencia del producto del cual queremos coger el precio.
* @param claseDiv {string}: class de CSS donde irá insertado el precio.
* @param textoDesde {string}: Se debe reportar 'Desde' o 'Cadauno' si se quiere añadir estos textos al precio. En caso contrario, string vacío. ("").
* @param colorTexto {string}: Se debe reportar 'black', 'red' o 'white' si se quiere añadir estos colores al texto del parámetro anterior. En caso contrario, string vacío. ("").
* @param claseCuote {string}: Se debe reportar el nombre de la clase donde se insertara el numero de cuotas de la financiacion
* @param cuote {integer}: Se debe reportar el numero de cuotas en que se divide el precio, e.g: 10
*/


$.ajax({
	            url: '//tiendas.mediamarkt.es/ecommerce/processes/PriceByProductId/PriceByProductId.cfm?pId=' + idProducto,//1217318
	            beforeSend: function() {
	            	$('.' + claseDiv).show(); 
	            }
	        })
.done(function(data) {
	$('.' + claseDiv).html(data);
	var precioRecibido = $('#precio').text();
	$('.'+ claseDiv + ' #precio').remove();
	$('.' + claseDiv).text(precioRecibido);
	$('.' + claseDiv).preciosMM().css('position','relative');
	if (textoDesde == "Desde") {
		$('.' + claseDiv).addClass('price-from-'+ colorTexto);
	}else{
		if (textoDesde == "Cadauno") {
			$('.' + claseDiv).addClass('price-to-'+ colorTexto);
		}	
	}

	$('.' + claseCuote).html(data);
	var cuota = precioRecibido/cuote;
				var priceString = String(cuota); // convertimos el integer en string para poder hacer un indexOf (just strings)
				var comparacion = priceString.indexOf('.'); //posición del signo decimal
				var redondeoDecimales = cuota.toFixed(2); // se eliminan los decimales apartir del tercero incluido
				if(comparacion != -1){  //si tiene decimales
				    //Conté decimals
				    $('.' + claseCuote).text(redondeoDecimales + " €"); 
				} else {
				    //No conté decimals
				    $('.' + claseCuote).text(cuota + " €"); 
				}

			})
.fail(function(){
	            // console.log('error en la conexión ajax');
	        })
.complete(function() {

	            //console.log('petición correcta');

	        });


}







}

/* @end		- Funciones de inyección de precios para landings */
/********************************************************************************************************************/
/* @start 	- Funciones de inyección de código */

customMMES.inject = {

	// Control de visibilidad: Categorías periódicas/puntuales

	leftNavigationControl: function() {

		customMMES.leftNavigation.OF1	= new customMMES.leftNavigation.Campaign('ddp', FNP1_FechaInicio, FNP1_FechaFin); // Ofertas Folleto
		customMMES.leftNavigation.OF2	= new customMMES.leftNavigation.Campaign('of2', FNP2_FechaInicio, FNP2_FechaFin); // Oferta Folleto
		customMMES.mmDate.MNS			= new customMMES.mmDate.MNSCampaignType(MNS_Dia, MNS_HoraInicio, MNS_HoraFin); // Martes Night Show
		customMMES.leftNavigation.BFR	= new customMMES.leftNavigation.Campaign('bfr', BFR_FechaInicio, BFR_FechaFin); // Black Friday, ctg-bfr
		customMMES.leftNavigation.MNS	= new customMMES.leftNavigation.Campaign('mns', customMMES.mmDate.MNS.inidatestring, customMMES.mmDate.MNS.enddatestring); // Martes Night Show
		customMMES.leftNavigation.DDM	= new customMMES.leftNavigation.Campaign('val', DDM_FechaInicio, DDM_FechaFin); // Campaña excp
		customMMES.leftNavigation.XHD	= new customMMES.leftNavigation.Campaign('xhd', XHD_FechaInicio, XHD_FechaFin); // XHD
		//customMMES.leftNavigation.CTGMOV	= new customMMES.leftNavigation.Campaign('movilmania', CTGMOV_FechaInicio, CTGMOV_FechaFin); // Movil Mania


		/*var DateEndXHD = customMMES.mmDate.formatDate(XHD_FechaFin);
		// especial Precios Rojos con variable XHD	Para esconder los botones
		if (DateNow <= DateEndXHD) 
		{
		customMMES.leftNavigation.XHD	= new customMMES.leftNavigation.Campaign('of1', XHD_FechaInicio, XHD_FechaFin); // Precios rojos
	}*/
},

	// Widget de suscripción a la newsletter

	newsletter: function(buttonText) {

		var btnText;
		buttonText == null ? btnText = "Avísame" : btnText = buttonText;

		var $newsletter = $("<div>", {id: "mm-newsletter-form-wrapper", class: "mm-custom-form-wrapper mm-block-wrapper"}),
		content =
		'<section>'
		+'<form id="mm-newsletter-form" class="mm-custom-form" method="get" action="">'
		+'<input type="email" id="mm-email-input" class="mm-text-input mm-input-shadow" placeholder="Tu correo electrónico" autocomplete="off">'
		+'<button type="button" id="mm-newsletter-submit" class="mm-black mm-arrow mm-button">' + btnText + '</button>'
		+'</form>'
		+'</section>';
		html = 	$.parseHTML(content);
		$newsletter.append(html);
		$newsletter.appendTo($("#newsletter-widget"));
		customMMES.newsletter.validation("mm-newsletter-form", "");
	},

		/* Newsletter Widget V2:
		 *Injecta un objeto de tipo Widget Newsletter con un label, input y un boton
		 * Reemplaza  el widgwet de Newsletter version V1
		 *@PARAMETERS LABEL:  texto que se inserta en el label del Widget, Se puede dejar como NULL y pillara el valor por defecto.
		 *PARAMETER BOTTONTEXT: texto del botón, se puede dejar a NULL y tomara como texto por defecto
		 * Este Widget se inserta al final del contenedor de cada landing, validado siempre que sea un landing ('/es/shop/')
		 */

		 WidgetNewsLandings: function(label,buttonText) {

		 	var btnText;
		 	var txtLabel;
		 	buttonText == null ? btnText = "Avísame" : btnText = buttonText;
		 	label == null ? txtLabel = "Y...?no te pierdas todas las novedades!" : txtLabel = label;

		 	var $newsletter = $("<div>", {id: "wrp-newsletter-landing", class: "wrp-newsletter-landing"}),

		 	content = 
		 	'<section>' 
		 	+'<div class="row newsletter-wrapper">' 
		 	+'<div class="col-xs-7 mm-inner">' 
		 	+'<h1>'+ txtLabel +'</h1>' 
		 	+'</div>' 
		 	+'<div class="col-xs-5 mm-inner">' 
		 	+'<div id="newsletter-widget">' 
		 	+'<div id="mm-newsletter-form-wrapper" class="mm-custom-form-wrapper mm-block-wrapper">' 
		 	+'<section>' 
		 	+'<form id="mm-newsletter-form" class="mm-custom-form" method="get" action="">' 
		 	+'<input type="email" id="mm-email-input" class="mm-text-input mm-input-shadow" placeholder="Tu correo electrónico" autocomplete="off">' 
		 	+'<button type="button" id="mm-newsletter-submit" class="mm-black mm-arrow mm-button">'+ btnText +'</button>' 
		 	+'</form>' 
		 	+'</section>' 
		 	+'</div>' 
		 	+'</div>' 
		 	+'</div>' 
		 	+'</div>' 
		 	+'</section>'; 
		 	html = 	$.parseHTML(content);
		 	$newsletter.append(html);
		 	$("#mm-bottom-column").append($newsletter);
		 	customMMES.newsletter.validation("mm-newsletter-form", "");
		 },	

		/* Funcion que inserta diamicamente el Google Analytic sobre el boton del Widget de Newsletter:
		*/	

		uaLanding: function(btn){

			var url = window.location.pathname;
			var arrayLanding = url.split("/");
			var nameLanding = arrayLanding[arrayLanding.length-1].split(".")[0];

			$("#" + btn).attr({
				onclick: "javascript:ga(\'send\', \'event\', \'clics\', \'boton-newsletter', \'" + nameLanding + "\');"
			});
		},




		/***********************************************************/





		generateSliderMedia: function() {

			var $mtSlider 	= $("<div>", {id: "mediatrends-slider", class: "mediatrends-slider"});
		//var $title		= $("<h2>", {id:"title"}), 
		var $title		= $("<a href='http://www.mediatrends.es/?utm_source=mediamarkt.es&utm_medium=carrusel-home&utm_campaign=logo-mediatrends'  target='_blank'><h2 id='title'>- Blog de tecnolog&iacute;a, tendencias y entretenimiento</h2></a>"),
		separator	=   
		'<div class="mm-separator">'
		+'<span class="mm-arrow-separator"></span>'
		+'</div>',

		content =   '<div class="owl-carousel owl-theme" id="slider-mediatrends">'
		+'<div class="content-item news">'
		+'<a href="http://www.mediatrends.es/noticias" target="_blank"><h4>Tendencias</h4></a>'
		+'<div class="body-item">'
		+'<figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/noticias"><img class="img-responsive" alt="" src="http://www.mediamarkt.es/static/index/slider/img/bg-img.jpg" title=""></a>'
		+'</figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/noticias"><p>Blog de tecnolog&iacute;a, tendendencias...</p></a>'
		+'</div>'
		+'</div>'
		+'<div class="content-item reviews">'
		+'<a href="http://www.mediatrends.es/analisis" target="_blank"><h4>Reviews</h4></a>'
		+'<div class="body-item">'
		+'<figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/analisis"><img class="img-responsive" alt="" src="http://www.mediamarkt.es/static/index/slider/img/bg-img.jpg" title=""></a>'
		+'</figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/analisis"><p>Blog de tecnolog&iacute;a, tendendencias...</p></a>'
		+'</div>'
		+'</div>'
		+'<div class="content-item comparativas">'
		+'<a href="http://www.mediatrends.es/comparativas" target="_blank"><h4>Comparativas</h4></a>'
		+'<div class="body-item">'
		+'<figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/comparativas"><img class="img-responsive" alt="" src="http://www.mediamarkt.es/static/index/slider/img/bg-img.jpg" title=""></a>'
		+'</figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/comparativas"><p>Blog de tecnolog&iacute;a, tendendencias...</p></a>'
		+'</div>'
		+'</div>'
		+'<div class="content-item how-to">'
		+'<a href="http://www.mediatrends.es/how-to" target="_blank"><h4>How to</h4></a>'
		+'<div class="body-item">'
		+'<figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/how-to"><img class="img-responsive" alt="" src="http://www.mediamarkt.es/static/index/slider/img/bg-img.jpg" title=""></a>'
		+'</figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/how-to"><p>Blog de tecnolog&iacute;a, tendendencias...</p></a>'
		+'</div>'
		+'</div>'
		+'<div class="content-item mediatrends-tv">'
		+'<a href="http://www.mediatrends.es/videos" target="_blank"><h4>MediaTrends TV</h4></a>'
		+'<div class="body-item">'
		+'<figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/videos"><img class="img-responsive" alt="" src="http://www.mediamarkt.es/static/index/slider/img/bg-img.jpg" title=""></a>'
		+'</figure>'
		+'<a target="_blank" href="http://www.mediatrends.es/videos"><p>Blog de tecnolog&iacute;a, tendendencias...</p></a>'
		+'</div>'
		+'</div>'
		+'</div>';
		separador 		=	$.parseHTML(separator);
		$mtSlider.appendTo($("#megabanner-3"));
		$title.appendTo($mtSlider);
		$mtSlider.append(separador);
		contentenido 	= 	$.parseHTML(content);
		$mtSlider.append(contentenido);

		/****************** Call function generate carousel ****************/


		customMMES.inject.generaSliderMedia("container-mediatrends-slider");


	},

	generaSliderMedia: function(){
		var owl = $("#slider-mediatrends");
		owl.owlCarousel({
			items : 5,
			itemsDesktop : [1500,5], 
			itemsDesktopSmall : [900,4], 
			itemsTablet: [600,4], 
			itemsMobile : false, 

			navigationText : ["Prev", "Next"],
			transitionStyle : true,
			paginationSpeed : 800,
			slideSpeed : 800,
			autoPlay : false,
			pagination : false,
			scrollPerPage: true,
			lazyLoad : true,
			navigation : true
		});
	}





};

// Llamada al control de campañas en el Left Navigation

customMMES.inject.leftNavigationControl();

/* @end 	- Funciones de inyección de código */
/********************************************************************************************************************/
/* @Start 	- Funci?n que comprueba la existencia de un fichero en el servidor */

customMMES.check = {

	ifFileExists: function(url) {
		var http = new XMLHttpRequest();
		http.open('HEAD', url, false);
		http.send();
		return http.status != 404;
	}
};

/* @end 	- Funci?n que comprueba la existencia de un fichero en el servidor */
/********************************************************************************************************************/
/* @Start 	- Funci?n de automatizaci?n de los banners de 'Folleto & Ofertas Especiales' */

customMMES.json = {

	foeBanners: function() {

		/*
		 * Inyecta las imágenes de los banners de la 1ª posición de cada categoría
		 * mediante un archivo JSON alimentado por base de datos
		 */

		 if (customMMES.mmDomain.contain('/es/shop/ofertas-mediamarkt') == true ) {

			// Banners de FFNN

			$('#mm-ddp .mm-last img').attr('src', '/static/Specials/folleto-ofertas-especiales/img/banner-' + customMMES.FNP1.folderName + '.jpg');
			$('#mm-of2 .mm-last img').attr('src', '/static/Specials/folleto-ofertas-especiales/img/banner-' + customMMES.FNP2.folderName + '.jpg');
			
			// Banners de MNS

			var MNSbanner = '/static/Specials/folleto-ofertas-especiales/img/mns-' + customMMES.mmDate.MNS.folderName + '.jpg';
			
			if (customMMES.check.ifFileExists(MNSbanner) == true) {
				$('#mm-mns .mm-last img').attr('src', '/static/Specials/folleto-ofertas-especiales/img/mns-' + customMMES.mmDate.MNS.folderName + '.jpg');
			}
			else {
				$('#mm-mns .mm-last img').attr('src', '/static/Specials/folleto-ofertas-especiales/img/mns-default.jpg');
			}

			// Banners fijos de categoría

			$.getJSON('//tiendas.mediamarkt.es/ecommerce/ajax/json/leftNavi.cfm', function(data) {
				$.each(data, function(key, value) {
					var categoryId = "mm-" + data[key].EXTERNALID;
					categoryId = categoryId.replace("ctg-", "");
					if (categoryId == 'mm-inf' 
					// ||  categoryId == 'mm-tel' categoria eliminada
					||  categoryId == 'mm-mov' /*categoria nueva:telefonia movil*/
					||  categoryId == 'mm-tlv'
					||  categoryId == 'mm-gel'
					||  categoryId == 'mm-pel'
					||  categoryId == 'mm-fot'
					||  categoryId == 'mm-con') {
						if ($('#' + categoryId) !== null) {
							var banner = data[key].banners['1'].BIGIMAGE;
							var target = data[key].banners['1'].DESKTOPURL;
							var title  = data[key].banners['1'].BUTTON;
							$('#' + categoryId + ' .mm-last a').attr('href', target).attr('title', title);
							$('#' + categoryId + ' .mm-last img').attr('src', banner);
						}
					}
				});
			});
		}
	} 
};

/* @end 	- Función de automatización de los banners de 'Folleto & Ofertas Especiales' */
/********************************************************************************************************************/
/* @Start 	- Funcion que reconoce el dispositivo por el cual se conecta via User Agent */

//Devuelve True si estás conectado desde un dispositivo móvil (por UserAgent).
customMMES.device = {
	isMobile: function(){
		var x = navigator.userAgent

		if (x.match(/Iphone/i)|| x.match(/Ipod/i)|| x.match(/Android/i)|| 
			x.match(/J2ME/i)|| x.match(/BlackBerry/i)|| x.match(/iPhone|iPad|iPod/i)|| 
			x.match(/Opera Mini/i)|| x.match(/IEMobile/i)|| x.match(/Mobile/i)|| 
			x.match(/Windows Phone/i)|| x.match(/windows mobile/i)|| x.match(/windows ce/i)|| 
			x.match(/webOS/i)|| x.match(/palm/i)|| x.match(/bada/i)|| x.match(/series60/i)|| 
			x.match(/nokia/i)|| x.match(/symbian/i)|| x.match(/HTC/i))
		{ 
			return true;
		}
		else
		{
			return false;
		}
	}
};

/* @End 	- Funcion que reconoce el dispositivo por el cual se conecta via User Agent */
/***********************************************************************/
/* @Start 	- Funciones de cambio de skin */

customMMES.skin = {

	set: function() {
		if (customMMES.mmDate.collate(ALT_FechaInicio, ALT_FechaFin) == true) {
			$("html").addClass(ALT_Class);
		}


		if (customMMES.mmDate.collate(customMMES.mmDate.MNS.inidatestring, customMMES.mmDate.MNS.enddatestring) == true) {
			$("html").addClass("mns");
		}		
	}
};

/* @end 	- Funciones de cambio de skin  */
/********************************************************************************************************************/
/* @Start 	- Funciones de activacion de Entry Point Webshoper*/

customMMES.entryPoints = {
	setLaboral: function(obj) {
		var vEntry	= new customMMES.mmDate.weeklyCampaign(LbEntry_DiaInicial,LbEntry_DiaFinal,LbEntry_HoraInicial,LbEntry_HoraFinal); // Visibilidad de Entrypoint

		if (vEntry.IsActive == true) {
			$("." + obj).show();
		}else{
			$("." + obj).hide();
		}

	},
	setFijo: function(obj) {
		var vEntry	= customMMES.mmDate.collate(Fijo_FechaInicio,Fijo_FechaFin); // Visibilidad de Entrypoint

		if (vEntry == true) {
			$("." + obj).show();
		}else{
			$("." + obj).hide();
		}

	}
};


/* @end 	- activacion de Entry Point Webshoper  */
/********************************************************************************************************************/
/* @Start 	- Countdown */

customMMES.countDown = {

	settings: function(campaign, endDate) {

		// Parámetro 1: [STRING] El ID de campaña: 
		//	"mns": Martes night show
		//	"of1": Ofertas-Folleto
		//	"of2": Oferta-Folleto  (alternativo)

		if (campaign !== null && endDate == null) {
			switch(campaign) {

				case "mns":
				var ahora = DateNow.getTime(); 
				var endMNS = customMMES.mmDate.formatDate(customMMES.mmDate.MNS.enddatestring); 
				var endMNS_time = endMNS.getTime(); 
				var tiempoContador = (endMNS_time - ahora) / 1000;
				return tiempoContador;
				break;

				case "ddp":
				var ahora = DateNow.getTime(); 
				var endOF1 = customMMES.mmDate.formatDate(FNP1_FechaFin); 
				var endOF1_time = endOF1.getTime(); 
				var tiempoContador = (endOF1_time - ahora) / 1000;
				return tiempoContador;
				break;

				case "of2":
				var ahora = DateNow.getTime(); 
				var endOF2 = customMMES.mmDate.formatDate(FNP2_FechaFin); 
				var endOF2_time = endOF2.getTime(); 
				var tiempoContador = (endOF2_time - ahora) / 1000;
				return tiempoContador;
				break;


				case "xhd":
				var ahora = DateNow.getTime(); 
				var endXHD = customMMES.mmDate.formatDate(XHD_FechaFin); 
				var endXHD_time = endXHD.getTime(); 
				var tiempoContador = (endXHD_time - ahora) / 1000;
				return tiempoContador;
				break;	
				
				case "val":
					var ahora = DateNow.getTime(); 
					var endDDM = customMMES.mmDate.formatDate(DDM_FechaFin); 
					var endDDM_time = endDDM.getTime(); 
					var tiempoContador = (endDDM_time - ahora) / 1000;
					return tiempoContador;
				break;			
			}
		}

		if (campaign == null && endDate !== null) {
			var ahora = DateNow.getTime(); 
			var endParam = customMMES.mmDate.formatDate(endDate); 
			var endParam_time = endParam.getTime(); 
			var tiempoContador = (endParam_time - ahora) / 1000;
			return tiempoContador;
		}
	},

	load: function(campaign, HighRange) {

		var myCampaign = campaign;
		var $myCountdown = new Countdown({
		    time: customMMES.countDown.settings(myCampaign), // 86400 seconds = 1 day
		    width:270, 
		    height:50,  
		    rangeHi: HighRange,
		    style:"flip"        
		})
	},

	/**
	* @campaign  STRING, corresponde al tipod e campaña, ejemplo: of1, of2, mns
	* @endDate STRING, Fecha final del Countdown y se pasa como string, ejemplo: "18/06/2015, 20:59:59"
	* @counterType VARIABLE inicializada  por defecto a 'DailyCounter'. Tipo de countdown, ejemplo: DailyCounter, HourlyCounter
	* @skin - STRING: Personaliza el CountDown con estilos para dos tipos Oscuro (Dark), claro (Light)
	* @label - STRING: Texto que tendra la label sobre el CountDown
	* @contentInsert - STRING: Id del contenedor donde insertaremos el CountDown
	* @Inicializacion del Countdown, Ejemplo: customMMES.countDown.loadNewCounter (null, '29/06/2015, 00:00:00', 'DailyCounter', 'dark', 'Estas ofertas termina dentro de:');  // customMMES.countDown.loadNewCounter ('of1', null, 'HourlyCounter', 'dark', 'Estas ofertas termina dentro de:');
	* @return object - A FlipClock object
	*/

	loadNewCounter: function(campaign, endDate, counterType, skin, label, contentInsert) {

		var clock;
		var type;
		
		if (campaign !== null && endDate == null) {
			var diff = customMMES.countDown.settings(campaign, null);
		}

		if (campaign == null && endDate !== null) {
			var diff = customMMES.countDown.settings(null, endDate);
		}

		if (skin == 'std') {
			$("#" + contentInsert).addClass('std custom-flip');
		}

		if (skin == 'dark') {
			$("#" + contentInsert).addClass('dark custom-flip');
		}

		if (skin == 'light') {
			$("#" + contentInsert).addClass('light custom-flip');
		}

		if (skin == 'white') {
			$("#" + contentInsert).addClass('white custom-flip');
		}		
		
		$( "#" + contentInsert).append( '<div class="countdown"></div>');
		$( "#" + contentInsert + " .countdown" ).before( "<p>" + label +"</p>" );

		counterType == null ? type = 'DailyCounter' : type = counterType; 

        // Instantiate a coutdown FlipClock

        clock = $( "#" + contentInsert + " .countdown" ).FlipClock(diff, {
        	clockFace: type,
        	countdown: true,
        	language:'es-es'
        });
    }
};

/* @end 	- Countdown */
/********************************************************************************************************************/
/* @Start 	- Corrections */

customMMES.corrections = {

	marketFlyers: function() {

		if (customMMES.mmDomain.contain('/marketinfo/') == true) {

			// Overrides the default class and MCS behavior

			$(".my-market-menu").find(".brochure").attr({id: "flyer-button-1", class: "folleto flyer unique-flyer"});
			$(".my-market-menu").find(".brochure-double").attr("class", "folleto flyer multi-flyer");

			// Defines the number of flyers published in a Store and a array to store flyer data

			var n = $(".flyer a").length;
			flyerArray = new Array(n);

			// Case #1: Only one flyer in a Store

			if (n == 1) {
				var storeName = $(".unique-flyer > a").attr("href").split("/")[5];
				var flyerName = $(".unique-flyer > a").attr("href").split("/")[6];
				flyerArray[0] = storeName;
				flyerArray[1] = flyerName;
				$(".unique-flyer > a").attr({
					href: "#",
					'data-store': flyerArray[0],
					'data-flyer': flyerArray[1],
				});
				$(".unique-flyer > a").click(function(event) {
					event.preventDefault();
					customMMES.modal.show(855, 575, 'http://redblue.scene7.com/is-viewers-3.9/flash/genericbrochure.swf?serverUrl=http://redblue.scene7.com/is/image/&contentRoot=http://redblue.scene7.com/skins/&config=redblue//MMFlyer_1&image=redblue/E_MM_' + flyerArray[0] + '_' + flyerArray[1] + '&textEntryTemplate=%252*idx%25-%252*idx%2b1%25&lang=es', null, 'folleto');
				});
			}

			// Case #2: Several flyers in a Store

			if (n > 1) {

				// Stores all flyers data in an array

				for (i = 0; i < n; i ++) {
					var storeName = [];
					var flyerName = [];
					var flyerText = [];
					storeName[i] = $($(".flyer a")[i]).attr("href").split("/")[5];
					flyerName[i] = $($(".flyer a")[i]).attr("href").split("/")[6];
					flyerText[i] = $($(".flyer a")[i]).html();

					flyerArray[i] = new Array(3);
					flyerArray[i][0] = storeName[i];
					flyerArray[i][1] = flyerName[i];
					flyerArray[i][2] = flyerText[i];

					// Dynamic flyer button generation with the needed data

					$("<li/>", {
						id: "flyer-button-" + [i],
						class: "folleto flyer-button",
						html: $("<a/>", {
							href: "#",
							'data-store': flyerArray[i][0],
							'data-flyer':flyerArray[i][1],
							onclick: "javascript: customMMES.modal.show(855, 575, 'http://redblue.scene7.com/is-viewers-3.9/flash/genericbrochure.swf?serverUrl=http://redblue.scene7.com/is/image/&contentRoot=http://redblue.scene7.com/skins/&config=redblue//MMFlyer_1&image=redblue/E_MM_" + flyerArray[i][0] + "_" + flyerArray[i][1] + "&textEntryTemplate=%252*idx%25-%252*idx%2b1%25&lang=es', null, 'folleto')",
							html: $("<span/>", {
								html: flyerArray[i][2]
							})
						})
					}).insertBefore($(".contact-data"));
				};

			// Removes default MCS widget for several flyers

			$("#flyer-button-0").remove();
			$(".multi-flyer").remove();
		}
	}

		// Adds a new title in Market's main page

		
		
		$('#all-markets .container h1').css({
			'color': '#d51900',
			'font-size': '3em',
			'font-family': 'mm-headline, Arial, Helvetica, sans-serif',
			'text-transform': 'none',
			'text-align': 'center',
			'padding-bottom': '15px',
			'margin': '0px 0 30px 0'
		});
	},


/*
	repairNewsTemp: function(){	
		if (window.location.pathname == "/webapp/wcs/stores/servlet/MultiChannelMAPersonal"){
			$($("#filters ul li a")[7]).attr("href","http://specials.mediamarkt.es/newsletter/registro");
		}else{
			$($("#filters ul li a")[4]).attr("href","http://specials.mediamarkt.es/newsletter/registro");
		}
	},

	*/

	repairStatus: function() {

		$('h1').text('Reparaciones');
		$('required').html('Campos obligatorios (<em>*</em>)').css({'position': 'relative', 'top': '0', 'right': '0'});
		var $wrapper = $('<div id="section-wrapper"></div>');
		$wrapper.insertAfter($('#repair-status-form'));
		$wrapper.load('/static/Specials/repair-status/table.html');
	},

	accordion: function() {

		// Add Inactive Class To All Accordion Headers

		$('.accordion-header').toggleClass('inactive-header');
		
		// Set The Accordion Content Width

		var contentwidth = $('.accordion-header').width();
		$('.accordion-content').css({'width' : contentwidth });
		
		// Open The First Accordion Section When Page Loads

		$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
		$('.accordion-content').first().slideDown().toggleClass('open-content');
		
		// The Accordion Effect

		$('.accordion-header').click(function () {
			if ($(this).is('.inactive-header')) {
				$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
				$(this).toggleClass('active-header').toggleClass('inactive-header');
				$(this).next().slideToggle().toggleClass('open-content');
			}
			else {
				$(this).toggleClass('active-header').toggleClass('inactive-header');
				$(this).next().slideToggle().toggleClass('open-content');
			}
		});
		return false;
	},


	misc: function() {

		// Corrections: Cambiar el link del logo

		$("#logo a").attr("href","http://" + domain + ".mediamarkt.es");

		// Corrections: Cambiar el link de  "Contacto" My account
		if (customMMES.mmDomain.query.IsCMS) {
			$(".side-nav>li:nth-child(2) a").attr("href","http://tiendas.mediamarkt.es/ecommerce/users/orders.cfm");
			$(".side-nav>li:nth-child(3) a").attr("href","https://specials.mediamarkt.es/newsletter/registro");
			$(".side-nav>li:nth-child(4) a").attr("href","http://" + domain + ".mediamarkt.es/es/shop/contacto.html");
		}
		
		// Corrections: Ocultar categorias pequeñosprecios & ParaMamáyPapá, AmigoInvisible, TuCasaEnNavidad, PremioGordo

		if (customMMES.mmDomain.query.IsShop == true) {
			$("#categoryTree_10003126, #categoryTree_10004824, #categoryTree_10004823, #categoryTree_10004822, #categoryTree_10004821, #categoryTree_10004847").hide();
		}
		if (customMMES.mmDomain.query.IsShop == true) {
			$("iframe.content-top-navi").hide();
		}

		if ($("#blueknow-widget").width() > 775 && $("#blueknow-widget").width() < 1000){
			$("#blueknow-widget").addClass("blueknow-one-column");
		}	

		// Corrections: Inserta USP en la cabecera

		$( "#infobar" ).append($("#mm-infobar"));

		// Corrections: Inserta Cabecera en la cabecera

		$('#header > header').append($('#mm-header'));

		// Corrections: Inserta Top Navi  en la cabecera
		$('#header > header').append($('.container-fluid.top-nav'));



        // Customizations: 'My account' page

        $('#my-account.my-account.personal-data .button.gray.password span').text('aquí');
        $('#my-account.personal-data #billing-address dt').text('Dirección de facturación');
        $('#my-account.personal-data #shipping dt').text('Dirección de envío');
        $("#my-account.my-account.register .required").insertBefore($('fieldset.box ul'));
        $('#my-account.my-account.register fieldset.box + div').insertAfter($('#my-account.my-account.register .buttons'));

	  	// Redirect a '/ofertas-folleto' del link 'Folleto Loco' del Dormat. Eliminar snippet tras el 07/05/2015

	  	if (customMMES.mmDate.collate('29/04/2015, 21:00:00', '06/05/2015, 20:59:59') == true) {
	  		$($('.container-doormat li a')[22]).attr('href', 'http://tiendas.mediamarkt.es/ofertas-folleto');
	  	}
	  },

	  mediapedia: function() {

	  	if (customMMES.mmDomain.contain("MultiChannelMediaPedia") == true) {

			// Some previous arranges in order to get Blueknow working in CMS 5 

			$(".breadcrumbs").attr("id", "breadcrumbfitter");
			$("body").addClass("mediapedia");
			$("#filters").insertBefore($("#infoportal"));
			$("<div>", {id: "contentRight"}).insertAfter($("#infoportal"));
			$(".breadcrumbs").insertBefore($("#filters"));

			// Including Blueknow API dinamically in the <head>

			$.when(
				$("<link/>", {
					rel: "stylesheet",
					type: "text/css",
					href: "//" + domain + ".mediamarkt.es/static/include/css/mediapedia.css"
				}).appendTo("head"), $.getScript("//" + domain + ".mediamarkt.es/static/include/jq-bk-mp.js")).done(function() {

	   			// Initializes Blueknow widget after BK API is loaded. The number of products is calculated by 'cSize/pSize'

	   			var MainColumnHeight = document.getElementById("infoportal").offsetHeight; 
	   			var ProductHeight = 270;  
	   			var products = (Math.floor(MainColumnHeight/ProductHeight) - 1);
	   			var settings = {
	   				recommendations: products,
	   				title: "Te recomendamos"
	   			};

			    // Drawing Blueknow widget in 'ContentRight' section

			    Blueknow.MP.Recommender('BK-228300128806-7', 'contentRight', settings);
			});
			}
		},

		popoverUSP: function(){

			var  idUSP = "";
			$('.header-usp li').click(function(e){
				idUSP = this.id;
				$(".header-usp li").each(function(){
					if($(this).attr("id") !== idUSP){
			 		//alert($(this).attr("id"));
			 		var idUSPs = "#" + $(this).attr("id") + " .popover-usp";
			 		$(idUSPs).hide();
			 	}
			 	
			 });
				idUSP = "#" + idUSP;

				var $popover = $(idUSP).find(".popover-usp");
				$popover.addClass('active');
				e.stopPropagation();
				$popover.toggle();      
			});

			$('body').on('click', function (e) {
				if ($(e.target).closest(".popover-usp").length === 0) {
					$(".popover-usp").hide();
				}
			});
		},

		repairStatus:function(){
			$(".required").html("campos obligatorios (<em>*</em>)").css({'position':'relative','top':'0', 'right':'0'});
			var $wrapper = $("<div id='section-wrapper'></div>");
			$wrapper.insertAfter($("#repair-status-form"));
			$wrapper.load("/static/Specials/repair-status/table.html");
		},


/*!
* Parametros: tipo = clases que controla los Entrypoints 
*view = Cantidad de Entrypoints a mostrar segun el random
* position = Columna del Entry `point (Left, middle, right)
*//*
	entrypoinRotator:function(position,tipo, view){ 

	    $("."+ position + " ."+ tipo).hide();
	    var elements = $("."+ position + " ."+ tipo);
	    var elementCount = elements.size();
	    var elementsToShow = view;
	    var alreadyChoosen = ",";
	    var i = 0;
	    while (i < elementsToShow) {
	        var rand = Math.floor(Math.random() * elementCount);
	        if (alreadyChoosen.indexOf("," + rand + ",") < 0) {
	            alreadyChoosen += rand + ",";
	            elements.eq(rand).show();
	            ++i;
	        }
	    };

	},
	*/


	randomEntrypoint:function(start,end,position,tipo){
		if (customMMES.mmDate.collate(start, end)) {
			var elements = $("."+ position + " ."+ tipo);
			var random = Math.floor(Math.random() * $(elements).length);
			$(elements).hide().eq(random).show();
		};
	},

	timerEntrypoint:function(start,end,position,tipo){
		if (customMMES.mmDate.collate(start, end)) {
			var elements = $("."+ position + " ."+ tipo);
			$(elements).addClass('rotator-middle');
		};
	},	

	modal: function() {

		$("[data-trigger='modal']").click(function(event) {
			event.preventDefault();
			var type = $(this).attr('data-type'); 
			if (type == 'privacy') {
				customMMES.modal.show(742, 531, customMMES.mmDomain.query.mm_protocol + '//' + customMMES.mmDomain.query.getEnvironment +'.mediamarkt.es/static/layer/terms/privacy/', 'Política de Privacidad', 'content');
			}
		});
	},


	/******* Funcion que solo ejecuta contenido o afecta a dispositivos moviles *************/
	mobileDevice: function() {
		var ua = navigator.userAgent;
		var checker = {
			iphone: ua.match(/(iPhone|iPod|iPad)/),
			blackberry: ua.match(/BlackBerry/),
			android: ua.match(/Android/)
		};
		if (checker.android){
			$("#campaign-video, #camp-btn-eflyer, #camp-btn-eflyer-can").remove();
		}
		else if (checker.iphone){
			$("#campaign-video, #camp-btn-eflyer, #camp-btn-eflyer-can").remove();
		}
		else if (checker.blackberry){
			$("#campaign-video, #camp-btn-eflyer, #camp-btn-eflyer-can").remove();
		}
	},
	/*

  redirectMobile: function() {
        var ua = navigator.userAgent;
        var checker = {
          iphone: ua.match(/(iPhone|iPod|iPad)/),
          blackberry: ua.match(/BlackBerry/),
          android: ua.match(/Android/)
        };
        if (checker.android || checker.iphone || checker.blackberry){
            window.location.href = "http://tiendas.mediamarkt.es";
        }
  },	
  */

	deleteInclude:function(filename, filetype){ // http://www.javascriptkit.com/javatutors/loadjavascriptcss2.shtml
			 var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
			 var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
			 var allsuspects=document.getElementsByTagName(targetelement) // http://www.javascriptkit.com/javatutors/loadjavascriptcss.shtml
			 //https://naveensnayak.wordpress.com/2013/06/26/dynamically-loading-css-and-js-files-using-jquery/
			for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
				if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
			   allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
		}	
	},

	addItemMyaccount:function(){
		if( $("#filters .side-nav") !==null ){  // Agrega el item  en el left navi de Mi Cuenta Área de cliente de financiación
			$('.side-nav li:last').after('<li class=""><a href="http://www.cetelem.es/zonaClienteMediaMarkt" class="">Área de cliente de financiación</a></li>');
			$('.side-nav li:nth-child(4)').after('<li class=""><a href="http://www.mediamarkt.es/es/shop/mi-cuenta/cita-previa.html" class="">Cita previa</a></li>');
		}
	},

	insertBannerPopover:function(){
		// Inserta el Mini - banner de Climatización en el popover
		if (customMMES.mmDate.collate('19/06/2015, 00:00:00', '29/06/2015, 23:59:00') == true) {
			$('<figure style="width: 100%; border-top: 1px solid #ddd;">'
				+'<a title="0% en Televisores de 48&quot; o más y proyectores" target="_blank" href="http://www.mediamarkt.es/es/shop/financiacion.html">'
				+'<img style="max-width: 300px; width: 100%; margin: 20px auto 0; display: block;" alt="0% en Televisores de 48&quot; o más y proyectores " src="//www.mediamarkt.es/static/Specials/financiacion/img/desplegable-tv-financiacion.jpg" />'
				+'</a>'
				+'</figure>').insertAfter($('#ctg-tlv.popover .description-product-top p, #ctg-aud.popover .description-product-top p'));
		};


			// Si la pantalla del dispositivo tiene un alto > 750px, se inyectan los banners de MM Reservas en los Popovers de C&V y C,M y L, en caso contrario no caben en los Popovers y por tanto, no los inyectamos

			if (customMMES.mmDate.collate('05/05/2015, 10:00:00', '01/01/2099, 00:00:00') == true) {
				$('<figure style="width: 100%; border-top: 1px solid #ddd;">'
					+'<a title="Ir a Reservas Media Markt - Videojuegos" target="_blank" href="http://reservas.mediamarkt.es/">'
					+'<img style="width: 220px; margin: 20px auto 0; display: block;" alt="reserva-videojuegos.jpg" src="//www.mediamarkt.es/static/index/img/banner-reserva-videojuegos.jpg" />'
					+'</a>'
					+'</figure>').insertAfter($('#ctg-con.popover .description-product-top p'));
				// $('<figure style="width: 100%; border-top: 1px solid #ddd;"><a title="Ir a Reservas Media Markt - Música y Cine" target="_blank" href="http://reservas.mediamarkt.es/musica-cine/"><img style="width: 220px; margin: 20px auto 0; display: block;" alt="reserva-musica-cine.jpg" src="//www.mediamarkt.es/static/index/img/banner-reserva-musica-cine.jpg" /></a></figure>').insertAfter($('#ctg-cml.popover .description-product-top p'));
			};					

		},

		setRedPrices: function() {

			var cnt = 0;
			var estado = 0;
			timer1 = setInterval(function() {
				cnt++;
				try {

					if(estado == 0 && (customMMES.mmDate.collate(FNP2_FechaInicio, FNP2_FechaFin) == true)){
						// Corrections: Inserta precios rojos a los carruseles
						$("#s-down .product-price .mm-price").addClass("mm-red");

						estado = 1;
					}

				} catch (e) {
					clearTimeout(timer1);
				}
				if (cnt > 14) {
					clearTimeout(timer1);
				}
			}, 100);		
		},
		returnMobile: function(){
			if(customMMES.device.isMobile() == true){
				$("#returnMobile").css("display", "block");

			}			

		}
	};

	/* @end		- Corrections */
	/********************************************************************************************************************/
	/* @start 	- Funciones referentes a tiendas */
	customMMES.Stores = {

	/*
	* Esta Funcion recoge la informacion de las tiendas cargando desde el JSON desde la ruta:  http://www.mediamarkt.es/static/json/stores.es.json
	* @callback. Variable, Objeto que se recoge del JSON
	* 
	* EJEMPLO DE FUNCION QUE SE CREA PARA RECOGER LOS PARAMETROS DE LA TIENDA:
	*
	*		customMMES.Stores.getInfoStoreByCookie(function(tienda){
	*		var SAP = tienda.IDSAP;
	*    	location.href = 'http://perf.mediamarkt.es/es/shop/solicitar-cita-previa.html?selectedStore=' + SAP;
	*	});
	*
	*
	* @MC_STORE_ID valor de la cookie
	*/
	

	getInfoStoreByCookie: function(callback) {
		var self = this;
		if (customMMES.mmDomain.query.IsShop == false) {
			if (customMMES.mmDomain.query.IsPROD == true) {
				var url = customMMES.mmDomain.query.mm_protocol + '//' + 'www.mediamarkt.es/static/json/stores.es.json';
			} else if (customMMES.mmDomain.query.IsPERF == true) {
				var url = customMMES.mmDomain.query.mm_protocol + '//' + 'perf.mediamarkt.es/static/json/stores.es.json';
			}
		};
		$.getJSON(url, function(data){
			var stores = data.stores;
			var cookie = $.cookie('MC_STORE_ID');

			if(typeof(cookie) != "undefined"){
				var tienda = self.getObjects(stores, "MC_STORE_ID", cookie)[0];
				callback(tienda);
			}

		});

	},


	getObjects: function (obj, key, val) {
		var objects = [];
		for (var i in obj) {
			if (!obj.hasOwnProperty(i)) continue;
			if (typeof obj[i] == 'object') {
				objects = objects.concat(this.getObjects(obj[i], key, val));    
			} else 
        //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
        if (i == key && obj[i] == val || i == key && val == '') { //
        	objects.push(obj);
        } else if (obj[i] == val && key == ''){
            //only add if the object is not already in the array
            if (objects.lastIndexOf(obj) == -1){
            	objects.push(obj);
            }
        }
    }
    return objects;
},

	/*
EJEMPLO DE FUNCION PARA RECUPERAR DATOS ESPECIFICOS DE UN JSON, En este caso recuperamos el IDSAP y lo metemos en un array


	storesTotal: function() {	
		var result = [];
		var url = customMMES.mmDomain.query.mm_protocol + '//' + customMMES.mmDomain.query.getEnvironment + '.mediamarkt.es/static/json/stores.es.json';
		$.getJSON(url, function(data) {
			$.each(data, function(key, value) {
					result.push(data[key].IDSAP);	
			});

			$('#all-markets .container h1').text(result.length + ' tiendas en España');
		});

		
	},

	contarTiendas: function(longitud){
		return this.longitud;
	},
	*/
	displayDays: function(){

			  var urlNow = window.location;
		
		if (customMMES.mmDomain.query.IsShop == false) {
			if (customMMES.mmDomain.query.IsPROD == true) {
				var url = customMMES.mmDomain.query.mm_protocol + '//' + 'www.mediamarkt.es/static/json/stores.es.json';
			} else if (customMMES.mmDomain.query.IsPERF == true) {
				var url = customMMES.mmDomain.query.mm_protocol + '//' + 'perf.mediamarkt.es/static/json/stores.es.json';
			}
		};
			  $.getJSON(url, function(data) {
			  	$.each(data, function(key, value) {
			  		if (key == 'stores') {
			  			var store = value;
			  			$.each(store, function(key, value) {
			  				var storeId =  store[key]['MC_STORE_ID'];

			  				if (customMMES.mmDomain.contain(storeId)) {


			  					if (this.OPENDAYS || this.CLOSEDDAYS || this.SPECIALDAYS) {

			  						$('.h-e-apertura ul').empty();
			  						if (this.OPENDAYS) {
			  							$('.h-e-apertura ul').append('<li class="abiertos"><b>Abierto</b>: '+this.OPENDAYS);
			  						}
			  						if (this.CLOSEDDAYS) {		                    	
			  							$('.h-e-apertura ul').append('<li class="cerrados"><b>Cerrado</b>: '+this.CLOSEDDAYS);
			  						}
			  						if (this.SPECIALDAYS) {		                    	
			  							$('.h-e-apertura ul').append('<li class="specials"><b>Horarios especiales</b>: '+this.SPECIALDAYS);
			  						}

			  					}
			  				}
			  			});
			  		}
			  	});
			  });

			},
			countStores: function() {
				var storesTotal;
// 
			if (customMMES.mmDomain.query.IsShop == false) {
				if (customMMES.mmDomain.query.IsPROD == true) {
					var url = customMMES.mmDomain.query.mm_protocol + '//' + 'www.mediamarkt.es/static/json/stores.es.json';
				} else if (customMMES.mmDomain.query.IsPERF == true) {
					var url = customMMES.mmDomain.query.mm_protocol + '//' + 'perf.mediamarkt.es/static/json/stores.es.json';
				}
			};
// 
				$.getJSON(url, function(data){
					var stores = data.stores;
					var storesTotal = stores.length;
					$('#all-markets .container h1').text(storesTotal + ' tiendas en España');
				});

			}


		};

		/* @end		- Funciones referentes a tiendas */
		/********************************************************************************************************************/
		/* @start 	- Funciones relativas a Cookies */

		/*! jQuery Cookies - undefined */
		(function(e){if(typeof define==="function"&&define.amd){define(["jquery"],e)}else if(typeof exports==="object"){e(require("jquery"))}else{e(jQuery)}})(function(e){function n(e){return u.raw?e:encodeURIComponent(e)}function r(e){return u.raw?e:decodeURIComponent(e)}function i(e){return n(u.json?JSON.stringify(e):String(e))}function s(e){if(e.indexOf('"')===0){e=e.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\")}try{e=decodeURIComponent(e.replace(t," "));return u.json?JSON.parse(e):e}catch(n){}}function o(t,n){var r=u.raw?t:s(t);return e.isFunction(n)?n(r):r}var t=/\+/g;var u=e.cookie=function(t,s,a){if(s!==undefined&&!e.isFunction(s)){a=e.extend({},u.defaults,a);if(typeof a.expires==="number"){var f=a.expires,l=a.expires=new Date;l.setTime(+l+f*864e5)}return document.cookie=[n(t),"=",i(s),a.expires?"; expires="+a.expires.toUTCString():"",a.path?"; path="+a.path:"",a.domain?"; domain="+a.domain:"",a.secure?"; secure":""].join("")}var c=t?undefined:{};var h=document.cookie?document.cookie.split("; "):[];for(var p=0,d=h.length;p<d;p++){var v=h[p].split("=");var m=r(v.shift());var g=v.join("=");if(t&&t===m){c=o(g,s);break}if(!t&&(g=o(g))!==undefined){c[m]=g}}return c};u.defaults={};e.removeCookie=function(t,n){if(e.cookie(t)===undefined){return false}e.cookie(t,"",e.extend({},n,{expires:-1}));return!e.cookie(t)}})




		customMMES.cookies = {

			law: function(){
				$.cookie('MC_COOKIES_LAW');

				if ($.cookie('MC_COOKIES_LAW') == null) {
					$(document).ready(function() {
						$("#mm-content-cookies").delay(500).show();
						$("#close-cookies-law").click(function() {
							$("#mm-content-cookies").hide();
						});
						$.cookie('MC_COOKIES_LAW', '1', {
							expires: 15
						});
					});	
				};		
			},

			testAB: function() {
				var origen_Cookie = "cookie";

		if ($.cookie('MC_COOKIES_AB') == null) { //Leer Cookie
			var origen_Cookie = "random";
			//Crear cookie vacía
			$.cookie('MC_COOKIES_AB', '');

			$(document).ready(function() {
				//$("#content-top-navi .first").removeClass("open");
			//Generar Random
			var classes 		= ['1','0']; 
			var randomnumber 	= Math.floor(Math.random()*classes.length);
			//Asignamos valor en cookie
			$.cookie('MC_COOKIES_AB', randomnumber, {
				expires: 15
			});
		});	

		} 

		$(document).ready(function() {
				//Plegar/Desplegar left-Navi en función del valor del test
				if ($.cookie('MC_COOKIES_AB') == '1') {
					//leftnavi desplegado

					$("#content-top-navi .first").removeClass('open');
					$('#mm-body-homepage, #mm-bottom-column').css('padding', '0');

					$('#mm-bottom-info .container, #mm-footer .content-footer, #mm-custom-doormat .container-doormat').removeClass('open-left-navi');
					$('.bottom-info .container, #doormat .container-doormat, #content-description, .content-footer').css('padding', '10px 0');

					$("#nav-bar, #left-nav").on("mouseenter", function() {
						$("#left-nav").show();
					}).on("mouseleave", function() {
						$("#left-nav").hide();       
						ga('send', 'event', 'Test-HOME', 'B-MenuOculto', origen_Cookie, 1, true);
					});	
				} else{
					ga('send', 'event', 'Test-HOME', 'A-MenuDesplegado', origen_Cookie, 1, true);
				}


			});

/*


				    var cab 			= $( "body" ).addClass( "test-ab" );
				    var sab 			= $( "body" ).removeClass( "test-ab" );
				    var ab         		= [cab,sab];                
				    var rand 			= Math.floor(Math.random()*ab.length);           
				    $( "body" ).addClass(ab[rand]);




				    var padXL 				= "0 0 0 215";
				    var padSmal 			= "0";
				    var padd         		= [padXL,padSmal];                
				    var rand 				= Math.floor(Math.random()*padd.length);           
				    $('#mm-body-homepage, #mm-bottom-column, #landingcrumbs, .open-left-navi').css("padding", padd[rand]);



		var nav 				= $( "#content-nav" );
		var withoutNav 			= nav.removeClass( "open" );
		var withNav 			= nav.addClass( "open" );
		var arrayNav 			= [padXL,padSmal]; 
		var randNav 			= Math.floor(Math.random()*arrayNav.length);
		$( "#content-nav" ).padd[rand];



		    $( "#content-nav" ).each( function (){
		    	var ClassRemove = ["open"];
		        var rng = Math.round(Math.random()*1);
		        $(this).removeClass( ClassRemove[rng]);
		    });	
*/




/*

		if (customMMES.mmDate.collate(TestAB_FechaInicio, TestAB_FechaFin) == true) {
			
		}
		*/
		
	},

	submenuMyaccount: function() {

		if ($.cookie('MC_USERTYPE') == 'R') {
			$('.top-nav .submenu.mi-cuenta').removeClass("unactive");
			$('.side-nav .active ul li:nth-child(2)').after('<li class=""><a href="//www.mediamarkt.es/webapp/wcs/stores/servlet/MultiChannelLogoff" class="">Cerrar sesión</a></li>');
		}
	},
	deleteCookie: function(){
		if ($.cookie('ignoreUA') == 'true') {
			var d = new Date();
			$.cookie('ignoreUA', null, { expires: d, path:'/', domain:'.mediamarkt.es' });

		}		

	},
	clickDeleteCookie: function(){
		$("#returnMobile").click(function () {
			customMMES.cookies.deleteCookie();
		});
	}
};

/* @end		- Funciones relativas a Cookies */
/********************************************************************************************************************/
/* @start 	- Función de llamada a script WWW optimizely*/

customMMES.cdnoptimizely = {
	insertopt: function(){
		var head= document.getElementsByTagName('head')[0];
		var script= document.createElement('script');
		script.type= 'text/javascript';
		script.src= '//cdn.optimizely.com/js/3696396223.js';
		head.appendChild(script);
	}
}

/* @end		- Función de llamada a script WWW optimizely*/
/********************************************************************************************************************/

/********************************************************************************************************************/
/* @start 	- Funciones relativas al buscador DooFinder */









	var chekOutURL = window.location.href.indexOf('/ecommerce/checkout/');

	if (chekOutURL == -1) {
		var doofinder_script = '//d3chj0zb5zcn0g.cloudfront.net/media/js/doofinder-4.latest.min.js';

		(function(d,t) {
			var f = d.createElement(t), s = d.getElementsByTagName(t)[0];
			f.async = 1;
			f.src = ('https:' == location.protocol ? 'https:' : 'http:') + doofinder_script;
			s.parentNode.insertBefore(f,s)
		}(document,'script'));

		if (!doofinder){
			var doofinder = {};
		}

		doofinder.options = {
			"lang": "es",
			"dtop": 15,
			"dleft": -1,
			"captureLength": 2,	 
			"results": {
				"defaultTemplate": "GridView",
				"width": 515,
				"maxHeight": 775,
				"imagesMaxWidth": 60,
				"imagesMaxHeight": 60
			},
			"header": {},
			"facets": {
				"attached": "left",
				"width": 280
			},
			"queryInput": "#mm-es-search",
			"hashid": "376fd770479c4a00b10f5b80887878e0"
		}
	}



/*

function clearSearchForm(){
	var searchPath = '';
	if ((typeof site != 'undefined') && (site.length != undefined)){
	  searchPath = '//' + site;
	}else if (typeof domain != 'undefined'){
	  switch(domain) {
		case 'mcstest':
			searchPath = '//tiendas-test.mediamarkt.es';
			break;
		case 'perf':
			searchPath = '//tiendas-pre.mediamarkt.es';
			break;
		case 'www':
			searchPath = '//tiendas.mediamarkt.es';
			break;
		default:
			searchPath = '//tiendas.mediamarkt.es';
	  } 
	}else{
	  searchPath = '//tiendas.mediamarkt.es';
	}
	$('#site-search').attr('data-search-url',searchPath + '/buscar');
	$('#site-search').attr('action',searchPath + '/buscar');		
	$('#search-category-select').remove();		
	$("input[name=storeId]").remove();
	$("input[name=langId]").remove();
	$("input[name=searchProfile]").remove();
	$("input[name=searchParams]").remove();									
}


$(function() {
	if ($('#site-search').val()!=undefined){
		clearSearchForm();
	}
});

*/

/* @end		- Funciones relativas al buscador DooFinder */
/********************************************************************************************************************/
/* @start 	- Funciones relativas al widget de Newsletter */

customMMES.newsletter = {

	validation: function(formID, storeID) {

		// Check Store parameter and store Cookie, if these are undefined, sets Tienda Online value as default

		var myStore = storeID;
		if (myStore == "" || myStore == null) {
			myStore = $.cookie("MC_STORE_ID");
		}

		if (myStore == "" || myStore == null) {
			myStore = "121956";
		}

		// Performs the data sending:

		// if enter key is pressed

		$("#" + formID + " input[type='email']").keypress(function(event) {
			if (event.which == 13) {		        
				event.preventDefault();
				var myEmail = $("#" + formID + " input[type='email']").val();
				if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(myEmail) == true) {
					window.parent.location.href = "https://specials.mediamarkt.es/newsletter/registro?email=" + myEmail + "&storeValue=" + myStore;
				}
				else {
					alert("Por favor, introduce un correo electrónico válido.");
				};
			}
		});

		// if submit button is clicked

		$("#" + formID + " button[type='button'], #" + formID + " input[type='button']").click(function() {
			var myEmail = $("#" + formID + " input[type='email']").val();
			if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(myEmail) == true) {
				window.parent.location.href = "https://specials.mediamarkt.es/newsletter/registro?email=" + myEmail + "&storeValue=" + myStore;
			}
			else {
				alert("Por favor, introduce un correo electrónico válido.");
			};
		});
	},

	get: function() {
		
		var userStore = customMMES.URLparam("storeValue");
		var userEmail = decodeURIComponent(customMMES.URLparam("email"));

		if (userStore !== null && userStore !== "") {
			$("#newsletterOutlet option[value='" + userStore +"']").attr("selected", "selected");
			var storeName = $("#newsletterOutlet option[value='" + userStore +"']").text();
			$(".select2-result-label:contains('" + storeName + "')").parent().addClass("select2-highlighted");

			/*rellenar combo con tienda seleccionada*/
			if (userStore != "" && userStore != undefined && userStore != null){
				var optionSeleccionado = $('option:selected').text();$('span.select2-chosen').empty().html(optionSeleccionado);
			};
			/*rellenar combo con tienda seleccionada*/			
		}

		if (userEmail !== null && userEmail !== "") {
			$("#newsletterEmail").attr("value", userEmail)
		}
	}
};

/* @end		- Funciones relativas al widget de Newsletter */
/********************************************************************************************************************/
/* @Start 	- Nueva modal */

customMMES.modal = {

	show: function(MaxWidth, MaxHeight, Source, Title, ContentType) {

		/* How to use parameters:
		 *
		 * @param MaxWidth:		[type: integer], [optional], [default: 600]
		 * @param MaxHeight:	[type: integer], [optional], [default: 480]
		 * @param Source:		[type: string],  [required], [type: "object {id}", "url"] // Source type depends on ContentType selected.
		 * @param ContentType:	[type: integer], [required], [values: "video", "image", "content", "inline"]
		 *
		 */

		 var Width;
		 var Height;
		 var title = Title;
		 var header_height = 0;

		// Setting up default values

		MaxWidth == null ? Width = "auto" : Width = MaxWidth;
		MaxHeight == null ? Height = "auto" : Height = MaxHeight;
		Title == null ? title_value = false : title_value = true;

		options = function() {

			// Applying modal title if set

			if (title !== null) {
				
				$("<div class='mmModalTittle '><span>" + title + "</span></div>").appendTo($(".mfp-content > div"));
				$(".mfp-close").addClass("close-inside").appendTo($(".mfp-content"));
				header_height = $(".mmModalTittle").height();
			}
			else {
				$(".modal-header").remove();
				$(".mfp-wrap").addClass("no-header");
				$(".modal-type-content .mfp-content iframe").addClass("no-header-iframe");
				$(".modal-type-video .mfp-content iframe").addClass("no-header-iframe");
				$(".modal-type-image button").appendTo($(".modal-type-image .mfp-container figure"));
			}

			// Applying sizes modal wrapper

			if (ContentType == "ffnn" || ContentType == "video" || ContentType == "folleto") {
				$(".mfp-content").css({
					"max-width": Width + "px",
					"height": (Height + header_height) + "px"
				})
			}
			else {
				$(".mfp-content").css({
					"max-width": Width + "px",
					"max-height": (Height + header_height) + "px"
				})
			}
		}

		// Generate modal window depending on content type

		switch (ContentType) {
			case "video":
			$.magnificPopup.open({
				items: {
					src: Source
				},
				callbacks: {
					open: function() {
						options();
					}
				},
				disableOn: 700,
				type: "iframe",
				mainClass: "modal modal-type-video my-mfp-slide-bottom",
				removalDelay: 160,
				preloader: false,
				fixedContentPos: false
			});
			break;

			case "image":
			$.magnificPopup.open({
				type: 'image',
				items: {
					src: Source
				},
				callbacks: {
					open: function() {
						options();
					}
				},
				closeBtnInside: false,
				closeOnContentClick: false,
				fixedContentPos: true,
				mainClass: 'modal modal-type-image my-mfp-slide-bottom',
				image: {
					verticalFit: true
				}
			});
			break;

			case "content":
			$.magnificPopup.open({
				type: "iframe",
				items: {
					src: Source
				},
				callbacks: {
					open: function() {
						options();
					}
				},
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: "auto",
				closeBtnInside: true,
				preloader: false,
				midClick: false,
				removalDelay: 300,
				mainClass: "modal modal-type-content my-mfp-slide-bottom"
			});
			break;

			case "inline":
			$.magnificPopup.open({
				type: "inline",
				items: {
					src: $("#" + Source)
				},
				callbacks: {
					open: function() {
						options();
					}
				},
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: "auto",
				closeBtnInside: true,
				preloader: false,
				midClick: false,
				removalDelay: 300,
				mainClass: "modal modal-type-inline my-mfp-slide-bottom"
			});
			break;

			case "ajax":
			$.magnificPopup.open({
				type: 'ajax',
				items: {
					src: Source
				},
				callbacks: {
					open: function() {
						options();
					}
				},
				mainClass: "modal modal-type-ajax my-mfp-slide-bottom",
				alignTop: true,
				overflowY: 'scroll'
			});
			break;

			case "ffnn":
			$.magnificPopup.open({
				type: "iframe",
				items: {
					src: Source
				},
				callbacks: {
					open: function() {
						options();
					}
				},
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: "auto",
				closeBtnInside: true,
				preloader: false,
				midClick: false,
				removalDelay: 300,
				mainClass: "modal modal-type-ffnn my-mfp-slide-bottom"
			});
			break;

			case "folleto":
			$.magnificPopup.open({
				type: "iframe",
				items: {
					src: Source
					
				},
				callbacks: {
					open: function() {
						options();
					}
				},
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: "10px",
				closeBtnInside: true,
				preloader: false,
				midClick: false,
				removalDelay: 3000,
				mainClass: "modal modal-type-folleto my-mfp-slide-bottom"
			});
			break;	

			case "intersitial":
			$.magnificPopup.open({
				type: 'ajax',
				items: {
					src: Source
				},
				callbacks: {
					open: function() {
						options();
					}
				},
				mainClass: "modal modal-type-intersitial my-mfp-slide-bottom",
				modal: true,
				closeOnContentClick: false,
				alignTop: false,
				showCloseBtn: false,
				overflowY: 'scroll'
			});
			break;			
		}
	}
};

/* @end 		- Nueva modal */
/********************************************************************************************************************/
/* @start		- Nuevo Intersitial */

customMMES.intersitial = {

	show: function(ancho, alto, urlIntersitial, segundosCierre) {

		if (customMMES.mmDomain.query.IsCMS && customMMES.mmDomain.query.IsHome) {
			if ($.cookie("MM_INTERS") == null) {
				$.cookie("MM_INTERS", "1", {
					path: ".mediamarkt.es", 
					expires: 1
				});
				
				// customMMES.modal.show(ancho, alto, "http://" + domain + ".mediamarkt.es/static/layer/interstitial/" + urlIntersitial, null, "intersitial");
				customMMES.modal.show(995, 770, "http://www.mediamarkt.es/static/layer/folleto-nacional/20160314-index.html", null, "folleto");
				$('.my-mfp-slide-bottom').css('top','0px');
				/*funcion timer*/
				var handle = setInterval(function() {
					myTimer()
				},
				segundosCierre
				);
				function myTimer() {
					$.magnificPopup.close();
				}
				function myStopFunction() {
					clearInterval(handle);
				}

				$("div.mfp-content").click(function() {
					myStopFunction();
				});
			}
		}
	}
};

/* @end 		- Nuevo Intersitial */
/********************************************************************************************************************/
/* @start		- Nueva visor para folletos locales/regionales */

customMMES.flyer = {

	show: function() {

		var dataStore;
		var dataFlyer;
		var n = $('.folleto a').length;
		var m = customMMES.URLparam("order");

		// Case #1: Only one flyer published for a Store

		if (n == 1) {
			dataStore = $("#flyer-button-1 a").attr("data-store");
			dataFlyer = $("#flyer-button-1 a").attr("data-flyer");
			customMMES.modal.show(855, 575, "http://redblue.scene7.com/is-viewers-3.9/flash/genericbrochure.swf?serverUrl=http://redblue.scene7.com/is/image/&contentRoot=http://redblue.scene7.com/skins/&config=redblue//MMFlyer_1&image=redblue/E_MM_" + dataStore + "_" + dataFlyer + "&textEntryTemplate=%252*idx%25-%252*idx%2b1%25&lang=es", null, 'folleto');
		}

		// Case #2: Several flyers published for a Store

		if (n > 1) {
			if (m !== "") {
				if (m <= n) {

					// Sets the flyer based on it's order (defined by the 'order' parameter)

					dataStore = $("#flyer-button-" + m + " a").attr("data-store");
					dataFlyer = $("#flyer-button-" + m + " a").attr("data-flyer");
					customMMES.modal.show(855, 575, "http://redblue.scene7.com/is-viewers-3.9/flash/genericbrochure.swf?serverUrl=http://redblue.scene7.com/is/image/&contentRoot=http://redblue.scene7.com/skins/&config=redblue//MMFlyer_1&image=redblue/E_MM_" + dataStore + "_" + dataFlyer + "&textEntryTemplate=%252*idx%25-%252*idx%2b1%25&lang=es", null, 'folleto');
				}
				else {

					// Prevents errors if m > n, setting the first flyer as default

					dataStore = $("#flyer-button-1 a").attr("data-store");
					dataFlyer = $("#flyer-button-1 a").attr("data-flyer");
					customMMES.modal.show(855, 575, "http://redblue.scene7.com/is-viewers-3.9/flash/genericbrochure.swf?serverUrl=http://redblue.scene7.com/is/image/&contentRoot=http://redblue.scene7.com/skins/&config=redblue//MMFlyer_1&image=redblue/E_MM_" + dataStore + "_" + dataFlyer + "&textEntryTemplate=%252*idx%25-%252*idx%2b1%25&lang=es", null, 'folleto');
				}
			}
			else {

				// Sets the first flyer as default if 'order' parameter is missing

				dataStore = $("#flyer-button-1 a").attr("data-store");
				dataFlyer = $("#flyer-button-1 a").attr("data-flyer");
				customMMES.modal.show(855, 575, "http://redblue.scene7.com/is-viewers-3.9/flash/genericbrochure.swf?serverUrl=http://redblue.scene7.com/is/image/&contentRoot=http://redblue.scene7.com/skins/&config=redblue//MMFlyer_1&image=redblue/E_MM_" + dataStore + "_" + dataFlyer + "&textEntryTemplate=%252*idx%25-%252*idx%2b1%25&lang=es", null, 'folleto');
			}
		}
	}
};

/* @end 		- Nueva visor para folletos locales/regionales */
/********************************************************************************************************************/
/* @start		- Nueva CutOver Layer */

customMMES.cutOverLayer = {

	show: function() {

		if (customMMES.mmDomain.query.IsShop == true) {
			$.when($("<script/>", {
				type: "text/javascript",
				src: "//" + domain +".mediamarkt.es/static/include/jquery.magnific-popup.min.js"
			}).appendTo("head")).done(function() {
				customMMES.cutOverLayer.init();
			});
		}
		else {
			customMMES.cutOverLayer.init();
		}
	},

	init: function() {

		$.magnificPopup.open({
			type: "iframe",
			items: {
				src: "//" + domain + ".mediamarkt.es/static/layer/cutOver/index.html"
			},
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: "hidden",
			closeBtnInside: false,
			preloader: false,
			midClick: false,
			removalDelay: 100,
			modal: true,
			mainClass: "cutover",
			callbacks: {
				open: function() {

					if (customMMES.mmDomain.query.IsShop == true) {
						$("#header").hide();
						$("#containerMain").hide();
						$(".contentspot").hide();
						$("body").addClass("blank");
					}

					if (customMMES.mmDomain.query.IsCMS == true) {
						$("#header").hide();
						$("#content").hide();
						$("#contentspot").hide();
						$("body").addClass("blank");
					}	
				},
				close: function() {

					if (customMMES.mmDomain.query.IsShop == true) {
						$("#header").show();
						$("#containerMain").show();
						$(".contentspot").show();
						$("body").removeClass("blank");
					}

					if (customMMES.mmDomain.query.IsCMS == true) {
						$("#header").show();
						$("#content").show();
						$("#contentspot").show();
						$("body").removeClass("blank");
					}
				}
			}
		});
},

close: function() {
	$.magnificPopup.close();
},

konamiCode: function(){
	$(window).konami({
		code:  [38,38,40,40,37,39,37,39,66,65], /* [Up, Up, Down, Down, Left, Right, Left, Right, B , A] */
		cheat: function() {
			alert("Desactivando CutOver Layer...");
			customMMES.cutOverLayer.close();
		}
	});
}
};

/* @end 		- Nueva CutOver Layer */
/********************************************************************************************************************/
/* @start 		- Funciones relativas a Google Analytics */

customMMES.ga = {

	contentValue: function() {
		var mmURL 	= window.location.protocol + "//" + window.location.host + window.location.pathname;
		var host 	= window.location.host;
		var path 	= window.location.pathname;

		if (host == domain + ".mediamarkt.es") {
			switch (true) {
				
				case path == "/":
				contenido = "home";
				break;
				
				case (customMMES.mmDomain.contain("/mp/") || customMMES.mmDomain.contain("/mediapedia/")) == true:
				p = $(".breadcrumbs li")
				contenido = "";
				for (i = 1; i < p.length; i ++) {
					contenido = contenido + $($(".breadcrumbs li")[1]).text();
					if (i == 1) {
						contenido = contenido.replace(/\s/g,'');
					}
					if (i < (p.length - 1)) {
						contenido = contenido + "/";
					}
				}
				break;
				
				case path == "/webapp/wcs/stores/servlet/MultiChannelMyStore":
				contenido = "tiendas";
				break;
				
				case customMMES.mmDomain.contain("/mcs/marketinfo/") == true:
				p = $(".breadcrumbs li")
				contenido = "tiendas/" + $(".breadcrumbs li:last").text();
				contenido = contenido.replace(/\s/g,'');
				break;
				
				case customMMES.mmDomain.contain("/webapp/wcs/stores/servlet/MultiChannelMAGiftCard") == true:
				p = $(".breadcrumbs li")
				contenido = "usuario/" + $(".breadcrumbs li:last").text();
				break;
				
				case customMMES.mmDomain.contain("/webapp/wcs/stores/servlet/MultiChannelJobs") == true:
				p = $(".breadcrumbs li")
				contenido = "general/" + $(".breadcrumbs li:last").text();
				break;
				
				case customMMES.mmDomain.contain("/es/shop/catalogo-ofertas-folleto-mediamarkt.html") == true:
				contenido = "Folleto/Nacional-" + customMMES.FNPA.folderName;
				break;
				
				case customMMES.mmDomain.contain("/es/shop/catalogo-ofertas-folleto-mediamarkt-canarias.html") == true:
				contenido = "Folleto/Nacional-" + customMMES.FNCA.folderName;
				break;

				case customMMES.mmDomain.contain("/es/shop/") == true:
				contenido = "/es/shop/" + window.location.pathname.split("/")[3].split(".")[0];

				if (customMMES.mmDomain.contain("productos-locos.html"))
					contenido = "microsite/folletoloco";

				if (customMMES.mmDomain.contain("error404.html"))
					contenido = "page_404";
				break;

				case customMMES.mmDomain.contain("/webapp/wcs/stores/servlet/LogonForm") == true:
				contenido = "usuario/login"
				break;

				case customMMES.mmDomain.contain("/webapp/wcs/stores/servlet/MultiChannelMARegister") == true:
				contenido = "usuario/registro";
				break;

				default:
				contenido = contenido == null ? path : contenido;
				break;
			}
			contenido = customMMES.normalize(contenido).replace(/\s/g,'-').toLowerCase();
		}
		else if (host != "tiendas.mediamarkt.es" && host != "www.mediamarkt.es" && host != "mcstest.mediamarkt.es" && host != "tiendas-test.mediamarkt.es" && host != "perf.mediamarkt.es" && host != "tiendas-pre.mediamarkt.es") {
			switch (true) {

				case host == "musica.mediamarkt.es":
				contenido = "wls/musica";
				break;

				case host == "descargajuegos.mediamarkt.es":
				contenido = "wls/descargaJuegos";
				break;

				case host == "revelado.mediamarkt.es":
				contenido = "wls/revelado";
				break;

				case host == "preciosbajos.mediamarkt.es":
				contenido = "preciosbajos";
				break;

				case host == "descargasoftware.mediamarkt.es/":
				contenido = "wls/descargaSoftware"
				
				default:
				contenido = "undefined/" + host;
				break;
			}
		}
		return contenido;
	}
};

/* @end			- Funciones relativas a Google Analytics */
/********************************************************************************************************************/
/* @start 		- Google Analytics snippet */

if (customMMES.mmDomain.query.IsShop == false) {
	if ((window.location.href !== 'http://www.mediamarkt.es/#/ffnn=true') 
		&& (window.location.href !== 'http://www.mediamarkt.es/#/ffnn=peninsula') 
		&& (window.location.href !== 'http://www.mediamarkt.es/#/ffnn=canarias')) {


		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-1325897-8', 'auto', {'legacyCookieDomain': 'mediamarkt.es'});
		ga('send', 'pageview', customMMES.ga.contentValue());

	}
}

/* @end			- Google Analytics snippet */
/********************************************************************************************************************/
/* @start		- Blueknow Carousels */

customMMES.blueknow = {


	identifier: function() {

		var Identifier = window.location.pathname.split("/")[3].split(".")[0];
		return Identifier;
	},

	carousel: function() {

		var rec = new Blueknow.Recommender("BK-228300128806-7");
		rec.setLanguage("es"); 
		rec.setCurrency("EUR");
		rec.setDomainName(".mediamarkt.es");
		rec.item2user(customMMES.blueknow.identifier(), {properties: [ "description" ], recommendations: 12, success: customMMES.blueknow.renderItems, error: customMMES.blueknow.processError});	
	},

	getPropertyValue: function(item, name) {
		var value = "";
		if (item.properties) {
			var props = item.properties.property;
			if (props.name) {
				value = props.name == name ? props.value : "";
			} else {
				for (var i=0; i<props.length; i++) {
					if (props[i].name == name) {
						value = props[i].value;
						break;
					}
				}
			}
		}
		return value;
	},




	renderItems: function(items, source, location) {

		var blueknow_header = '\
		<h2>Quizás te pueda interesar</h2>\
		<div class="mm-separator">\
		<span class="mm-arrow-separator"></span>\
		</div>\
		<div class="first-carousel-container" style="width: 100%;">\
		<div id="owl-BK" class="owl-carousel">';

		var blueknow_footer = '\
		</div>\
		</div>';

		var blueknow_template = '\
		<div class="item">\
		<div class="product-img">\
		<a href="#url#" onclick="javascript: rec.onclick(\'#id#\',\'#source#\',\'#location#\'); _gaq.push([\'_trackEvent\', \'clics\', \'' + customMMES.blueknow.identifier() + '\', \'Carrusel\']);" target="_parent">\
		<img id="productImage_#galleryIndex#_#index#" name="productImage_#galleryIndex#_#index#" src="#image#" style="display: inline-block;">\
		</a>\
		</div>\
		<div class="product-name">\
		<a href="#url#" onclick="javascript: rec.onclick(\'#id#\',\'#source#\',\'#location#\'); _gaq.push([\'_trackEvent\', \'clics\', \'' + customMMES.blueknow.identifier() + '\', \'Carrusel\']);" style="text-decoration: none" target="_parent">\
		<span>#name#</span>\
		<span class="product-description">#description#</span>\
		</a>\
		</div>\
		<div id="s-down">\
		<div class="product-price">\
		<img id="productPrice_#galleryIndex#_#index#" name="productPrice_#galleryIndex#_#index#" src= "//d243u7pon29hni.cloudfront.net/price2.0/slES-MM/sht/dfpng/va#price#/ft0000.-/ys55/by46/bpcl/lode-AT/boxprice.png">\
		</div>\
		</div>\
		</div>';

		var galleryIndex = 0;
		var galleryImages = [];
		var galleryPrices = [];

		if (items.length > 0) {
			galleryImages[galleryIndex] = [];
			galleryPrices[galleryIndex] = [];
			var widget = "";

			//add header

			var header = blueknow_header;
			widget += header ? blueknow_header : "";

			//items iteration

			for (var i = 0; i < items.length; i ++) {
				var item = items[i];

				//get product template and replace item properties

				var template = new String(blueknow_template);

				//no visual properties

				template = template.replace(/#index#/g, i);
				template = template.replace(/#id#/g, item.id);
				template = template.replace(/#source#/g, source);
				template = template.replace(/#location#/g, location);

				//product name

				var pName = item.name.length > 80 ? item.name.substr(0,item.name.substr(0,60).lastIndexOf(" ")) + "..." : item.name;
				template = template.replace(/#name#/g, pName);

				//product URL

				template = template.replace(/#url#/g, item.url);

				// Description

				template = template.replace(/#description#/g, customMMES.blueknow.getPropertyValue(item, "description"));

				//product image

				/*
				galleryImages[galleryIndex][i] = item.image;
				template = template.replace(/#image#/g, item.image);
				*/

				var pImage = item.image.replace("_s.", "_m.");
				galleryImages[galleryIndex][i] = pImage;
				template = template.replace(/#image#/g, pImage);


				//product price

				galleryPrices[galleryIndex][i] = "//d243u7pon29hni.cloudfront.net/price2.0/slES-MM/sht/dfpng/va" + item.price + "/ft0000.-/ys55/by46/bpcl/lode-AT/boxprice.png"
				template = template.replace(/#price#/g, item.price);

				//add item to the widget

				widget += template;
			}

			//add footer

			widget += blueknow_footer;

			//set gallery index

			widget = widget.replace(/#galleryIndex#/g, galleryIndex);

			//add widget to the DOM

			document.getElementById("blueknow-widget").innerHTML = widget;

			//apply carousel

			$("#owl-BK").owlCarousel({
			      items : 3,  // Altas resoluciones
			      itemsDesktop : [1500,3],  // Resolucion entre 1500 y 900
			      itemsDesktopSmall : [900,3],  // Resolucion entre 9000 y 600
			      itemsTablet: [600,3],  // Resolucion entre 600 y mobile
			      itemsMobile : false, 
			      navigationText : ["Prev", "Next"],
			      transitionStyle : true,
			      paginationSpeed : 800,
			      slideSpeed : 800,
			      pagination : false,
			      scrollPerPage: true,
			      lazyLoad : true,
			      lazyFollow : true,
			      lazyEffect : "fade",
			      navigation : true
			  });

		} 
	},

	processError: function(message) {

	},

	load: function() {

		$.when($.getScript("http://static.blueknow.com/bk-r.js"), $.getScript("http://tiendas.mediamarkt.es/js/jQ_slidersResp.min_1.js")).done(function() {
			customMMES.blueknow.carousel();
		});		
	}


};

/* @end			- Blueknow Carousels */
/********************************************************************************************************************/


customMMES.slideBigBanner = {

	view: function() {

		$('#layerslider').layerSlider({

			autoStart: true,
			responsive: true,
			responsiveUnder: false,
			pauseOnHover: true,
			firstSlide: 1,
			thumbnailNavigation:'disabled',
			touchNav: true,
			lazyLoad:true,
			skin:'borderlesslight3d',
			skinsPath: '/static/index/slider/skins/'          

		});

	}
}

/* @end			- Slider Big Banner */
/********************************************************************************************************************/
/* @Start - Top Navigation Fixed  */

customMMES.topNaviFixed = {

	topFixed: function() {

		var num = 138; 
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > num) {
				$('#content-top-navi').addClass('content-top-navi-fixed');
				$("#mm-content-cookies").css({	
					'-webkit-transform': 'translate(45em,0)',
					'-moz-transform': 'translate(45em,0)',
					'-o-transform': 'translate(45em,0)',
					'-ms-transform': 'translate(45em,0)',
					'transform': 'translate(45em,0)',
					'-webkit-transition': '1s ease-in-out',
					'-moz-transition': '1s ease-in-out',
					'-o-transition': '1s ease-in-out',
					'transition': '1s ease-in-out'
				});
			} else {
				$('#content-top-navi').removeClass('content-top-navi-fixed');
			}
		});

	}
}

/* @end	- Top Navigation Fixed */
/********************************************************************************************************************/
/* @Start - Links Footer  */

customMMES.footer = {

	modalLinks: function() {
		$("#link-t-c").click(function(e){
			e.preventDefault();
			customMMES.modal.show(742, 531, customMMES.mmDomain.query.mm_protocol + '//' + customMMES.mmDomain.query.getEnvironment +'.mediamarkt.es/static/layer/terms/condiciones/','Condiciones de contratación','content');
		});

	}
}

/* @end	- Links Footer */
/********************************************************************************************************************/
/* @Start - Inject AJAX destacado Left Navigation  */




customMMES.injectDestacadoLeftNav = {

	top: function() {
		$.ajax({
			url: '//' + customMMES.mmDomain.getTiendaDomain() + '.mediamarkt.es/ecommerce/ajax/home/popOver.html',
			type: 'GET',
			cache: false,
			success: function(responseText) {

				$(".popover").each(function(index) {
					dataIDS = $(this).attr('data-id');

					$('[data-id="' + dataIDS +'"]').appendTo('[data-submenu-id="' + dataIDS + '"] div#inf .popover-content');
				})
			},
		});
	}
};

/* @end	- Inject AJAX header */
/********************************************************************************************************************/
/* @Start - Inject AJAX Bigbanner  */


customMMES.ajaxInject = {

	ajaxLoad: function(idBanner) {

		var bannerElement = $('#' + idBanner);
		$.ajax({
			url: '//' + customMMES.mmDomain.getTiendaDomain() + '.mediamarkt.es/ecommerce/ajax/home/bigBanner-home.html',
			type: 'GET',
			async: false,
			cache: false,
			success: function(responseText) {
				if(responseText.length > 1) {
					bannerElement.html(responseText);
				}

				if((customMMES.mmDomain.query.IsHome == true) || (customMMES.mmDomain.query.IsHomeTiendas == true)) {
					customMMES.slideBigBanner.view();
				}		          	
			},
		});
	},

	setSpan: function() {
		var cnt = 0;
		timer1 = setInterval(function() {
			cnt++;
			try {
				var item = 0;
				if($(".ls-bottom-slidebuttons a").length <= 7){				         
					if($(".ls-bottom-slidebuttons") != null && ($(".ls-bottom-slidebuttons a").length > $(".ls-bottom-slidebuttons a span").length)){
						$(".ls-bottom-slidebuttons a").each(function(item) {
							item ++;
							$($(".ls-bottom-slidebuttons a")[item-1]).attr("id","position_" + item);
							$($(".ls-bottom-slidebuttons a")[item-1]).append('<span></span>');

						});

						var index = 1;
						$(".ls-slide").each(function(index) {
							index ++;
							var name = $(this).attr("name");
							$("#position_" + index + " span").text(name);
						});
						/* set width */

						var numItems = ($(".ls-bottom-slidebuttons a").length);

						switch (numItems) {
							case 2:
							$(".ls-bottom-slidebuttons a").css("width", "50%");
							break;
							case 3:
							$(".ls-bottom-slidebuttons a").css("width", "33.33333333%");
							break;
							case 4:
							$(".ls-bottom-slidebuttons a").css("width", "25%");
							break;
							case 5:
							$(".ls-bottom-slidebuttons a").css("width", "20%");
							break;
							case 6:
							$(".ls-bottom-slidebuttons a").css("width", "16.66666666%");
							break;
							case 7:
							$(".ls-bottom-slidebuttons a").css("width", "14.28571428%");
							break;

						}


						$("#layerslider").addClass("btnsbig");	
					}
				}else{
					$("#layerslider").addClass("btns-small");
				}

	// Tamaño de la fuente cuando hay 7 items en el Big Banner
	if($(".ls-bottom-slidebuttons a").length == 7){
		$("	.btnsbig .ls-bottom-slidebuttons a").css( "font-size", "0.8em" );
	}	

} catch (e) {
	clearTimeout(timer1);
}
if (cnt > 14) {
	clearTimeout(timer1);
}
}, 100);		 			 			 			 

},

		mediatrends: function(options){
				//var $this = elemento;

				defaults = {
					url: '',
					success: function(){},
					error: function(){},
				};

				//var options = $.extend({}, defaults, options);

				if(options.url!=''){
					$.ajax({
						url: options.url,
						dataType: 'JSONP',
						jsonpCallback: 'callback',
					    //jsonp: false,
					    cache:true,
					    type: 'GET',
					    success: function (data) {
					    	if(data.status == "error"){
					    		var e = {};
					    		e.statusText = "Controlador desconocido";
					    		options.error(e);
					    	} else{
					    		options.success(data);
					    	}

					    },
					    error: function(e){
					    	options.error(e);
					    }
					});
				} 
			},

setShopingCar: function() {

	var cnt = 0;
	var estado = 0;
	timer1 = setInterval(function() {
		cnt++;
		try {

			if(estado == 0){
						// Corrections: Inserta el carrito de la compra  en la cabecera
						$('#content-cart i').append($('#mm-wrapper .cart'));

						estado = 1;
					}

				} catch (e) {
					clearTimeout(timer1);
				}
				if (cnt > 14) {
					clearTimeout(timer1);
				}
			}, 100);		
}	 
};

/* @end	- Inject AJAX Bigbanner */
/********************************************************************************************************************/
/* @Start - FUNCIONES DEL TOP NAVI Y LEFT NAVI - INTEGRACION CON SPECIALS  */

customMMES.menu = {

	/**
* The function below toggles the visibility of the sub-menu options
* in Desktop's main menu. It works through a 'mouseover' event.
*/

 slideTogglerOnHover: function(trigger, target) {

	$('[data-target="' + target + '"]').hide();
	$('[data-trigger="' + trigger + '"]').hover(
		function() {
			$('[data-target="' + target + '"]').show();
		},
		function() {
			$('[data-target="' + target + '"]').hide();
		}
	);
},

/**
* The function below gets data from a Trilogi's JSON and mounts the retrieved data
* on each first-level item menu of the Left Nav.
*/

setPopoverMarkup:function() {

	var html;

	html  = '<div class="layerOver">';
	html += 	'<section>';
	html += 		'<div class="mm row">';
	html += 			'<div class="mm col-xs-5 categories">';
	html += 				'<nav>';
	html += 					'<ul>';
	html += 					'</ul>';
	html += 				'</nav>';
	html += 			'</div>';
	html += 			'<div class="mm col-xs-7 main-product">';
	html += 				'<div class="title">';
	html += 					'<header>';
	html += 						'<h3>';
	html += 						'</h3>';
	html += 					'</header>';
	html += 				'</div>';
	html += 				'<div class="figure">';
	html += 					'<a href="">';
	html += 						'<figure>';
	html += 							'<img class="image img-responsive" src="" alt="Imagen del producto" />';
	html += 						'</figure>';
	html += 						'<div class="mm-price md layer-price"></div>';
	html += 					'</a>';
	html += 				'</div>';
	html += 				'<div class="description">';
	html += 					'<p>';
	html += 					'</p>';
	html += 				'</div>';
	html += 			'</div>';
	html += 		'</div>';
	html += 	'</section>';
	html +=	'</div>';

	return html;
},

/**
* The function below toggles visibility of the Left Nav popover,
* when mouseenter/mouseleave event is fired over the parent menu's item.
*/

togglePopoverVisibility:function() {
	
	$('.dropdown-menu li').hover(
		
		function() {

			// The following condition avoids show popover with no subcategories

			if ($(this).find('ul li').length > 0) {
				
				$(this).find('.layerOver').show();
			}
		},

		function() {

			$(this).find('.layerOver').hide();
		}	
	);
},

leftNavPopover:function() {

	$.getJSON('//tiendas.mediamarkt.es/ecommerce/ajax/json/leftNavi.cfm', function(data) {
	            
	    $.each(data, function(key, val) {

	    	// Gets the category identifier, used to match the category in each iteration
	    	// ga('send', 'event', 'menu', 'telefonia movil');
	    	
	    	var ctg		= data[key].EXTERNALID;
	    	var url		= data[key].LINK;
	    	var name	= data[key].NAME;
	    	var category = name;

    	
	    	$item = $('<li data-id="' + ctg + '"><a href="' + url + '" onclick="ga(\'send\', \'event\', \'menu\', \''+ customMMES.normalizeGA(name) +'\', \''+ customMMES.normalizeGA(name) +'\');">' + name + '</a></li>');
	    	$item.appendTo($('#dropdown-menu'));

	    	// Checks if the category has a 'highlight' product, and then loops the needed values


	    	if (typeof data[key]['PRODUCT_TOP'] !== 'undefined') {

	    		$item.addClass('popover-entry');
	    		$item.append(customMMES.menu.setPopoverMarkup());
		    	
	    		var name  = data[key]['PRODUCT_TOP']['NAME'];
	    		var image = data[key]['PRODUCT_TOP']['PHOTO'];
							image = image.replace('http:', ''); // Avoids 'http' on image sources, in a https mode
	    		//var price = data[key]['PRODUCT_TOP']['PRICESRC'];
	    		var price = data[key]['PRODUCT_TOP']['PRICE'];
	    				//price = price.replace('http:', ''); // Avoids 'http' on price sources, in a https mode
	    		var desc  = data[key]['PRODUCT_TOP']['SHORTDESCRIPTION'];
	    		var link  = data[key]['PRODUCT_TOP']['LINK'];

	 

	    		// This is the jQuery identificator of the categories, for each iteration

		    	var $popover = $('[data-id="' + ctg + '"] .layerOver');   

		    	// The following appends the retrieved data from the JSON to the corresponding popover's wrapper

		    	$popover.find('.title h3').append(name);
		    	$popover.find('.image').attr('src', image);
		    	$popover.find('.mm-price').text(price);
		    	//$popover.find('.price img').attr('src', price);
		    	$popover.find('.description p').append(desc);
		    	$popover.find('.figure a').attr('href', link);

		    	// Checks if the category has a 'subcategories', and then loops the needed values
	    		
	    		if (typeof data[key]['SUBCATEGORIES'] !== 'undefined') {

	    			var subCategories = [],
	    					maxNumOfItems = 15,
	    					n = 1;

	    			// Checks the height of the windows, and reduces the maximum number of items if needed

	    			if ($(window).height() <= 750) {
						
							maxNumOfItems = 13;
						}

						// Stores the subcategories from the JSON, so it could be iterated in the loop below

    				subCategories = data[key]['SUBCATEGORIES'];

    				// Loops the 'subCategories' Object, getting the subcategory name and url (limited to the above number of items)
		    		
		    		$.each(subCategories, function(key, val) {

		    			if (n < maxNumOfItems) {

			    			var name = subCategories[key]['NAME'],
			    			  	href = subCategories[key]['HREF'],
			    					li   = $('<li><a href="' + href + '" onclick="ga(\'send\', \'event\', \'menu\', \''+ customMMES.normalizeGA(category) +'\', \''+ customMMES.normalizeGA(name) +'\');">' + name + '</a></li>');

			    			//Add GA in the images popover
		    				$popover.find('.figure a').attr('onclick', "ga(\'send\', \'event\', \'menu\', \'"+ customMMES.normalizeGA(category) +"\',\'imagen de producto\');");
			    			// Appends the retrieved data to the <ul> wrapper
			    			$popover.find('ul').append(li);
		    			}
		    		
		    			n ++;
		    		});

		    		// Gets the url of the parent <li> and appends a 'More' link at bottom of the subcategories' list, 
		    		// if the maximum number of items is reached

		    		if (n >= maxNumOfItems) {

		    			var moreLink = 	$('[data-id="' + ctg + '"] a').attr('href'),
		    				$moreItem = $('<li><a href="' + moreLink + '">Más...</a></li>');

		    			$popover.find('ul').append($moreItem);
		    		}
		   		}
	    	}
	    });

	}).done(function() {
		customMMES.menu.togglePopoverVisibility();
		$('.layerOver .mm-price').preciosMM();
	});
}




}










/* @end	- FUNCIONES DEL TOP NAVI Y LEFT NAVI - INTEGRACION CON SPECIALS  */
/********************************************************************************************************************/
/* @Start - Limit Character  */

customMMES.limitCharater = {

	limit: function() {
		var cnt = 0;
		var estado = 0;
		timer1 = setInterval(function() {
			cnt++;
			try {

			     	if(estado == 0){
				     	$(".popover-title").limitChar(65, true);
						$(".prod-description").limitChar(120, true);
						$(".product-description").limitChar(100, true);
						$(".product-name a span:first-child").limitChar(66, true);
						$(".banner-block-content p").limitChar(110, false);
						estado = 1;
			     	}
					         
			     } catch (e) {
			         clearTimeout(timer1);
			     }
			     if (cnt > 14) {
			         clearTimeout(timer1);
			     }
			 }, 100);		 			 			 			 
    }
}

/* @end	- Limit Character */
/********************************************************************************************************************/
/* @Start - Text torator  */

customMMES.textRotator = {

	runSmall: function() {
		var divs = $('.text-rotator');
		function animateText() {
			var current = $('.current-text');
			var currentIndex = divs.index(current),
			nextIndex = currentIndex + 1;

			if (nextIndex >= divs.length) {
				nextIndex = 0;
			}

			var next = divs.eq(nextIndex);

			next.stop().fadeIn(500, function() {
				$(this).addClass('current-text');
			});

			current.stop().fadeOut(500, function() {
				$(this).removeClass('current-text');
				setTimeout(animateText, 10000);
			});
		}

		animateText(); 
	},			 			

	runPar: function() {

		var divs = $('.text-rotator-par');
		function animateTextPar() {
			var current = $('.current-text-par');
			var currentIndex = divs.index(current),
			nextIndex = currentIndex + 1;

			if (nextIndex >= divs.length) {
				nextIndex = 0;
			}

			var next = divs.eq(nextIndex);

			next.stop().fadeIn(500, function() {
				$(this).addClass('current-text-par');
			});

			current.stop().fadeOut(500, function() {
				$(this).removeClass('current-text-par');
				setTimeout(animateTextPar, 10000);
			});
		}

		animateTextPar(); 

	},		
	
	runNon: function() {	
		
		var divs = $('.text-rotator-non');
		function animateTextNon() {
			var current = $('.current-text-non');
			var currentIndex = divs.index(current),
			nextIndex = currentIndex + 1;

			if (nextIndex >= divs.length) {
				nextIndex = 0;
			}

			var next = divs.eq(nextIndex);

			next.stop().fadeIn(500, function() {
				$(this).addClass('current-text-non');
			});

			current.stop().fadeOut(500, function() {
				$(this).removeClass('current-text-non');
				setTimeout(animateTextNon, 10000);
			});
		}

		animateTextNon(); 

	}
}

/* @end	- Text rotator*/
/********************************************************************************************************************/
/* @Start - Text torator  */

customMMES.scrollPosition = {

	scrollUp: function() {		

		$(window).scroll(function() {
			if ($(this).scrollTop() > 100) {
				$('#scroll-top').fadeIn();
			}
			else {
				$('#scroll-top').fadeOut();
			}
		});

		//Click event to scroll to top

		$('#scroll-top').click(function() {
			$('html, body').animate({scrollTop : 0}, 800);
			return false;
		});
	}
}


/* @end	- Text rotator*/
/********************************************************************************************************************/
/* @Start - interval Entry Point */
/*
customMMES.entryPoint = {

			setRandom: function() {
			 var cont = 0;
			 $("#entryPoint-item2").hide();
			 timerEntry = setInterval(function() {
			     try {
					if(cont == 0){
						$("#entryPoint-item1").fadeOut();
						$("#entryPoint-item2").fadeIn();
						cont=1;
					}else{
						$("#entryPoint-item2").fadeOut();
						$("#entryPoint-item1").fadeIn();
						cont=0;
					}
         
			     } catch (e) {
			         clearTimeout(timerEntry);
			     }
			 }, 8000);		 			 			 			 
    }
};
*/

/* @end	- interval Entry Point */
/********************************************************************************************************************/
/* @start 		- Left Navigation */

customMMES.leftNavi = {

	control: function() {
		var $menu = $(".dropdown-menu");

		$menu.menuAim({
			activate: activateSubmenu,
			deactivate: deactivateSubmenu
		});

		function activateSubmenu(row) {
			var $row = $(row),
			submenuId = $row.data("submenuId"),
			$submenu = $("#" + submenuId),
			height = $menu.outerHeight(),
			width = $menu.outerWidth();

			$submenu.css({
				display: "block",
				top: -1,
				left: width - 2,
				height: height - 4  
			});

			$row.find("a").addClass("maintainHover");
            //$("#mm-content").addClass('open-left-navi');
        }

        function deactivateSubmenu(row) {
        	var $row = $(row),
        	submenuId = $row.data("submenuId"),
        	$submenu = $("#" + submenuId);

        	$submenu.css("display", "none");
        	$row.find("a").removeClass("maintainHover");
            //$("#mm-content").removeClass('open-left-navi');
        }

        $(".dropdown-menu li").click(function(e) {
        	e.stopPropagation();
        });

        $(document).click(function() {
        	$(".popover").css("display", "none");
        	$("a.maintainHover").removeClass("maintainHover");
        });

        $(".dropdown-menu li").hover(
        	function() {
        		var options = { direction: 'right' };
        		$(this).children(".popover").fadeIn(200);
        	}, 
        	function() {
        		$(this).children(".popover").hide();
        	}
        	);
    },

    reOrderContent: function() {
    	$("#nav-bar").click(function() {
    		//$("#mm-content").toggleClass('open-left-navi');
    		$(".bottom-info .container").toggleClass('open-left-navi');
    		$("#doormat .container-doormat").toggleClass('open-left-navi');
    		$("#mm-footer .content-footer").toggleClass('open-left-navi');
    	});
/*
    		$(document).click(function() {
    		$("#mm-content").removeClass('open-left-navi');
    		$(".bottom-info .container").removeClass('open-left-navi');
    		$("#doormat .container-doormat").removeClass('open-left-navi');
    		$("#mm-footer .content-footer").removeClass('open-left-navi');
    	});    

*/		
},

scrollNav: function() {

			var scrolled=0;
			$('#left-nav').addClass('collapsed');
			$("#down").show();			
		    $("#down").on("click" ,function(){
		                scrolled=scrolled+1000;		        
						$(".dropdown-menu").animate({scrollTop:  scrolled }, 800);
						$("#up").show();
						$("#down").hide();

					});			

			$('#up').click(function() {
						$('.dropdown-menu').animate({scrollTop : 0}, 800);
						$("#down").show();
						$("#up").hide();
						return false;
			});
			$(".layerOver .title h3").limitChar(110, true);

}
}


/* @end			- Left Navigation */
/********************************************************************************************************************/
/* @start 		- Left Navigation */

customMMES.menuTiendas = {

	activenavO: function() {
		$('.menu-markt').click(function(){
			$('.menu-markt').removeClass("active");
			$(this).addClass("active");
		});		
	},

	activenavT: function() {
		var url = window.location.pathname;
		urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); 
		jQuery('#mm-left-navi-market a').each(function(){
			if(urlRegExp.test(this.href.replace(/\/$/,''))){
				jQuery(this).addClass('active');
			}
		});	
	},

	activeTopNavi: function() {

		$('#main-menu .parent> a, #main-menu .current-menu-item> a').each(function(){
			if(customMMES.mmDomain.contain("/es/shop/contacto")){
				$($("#main-menu .parent> a")[1]).addClass("active");
					//console.log("Si");
				}
				if(customMMES.mmDomain.contain("es/shop/ofertas-mediamarkt")){	
					$("#main-menu .current-menu-item> a").addClass("active");
					console.log("Si");
				}
				if(customMMES.mmDomain.contain("/mcs/marketinfo") || customMMES.mmDomain.contain("/webapp/wcs/stores/servlet/")){
					$($("#main-menu .parent> a")[0]).addClass("active");
					//console.log("Si");
				}

			});	

	}	
}

/* @end			- Left Navigation */
/********************************************************************************************************************/
/* @start		- Función de inyección de mega banners */

customMMES.controller = {

		/**
		* @function The function below prints a Mega Banner (A Mega Banner is a banner inserted between the Home's carousels).
		*
		* @param {string} start - A date, in 'dd/mm/aaaa, hh:mm:ss' format, that controls when the Mega Banner will be published.
		* @param {string} end - A date, in 'dd/mm/aaaa, hh:mm:ss' format, that controls when the Mega Banner will become unpublished.
		* @param {string} position - A number (parsed as string) that controls in which mega banner's container the object will be appended to. Allowed values are '1', '2' and '3'.
		* @param {string} filename - The name of the picture which will be shown in front end. Avoid writing the filename's extension, this function will do it for you: '.jpg' is the default extension.
		* 	Notice that the filename must match the same name requested as the fifth parameter in a Google Analytics call:
		*	i.e.: ga('send','event','megaBanner','Home','name-used');
		*	In this case 'name.jpg' will be the filename and also the 'alias' to identify this banner in Google Analytics.
		*	All the files will be located at the following folder: '/static/index/img/mega-banner/'.
		*
		* @param {string} link - A string with the url the Mega Banner will link to. It accepts target parameters within itself, in the following way:
		* 	i.e.: '//tiendas/mediamarkt.es/informatica?target=_blank'
		*	The link used in this example will be '//tiendas/mediamarkt.es/informatica', and the parameter '_blank' will be used as a target attribute.
		*	If no target is given, the link will work as '_self' target does. Of course, '_parent' and '_top' target attributes are allowed too.	
		*/

		megaBanner: function (start, end, position, filename, link) {

			// Checking if there's an active period & if the mega banner exists (through the position parameter)
			
			if (customMMES.mmDate.collate(start, end) && (position > 0 && position <= 7)) {

				// Checking if the current URL is the home of 'www' or 'tiendas' websites

				if (customMMES.mmDomain.query.IsHome && (customMMES.mmDomain.query.IsShop || customMMES.mmDomain.query.IsCMS)) {

					// Checking if there's an url paramenter, noticing the link target attribute, in the last function's parameter

					// if (link.indexOf('?') > -1) {
					// 	var href	= link.split('?')[0],
					// 		target	= link.split('target=')[1];
					// }
					// else {
					// 	var href	= link.split('?')[0],
					// 		target	= '_self';
					// }

					// Creating a new object, which contains the MegaBanner's markup and append to it's container

					var $item 		= $('<div>', {'id': 'mega-banner-item-' + position, 'class': 'mega-banner-items'}),
					$markup 	= $.parseHTML(
										// '<a href="' + href + '" target="' + target + '" onclick="javascript: ga(\'send\', \'event\', \'megaBanner\', \'Home\',\'' + filename + '\');">' +
										'<a href="' + link + '" onclick="javascript: ga(\'send\', \'event\', \'megaBanner\', \'Home\',\'' + filename + '\');">' +
										'<figure>' +
										'<img src="//d243u7pon29hni.cloudfront.net/static/index/img/mega-banner/' + filename + '.jpg" alt="' + filename + '.jpg" />' +
										'</figure>' + 
										'</a>'
										);
					var $content 	= $item.append($markup),
					$container 	= $('#megabanner-' + position);
					
					// Avoiding duplicates in 'tiendas' checking if container is empty, before injecting anything on it

					if ($container.find('*').length == 0) {
						$content.appendTo($container);
					}
				}
			}
		},

		/**
		* @function The function below prints a Mega Banner (A Mega Banner is a banner inserted between the Home's carousels).
		*
		* @param {string} start - A date, in 'dd/mm/aaaa, hh:mm:ss' format, that controls when the Mega Banner will be published.
		* @param {string} end - A date, in 'dd/mm/aaaa, hh:mm:ss' format, that controls when the Mega Banner will become unpublished.
		* @param {string} position - A number (parsed as string) that controls in which mega banner's container the object will be appended to. Allowed values are '1', '2' and '3'.
		* @param {string} filename - The name of the picture which will be shown in front end. Avoid writing the filename's extension, this function will do it for you: '.jpg' is the default extension.
		* 	Notice that the filename must match the same name requested as the fifth parameter in a Google Analytics call:
		*	i.e.: ga('send','event','megaBanner','Home','name-used');
		*	In this case 'name.jpg' will be the filename and also the 'alias' to identify this banner in Google Analytics.
		*	All the files will be located at the following folder: '/static/index/img/mega-banner/'.
		*
		* @param {string} shape1 & shape2 - You can choice between 3 shapes : RECT, POLY, CIRCLE: 'RECT' is the default shape.
		* @param {string} coords1 & coords2 - A numbers (parsed as strings) You must to write four coordinates like.... ('506,1,995,200')
		* @param {string} link1 & link2 - A string with the url the Mega Banner will link to. It accepts target parameters within itself, in the following way:
		* 	i.e.: '//tiendas/mediamarkt.es/informatica?target=_blank'
		*	The link used in this example will be '//tiendas/mediamarkt.es/informatica', and the parameter '_blank' will be used as a target attribute.
		*	If no target is given, the link will work as '_self' target does. Of course, '_parent' and '_top' target attributes are allowed too.	
		* @param {string} ga1 & ga2 - The name of the campaign that Marketing tell us for both Google Analytic code. 
		*
		* customMMES.controller.megaBannerMapeado('21/11/2015, 00:00:00', '29/11/2016, 20:00:00', '1', 'amigo-invisible-barba', 'RECT', 'RECT', '0,0,505,200', '506,1,995,200', 'http://specials.mediamarkt.es/calendario-adviento-ofertas-navidad','http://specials.mediamarkt.es/sorteo-regalos-amigo-invisible-mediamarkt', 'calendario-adviento', 'amigo-invisible');
		* @Example: 
		*/

		megaBannerMapeado: function (start, end, position, filename, shape1, shape2, coords1, coords2, link1, link2, ga1, ga2) {


			if (shape1 == null || shape1 == undefined || shape1 == "") {
				shape1 = "RECT";
			};

			if (shape2 == null || shape2 == undefined || shape2 == "") {
				shape2 = "RECT";
			};
			// Checking if there's an active period & if the mega banner exists (through the position parameter)
			
			if (customMMES.mmDate.collate(start, end) && (position > 0 && position <= 7)) {

				// Checking if the current URL is the home of 'www' or 'tiendas' websites

				if (customMMES.mmDomain.query.IsHome && (customMMES.mmDomain.query.IsShop || customMMES.mmDomain.query.IsCMS)) {

					// Checking if there's an url paramenter, noticing the link target attribute, in the last function's parameter

					// if (link.indexOf('?') > -1) {
					// 	var href	= link.split('?')[0],
					// 		target	= link.split('target=')[1];
					// }
					// else {
					// 	var href	= link.split('?')[0],
					// 		target	= '_self';
					// }

					// Creating a new object, which contains the MegaBanner's markup and append to it's container

					var $item 		= $('<div>', {'id': 'mega-banner-item-' + position, 'class': 'mega-banner-items'}),
					$markup 	= $.parseHTML(
										// '<a href="' + href + '" target="' + target + '" onclick="javascript: ga(\'send\', \'event\', \'megaBanner\', \'Home\',\'' + filename + '\');">' +
										// '<a href="' + link + '" onclick="javascript: ga(\'send\', \'event\', \'megaBanner\', \'Home\',\'' + filename + '\');">' 
										'<figure>' +
										'<img src="//d243u7pon29hni.cloudfront.net/static/index/img/mega-banner/' + filename + '.jpg" alt="' + filename + '.jpg" usemap="#map-megabanner-'+ position+'"/>' +
										'</figure>' + 
										// '</a>'+
										'<map name="map-megabanner-'+position+'" id="map-megabanner-'+position+'">'+
										'<area alt="'+filename+'" title="'+filename+'" shape="'+shape1+'" href="'+link1+'" coords="'+coords1+'" target="_self" onclick="javascript:ga(\'send\', \'event\', \'megaBanner\', \'Home\',\'' + ga1 + '\');" style="outline:none;"></area>'+
										'<area alt="'+filename+'" title="'+filename+'" shape="'+shape2+'" href="'+link2+'" coords="'+coords2+'" target="_self" onclick="javascript:ga(\'send\', \'event\', \'megaBanner\', \'Home\',\'' + ga2 + '\');" style="outline:none;"></area>'+
										'</map>'
										);
var $content 	= $item.append($markup),
$container 	= $('#megabanner-' + position);

					// Avoiding duplicates in 'tiendas' checking if container is empty, before injecting anything on it

					if ($container.find('*').length == 0) {
						$content.appendTo($container);
					}
				}
			}
		},

		campaigns: function() {

			// Controls several element appareance on 'Folleto & Ofertas Especiales'
			
			if (customMMES.mmDomain.contain('/es/shop/ofertas-mediamarkt.html')) {

				// MNS Pre-Campaign

				if (customMMES.mmDate.collate(customMMES.mmDate.MNS.inidatestring, customMMES.mmDate.MNS.predatestring) == true) {
					$('#mm-mns .button span').text("Más info");
					$('#mm-mns #countdown-mns').hide();
				}

				// Active MNS Campaign

				if (customMMES.mmDate.collate(customMMES.mmDate.MNS.predatestring, customMMES.mmDate.MNS.enddatestring) == true) {
					$('#mm-mns .button span').text("Ver súper ofertas");
					$('#mm-mns #countdown-mns').show();
					customMMES.countDown.loadNewCounter('mns', null, 'HourlyCounter', 'std', 'Estas ofertas caducan en:', 'countdown-mns'); 
				}


				// OF1 & OF2 Countdowns on landing: 'Folleto & Ofertas Especiales'

				customMMES.countDown.loadNewCounter('ddp', null, 'DailyCounter', 'std', 'Estas ofertas caducan en:', 'countdown-ddp'); 
				customMMES.countDown.loadNewCounter('of2', null, 'DailyCounter', 'std', 'Estas ofertas caducan en:', 'countdown-of2');
				//customMMES.countDown.loadNewCounter('xhd', null, 'DailyCounter', 'std', 'Estas ofertas caducan en:', 'countdown-xhd');

				//var DateEndXHD = customMMES.mmDate.formatDate(XHD_FechaFin);
				// especial Precios Rojos con variable XHD	Para esconder los botones
				/*if (DateNow <= DateEndXHD) //
				{
				$('#countdown-of1').empty();	
				customMMES.countDown.loadNewCounter (null, XHD_FechaFin, 'HourlyCounter','std','Estas ofertas caducan en:', 'countdown-of1'); 
				$('#mm-of1 .mm-info-flyer.mm-nac').empty();
				$('#mm-of1 h2').empty();
				$('#mm-of1 h2').text('Precios Rojos');
				$('#mm-of1 .mm-info-flyer.mm-nac').text('Anticipo de ofertas online hasta el 06/01 a las 21:00h');
				$("#mm-of1 .mm-button-wrapper .btn-ofertas-es-gray.mm-left").css('display','none');
			}*/

			var DateActXHD = customMMES.mmDate.formatDate(XHD_FechaActCamp);

				// XHD Pre-Campaign
				if (DateNow < DateActXHD) 
				{
					$('#mm-xhd h2').empty();
					$('#mm-xhd h2').text('Galaxy Happy Night');
					$('#mm-xhd p').empty();
					$('#mm-xhd p').text('Súper ofertas sólo hoy de 22:00h a 10:00h. ¡Aprovecha el Renove Galaxy!');
					$('#mm-xhd .button span').text("Más info");
					//$('#mm-xhd #countdown-mns').hide();
				}

				// Active XHD Campaign

				if (DateNow >= DateActXHD)  {
					$('#mm-xhd h2').empty();
					$('#mm-xhd h2').text('Galaxy Happy Night');
					$('#mm-xhd p').empty();
					$('#mm-xhd p').text('Súper ofertas sólo hoy de 22:00h a 10:00h. ¡Aprovecha el Renove Galaxy!');
					$('#mm-xhd .button span').text("Ver súper ofertas");
					$('#mm-xhd #countdown-mns').show();
					//customMMES.countDown.loadNewCounter('xhd', null, 'HourlyCounter', 'std', 'Estas ofertas caducan en:', 'countdown-mns'); 
				}

			}
		}
	};

	/* @end			- Función de inyección de mega banners */
	/********************************************************************************************************************/
	/* @start		- Función jsPrices para Media Markt */

	jQuery.fn.extend({
		preciosMM: function() {
		//nuevas funciones
		//nuevas funciones
		// valorPrecio = $('.mm-price').text();
		// var numeroInput = valorPrecio.replace(".",",");
		// var enteroInput = numeroInput.split(",")[0];
		// var decimalInput = numeroInput.split(",")[1];
		// $('.mm-price').text(numeroInput);

		

		//nuevas funciones
		//nuevas funciones
		$('.mm-price').contents().filter(function() {
			return this.nodeType == 3
		}).each(function(){
   // var serialNumber = this.textContent; //numero real con punto o coma
    //this.textContent = this.textContent.replace(',','.'); //se pasa con punto ya que los numeros se separan los decimales con puntos
    //var sindecimales = this.textContent; //igualamos variables

	//var numFinal = dosDecimales.substring(0,2).replace('.',','); //redonde + convertir a string + cambiamos el punto por coma, ya que nosotros queremos coma
	//this.textContent = numFinal; //devuelvo el resultado al this para continuar pasandolo	



});
		return this.each(function() {
			valorPrecio = $(this).text();
			var numeroInput = valorPrecio.replace(".",",");
			var enteroInput = numeroInput.split(",")[0];
			var decimalInput = numeroInput.split(",")[1];
			$(this).text(numeroInput);
		// selecciono la segunda clase del div que será la que indique el tamañoo de los precios  -> xxs, xs, sm, md, lg, xl, xxl
		var claseParent = $(this).attr('class').split(' ')[1]; 
		//capturamos el valor del div
		var numeroInput = valorPrecio.replace(".",",");
 		//capturamos del numero anterior la parte entera
 		var enteroInput = numeroInput.split(",")[0];
 		//capturamos del numero anterior la parte decimal
 		var decimalInput = numeroInput.split(",")[1];
 		


 		if (decimalInput == undefined || decimalInput == "0" || decimalInput == "00" || decimalInput == "" ) {
			//pintamos solo la parte entera y le añaadimos el guión "-", ya que los dobles ceros no los pintamos
			//sin decimales, con un cero, o doble cero
			decimalInput="-";
			$(this).text(enteroInput.replace(/^(-?)0+/,'') + decimalInput);
			// console.log(enteroInput + decimalInput);
		} else {
			//sin doble cero

			if (decimalInput.length == 1){
				decimalInput = decimalInput + '0';
			}
			decimalInput = decimalInput.substring(0,2);


			$(this).text(enteroInput.replace(/^(-?)0+/,'') + ',' + decimalInput);
			// console.log(enteroInput + decimalInput);
		}
		$(this).contents().each(function() {
				if(this.nodeType === 1) { // si el  objeto tratado es un elemento ...

					//si se pinta bien el numero no ha de pasar nunca por aki
					// alert('yeahhhh');
				}
				else if(this.nodeType === 3) {
					
					

					$(this).replaceWith($.map(this.nodeValue.split(''), function(c) { 
					//el array que llamamos con map nos devolvera el valor de ese elemento, y lo reemplazarÃ¡ por el contenido actual (replaceWith)
		//bucle			


		switch(c) {

							//declaramos los casos a tratar

							case '.':
							//c es la clase que le añaadiremos al span creado, y c2 el valor en si, 
							//se crean dos variable porque "," no puede ser un nombre de una clase
							c='-comma'; c2 =","; 
							return '<span class="'+ claseParent +' p' + c +'">' + c2 + '</span>';
							break;
							case ',':
							//c es la clase que le añadiremos al span creado, y c2 el valor en si, 
							//se crean dos variable porque "," no puede ser un nombre de una clase
							c='-comma'; c2 =","; 
							return '<span class="'+ claseParent +' p' + c +'">' + c2 + '</span>';
							break;

							case '-':
							//c es la clase que le añadiremos al span creado, y c2 el valor en si, 
							//se crean dos variable porque "-" no puede ser un nombre de una clase, ya que en este caso es para cuando se elimina el doble "00"
							c='-nocomma';c3='-';
							return '<span class="'+ claseParent +' p' + c +'">' + c3 + '</span>';
							break;
							default:
							//c es la clase que le añadiremos al span creado, y c2 el valor en si, 
							//se crean dos variable porque "-" no puede ser un nombre de una clase, ya que en este caso es para cuando se elimina el doble "00"
							return '<span class="'+ claseParent +' p' + c +'">' + c + '</span>';
							break;
						}




					}).join('')); //juntamos todos los trozos (span's)
}
});
			//a los 2 siguientes hermanos de la clase .p-comma, les añadimos la clase .small, ya que son decimales
			$('.p-comma').next().addClass('small').next().addClass('small'); 
		});

}
});
	//ejecutamos la funcion llamar precios



	/* @end			- Función jsPrices para Media Markt */
	/********************************************************************************************************************/
	/* @start 		- Llamadas en el 'Domready' */

	$(function() {

	// llamada a laas funciones que controlan el flyout de "como podemos ayudarte" y "mi cuenta"
	customMMES.menu.slideTogglerOnHover('help', 'help');
	customMMES.menu.slideTogglerOnHover('my-account', 'my-account');


	
	
	if (customMMES.mmDomain.query.IsCMS == true) {
		// Si es CMS insertar la llamada al script bluecart
		$('#mm-footer').append('<script src="https://www.mediamarkt.es/static/include/bluecart.js"></script>');
		// Llamada al script WWW Optimizely
		customMMES.cdnoptimizely.insertopt();
	}

	// Código Google Tag Manager

	// Se ejecutara en todas las páginas, del dominio de mediamarkt.es; NO para tiendas ni WLS.

	if (customMMES.mmDomain.query.IsCMS == true) {
		$('body').prepend('<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-XNT5" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>'+
			'<script>(function (w, d, s, l, i) {'+
				'w[l] = w[l] || [];'+
				'w[l].push({"gtm.start":'+
					'new Date().getTime(), event: "gtm.js"});'+
		'var f = d.getElementsByTagName(s)[0],'+
		'j = d.createElement(s), dl = l != "dataLayer" ? "&l=" + l : "";'+
		'j.async = true;'+
		'j.src ='+
		'"//www.googletagmanager.com/gtm.js?id=" + i + dl;'+
		'f.parentNode.insertBefore(j, f);'+
		'})(window, document, "script", "dataLayer", "GTM-XNT5");</script>');
	}
	
	/********************************************************************************************************************/
	// Se ejecutará en el dominio de mediamarkt.es; NO para tiendas ni WLS.
	/********************************************************************************************************************/

	if (customMMES.mmDomain.query.IsCMS) {
		
	$("#content-top-navi .first").addClass("open");
	customMMES.menu.togglePopoverVisibility();
	customMMES.menu.setPopoverMarkup();
	customMMES.menu.leftNavPopover();

			// Llamada a la funcion que redimensiona el leftnavi segun la resolucion del dispositivo

			if ($(window).height() <= 750) {
				$(".layerOver .description p").limitChar(75, true);
				$(".layerOver .figure img").css('max-height', '140px');
				customMMES.leftNavi.scrollNav();			
			}else{
				$('#left-nav').removeClass('collapsed');
			}
			$(window).resize(function() {
				customMMES.leftNavi.scrollNav();
			});	



		//$("#content-top-navi .first").addClass("open");
		customMMES.ajaxInject.ajaxLoad("mm-bigBanner");
		

		// Funcion que inserta contenido HTMl en los botones del big banner

		customMMES.ajaxInject.setSpan();

		//customMMES.ajaxInject.ajaxTop('mm-footer');
		

		// Llamada a la funcion active del menu ficha de producto de tiendas

		customMMES.menuTiendas.activenavO();
		customMMES.menuTiendas.activenavT();
		customMMES.menuTiendas.activeTopNavi();

		// Llamada a la funcion que genera el Left Navigation

		customMMES.leftNavi.control();
		customMMES.leftNavi.reOrderContent();

	

		// Llamada a la funcion que fija el top Navi

		customMMES.topNaviFixed.topFixed();

		// Llamada a la funcion que limita el número de caracteres en un párrafo

		customMMES.limitCharater.limit();

		// Llamada a la rotación de entry Point
		// customMMES.entryPoint.setRandom();

		// Llamada a la funcion que rota los parrafos de las USP

		if ($('#mm-content').width() <= 1300) {
			customMMES.textRotator.runSmall();	
		}
		else {
			customMMES.textRotator.runPar();	
			customMMES.textRotator.runNon();	
		}	


		// Oculta el Left navi en "Mi tienda"

		if (customMMES.mmDomain.contain("/servlet/LogonForm")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMARepairStatus")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMAPersonal")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMAMasterData")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMAChangePassword")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMACancelAccount")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMANewsletterRegister")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMAForgetPassword")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMARequestPasswordLinkSuccess")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMAAccountDeleteSuccess")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMAConfirmPassword")
			|| customMMES.mmDomain.contain("/shop/atencion-al-cliente")
			|| customMMES.mmDomain.contain("/shop/trabaja-con-nosotros")
			|| customMMES.mmDomain.contain("/servlet/ReLogonFormView")
			|| customMMES.mmDomain.contain("/shop/tipos-envio")
			|| customMMES.mmDomain.contain("/shop/contacto")
			|| customMMES.mmDomain.contain("/shop/mi-cuenta/cita-previa")
			|| customMMES.mmDomain.contain("/shop/atencion-al-cliente")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMAGiftCard")
			|| customMMES.mmDomain.contain("static/errors/404")
			|| customMMES.mmDomain.contain("/servlet/MultiChannelMARegister")) {
	$("#content-top-navi .first").removeClass("open");
	$("#doormat .container-doormat").removeClass('open-left-navi');
	$("#mm-footer .content-footer").removeClass('open-left-navi');
}

if (customMMES.mmDomain.contain("/atencion-al-cliente/")) {
	$("#mm-bottom-column").css("min-height", "985px");
}

		// Llamada al control de Skins

		customMMES.skin.set();

		// Llamada  ejecuta la modal de enlaces footer

		customMMES.footer.modalLinks();

		// Llamada a la función de validación del widget de Newsletter en la Home

		if (customMMES.mmDomain.query.IsHome == true)  {
			customMMES.newsletter.validation("mm-newsletter-form", "");
		}

		// Llamada a la función de validación del widget de Newsletter en el Doormat de Trilogi

		if (customMMES.mmDomain.query.IsShop == true)  {
			customMMES.newsletter.validation('mm-newsletter-form-footer', null);
		}

		// Llamada a las correcciones en el Accordion de la Home

		customMMES.corrections.accordion();

		//customMMES.corrections.setRedPrices();

		// Llamada a la modal de FFNN si la url contiene los hash "ffnn=peninsula o ffnn=canarias"

		customMMES.visorFN();

		// Llamada la inserción de variables en la landing de Folleto Nacional

		customMMES.FNDateVariables();

		// CutOver Layer

		if (customMMES.mmDate.collate(CUT_FechaInicio, CUT_FechaFin) == true ) {
	        //if (customMMES.mmDomain.query.IsShop || (customMMES.mmDomain.query.IsCMS && customMMES.mmDomain.query.IsHome)) {
	        	if (customMMES.mmDomain.query.IsCMS) {
	        		customMMES.cutOverLayer.show();
	        		customMMES.cutOverLayer.konamiCode();
	        	}
	        };

		// Llamada al plugin jQuery Placeholder

		$("input, textarea").placeholder();

		// Llamada a la función de recepción de datos en landing de registro de newsletter
		
		if (customMMES.mmDomain.contain("NewsletterRegister") == true) {
			customMMES.newsletter.get();
		}

		// Llamada al widget de Blueknow

		if ($("#blueknow-widget").length == 1) {
			customMMES.blueknow.load();
		}

		// Llamada al widget de inscripción a la Newsletter

		if ($("#newsletter-widget").length == 1) {
			customMMES.inject.newsletter("Avísame");
		}


		if ( (customMMES.mmDomain.contain('/es/shop/') == true) && ($("#mm-bottom-column").length == 1)  ){
			customMMES.inject.WidgetNewsLandings("Y...¡no te pierdas todas las novedades!","Avísame");
			customMMES.inject.uaLanding("mm-newsletter-submit");

		}



		// Validación widget suscripción a la newsletter en el footer

		customMMES.newsletter.validation("mm-newsletter-form-footer");

		//customMMES.corrections.onready();

		customMMES.corrections.mediapedia();

		//Llamada funcion counter stores

		customMMES.Stores.countStores();

		// Llamada al visor de folleto en las landings de tienda, si existe folleto local/regional

		if ((customMMES.mmDomain.contain('/marketinfo/') == true) && (customMMES.URLparam("modal") == "flyer") && ($('.folleto a').length > 0)) {
			customMMES.flyer.show();
		}

		// Llamada al snippet para ir al Top 

		customMMES.scrollPosition.scrollUp();

	  	// Remove elements from header

	  	$("#header .cart>.button span, #top-navigation, .right-meta-navigation, .legal-notice, .left-meta-navigation, #infobar .wrapper .close, #infobar .wrapper > ul a:after, #header #logo, #header .search" ).remove();


	  	if (customMMES.mmDomain.query.IsHome) {
	  		addSlide(path);  
	  	}

		// Llamada al Intersitial
		
		if (customMMES.mmDate.collate(INT_FechaInicio, INT_FechaFin) == true) {
			customMMES.intersitial.show(934, 539, "20151126/index.html?v1", 10000);
			$('.mfp-content').css('margin','60px auto');
		}
		if (customMMES.mmDate.collate(INT2_FechaInicio, INT2_FechaFin) == true) {
			customMMES.intersitial.show(934, 539, "20151126-2/index.html?v1", 10000);
			$('.mfp-content').css('margin','160px auto');
		}

		if (customMMES.mmDomain.contain("MultiChannelMARepairStatus")) { 
			customMMES.corrections.repairStatus();
		}	


		// Launches JSON injection in FOE banners	
		customMMES.json.foeBanners();
		
		// Launches counter on FOE landing
		customMMES.controller.campaigns();



	}



	/********************************************************************************************************************/
	//Se ejecutará en el dominio de mediamarkt.es y WLS; NO para el dominio de tiendas.mediamarkt.es .
	/********************************************************************************************************************/

	if (customMMES.mmDomain.query.IsShop == false) {

		//Cabecera, Skin 

	}






		/********************************************************************************************************************/
	// Se ejecutará en la home de mediamarkt.es y de tiendas.mediamarkt.es; NO para la home de WLS.
	/********************************************************************************************************************/

	if ( customMMES.mmDomain.query.IsHome && ((customMMES.mmDomain.query.IsCMS == true) || (customMMES.mmDomain.query.IsHomeTiendas == true))) {

		var randomEntry = new customMMES.mmDate.weeklyCampaign(LbEntry_DiaInicial,LbEntry_DiaFinal,LbEntry_HoraInicial,LbEntry_HoraFinal);
		var fijoEntry 	=  customMMES.mmDate.collate(Fijo_FechaInicio,Fijo_FechaFin);


		if ( randomEntry.IsActive == true ) { // Si virtual shopper esta activo
			customMMES.entryPoints.setLaboral("laboral"); //Ejecutamos el virtual shopper
			customMMES.corrections.randomEntrypoint("03/12/2015, 00:00:00", "03/12/2025, 00:00:00","middle", "rotator"); //Ejecutamos los random de la columna de la mitad
		}else{ //Si virtual shopper NO esta activo
			if (fijoEntry == true ) {//Si tenemos Entry Point fijo en la columna de la Izquierda. //Ejecutamos el FIJO - //Ejecutamos los random de la columna del centro
				customMMES.entryPoints.setFijo("fijo"); 
				customMMES.corrections.randomEntrypoint("03/12/2015, 00:00:00", "03/12/2025, 00:00:00","middle", "rotator"); 
			}else{//Si NO tenemos Fijo a la Izquierda - Ejecutamos los random de la columna de izquierda y centro
				customMMES.corrections.randomEntrypoint("03/12/2015, 00:00:00", "03/12/2025, 00:00:00","left", "rotator-left"); 
				customMMES.corrections.randomEntrypoint("03/12/2015, 00:00:00", "03/12/2025, 00:00:00","middle", "rotator-middle"); 
			customMMES.corrections.timerEntrypoint("01/02/2016, 00:00:00", "14/02/2016, 23:59:00","middle", "timer"); // Rotando en la mitad con temporizador
		};

	};

/*

		if ((customMMES.mmDate.collate(Financiacion_FechaInicio, Financiacion_FechaFin) == true) ) {
			// customMMES.corrections.entrypoinRotator("fijo", "1");		
			
			if( randomEntry.IsActive == true ){
				//customMMES.corrections.entrypoinRotator("left", "rotator", "1");
				//customMMES.corrections.entrypoinRotator("middle", "laboral", "1");
				//customMMES.corrections.entrypoinRotator("right", "rotator", "1");
			}else{		
				//customMMES.corrections.entrypoinRotator("left",  "rotator", "1");
				//customMMES.corrections.entrypoinRotator("middle",  "rotator", "1");
				//customMMES.corrections.entrypoinRotator("right",  "rotator", "1");
			}	
		} else {
			if( randomEntry.IsActive == true ){
				customMMES.corrections.randomEntrypoint("03/12/2015, 00:00:00", "03/12/2025, 00:00:00","middle", "rotator");
			}else{	
				customMMES.corrections.randomEntrypoint("03/12/2015, 00:00:00", "03/12/2025, 00:00:00","left", "rotator-left");
				customMMES.corrections.randomEntrypoint("03/12/2015, 00:00:00", "03/12/2025, 00:00:00","middle", "rotator-middle");
			}
		}
		*/

		//Antes de configurar un megabanner eliminar los banners ya caducados
		//parametros  (start, end, position, filename, link)

				// Mega Banner #1

				//customMMES.controller.megaBanner('29/02/2016, 00:00:00', '06/03/2017, 23:59:59', '1', 'ser-feliz-cuesta-muy-poco', 'http://specials.mediamarkt.es/ser-feliz-cuesta-muy-poco');
				customMMES.controller.megaBanner('05/11/2015, 00:00:00', '20/03/2016, 23:59:59', '1', 'tarjeta-financiacion', 'http://www.mediamarkt.es/es/shop/financiacion.html');
				customMMES.controller.megaBanner('28/01/2016, 00:00:00', '05/02/2016, 21:00:00', '1', 'clima', 'http://tiendas.mediamarkt.es/electrodomestico-aire-acondicionado-climatizacion');
				// customMMES.controller.megaBanner('05/02/2016, 19:00:00', '14/02/2016, 23:59:59', '1', 'sanvalentin', 'http://tiendas.mediamarkt.es/ofertas-san-valentin');
				customMMES.controller.megaBannerMapeado('08/02/2016, 00:00:00', '14/02/2016, 00:00:00', '1', 'megabbaner-SanValentin', 'POLY', 'POLY', '598,198,692,2,7,4,1,194,598,198', '615,198,702,3,988,4,989,188', 'http://tiendas.mediamarkt.es/ofertas-san-valentin ','http://specials.mediamarkt.es/envolver-para-regalo', 'sanvalentin', 'papel-regalo');

				// Mega Banner #2
				customMMES.controller.megaBanner('03/03/2016, 00:00:00', '10/03/2016, 23:59:59', '2', 'samsung-s7', 'http://specials.mediamarkt.es/nuevo-galaxy-s7');
				customMMES.controller.megaBanner('29/02/2016, 00:00:00', '03/03/2016, 00:00:00', '2', 'premio-comercio', 'http://specials.mediamarkt.es/premio-comercio');
				customMMES.controller.megaBanner('11/01/2016, 00:00:00', '20/01/2016, 00:59:59', '2', 'copias', 'http://revelado.mediamarkt.es/Copias');
				customMMES.controller.megaBanner('20/01/2016, 00:00:00', '01/02/2016, 08:59:59', '2', 'configurador', 'http://tiendas.mediamarkt.es/configurador-pc-a-medida');
				customMMES.controller.megaBanner('01/02/2016, 09:00:00', '03/02/2016, 23:59:59', '2', 'sanvalentin', 'http://tiendas.mediamarkt.es/ofertas-san-valentin');
				customMMES.controller.megaBanner('04/02/2016, 00:00:00', '17/02/2016, 23:59:59', '2', 'b2b', 'http://specials.mediamarkt.es/solucion-empresas');
				// customMMES.controller.megaBanner('05/11/2015, 00:00:00', '10/01/2016, 23:59:59', '2', 'tarjeta-financiacion', 'http://www.mediamarkt.es/es/shop/financiacion.html');
                //customMMES.controller.megaBannerMapeado('03/12/2015, 00:00:00', '24/12/2015, 23:59:59', '2', 'amigo-invisible-barba', 'RECT', 'RECT', '0,0,505,200', '506,1,995,200', 'http://specials.mediamarkt.es/calendario-adviento-ofertas-navidad','http://specials.mediamarkt.es/sorteo-regalos-amigo-invisible-mediamarkt', 'calendario-adviento', 'amigo-invisible');
                //customMMES.controller.megaBanner('25/12/2015, 00:00:00', '06/01/2015, 23:59:00', '2' , 'amigo-invisible', 'http://specials.mediamarkt.es/sorteo-regalos-amigo-invisible-mediamarkt' );
                //customMMES.controller.megaBannerMapeado('25/12/2015, 00:00:00', '06/01/2016, 23:59:00', '2' , 'amigo-invisible-felicitacion', 'POLY', 'POLY', '2,2,448,1,497,199,1,200', '451,1,994,3,994,198,497,202', 'http://specials.mediamarkt.es/video-felicitacion', 'http://specials.mediamarkt.es/sorteo-regalos-amigo-invisible-mediamarkt', 'video-felicitacion', 'amigo-invisible');
				// Mega Banner #3
				customMMES.controller.megaBanner('29/02/2016, 00:00:00', '06/03/2017, 23:59:59', '4', 'ser-feliz-cuesta-muy-poco', 'http://specials.mediamarkt.es/ser-feliz-cuesta-muy-poco');
				customMMES.controller.megaBanner('03/03/2016, 00:00:00', '04/03/2016, 23:59:59', '4', 'premio-comercio', 'http://specials.mediamarkt.es/premio-comercio');
				customMMES.controller.megaBanner('14/12/2015, 00:00:00', '03/03/2016, 00:00:00', '4', 'newsletter', 'https://specials.mediamarkt.es/newsletter/registro');
                customMMES.controller.megaBanner('26/12/2015, 00:00:00', '27/12/2015, 23:59:59', '4', 'gransinpa', 'http://specials.mediamarkt.es/el-gran-sinpa ');                //semana 8

				// Mega Banner #4
				customMMES.controller.megaBanner('03/03/2016, 00:00:00', '31/03/2016, 23:59:59', '5', 'newsletter', 'https://specials.mediamarkt.es/newsletter/registro');
				customMMES.controller.megaBanner('30/11/2015, 00:00:00', '20/01/2016, 23:59:59', '5', 'megabanner-starwars', 'http://specials.mediamarkt.es/novedades-star-wars');
				// customMMES.controller.megaBanner('13/01/2016, 23:59:59', '30/11/2016, 23:59:59', '5', 'ser-feliz-cuesta-muy-poco', 'http://specials.mediamarkt.es/ser-feliz-cuesta-muy-poco');
				
				// Mega Banner #5
    			// customMMES.controller.megaBanner('29/02/2016, 00:00:00', '04/03/2016, 23:59:59', '5', 'newsletter', 'http://specials.mediamarkt.es/newsletter/registro');


    		}


    		/********************************************************************************************************************/
	//Se ejecutará para el dominio de mediamarkt.es y para tiendas.mediamarkt.es
	/********************************************************************************************************************/

	if (customMMES.mmDomain.query.IsCMS || customMMES.mmDomain.query.IsShop) {
	// Inserta los mini banners del popover
	customMMES.corrections.insertBannerPopover();

};

/********************************************************************************************************************/
	//Se está ejecutando en todos los sitios
	/********************************************************************************************************************/

	// Llamada a la funcion que ejecuta la accion para  mostrar el enlace de "volver a mobile"
	
	customMMES.corrections.returnMobile();

	// Llamada a la funcion que elimina la cookie para  "volver a mobile"
	customMMES.cookies.clickDeleteCookie();

	//customMMES.corrections.repairNewsTemp();
	customMMES.corrections.modal();

	// lammada a la fiuncion para dispositivos moviles
	customMMES.corrections.mobileDevice();



//********** Eliminar despues del dia 20151014 *****/

	//megabanner mapeado posicioón 5
	//$('#megabanner-5').empty();  esto elimina el banner  mapeado desde el CMS para poder insertarlo desde la siguiente línea
	// customMMES.controller.megaBannerMapeado('14/12/2015, 00:00:00', '20/12/2015, 00:00:00', '5', 'megabanner-14-20-12', 'POLY', 'POLY', '0,3,525,1,460,198,0,199', '531,1,989,2,994,198,469,195', 'http://www.mediamarkt.es/es/shop/tarjeta-regalo.html','http://revelado.mediamarkt.es/', 'tarjeta-regalo', 'revelado');
    //customMMES.controller.megaBannerMapeado('29/12/2015, 00:00:00', '31/12/2015, 23:59:59', '5', 'tarjeta-regalo-revelado', 'RECT', 'RECT', '0,0,498,200', '498,0,995,200', 'http://www.mediamarkt.es/es/shop/tarjeta-regalo.html','http://revelado.mediamarkt.es/', 'tarjeta-regalo', 'revelado-online');




});




/* @end			- Llamadas en el 'Domready' */
/********************************************************************************************************************/
/* @start		- Llamadas que no funcionan en el 'Domready' */


	// Delete includes de CMS en el header
	//customMMES.corrections.deleteInclude("base.min.css", "css");
	//customMMES.corrections.deleteInclude("screen.min.css", "css");

	// Agrega u item al Left navi de  Mi cuenta
	customMMES.corrections.addItemMyaccount();

	// Activa o desactiva el Top navi submenu de mi cuenta 
	customMMES.cookies.submenuMyaccount();

	// Llamada a la funcion Popovers USP's Cabecera
	if (customMMES.mmDomain.query.IsCheckout == 0) {
		customMMES.corrections.popoverUSP();
	}

	if (customMMES.mmDomain.query.IsHome  || customMMES.mmDomain.query.IsHomeTiendas){
		customMMES.ajaxInject.mediatrends({
			url: '//www.mediatrends.es/api/trends/get_last_post/',
			success: function(data){
				//console.log(data);
				//console.log(data.posts[0].title);
				$(".tr-destacado .banner-block-content h1").text("Lo último en MediaTrends");
				$(".tr-destacado .banner-block-content p").html(data.posts[0].title);
				$(".tr-destacado .banner-block-content img").attr('src', data.posts[0].thumbnail);
				$(".tr-destacado").attr('href', data.posts[0].link);
			},
			error: function(e){
				console.log(e);
			}
		});		
	}

			// Llamada al widget-Carousel de mediatrends
			if ($("#megabanner-3").length == 1) {

				function  slider_mediatrends(obj,jsonURL,seccionURL){
					$.getJSON(jsonURL, function(data) {
						var urlTitle = (seccionURL+"/?utm_source=mediamarkt.es&utm_medium=widget&utm_campaign=ultimas-noticias&utm_term=" + data.posts[0].title_term +  "&utm_content=seccion");
						$.each(data, function(key, value) {
							$(".content-item." + obj + " .body-item img").attr('src', data.posts[0].thumbnail);
							$(".content-item." + obj + " .body-item img").attr('alt', data.posts[0].title);
							$(".content-item." + obj + " .body-item img").attr('title', data.posts[0].title);
							$(".content-item." + obj + " .body-item a p").html(data.posts[0].title_short_55);
							$(".content-item." + obj + " .body-item figure a").attr('href', data.posts[0].link);
							$(".content-item." + obj + " .body-item  a").attr('href', data.posts[0].link);
							$(".content-item." + obj + " >  a").attr('href', urlTitle);							
						});

					});
				}
				slider_mediatrends("news","http://www.mediatrends.es/api/trends/get_mt_noticias", "http://www.mediatrends.es/noticias");
				slider_mediatrends("reviews","http://www.mediatrends.es/api/trends/get_mt_reviews","http://www.mediatrends.es/analisis");
				slider_mediatrends("comparativas","http://www.mediatrends.es/api/trends/get_mt_comparativas","http://www.mediatrends.es/comparativas");
				slider_mediatrends("how-to","http://www.mediatrends.es/api/trends/get_mt_howto","http://www.mediatrends.es/how-to");
				slider_mediatrends("mediatrends-tv","http://www.mediatrends.es/api/trends/get_mt_videos","http://www.mediatrends.es/videos");
				customMMES.inject.generateSliderMedia();


			}


	// Llamada al shopingcart 

	customMMES.ajaxInject.setShopingCar();

	// Inserta texto en la cabecera del checkout

	if (customMMES.mmDomain.contain("/ecommerce/checkout/") ) {
		$(".mm-logo").removeAttr("href");
		$(".header-usp li a").removeAttr("href");
		$(".header-usp li a").attr("cursor","auto");
		$( ".mm-logo" ).after( "<h4>¿Necesitas ayuda? Llámanos al 902 102 573</h4>" );
		$('.content-header').append($('#mm-wrapper .cart'));
	}	

	if (customMMES.mmDomain.query.IsShop == false) {

		// Llamada a correcciones en el site

		customMMES.corrections.marketFlyers();

		// llamada a la funcion miscelanea, inserta  y corrige varios componentes

		customMMES.corrections.misc();

		customMMES.cookies.law();

		if ( (customMMES.mmDate.collate(TestAB_FechaInicio, TestAB_FechaFin) == true) && (customMMES.mmDomain.query.IsHome ||  customMMES.mmDomain.query.IsHomeTiendas) ) {
			customMMES.cookies.testAB();

		}	
	}

	/*Inicio Llamada para ocultar categoría filtrable (3r nivel) de Television*/
	function getUrlParameterNews(sParam) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {

				if (customMMES.mmDomain.contain('es/shop/newsletterregister') == true) {
					if (sParam=="E516") {
						$('iframe').attr('src','');
						$('iframe').attr('src','http://rdir.medsat.agnitas.de/form.do?agnCI=586&agnFN=subscribe&email=&storeValue=372&gama=')
					}
				}

            // return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function esconErge() {
	if (customMMES.mmDomain.contain('television') == true) {
		$('span#checkCutedtelevisor-eficiencia-ergerica').parent().hide();
		$('span#checkCutedsamsung-tv-smart-2015').parent().hide();
		$('span#checkCutedsamsung-tv-curvos-2015').parent().hide();
		$('span#checkCutedsamsung-tv-suhd-2015').parent().hide();
		$('span#checkCutedsamsung-tv-ultra-hd-4k-2015').parent().hide();
	}
};

function precioFichaXL () {
	if (customMMES.mmDomain.contain('/p/') == true) {
		$('.productDetailPrice .mm-price').addClass('xl').removeClass('md');
		$('.productDetailPrice .mm-price .md').addClass('xl').removeClass('md');
	}
}

function removeRelPrice () {
	if (customMMES.mmDomain.contain('/es/shop/lavavajillas-tercera-bandeja-portacubiertos') == true) {
		$('#mm-main .mm-price').attr('style', '').css('position', 'absolute');
		$("#mm-main #carousel-custom").carousel('pause');

	}
}

if (customMMES.mmDomain.contain('/es/shop/smartphones-motorola') == true) {
	$('#newsletter-widget div').first().remove();	
}



window.setInterval(esconErge, 1000);
// window.setInterval(precioFichaXL, 1000);
window.setInterval(removeRelPrice, 2000);
	// window.setInterval(BFRSHOW, 3000);
	//var random = Math.floor(Math.random()*4);
		//$("#banner-block .middle ul li").eq(random).css('display','block');

		$("nav#left-nav [data-submenu-id='ctg-cm']").show();
		getUrlParameterNews('E516');
		if (customMMES.mmDomain.contain('/mcs/marketinfo/') == true) {
			customMMES.Stores.displayDays();
		};

		$('#mm-reg-nav .ffnn-start-day').text('10');
		$('#mm-reg-nav .ffnn-end-day').text('16');
		// $(".userbar a.news-subscribe").attr("href","https://specials.mediamarkt.es/newsletter/registro");

		/*Fin Llamada para ocultar categoría filtrable (3r nivel) de Television*/

		/*Parche pagina newsletter  https://www.mediamarkt.es/webapp/wcs/stores/servlet/MultiChannelMARegister?storeId=19601&langId=-5&redirectURL=http%3A%2F%2Fwww.mediamarkt.es%2F  */
		$('.my-account.register .content .register-form ul li > label[for="register-newsletter"]').css('width','auto');

		/** Parche para que aparezca en  la sección atención al cliente el enlace a "cambiazo" **/
		/*if (customMMES.mmDomain.contain('/es/shop/atencion-al-cliente/') == true && customMMES.mmDomain.contain('/es/shop/atencion-al-cliente/renove.html') == false  ) {
			$('ul.side-market li ul').prepend('<li><a href="/es/shop/atencion-al-cliente/renove.html">Cambiazo</a></li>');
		};*/


		/* @end			- Llamadas que no funcionan en el 'Domready' */
		/********************************************************************************************************************/


