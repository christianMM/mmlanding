$(window).load(function() {

});

$(document).ready(function() {
 
    /* Running Carrusel 1 */ 
    var carrusel1 = $(".carrusel-1");

    carrusel1.owlCarousel({
        loop:true,
        lazyLoad: true,
        pagination : false,
 		navigationText : false,
        items : 3, //10 items above 1000px browser width
        nav:true,
        responsive:{
 			320:{
 				items:1
 			},
 			500:{
 				items:3
 			},    	    	
 			768:{
 				items:3
 			},
 			992:{
 				items:3
 			}
 		}
    });
    
    /* Running Carrusel 2 */ 
    var carrusel2 = $(".carrusel-2");

    carrusel2.owlCarousel({
        loop:true,
        lazyLoad: true,
        pagination : false,
 		navigationText : false,
        items : 3, //10 items above 1000px browser width
        nav:true,
        responsive:{
 			320:{
 				items:1
 			},
 			500:{
 				items:3
 			},    	    	
 			768:{
 				items:3
 			},
 			992:{
 				items:3
 			}
 		}
    });
    
    /* Anchor to Indoor */
    $('#kh-indoor').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 500);
        return false;
    });
    /* Anchor to Outdoor */
    $('#kh-outdoor').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 500);
        return false;
    });
    
    /* Thumdnails gallery */
    $(".toggle-thumbnail").on("mouseover mouseout", "figure", function () {
        $(this).parent().parent().find( ".thdefault" ).toggle();
        $('.' + $(this).attr('thlink')).toggle();
    });
    
    

    /* Block Scroll Buttons */
    $('.slide-arrow').click(function(){
        $(this).parents('.col-md-12').next('.col-md-12').animate({'height':'205px'});
    });
    $('.slide-arrow-down').click(function(){
        $(this).parents('.col-md-12').animate({'height':'0'});
    });
 
});

/*
 * Render Price
 */

renderPrice('1268161','ref-1268161');
renderPrice('1268162','ref-1268162');
renderPrice('1268163','ref-1268163');
renderPrice('1268152','ref-1268152');
renderPrice('1268154','ref-1268154');
renderPrice('10081030','ref-10081030');
renderPrice('1268156','ref-1268156');
renderPrice('1268157','ref-1268157');
renderPrice('1311579','ref-1311579');
renderPrice('1311580','ref-1311580');
renderPrice('1311581','ref-1311581');
renderPrice('1311578','ref-1311578');


