<?php /* * INIT Variables * */ $n_node="node-114" ; ?>
<div id="<?php echo $n_node; ?>">
    <div class="container container-1250">
        <?php if (!$detect->isMobile()): ?>
        <figure>
            <?php img('background-parallax.jpg', 'cdn', null, 'img-responsive background-parallar', 'Karcher BackParall', 'Karcher BackParall'); ?>
        </figure>
        <?php endif;?>
    </div>
    <div class="container container-1250">
        <?php 
        /*
         * 
         *
         * HEADER
         *
         *
         */
        ?>
        <section>
            <div class="row col-md-12 block-logo">
                <figure>
                    <?php img('kaercher_logo_2015_4c_87815_cmyk.1.png', 'cdn', null, 'img-responsive center-left', 'Karcher', 'Karcher'); ?>
                </figure>
            </div>
        </section>
        <?php 
        /*
         * 
         *
         * BLOCK 1 
         *
         *
         */
        ?>
        <section>
            <div class="block-menu text-center row">
                <div class="col-xs-12 col-md-6">
                    <a href="#indoor" id="kh-indoor">
                        <figure><?php img( 'karcher_03.jpg', 'cdn', null, 'background img-responsive', 'Karcher Indoor', 'Karcher Indoor'); ?></figure>
                        <div><h3>Indoor</h3></div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-6">
                    <a href="#outdoor" id="kh-outdoor">
                        <figure><?php img( 'karcher_05.jpg', 'cdn', null, 'background img-responsive', 'Karcher Ourdoor', 'Karcher Ourdoor'); ?></figure>
                        <div><h3>Ourdoor</h3></div>
                    </a>
                </div>
            </div>
        </section>
        <?php 
        /*
         * 
         *
         * BLOCK 2 - Title Indoor
         *
         *
         */
        ?>
        <section class="block-indoor-title">
            <div class="row block-indoor-title">
                <div class="col-md-12 text-center"><a name="indoor" id="indoor"></a><h3>Indoor</h3></div>
            </div>
        </section>
        <?php 
        /*
         * 
         *
         * BLOCK 3 - Title Aspiradores WD
         *
         *
         */
        ?>
        <section class="subtitle-aspiradores">
            <div class="row block-indoor-subtitle">
                <div class="col-md-6">
                    <div><h3>Aspiradores WD >></h3></div>
                    <?php if (!$detect->isMobile()): ?>
                    <div class="btn" data-toggle="modal" data-target="#md-aspiradores">
                        <figure><?php img( 'capa_3_copia_1.jpg', 'cdn', null, 'background img-responsive', 'Aspiradores WD Video', 'Aspiradores WD Video'); ?></figure>
                    </div>
                    <?php endif; ?>
                </div>
                <?php if (!$detect->isMobile()): ?>
                <!-- Modal Aspiradores -->
                <div id="md-aspiradores" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Gama de aspiradores WD de Kärcher</h4>
                            </div>
                            <div class="modal-body">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/_dX-uv6LGXo" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <?php else:?>
                 <iframe width="560" height="315"
                    src="https://www.youtube.com/embed/_dX-uv6LGXo"
                    frameborder="0" allowfullscreen></iframe>
                <?php endif;?>
            </div>
        </section>
        <?php 
        /*
         * 
         *
         * BLOCK 4 - Grid product aspiradoras
         *
         *
         */
        ?>
        <section>
             <?php 
                /*
                 * BLOCK 4 - Block Left 
                 */
                ?>
            <div class="row">
                <div class="col-xs-12 col-md-4 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'wd_3_16298000.jpg', 'cdn', null, 'img-responsive', 'Aspirador con bolsa WD 3', 'Aspirador con bolsa WD 3'); ?></figure>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-1268161"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-1268161"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/aspirador-con-bolsa-karcher-mv-3-poten-1268161"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-wd3');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Aspirador con bolsa WD 3</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">El aspirador multiuso WD 3, es un modelo súper potente y con un consumo eléctrico de tan solo 1000 W. Su robusto recipiente de plástico resistente a los golpes tiene una capacidad de 17 l y viene de serie con varios accesorios.</p>
                            <div class="block-p-table">
                                <span>Postencia de aspiración real* (W)</span><span>200</span>
                                <span>Potencia de absorbida (W)</span><span>1000</span>
                                <span>Capacidad del depósito (l)</span><span>17</span>
                                <span>Material del recipiente</span><span>Plástico</span>
                                <span>Cable de conexión (m)</span><span>4</span>
                            </div>
                        </div>
                        <figure><?php img( 'objeto_inteligente_vectorial_4.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
                <?php 
                    /*
                     * BLOCK 4 - Block Center
                     */
                    ?>
                <div class="col-xs-12 col-md-4 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'aspirador_con_bolsa_karcher_mv4_principal_l.jpg', 'cdn', null, 'img-responsive', 'Aspirador con bolsa WD 3', 'Aspirador con bolsa WD 3'); ?></figure>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-1268162"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-1268162"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/aspirador-con-bolsa-karcher-mv-4-poten-1268162"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-wd4');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Aspirador con bolsa WD 4</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">El aspirador multiuso WD 4, es un modelo súper potente y con un consumo eléctrico de tan solo 1000 W. Su robusto recipiente de plástico resistente a los golpes tiene una capacidad de 20 y viene de serie con varios accesorios</p>
                            <div class="block-p-table">
                                <span>Postencia de aspiración real* (W)</span><span>200</span>
                                <span>Potencia de absorbida (W)</span><span>1000</span>
                                <span>Capacidad del depósito (l)</span><span>20</span>
                                <span>Material del recipiente</span><span>Plástico</span>
                                <span>Cable de conexión (m)</span><span>5</span>
                            </div>
                        </div>
                        <figure><?php img( 'objeto_inteligente_vectorial_4.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
                <?php 
                    /*
                     * BLOCK 4 - Block Right
                     */
                    ?>
                <div class="col-xs-12 col-md-4 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'wd_5_13481900.jpg', 'cdn', null, 'img-responsive', 'Aspirador con bolsa WD 3', 'Aspirador con bolsa WD 3'); ?></figure>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-1268163"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-1268163"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/aspirador-con-bolsa-karcher-mv-5-poten-1268163"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-wd5');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Aspirador con bolsa WD 5</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">El modelo de gama superior WD 5 es un aspirador multiuso extremadamente potente que convence con un consumo eléctrico de tan solo 1100 W. Dispone de un recipiente de plástico de 25 l resistente a los golpes y un filtro plegado plano que se encuentra en un cartucho filtrante.</p>
                            <div class="block-p-table">
                                <span>Postencia de aspiración real* (W)</span><span>240</span>
                                <span>Potencia de absorbida (W)</span><span>1100</span>
                                <span>Capacidad del depósito (l)</span><span>25</span>
                                <span>Material del recipiente</span><span>Plástico</span>
                                <span>Cable de conexión (m)</span><span>5</span>
                            </div>
                        </div>
                        <figure><?php img( 'objeto_inteligente_vectorial_4.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
            </div>
        </section>
        <?php 
        /*
         * 
         *
         * BLOCK 5 - Carrusel 1  
         *
         *
         */
        ?>        
        <section>
    		<div class="carrusel-1 owl-carousel owl-theme">
    		    <div class="item">
    		        <figure><?php img( 'carrusel-1/Ambiente-WD-1.jpg', 'cdn', null, '', 'Carrusel 1 - Img 1', 'Carrusel 1 - Img 1'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-1/Ambiente-WD-2.jpg', 'cdn', null, '', 'Carrusel 1 - Img 2', 'Carrusel 1 - Img 2'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-1/Ambiente-WD-3.jpg', 'cdn', null, '', 'Carrusel 1 - Img 3', 'Carrusel 1 - Img 3'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-1/Ambiente-WD-4.jpg', 'cdn', null, '', 'Carrusel 1 - Img 4', 'Carrusel 1 - Img 4'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-1/Ambiente-WD-5.jpg', 'cdn', null, '', 'Carrusel 1 - Img 5', 'Carrusel 1 - Img 5'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-1/Ambiente-WD-6.jpg', 'cdn', null, '', 'Carrusel 1 - Img 6', 'Carrusel 1 - Img 6'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-1/Ambiente-WD-7.jpg', 'cdn', null, '', 'Carrusel 1 - Img 7', 'Carrusel 1 - Img 7'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-1/Ambiente-WD-8.jpg', 'cdn', null, '', 'Carrusel 1 - Img 8', 'Carrusel 1 - Img 8'); ?></figure>
    		    </div>
    		</div>
    	</section>
    	
    	 <?php 
        /*
         * 
         *
         * BLOCK 6 - Limpiacristales WV
         *
         *
         */
        ?> 	
    	<section>
    	    <div class="row container-limpiacristales-wv">
        		<div class="col-md-12 block-indoor-subtitle"><h3>Limpiacristales WV >></h3></div>
        		<div class="col-xs-12 col-md-6">
        		    <div class="toggle-show">
        			    <figure class="thdefault"><?php img( 'limpiacristales_karcher_wv_classic_principal_sf_l.png', 'cdn', null, 'background img-responsive', 'Limpiacristales WD', 'Limpiacristales WD'); ?></figure>
        			    <figure class="limpia-wp-img1"><?php img( 'limpiacristales_karcher_wv_classic_ejemplo_ad_l-big.jpg', 'cdn', null, 'background img-responsive', 'Limpiacristales WD', 'Limpiacristales WD'); ?></figure>
        			    <figure class="limpia-wp-img2"><?php img( 'limpiacristales_karcher_wv_classic_ejemplo3_ad_l-big.jpg', 'cdn', null, 'background img-responsive', 'Limpiacristales WD', 'Limpiacristales WD'); ?></figure>
        			</div>
        			<div class="toggle-thumbnail">
        			     <figure thlink="limpia-wp-img1"><?php img( 'limpiacristales_karcher_wv_classic_ejemplo_ad_l.jpg', 'cdn', null, 'background img-responsive', 'Limpiacristales WD', 'Limpiacristales WD'); ?></figure>
        			     <figure thlink="limpia-wp-img2"><?php img( 'limpiacristales_karcher_wv_classic_ejemplo3_ad_l.jpg', 'cdn', null, 'background img-responsive', 'Limpiacristales WD', 'Limpiacristales WD'); ?></figure>
        			</div>
        		</div>
        		<div class="col-xs-12 col-md-6">
        			<h2>Limpiacristales - Karcher WV CLASSIC</h2>
        			<p>Con la innovadora limpiadora de cristales, Kärcher ha revolucionado la limpieza de ventanas. La limpiadora de cristales original de Kärcher hace que la limpieza sea un juego de niños y ahorra tiempo y esfuerzo.</p>
        			<div class="block-p-table">
                        <span>Ancho útil de la boquilla de aspiración (mm)</span><span>130</span>
                        <span>Contenido del depósito de agua sucia (ml)</span><span>100</span>
                        <span>Tiempo de carga de acumuladores (min)</span><span>120</span>
                        <span>Rendimiento de la batería (min)</span><span>20</span>
                        <span>Rendimiento de limpieza por cargador de baterías</span><span>Aprox. 75 m² = 20 ventanas</span>
                    </div>
                    <?php if (!$detect->isMobile()): ?>
                    <div class="btn btn-modal" data-toggle="modal" data-target="#md-limpiacristales">
                            <figure><?php img( 'capa_2.jpg', 'cdn', null, 'background img-responsive', 'Limpiacristales WD Video', 'Limpiacristales WD Video'); ?></figure>
                    </div>
                    <!-- Modal Limpiacristales -->
                    <div id="md-limpiacristales" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Kärcher limpiadora de cristales WV 50</h4>
                                </div>
                                <div class="modal-body">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/BO383IFDkzw" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php else:?>
                      <iframe width="560" height="315"
                    src="https://www.youtube.com/embed/BO383IFDkzw"
                    frameborder="0" allowfullscreen></iframe>
                    <?php endif;?>
                    <?php /* PRECIO */ ?> 
                    <div class="bt-price price-2">
                        <?php if (!$detect->isMobile()): ?>
                            <div class="price xl ref-1268152"></div><?php endif; ?>
                        <?php if ($detect->isMobile()): ?>
                            <div class="price md ref-1268152"></div><?php endif; ?>
                        <div class="btn-1">
                            <a href="http://tiendas.mediamarkt.es/p/limpiacristales-karcher-wv-classic-esp-1268152"
                               onclick="ga('send','event','microsites','novedades-karcher','comprar-wv-classic');"
                               target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                        </div>
                    </div>
        		</div>
        	</div>
    	</section>
        <?php 
            /*
             * 
             *
             * BLOCK 7 - Grid Product Limpiadoras
             *
             *
             */
            ?> 
    	<section>
    	    <div class="row grid-product-limpidoras">
        		<div class="col-md-12 block-indoor-subtitle"><h3>Limpiadoras de vapor >></h3></div>
        	    <?php 
                /*
                 * BLOCK 7 - Left 
                 */
                ?>
                <div class="col-xs-12 col-md-3 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'limpiador_de_vapor_karcher_sc1floorkit_frontal_l.jpg', 'cdn', null, 'img-responsive', 'Aspirador con bolsa WD 3', 'Aspirador con bolsa WD 3'); ?></figure>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-1268154"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-1268154"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/limpiador-de-vapor-karcher-sc-1-floor-1268154"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-sc1');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Kärrcher SC 1 FLOOR KIT</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_copia_5_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">La fregadora de vapor compacta de Kärcher dos en uno SC 1 Floor Kit (set de limpieza para suelos) puede utilizarse de forma universal en todo el hogar y limpia sin productos químicos.</p>
                            <div class="block-p-table">
                                <span>Rendimiento de superficie (m²))</span><span>20</span>
                                <span>Potencia calorífica (W)</span><span>1200</span>
                                <span>Presiónn máx. de vapor (bar)</span><span>3</span>
                                <span>Tiempo de calentamiento (min)</span><span>3</span>
                                <span>Contenido del depósito /de la caldera</span><span>0.2 L</span>
                            </div>
                        </div>    
                        <figure><?php img( 'objeto_inteligente_vectorial_copia_5.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
        	    <?php 
                /*
                 * BLOCK 7 - Center Left 
                 */
                ?>
                <div class="col-xs-12 col-md-3 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'limpiadora_a_vapor_karcher_sc_2_principal1_l.jpg', 'cdn', null, 'img-responsive', 'Aspirador con bolsa WD 3', 'Aspirador con bolsa WD 3'); ?></figure>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-10081030"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-10081030"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/karcher-1.512.000-0-sc-2-10081030"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-sc2');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Kärrcher SC 1 FLOOR KIT</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_copia_5_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">La iniciación a la limpieza con vapor. La limpiadora de vapor de iniciación SC 2 de manejo intuitivo y sencillo ofrece una regulación del vapor de dos etapas para adaptar la intensidad del vapor a la superficie y al grado de suciedad..</p>
                            <div class="block-p-table">
                                <span>Rendimiento de superficie (m²))</span><span>75</span>
                                <span>Potencia calorífica (W)</span><span>1500</span>
                                <span>Presiónn máx. de vapor (bar)</span><span>3.2</span>
                                <span>Tiempo de calentamiento (min)</span><span>6.5</span>
                                <span>Contenido del depósito /de la caldera</span><span>1 L</span>
                            </div>
                        </div>    
                        <figure><?php img( 'objeto_inteligente_vectorial_copia_5.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
        	    <?php 
                /*
                 * BLOCK 7 - Center Right
                 */
                ?>
                <div class="col-xs-12 col-md-3 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'limpiadora_a_vapor_karcher_sc_3_principal_sinfondo2_l.jpg', 'cdn', null, 'slide-arrow img-responsive', 'Aspirador con bolsa WD 3', 'Aspirador con bolsa WD 3'); ?></figure>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-1268156"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-1268156"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/limpiador-de-vapor-karcher-sc-3-potenc-1268156"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-sc3');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Kärrcher SC 1 FLOOR KIT</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_copia_5_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">La SC 3 de Kärcher es la limpiadora de vapor más rápida del momento. Con un tiempo de calentamiento de solo 30 segundos, la SC 3 está lista para empezar en un abrir y cerrar de ojos. La posibilidad de rellenar el depósito de agua de forma permanente y rápida permite trabajar sin interrupciones y ahorra mucho tiempo.</p>
                            <div class="block-p-table">
                                <span>Rendimiento de superficie (m²))</span><span>75</span>
                                <span>Potencia calorífica (W)</span><span>1900</span>
                                <span>Presiónn máx. de vapor (bar)</span><span>3.5</span>
                                <span>Tiempo de calentamiento (min)</span><span>0.5</span>
                                <span>Contenido del depósito /de la caldera</span><span>1 L</span>
                            </div>
                        </div>    
                        <figure><?php img( 'objeto_inteligente_vectorial_copia_5.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
        	    <?php 
                /*
                 * BLOCK 7 - Right
                 */
                ?>
                <div class="col-xs-12 col-md-3 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'limpiadora_a_vapor_karcher_sc_3_frontal1_l.jpg', 'cdn', null, 'slide-arrow img-responsive', 'Aspirador con bolsa WD 3', 'Aspirador con bolsa WD 3'); ?></figure>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-1268157"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-1268157"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/limpiador-de-vapor-karcher-sc-4-potenc-1268157"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-sc4');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Kärrcher SC 1 FLOOR KIT</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_copia_5_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">La limpiadora de vapor SC 4 proporciona una gran comodidad y destaca por un compartimento para el cable integrado, un compartimento de almacenaje para accesorios y una posición de estacionamiento para la boquilla para suelos.</p>
                            <div class="block-p-table">
                                <span>Rendimiento de superficie (m²))</span><span>100</span>
                                <span>Potencia calorífica (W)</span><span>2000</span>
                                <span>Presiónn máx. de vapor (bar)</span><span>3.5</span>
                                <span>Tiempo de calentamiento (min)</span><span>4</span>
                                <span>Contenido del depósito/de la caldera</span><span>0.2/0.8 L</span>
                            </div>
                        </div>
                        <figure><?php img( 'objeto_inteligente_vectorial_copia_5.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
            </div>    
    	</section>
        <?php 
            /*
             * 
             *
             * BLOCK 8 - Carrusel 2
             *
             *
             */
            ?> 
         <section>
    		<div class="carrusel-2 owl-carousel owl-theme">
    		    <div class="item">
    		        <figure><?php img( 'carrusel-2/Ambiente-SC-1_11562200-2.jpg', 'cdn', null, '', 'Carrusel 2 - Img 1', 'Carrusel 2 - Img 1'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-2/Ambiente-SC-1_11562200.jpg', 'cdn', null, '', 'Carrusel 2 - Img 2', 'Carrusel 2 - Img 2'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-2/Ambiente-SC-3_15130000_5-1.jpg', 'cdn', null, '', 'Carrusel 2 - Img 3', 'Carrusel 2 - Img 3'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-2/Ambiente-SC-3_15130000_5-2.jpg', 'cdn', null, '', 'Carrusel 2 - Img 4', 'Carrusel 2 - Img 4'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-2/Ambiente-SC-3_15130000_5-3.jpg', 'cdn', null, '', 'Carrusel 2 - Img 5', 'Carrusel 2 - Img 5'); ?></figure>
    		    </div>
    		     <div class="item">
    		        <figure><?php img( 'carrusel-2/Ambiente-SC-3_15130000_5-4.jpg', 'cdn', null, '', 'Carrusel 2 - Img 6', 'Carrusel 2 - Img 6'); ?></figure>
    		    </div>
    		</div>
    	</section>
        <?php 
            /*
             * 
             *
             * BLOCK 9 - Title Ourdoor
             *
             *
             */
            ?>
        <section>
            <div class="row block-outdor-title">
                <div class="col-md-12 text-center"><a name="outdoor" id="outdoor"></a><h3>Outdoor</h3></div>
            </div>
        </section>
    	<?php 
        /*
         * 
         *
         * BLOCK 10 - Gama Full Control Title
         *
         *
         */
        ?>
        <section>
            <div class="row block-indoor-subtitle">
                <div class="col-md-6">
                    <div><h3>Gama Full Control >></h3></div>
                    <?php if (!$detect->isMobile()): ?>
                    <div class="btn" data-toggle="modal" data-target="#md-fullcontrol">
                        <figure><?php img( 'capa_3_copia_1.jpg', 'cdn', null, 'background img-responsive', 'Gama Full Control', 'Gama Full Control'); ?></figure>
                    </div>
                    <?php endif;?>
                </div>
                <?php if (!$detect->isMobile()): ?>
                <!-- Modal Aspiradores -->
                <div id="md-fullcontrol" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Gama Hidrolimpiadoras Full Control de Kärcher</h4>
                            </div>
                            <div class="modal-body">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/8IM8vKqSUos" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <?php else:?>
                 <iframe width="560" height="315"
                    src="https://www.youtube.com/embed/8IM8vKqSUos"
                    frameborder="0" allowfullscreen></iframe>
                <?php endif;?>
            </div>
        </section>
         <?php 
        /*
         * 
         *
         * BLOCK 11 - Grid product Full Control
         *
         *
         */
        ?>
        <section>
            <div class="row grip-product-fullcontrol">
                <?php 
                    /*
                     * BLOCK 11 - Block Left 
                     */
                    ?>
                <div class="col-xs-12 col-md-4 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'k_4_premium_fc_13241000.jpg', 'cdn', null, 'img-responsive', 'K 4 PREMIUM FULL CONTROL', 'K 4 PREMIUM FULL CONTROL'); ?></figure>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-1311579"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-1311579"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/hidrolimpiadora-karcher-k4-full-contro-1311579"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-k4');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>K 4 PREMIUM FULL CONTROL</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">La limpiadora de alta presión con enrollador de mangueras es ideal para limpiezas ocasionales contra la suciedad de grado medio y para un rendimiento de superficie de 30 m²/h. </p>
                            <div class="block-p-table">
                                <span>Presión (bar/MPa)</span><span>20 - Máx 145/2 - hasta 14.5</span>
                                <span>Caudal (l/h)</span><span>Máx. 500</span>
                                <span>Temparatura de entrada máx.</span><span>hasta 40.(ºC)</span>
                                <span>Tensión (V)</span><span>230</span>
                                <span>Potencia absorbida</span><span>2.1</span>
                            </div>
                        </div>
                        <figure><?php img( 'objeto_inteligente_vectorial_4.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
                <?php 
                    /*
                     * BLOCK 11 - Block Center
                     */
                    ?>
                <div class="col-xs-12 col-md-4 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'k_5_premium_fc_13246000.jpg', 'cdn', null, 'img-responsive', 'K 5 PREMIUM FULL CONTROL', 'K 5 PREMIUM FULL CONTROL'); ?></figure>
                        <?php /* PRECIO */ ?> 
                    <div class="bt-price price-2">
                        <?php if (!$detect->isMobile()): ?>
                            <div class="price xl ref-1311580"></div><?php endif; ?>
                        <?php if ($detect->isMobile()): ?>
                            <div class="price md ref-1311580"></div><?php endif; ?>
                        <div class="btn-1">
                            <a href="http://tiendas.mediamarkt.es/p/hidrolimpiadora-karcher-k5-full-contro-1311580"
                               onclick="ga('send','event','microsites','novedades-karcher','comprar-k5');"
                               target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-12">
                        <h3>K 5 PREMIUM FULL CONTROL</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">El aspirador multiuso WD 4, es un modelo súper potente y con un consumo eléctrico de tan solo 1000 W. Su robusto recipiente de plástico resistente a los golpes tiene una capacidad de 20 y viene de serie con varios accesorios</p>
                            <div class="block-p-table">
                                <span>Presión (bar/MPa)</span><span>20 - Máx 160/2 - hasta 16</span>
                                <span>Caudal (l/h)</span><span>Máx. 600</span>
                                <span>Temparatura de entrada máx.</span><span>Máx. 60.(ºC)</span>
                                <span>Tensión (V)</span><span>230</span>
                                <span>Potencia absorbida</span><span>3</span>
                            </div>
                        </div>
                        <figure><?php img( 'objeto_inteligente_vectorial_4.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
                <?php 
                    /*
                     * BLOCK 11 - Block Right
                     */
                    ?>
                <div class="col-xs-12 col-md-4 block-producto-slide">
                    <div class="col-md-12">
                        <figure><?php img( 'k_7_premium_fc_home_t450_13171030.jpg', 'cdn', null, 'img-responsive', 'K 6 PREMIUM FULL CONTROL', 'K 6 PREMIUM FULL CONTROL'); ?></figure>
                        <?php /* PRECIO */ ?> 
                    <div class="bt-price price-2">
                        <?php if (!$detect->isMobile()): ?>
                            <div class="price xl ref-1311581"></div><?php endif; ?>
                        <?php if ($detect->isMobile()): ?>
                            <div class="price md ref-1311581"></div><?php endif; ?>
                        <div class="btn-1">
                            <a href="http://tiendas.mediamarkt.es/p/hidrolimpiadora-karcher-k7-full-contro-1311581"
                               onclick="ga('send','event','microsites','novedades-karcher','comprar-k7');"
                               target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-12">
                        <h3>K 6 PREMIUM FULL CONTROL</h3>
                        <figure><?php img( 'objeto_inteligente_vectorial_1.png', 'cdn', null, 'slide-arrow img-responsive', 'Arrow Down', 'Arrow Down'); ?></figure>
                    </div>
                    <div class="col-md-12">
                        <div class="container-scroll">
                            <p class="block-p">El modelo de gama superior WD 5 es un aspirador multiuso extremadamente potente que convence con un consumo eléctrico de tan solo 1100 W. Dispone de un recipiente de plástico de 25 l resistente a los golpes y un filtro plegado plano que se encuentra en un cartucho filtrante.</p>
                            <div class="block-p-table">
                                <span>Presión (bar/MPa)</span><span>20 - hasta 130/2 - hasta 13</span>
                                <span>Caudal (l/h)</span><span>Máx. 420</span>
                                <span>Temparatura de entrada máx.</span><span>Máx. 40.(ºC)</span>
                                <span>Tensión (V)</span><span>230</span>
                                <span>Potencia absorbida</span><span>1.8(kW)</span>
                            </div>
                        </div>
                        <figure><?php img( 'objeto_inteligente_vectorial_4.png', 'cdn', null, 'slide-arrow-down img-responsive', 'Arrow Up', 'Arrow Up'); ?></figure>
                    </div>
                </div>
            </div>    
        </section>
    </div>
    <div class="container container-1250">
        <section>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-12 col-md-8">
                       <div class="toggle-show">
            			    <figure class="thdefault"><?php img( 'k_2_compact_16731040.jpg', 'cdn', null, 'background img-responsive', 'K2 COMPACT', 'K2 COMPACT'); ?></figure>
            			    <figure class="tc-img1"><?php img( 'nig-k_2_compact_ambiente_16021050_copia.jpg', 'cdn', null, 'background img-responsive', 'K2 COMPACT', 'K2 COMPACT'); ?></figure>
            			    <figure class="tc-img2"><?php img( 'big-k_2_compact_detalle_16731000.jpg', 'cdn', null, 'background img-responsive', 'K2 COMPACT', 'K2 COMPACT'); ?></figure>
            			    <figure class="tc-img3"><?php img( 'big-k_2_compact_detalle_16731000_2.jpg', 'cdn', null, 'background img-responsive', 'K2 COMPACT', 'K2 COMPACT'); ?></figure>
            			    <figure class="tc-img4"><?php img( 'big-k_2_compact_detalle_16731000_3.jpg', 'cdn', null, 'background img-responsive', 'K2 COMPACT', 'K2 COMPACT'); ?></figure>
            			</div>
            			<div class="toggle-thumbnail">
            			     <figure thlink="tc-img1"><?php img( 'k_2_compact_ambiente_16021050_copia.jpg', 'cdn', null, 'background img-responsive', 'K2 COMPACT', 'K2 COMPACT'); ?></figure>
            			     <figure thlink="tc-img2"><?php img( 'k_2_compact_detalle_16731000.jpg', 'cdn', null, 'background img-responsive', 'K2 COMPACT', 'K2 COMPACT'); ?></figure>
            			     <figure thlink="tc-img3"><?php img( 'k_2_compact_detalle_16731000_2.jpg', 'cdn', null, 'background img-responsive', 'K2 COMPACT', 'K2 COMPACT'); ?></figure>
            			     <figure thlink="tc-img4"><?php img( 'k_2_compact_detalle_16731000_3.jpg', 'cdn', null, 'background img-responsive', 'K2 COMPACT', 'K2 COMPACT'); ?></figure>
            			</div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <h2>K 2 COMPACT</h2>
                        <p>La hidrolimpiadora Kärcher K2 compact es el mejor complemento para tu jardín. Podrás tenerlo todo bien conservado y en magníficas condiciones, libre de suciedad de la forma más fácil.</p>
                        <div class="block-p-table">
                            <span>Presión (bar/MPa):</span><span>Máx.110 / Máx.11</span>
                            <span>Caudal (l/h):</span><span>Máx.360</span>
                            <span>Temperatura de entrada máx.: (°C)</span><span>Máx.40</span>
                            <span>Tensión (V):</span><span>220-240</span>
                            <span>Potencia absorbida (kW):</span><span>1.4</span>
                        </div>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-1220955"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-1220955"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/hidrolimpiadora-karcher-k2-compact-110-bares-de-presion-caudal-360-ltrshora-1400w-de-potencia-1220955"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-k2');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-12 col-md-8">
                        <div class="toggle-show">
            			    <figure class="thdefault"><?php img( 'k_2_home_t150_16732230.jpg', 'cdn', null, 'background img-responsive', 'K 2 Home T 150', 'K 2 Home T 150'); ?></figure>
            			    <figure class="t150-img1"><?php img( 'big-k_2_home_t150_16732200.jpg', 'cdn', null, 'background img-responsive', 'K 2 Home T 150', 'K 2 Home T 150'); ?></figure>
            			    <figure class="t150-img2"><?php img( 'nig-k_2_compact_ambiente_16021050_copia.jpg', 'cdn', null, 'background img-responsive', 'K 2 Home T 150', 'K 2 Home T 150'); ?></figure>
            			</div>
            			<div class="toggle-thumbnail">
            			     <figure thlink="t150-img1"><?php img( 'k_2_home_t150_16732200.jpg', 'cdn', null, 'background img-responsive', 'K 2 Home T 150', 'K 2 Home T 150'); ?></figure>
            			     <figure thlink="t150-img2"><?php img( 'k_2_home_t150_ambiente__16731060.jpg', 'cdn', null, 'background img-responsive', 'K 2 Home T 150', 'K 2 Home T 150'); ?></figure>
            			</div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <h2>K 2 HOME T 150</h2>
                        <p>La «K2 Home» incluye una pistola, dos neumáticos de marcha suave, una manguera de alta presión de 4 m, un tubo pulverizador simple, una boquilla turbo con chorro giratorio contra suciedad incrustada y un filtro de agua para proteger la bomba.</p>
                        <div class="block-p-table">
                            <span>Presión (bar/MPa):</span><span>Máx.110 / Máx.11</span>
                            <span>Caudal (l/h):</span><span>Máx.360</span>
                            <span>Temperatura de entrada máx.: (°C)</span><span>Máx.40</span>
                            <span>Tensión (V):</span><span>220-240</span>
                            <span>Potencia absorbida (kW):</span><span>1.4</span>
                        </div>
                        <?php /* PRECIO */ ?> 
                        <div class="bt-price price-2">
                            <?php if (!$detect->isMobile()): ?>
                                <div class="price xl ref-1311578"></div><?php endif; ?>
                            <?php if ($detect->isMobile()): ?>
                                <div class="price md ref-1311578"></div><?php endif; ?>
                            <div class="btn-1">
                                <a href="http://tiendas.mediamarkt.es/p/hidrolimpiadora-karcher-k2-compact-hom-1311578"
                                   onclick="ga('send','event','microsites','novedades-karcher','comprar-k2-t150');"
                                   target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </section>
    </div>
</div>