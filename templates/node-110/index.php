        <!-- Landing Design Block -->
        <section>
            <?php
            /*
             * INIT Variables
             * */
            $n_node = "node-110";

            ?>
            <div class="container" id="<?php echo $n_node; ?>">
                    <!-- Header -->
                    <div class="block-header">
                        <div class="block-p col-md-3">
                            <figure>
                                <?php img('netatmo-title-brand.png', 'cdn', null, 'background img-responsive', 'Netatmo', 'Netatmo'); ?>
                            </figure>
                            <p>Cámaras de vigilancia, termostatos, estaciones metereológicas, pluviómetros… Netatmo te
                                presenta sus innovadoras novedades para que tu hogar esté a la última.</p>
                        </div>
                        <figure>
                            <?php img('netatmo-b0-background.jpg', 'cdn', null, 'background img-responsive', 'Termostato para Smarthphone Netatmo', 'Termostato para Smarthphone Netatmo'); ?>
                        </figure>
                    </div>

                    <!-- Block 1 -->
                    <div class="block content-block-1">
                        <div class="block-top-zone btz-1">
                            La calefacción reinventada
                        </div>
                        <div class="col-md-6 special-col-md-6-hlimit">
                            <figure>
                                    <?php img('netatmo-product-b-1-1.png', 'cdn', null, 'img-product img-special-top img-responsive', 'Termostato para Smarthphone Netatmo', 'Termostato para Smarthphone Netatmo'); ?>
                            </figure>
                            <div class="media-trends">
                                <figure>
                                    <a href="http://www.mediatrends.es/a/55210/termostato-netatmo-review-precio/"
                                       onclick="ga('send','event','microsites','novedades-netatmo','mediatrends-controla-calefaccion-movil');"
                                       target="_blank">
                                            <?php img('media-trends.png', 'cdn', null, 'img-responsive', 'Media Trends controla la calefacción con el móvil', 'Media Trends controla la calefacción con el móvil'); ?>
                                    </a>
                                </figure>
                            </div>
                            <div class="bt-price price-1">
                                <?php if (!$detect->isMobile()): ?>
                                    <div class="price xl ref-1233700"></div><?php endif; ?>
                                <?php if ($detect->isMobile()): ?>
                                    <div class="price md ref-1233700"></div><?php endif; ?>
                                <div class="btn-1">
                                    <a href="http://tiendas.mediamarkt.es/p/termostato-para-smartphone-netatmo-nth-1233700"
                                       onclick="ga('send','event','microsites','novedades-netatmo','comprar-termostato');"
                                       target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                                </div>
                            </div>
                            <?php if (!$detect->isMobile()): ?>
                                <div class="option-clicks show-option iconos-plus10 opt-click-1"
                                     data-show="blc-1-opt-1"></div>
                                <div class="option-clicks show-option iconos-plus10 opt-click-2"
                                     data-show="blc-1-opt-2"></div>
                                <div class="option-clicks show-option iconos-plus10 opt-click-3"
                                     data-show="blc-1-opt-3"></div>
                                <div class="option-clicks show-option iconos-plus10 opt-click-4"
                                     data-show="blc-1-opt-4"></div>
                            <?php endif ?>
                        </div>
                        <div class="col-md-6 special-col-md-6-hlimit">
                            <div class="block-p bp-1">
                                <p>Con el termostato para Smartphone de Netatmo vas a poder controlar la temperatura de
                                    tu casa a distancia. Compatible con iOs y Android, este dispositivo de diseño
                                    moderno es capaz de ayudarte a programar la calefacción según tus hábitos para
                                    lograr siempre la temperatura ideal y reducir considerablemente tu consumo
                                    energético así como tu huella de carbono.</p>
                                <p id="blc-1-opt-1" class="option <?php if (!$detect->isMobile()) {
                                    echo 'invisible';
                                } ?>">Controla la temperatura de tu casa desde cualquier
                                    lugar</p>
                                <p id="blc-1-opt-2" class="option <?php if (!$detect->isMobile()) {
                                    echo 'invisible';
                                } ?>">Reduce tu consumo energético</p>
                                <p id="blc-1-opt-3" class="option <?php if (!$detect->isMobile()) {
                                    echo 'invisible';
                                } ?>">Programa la calefacción según tus hábitos</p>
                                <p id="blc-1-opt-4" class="option <?php if (!$detect->isMobile()) {
                                    echo 'invisible';
                                } ?>">Diseño elegante y sencillo</p>
                            </div>
                            <div class="video">
                                <?php if (!$detect->isMobile()): ?>
                                    <div class="btn" data-toggle="modal" data-target="#video-1-modal">
                                        <figure>
                                                <?php img('box-video.jpg', 'cdn', null, 'img-responsive', 'Netatmo Video', 'Netatmo Video'); ?>
                                        </figure>
                                        <div class="bt-video bt-v-1 text-center iconos-play"></div>
                                    </div>
                                    <!-- Modal Video 1 -->
                                    <div id="video-1-modal" class="modal fade" role="dialog">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Netatmo Termostato</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <iframe width="560" height="315"
                                                            src="https://www.youtube.com/embed/GA1WwpUL4qE"
                                                            frameborder="0" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <iframe width="560" height="315"
                                            src="https://www.youtube.com/embed/GA1WwpUL4qE"
                                            frameborder="0" allowfullscreen></iframe>
                                <?php endif ?>
                            </div>
                        </div>

                    </div>
                    <!-- Block 2 -->
                    <div class="block content-block-2">
                        <div class="block-top-zone btz-2">
                            Protección inteligente
                        </div>
                        <div class="col-md-6 special-col-md-6">
                            <figure>
                                    <?php img('netatmo-product-b-2-1.png', 'cdn', null, 'img-product img-special-top img-responsive', 'Camara Wifi', 'Camara Wifi'); ?>
                            </figure>
                            <div class="bt-price price-2">
                                <?php if (!$detect->isMobile()): ?>
                                    <div class="price xl ref-1308363"></div><?php endif; ?>
                                <?php if ($detect->isMobile()): ?>
                                    <div class="price md ref-1308363"></div><?php endif; ?>
                                <div class="btn-1">
                                    <a href="http://tiendas.mediamarkt.es/p/camara-wi-fi-netatmo-welcome-con-reconocimiento-facial-1308363"
                                       onclick="ga('send','event','microsites','novedades-netatmo','comprar-camara-wi-fi');"
                                       target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                                </div>
                            </div>
                            <?php if (!$detect->isMobile()): ?>
                                <div class="option-clicks show-option iconos-plus10 opt-click-1"
                                     data-show="blc-2-opt-1"></div>
                                <div class="option-clicks show-option iconos-plus10 opt-click-2"
                                     data-show="blc-2-opt-2"></div>
                                <div class="option-clicks show-option iconos-plus10 opt-click-3"
                                     data-show="blc-2-opt-3"></div>
                                <div class="option-clicks show-option iconos-plus10 opt-click-4"
                                     data-show="blc-2-opt-4"></div>
                            <?php endif ?>
                        </div>
                        <div class="col-md-6">
                            <div class="block-p bp-2">
                                <p>La cámara doméstica Welcome de Netatmo y su revolucionaria tecnología con
                                    reconocimiento facial es sinónimo de más seguridad para tu hogar. Puede reconocer
                                    las caras de tus hijos y enviarte notificaciones a tu Smartphone cuando estén en
                                    casa o al detectar un extraño. Mientras que mediante la App es posible acceder a
                                    eventos pasados o en directo. Todo esto junto con un diseño minimalista, imágenes en
                                    alta resolución y visión nocturna.</p>

                                <p id="blc-2-opt-1" class="option <?php if (!$detect->isMobile()) {
                                    echo 'invisible';
                                } ?>">Tecnología de reconomiento facial</p>
                                <p id="blc-2-opt-2" class="option <?php if (!$detect->isMobile()) {
                                    echo 'invisible';
                                } ?>">Envía notificaciones a tu Smartphone</p>
                                <p id="blc-2-opt-3" class="option <?php if (!$detect->isMobile()) {
                                    echo 'invisible';
                                } ?>">Accede a eventos pasados o en directo</p>
                                <p id="blc-2-opt-4" class="option <?php if (!$detect->isMobile()) {
                                    echo 'invisible';
                                } ?>">Diseño minimalista</p>
                            </div>
                        </div>
                    </div>

                    <!-- Block 3-->
                    <div class="block content-block-3">
                        <div class="block-top-zone btz-3">
                            Controla el ambiente
                        </div>
                        <div class="block-central-zone">
                            <div class="col-md-6 special-col-md-6-hlimit">
                                <figure>
                                        <?php img('netatmo-product-b-3-1.png', 'cdn', null, 'img-product img-special-top img-responsive', 'Estación Meteorológica', 'Estación Meteorológica'); ?>
                                </figure>
                                <div class="bt-price price-3">
                                    <?php if (!$detect->isMobile()): ?>
                                        <div class="price xl ref-1233699"></div><?php endif; ?>
                                    <?php if ($detect->isMobile()): ?>
                                        <div class="price md ref-1233699"></div><?php endif; ?>
                                    <div class="btn-1">
                                        <a href="http://tiendas.mediamarkt.es/p/estacion-metereologica-netatmo-nsw01-ec-1233699"
                                           onclick="ga('send','event','microsites','novedades-netatmo','comprar-estacion-metereologica');"
                                           target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                                    </div>
                                </div>
                                <?php if (!$detect->isMobile()): ?>
                                    <div class="option-clicks show-option iconos-plus10 opt-click-1"
                                         data-show="blc-3-opt-1"></div>
                                    <div class="option-clicks show-option iconos-plus10 opt-click-2"
                                         data-show="blc-3-opt-2"></div>
                                    <div class="option-clicks show-option iconos-plus10 opt-click-3"
                                         data-show="blc-3-opt-3"></div>
                                    <div class="option-clicks show-option iconos-plus10 opt-click-4"
                                         data-show="blc-3-opt-4"></div>
                                <?php endif ?>
                            </div>
                            <div class="col-md-6 special-col-md-6-hlimit">
                                <div class="block-p bp-3">
                                    <p>La Estación Metereológica de Netatmo te permite medir la temperatura, la humedad,
                                        los
                                        niveles de CO2 o la presión atmosférica interior y exterior. Con el módulo
                                        interior
                                        comprobarás la comodidad de tu casa para recibir avisos cuando debas ventilarla
                                        para
                                        aumentar tu bienestar. Mientras que su módulo exterior te ofrecerá información
                                        sobre
                                        el tiempo, recabada desde la misma puerta de tu hogar.</p>
                                    <p id="blc-3-opt-1" class="option <?php if (!$detect->isMobile()) {
                                        echo 'invisible';
                                    } ?>">Mide la temperatura, humedad, CO2, presión...</p>
                                    <p id="blc-3-opt-2" class="option <?php if (!$detect->isMobile()) {
                                        echo 'invisible';
                                    } ?>">App para analizar información pasada y presente</p>
                                    <p id="blc-3-opt-3" class="option <?php if (!$detect->isMobile()) {
                                        echo 'invisible';
                                    } ?>">Módulo exterior y módulo interior</p>
                                    <p id="blc-3-opt-4" class="option <?php if (!$detect->isMobile()) {
                                        echo 'invisible';
                                    } ?>">Diseño sólido y elegante</p>
                                </div>
                                <div class="video">
                                    <?php if (!$detect->isMobile()): ?>
                                        <div class="btn" data-toggle="modal" data-target="#video-3-modal">
                                            <figure>
                                                    <?php img('box-video.jpg', 'cdn', null, 'img-responsive', 'Netatmo Video', 'Netatmo Video'); ?>
                                            </figure>
                                            <div class="bt-video bt-v-3 text-center iconos-play"></div>
                                        </div>
                                        <!-- Modal Video 3 -->
                                        <div id="video-3-modal" class="modal fade" role="dialog">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close"
                                                                data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">La Estación Meteorológica para
                                                            Smartphone</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <iframe width="560" height="315"
                                                                src="https://www.youtube.com/embed/k7UITezaXVs"
                                                                frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <iframe width="560" height="315"
                                                src="https://www.youtube.com/embed/k7UITezaXVs"
                                                frameborder="0" allowfullscreen></iframe>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                        <div class="block-central-zone">
                            <div class="col-md-6 block-zone-3-accesorios">
                                <div class="block-bottom-zone-3">
                                    <div class="col-md-5">
                                        <div class="block-p bp-3-bt-left">
                                            <p>Pluviómetro Wifi</p>
                                            <p>Controla la lluvia con este pluviómetro. Un módulo adicional para tu
                                                Estación
                                                Metereológica Netatmo con el que no se te escapará ningún detalle sobre
                                                la
                                                cantidad de
                                                lluvia en tiempo real o datos sobre precipitaciones acumuladas.</p>
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <figure>
                                                <?php img('netatmo-product-b-3-2.png', 'cdn', null, 'img-product img-block-bottom-lef img-responsive', 'Pluviómetro', 'Pluviómetro'); ?>
                                        </figure>
                                    </div>
                                    <div class="bt-price price-4">
                                        <?php if (!$detect->isMobile()): ?>
                                            <div class="price xl ref-1267528"></div><?php endif; ?>
                                        <?php if ($detect->isMobile()): ?>
                                            <div class="price md ref-1267528"></div><?php endif; ?>
                                        <div class="btn-1">
                                            <a href="http://tiendas.mediamarkt.es/p/pluviometro-netatmo-nrg01-ww-material-1267528"
                                               onclick="ga('send','event','microsites','novedades-netatmo','comprar-pluviometro');"
                                               target="_self"
                                               class="btn btn-black btn-regular btn-caret">Comprar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 block-zone-3-accesorios">
                                <div class="block-bottom-zone-3">
                                    <div class="col-md-6">
                                        <figure>
                                                <?php img('netatmo-product-b-3-3.png', 'cdn', null, 'img-product img-block-bottom-right img-responsive', 'Estación meteorologica módulo', 'Estación meteorologica módulo'); ?>
                                        </figure>
                                    </div>
                                    <div class="block-p col-md-6 z3-a2-block-description">
                                        <p>Módulo adicional estación meteorológica</p>
                                        <p>Potencia tu Estación Metereológica de Netatmo con este Módulo adicional
                                            interior.
                                            Tendrás
                                            más información sobre aspectos como la humedad o la concentración de CO2 en
                                            la
                                            habitación donde lo coloques. </p>
                                    </div>
                                    <div class="bt-price price-5">
                                        <?php if (!$detect->isMobile()): ?>
                                            <div class="price xl ref-1308362"></div><?php endif; ?>
                                        <?php if ($detect->isMobile()): ?>
                                            <div class="price md ref-1308362"></div><?php endif; ?>
                                        <div class="btn-1"><a
                                                href="http://tiendas.mediamarkt.es/p/modulo-adicional-estacion-metereologica-netatmo-1308362"
                                                onclick="ga('send','event','microsites','novedades-netatmo','comprar-modulo');"
                                                target="_self" class="btn btn-black btn-regular btn-caret">Comprar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
