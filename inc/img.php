<?php
/**
 * Image URL CORE
 *
 * @package SIRAC Landing  
 * @version 1.0
 * @author Hugo Robles <hugorobles@twentyfourcolors.com,h.robles@nextt.es>
 */
 
 function img($file_name,$null1,$null,$class_img,$title_img,$alt_img){
     global $img_node_path;
     echo '<img class="'.$class_img.'" src="'.$img_node_path.$file_name.'" title="'.$title_img.'" alt="'.$alt_img.'">';
 }
     