<?php

/**
 * Automatic Tiny Optmice Image
 *
 * @package SIRAC Landing  
 * @version 1.0
 * @author Hugo Robles <hugorobles@twentyfourcolors.com,h.robles@nextt.es>
 */

// Load DataLibrary Images Optimice
$img_opt_db = fopen($inc_path.'tinypng.txt',"r");
$img_opt_check = array();

while(!feof($img_opt_db)) {
    $img_opt_check[] = fgets($img_opt_db);
}
fclose($img_opt_db);

// Function Check Images Type
function isImage($path){
	$imageSizeArray = getimagesize($path);
	$imageTypeArray = $imageSizeArray[2];

	//return (bool)(in_array($imageTypeArray , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP))); //Optimiza formatos JPG, JPEG, PNG, GIF y BMP
	return (bool)(in_array($imageTypeArray , array(IMAGETYPE_JPEG,IMAGETYPE_PNG))); //Optimiza formatos JPG, JPEG
}

// Function images optimize
function optimize_path($path){
    
    global $inc_path;
    global $img_opt_check;
    global $node_path;
    
	if (is_dir($path)){
		$open_dir = opendir($path);

		while (($file = readdir($open_dir)) !== false)  {
			$full_path = $path . "/" . $file;

			if ($file != "." && $file != "..") {
				if (is_dir($full_path)) {
					optimize_path($full_path);
				} else {
					if (isImage($full_path)) {
			            $node_img_path = str_replace($node_path,'',$full_path);
			            $img_opt_check = array_map('trim', $img_opt_check);
						if(!in_array($node_img_path,$img_opt_check)){
						    $img_tiny_opt = \Tinify\fromFile($full_path);
                            $img_tiny_opt->toFile($full_path);
						    $img_opt_db_save = fopen($inc_path.'tinypng.txt',"a");
                            fwrite($img_opt_db_save,$node_img_path.PHP_EOL);
						}
					}
				}
			}
		}
		closedir($open_dir);
	} else {
		echo "No es una ruta de directorio valida<br/>";
	}
	fclose($img_opt_db_save);
}

// Optimiza las imágenes de lo directorios incluído en la configuración
optimize_path($node_path.$node.'/img');


