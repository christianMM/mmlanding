<?php
// Root Path
$home_path = __DIR__.'/../';
$base_path = '/';


// VERSION FILES
//$vCSS = '?0.50.53';
$vCSS = '?'.date('Y-m-d H:i:s');
$vCSSicons = '?1.0.3';
$vJS = '?'.date('Y-m-d H:i:s');
//$vJS = '?0.50.16';

// General Path
$node_path = $home_path.'templates/';
$node_base_path = $base_path.'templates/';
$css_path = $base_path.'css/';
$css_node_path = $node_base_path.$node.'/css/';
$css_lib_path = $base_path.'css/lib/';
$css_tablet_path = $base_path.'css/tablet/';
$css_mobile_path = $base_path.'css/mobile/';
$css_module_path = $base_path.'css/module/';
$js_path = $base_path.'js/';
$js_lib_path = $base_path.'js/lib/';
$js_lib_ng_path = $base_path.'js/lib/ng/';
$js_tablet_path = $base_path.'js/tablet/';
$js_mobile_path = $base_path.'js/mobile/';
$js_module_path = $base_path.'js/module/';
$js_node_path = $node_base_path.$node.'/js/';
$img_node_path = $node_base_path.$node.'/img/'.$device.'/';
$fonts_path = $base_path.'fonts/';

// Include & Library Path
$lib_path = $home_path.'lib/';
$lib_php_path = $lib_path.'php/';
$inc_path = $home_path.'inc/';
