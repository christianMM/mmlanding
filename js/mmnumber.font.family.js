/**
 * Created by hugorobles on 13/02/16.
 */

$(document).ready(function() {
 /*   $("#btn-down").click(function() {
        alert('cl');
        html2canvas($(".prices"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);

                // Convert and download as image
                Canvas2Image.saveAsPNG(canvas);
                $(".prices").append(canvas);
                // Clean up
                //document.body.removeChild(canvas);
            }
        });
    });*/

    /*
     * Custom position order for MM-Prices
     */

    $(".prices").each(function() {
        var  e = $(this);
        var a = e.text();
        e.each(function() {
            var e = a.replace(".", ","),
                t = e.split(",")[0],
                n = e.split(",")[1];
            $(this).text(e);
            var r = $(this).attr("class").split(" ")[1];
            void 0 == n || "0" == n || "00" == n || "" == n ? (n = "-", $(this).text(t.replace(/^(-?)0+/, "") + n)) : (1 == n.length && (n += "0"), n = n.substring(0, 2), $(this).text(t.replace(/^(-?)0+/, "") + "," + n)), $(this).contents().each(function() {
                1 === this.nodeType ? alert("yeahhhh") : 3 === this.nodeType && $(this).replaceWith($.map(this.nodeValue.split(""), function(e) {
                    switch (e) {
                        case ".":
                            return e = "-comma", c2 = ",", '<span class="' + r + " p" + e + '">' + c2 + "</span>";
                        case ",":
                            return e = "dot", c2 = ",", '<span class="price-d-' + e + '"><span class="path1"></span><span class="path2"></span></span>';
                        case "-":
                            return e = "euro", c3 = "-", '<span class="price-d-' + e + '"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>';
                        default:
                            return '<span class="price-d-' + e + '"><span class="path1"></span><span class="path2"></span></span>';
                    }
                }).join(""))
            }), $(this).find(".p-comma").next().addClass("small").next().addClass("small")
        }), e.append('<meta class="test-after-load-price" itemprop="price" content="' + a + '">')
    });


    $(".prices > span").each(function (i){
        console.log('Repeat:'+i);
        /*
         * Previous span tag order by digital and detect point
         */
        var dp = $(this).prev().attr('class');
        if(typeof dp !== 'undefined'){
            if(dp == 'price-d-dot'){
                var dpp = $(this).prev().prev().attr('class');

                console.log('Digit Point:'+dp);
                console.log('Class Prev Digit Point:'+dpp);

                var dppsz = $("."+dpp).css("font-size");

                console.log('Font Size Prev Digit Point:'+dppsz);

                var dppszc = dppsz.replace("px","");
                $(this).attr("pos", "d-post-dot");
                $(this).css('font-size',+(dppszc/3)*2+'px');
                $(this).css('top',-(dppszc/3.3)+'px');
            }else if($(this).prev().attr("pos") == 'd-post-dot'){
                $(this).attr("pos","d-post-dot");
                $(this).css('font-size',$(this).prev().css("font-size"));
                var dpsz = $(this).prev().prev().css("font-size");
                var dpsz = dpsz.replace("px","");
                $(this).css('top',-(dpsz/3.3)+'px');
            }
            var p = dp.replace("price-d-", "");

            console.log('Digit Pre:'+dp);
        }

        /*
         * Actual span tag
         */
        var d = $(this).attr('class');
        var ds = $(this).css("font-size");
        var ds = ds.replace("px","");
        var e = d.replace("price-d-","");
        var dor = 0;
        console.log('Widht:'+ds);
        console.log('Class now:'+d);

        /*
         * All detect case to distance order
         */
        switch(e){
            case "0":
                if(p == 1) {
                    dor = 2.35;
                }else if(p == 2){
                    dor = 4.3;
                }else if(p == 3){
                    dor = 3.5;
                }else if(p == 4){
                    dor = 3.8;
                }else if(p == 5){
                    dor = 3.4;
                }else if(p == 6){
                    dor = 3.4;
                }else if(p == 7){
                    dor = 3.3;
                }else if(p == 8){
                    dor = 3.45;
                }else if(p == 9){
                    dor = 3.3;
                }else if(p == 0){
                    dor = 3.4;
                }else if(p == 'dot'){
                    dor = 1.55;
                }
                break;
            case "1":
                if(p == 1) {
                    dor = 2.3;
                }else if(p == 2){
                    dor = 3.85;
                }else if(p == 3){
                    dor = 3.1;
                }else if(p == 4){
                    dor = 2.9;
                }else if(p == 5){
                    dor = 2.9;
                }else if(p == 6){
                    dor = 3;
                }else if(p == 7){
                    dor = 2.65;
                }else if(p == 8){
                    dor = 3;
                }else if(p == 9){
                    dor = 2.95;
                }else if(p == 0){
                    dor = 3.1;
                }else if(p == 'dot'){
                    dor = 1.3;
                }
                break;
            case "2":
                if(p == 1){
                    dor = 2.25;
                }else if(p == 2){
                    dor = 3.6;
                }else if(p == 3){
                    dor = 2.9;
                }else if(p == 4){
                    dor = 3.3;
                }else if(p == 5){
                    dor = 2.95;
                }else if(p == 6){
                    dor = 2.95;
                }else if(p == 7){
                    dor = 2.65;
                }else if(p == 8){
                    dor = 2.8;
                }else if(p == 9){
                    dor = 3;
                }else if(p == 0){
                    dor = 3.1;
                }else if(p == 'dot'){
                    dor = 1.5;
                }
                break;
            case "3":
                if(p == 1){
                    dor = 2.55;
                }else if(p == 2){
                    dor = 4.65;
                }else if(p == 3){
                    dor = 3.5;
                }else if(p == 4){
                    dor = 3.45;
                }else if(p == 5){
                    dor = 3.35;
                }else if(p == 6){
                    dor = 3.3;
                }else if(p == 7){
                    dor = 3.2;
                }else if(p == 8){
                    dor = 3.5;
                }else if(p == 9){
                    dor = 3.5;
                }else if(p == 0){
                    dor = 3.65;
                }else if(p == 'dot'){
                    dor = 1.55;
                }
                break;
            case "4":
                if(p == 1){
                    dor = 2.55;
                }else if(p == 2){
                    dor = 4.9;
                }else if(p == 3){
                    dor = 3.75;
                }else if(p == 4){
                    dor = 3.65;
                }else if(p == 5){
                    dor = 3.6;
                }else if(p == 6){
                    dor = 3.7;
                }else if(p == 7){
                    dor = 2.2;
                }else if(p == 8){
                    dor = 3.7;
                }else if(p == 9){
                    dor = 3.6;
                }else if(p == 0){
                    dor = 3.7;
                }else if(p == 'dot'){
                    dor = 1.6;
                }
                break;
            case "5":
                if(p == 1){
                    dor = 2.35;
                }else if(p == 2){
                    dor = 4.3;
                }else if(p == 3){
                    dor = 3.45;
                }else if(p == 4){
                    dor = 3.72;
                }else if(p == 5){
                    dor = 3.4;
                }else if(p == 6){
                    dor = 3.35;
                }else if(p == 7){
                    dor = 3.25;
                }else if(p == 8){
                    dor = 3.3;
                }else if(p == 9){
                    dor = 3.2;
                }else if(p == 0){
                    dor = 3.35;
                }else if(p == 'dot'){
                    dor = 1.55;
                }
                break;
            case "6":
                if(p == 1){
                    dor = 2.4;
                }else if(p == 2){
                    dor = 4.3;
                }else if(p == 3){
                    dor = 3.5;
                }else if(p == 4){
                    dor = 3.85;
                }else if(p == 5){
                    dor = 3.35;
                }else if(p == 6){
                    dor = 3.4;
                }else if(p == 7){
                    dor = 3.15;
                }else if(p == 8){
                    dor = 3.35;
                }else if(p == 9){
                    dor = 3.25;
                }else if(p == 0){
                    dor = 3.4;
                }else if(p == 'dot'){
                    dor = 1.55;
                }
                break;
            case "7":
                if(p == 1){
                    dor = 2.95;
                }else if(p == 2){
                    dor = 6.5;
                }else if(p == 3){
                    dor = 4.6;
                }else if(p == 4){
                    dor = 4.35;
                }else if(p == 5){
                    dor = 3.6;
                }else if(p == 6){
                    dor = 4.3;
                }else if(p == 7){
                    dor = 3.85;
                }else if(p == 8){
                    dor = 4.35;
                }else if(p == 9){
                    dor = 4.35;
                }else if(p == 0){
                    dor = 4.6;
                }else if(p == 'dot'){
                    dor = 1.55;
                }
                break;
            case "8":
                if(p == 1){
                    dor = 2.35;
                }else if(p == 2){
                    dor = 4.25;
                }else if(p == 3){
                    dor = 3.55;
                }else if(p == 4){
                    dor = 4.15;
                }else if(p == 5){
                    dor = 3.4;
                }else if(p == 6){
                    dor = 3.4;
                }else if(p == 7){
                    dor = 3.1;
                }else if(p == 8){
                    dor = 3.35;
                }else if(p == 9){
                    dor = 3.3;
                }else if(p == 0){
                    dor = 3.45;
                }else if(p == 'dot'){
                    dor = 1.55;
                }
                break;
            case "9":
                if(p == 1){
                    dor = 2.4;
                }else if(p == 2){
                    dor = 4.55;
                }else if(p == 3){
                    dor = 3.5;
                }else if(p == 4){
                    dor = 4;
                }else if(p == 5){
                    dor = 3.4;
                }else if(p == 6){
                    dor = 3.4;
                }else if(p == 7){
                    dor = 3.3;
                }else if(p == 8){
                    dor = 3.4;
                }else if(p == 9){
                    dor = 3.35;
                }else if(p == 0){
                    dor = 3.5;
                }else if(p == 'dot'){
                    dor = 1.55;
                }
                break;
            case "euro":
                if(p == 1){
                    dor = 2.5;
                }else if(p == 2){
                    dor = 4.5;
                }else if(p == 3){
                    dor = 3.7;
                }else if(p == 4){
                    dor = 4.2;
                }else if(p == 5){
                    dor = 3.6;
                }else if(p == 6){
                    dor = 3.5;
                }else if(p == 7){
                    dor = 2.3;
                }else if(p == 8){
                    dor = 3.45;
                }else if(p == 9){
                    dor = 3.4;
                }else if(p == 0){
                    dor = 3.55;
                }
                break;
            case "dot":
                if(p == 1){
                    dor = 2.5;
                }else if(p == 2){
                    dor = 4.5;
                }else if(p == 3){
                    dor = 3.7;
                }else if(p == 4){
                    dor = 4.2;
                }else if(p == 5){
                    dor = 3.6;
                }else if(p == 6){
                    dor = 3.5;
                }else if(p == 7){
                    dor = 2.3;
                }else if(p == 8){
                    dor = 3.6;
                }else if(p == 9){
                    dor = 3.4;
                }else if(p == 0){
                    dor = 3.55;
                }
                break;
        }
        /*
         * Apply order to digital
         */
        return $(this).css('margin-left','-'+ds/dor+'px');
    });

});