function renderPrice(productId, targetClass, fromText, colorText) {

    $.ajax({
            url: '//tiendas.mediamarkt.es/ecommerce/processes/PriceByProductId/PriceByProductId.cfm?pId=' + productId,
            beforeSend: function() {
                $('.' + targetClass).show();
            }
        })
        .done(function(data) {
            $('.' + targetClass).html(data);
            var priceText = $('#precio').text();
            $('.'+ targetClass + ' #precio').remove();
            $('.' + targetClass).text(priceText);
            $('.' + targetClass).price().css('position','relative');

            if (fromText == "Desde") {
                $('.' + targetClass).addClass('price-from-'+ colorText);
            }
            else {

                if (fromText == "Cadauno") {
                    $('.' + targetClass).addClass('price-to-'+ colorText);
                }
            }
        });
};