<?php
/* INIT FireLanding Build APP */
require_once 'inc/session.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Media Markt Landing - <?php echo $node?></title>

    <!-- Master Template -->
    <link rel="stylesheet" href="<?php echo $css_lib_path;?>bootstrap.3.3.6.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="<?php echo $css_path;?>mmfont.css" rel="stylesheet">
    <link href="<?php echo $css_path;?>mm.landing.custom.css" rel="stylesheet">
    <link href="<?php echo $css_path;?>mm.original.css" rel="stylesheet">
    <link href="<?php echo $css_path;?>mm.drupal.core.css" rel="stylesheet">
    <link href="<?php echo $css_path;?>owl.carousel.css" rel="stylesheet">
    <link href="<?php echo $css_node_path;?>screen.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="mm-bottom-column" class="fg-box bpx0 bpy0 bsx5 bsy1 mpx0 mpy0 msx4 msy1 spx0 spy0 ssx3 ssy1"
     style="top: 0px; height: auto;">
    <div class="cms_html_container">

        <!-- Landing Design Block -->
        <?php include $node_path.$node.'/index.php'; ?>

        <!-- After Landing JS Load -->
        <script src="<?php echo $js_lib_path;?>1.12.0.jquery.min.js"></script>
        <script src="<?php echo $js_lib_path;?>1.11.4.jquery-ui.min.js"></script>
        <script src="<?php echo $js_lib_path;?>3.3.5.bootstrap.min.js"></script>
        <script src="<?php echo $js_lib_path;?>owl.carousel.min.js"></script>
        
        <script src="<?php echo $js_path;?>slider-landings.js"></script>
        <script src="<?php echo $js_path;?>mmlanding.func.js"></script>
        <script src="<?php echo $js_node_path;?>script.js"></script>
    </div>
</div>
</body>
</html>